export const boxShadow = [
    "0 12px 15px 0 rgba(0, 0, 0, 0.1), 0 17px 50px 0 rgba(0, 0, 0, 0.1)",
    "2px 12px 9px rgba(0,0,0,0.1)",
    "0 2px 5px rgba(0, 0, 0, .2)"
]

export const baseStyles = {
    primaryColor: "#FF9D00",
    redColor: "#f50",
    greyColor: "#999",
    yellowColor: "#fbe126",
    lightGrey: {
        one: "#eee"
    },
    boxShadow: {
        main: "0 12px 15px 0 rgba(0, 0, 0, 0.1), 0 17px 50px 0 rgba(0, 0, 0, 0.1)",
        hover: "0 2px 5px rgba(0, 0, 0, .2)"
    }
}
