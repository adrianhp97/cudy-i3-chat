import { gql } from "apollo-boost"

export const mutateBasicProfile = gql`
    mutation mutateBasicProfile($desc: String!, $address: String!) {
        updateBasicProfile(desc: $desc, address: $address)
    }
`

export const mutateUpdateProfile = gql`
    mutation mutateUpdateProfile(
        $firstName: String!
        $lastName: String!
        $dob: Date
        $nric: String
        $phone: String
        $gender: String
        $race: String
        $address: String
        $description: String
    ) {
        updateProfile(
            firstName: $firstName
            lastName: $lastName
            dob: $dob
            nric: $nric
            phone: $phone
            gender: $gender
            race: $race
            address: $address
            description: $description
        ) {
            pvid
            firstName
            lastName
            dateModified
            dob
            nric
            phone
            gender
            race
            description
            address
        }
    }
`

export const mutateUpdateCurriculum = gql`
    mutation mutateUpdateCurriculum($list: [InputProfileCurriculum]!) {
        updateProfileCurriculum(list: $list) {
            pvid
        }
    }
`

export const mutateAddSubjects = gql`
    mutation mutateAddSubjects($list: [InputUserSubject]!) {
        updateSubject(list: $list) {
            pvid
        }
    }
`

export const mutateEducations = gql`
    mutation mutateEducation($list: [InputEducation]!) {
        updateEducation(list: $list) {
            pvid
        }
    }
`

export const mutateExperiences = gql`
    mutation mutateExperiences($list: [InputExperience]!) {
        updateExperience(list: $list) {
            pvid
        }
    }
`

export const mutateUserProfileHome = gql`
    mutation mutateUserProfileHome {
        getProfile {
            firstName
            lastName
            avatar {
                path
                fileName
            }
        }
    }
`

export const mutateUserProfile = gql`
    mutation mutateUserProfile {
        getProfile {
            userCode
            pvid
            firstName
            lastName
            dateModified
            dob
            nric
            phone
            gender
            race
            description
            address
            referralCode
            roleCode
            roleName

            skillList {
                title
                pvid
            }
            expertiseList {
                title
                pvid
            }
            experienceList {
                pvid
                fromDate
                toDate
                isPrivateTutor
                isPresent
                level
                institution
                curriculum
                subject
            }
            educationList {
                pvid
                fromDate
                toDate
                institution
                isPresent
            }
            subjectList {
                pvid
                orderList
                pricePerHour
                level {
                    pvid
                    name
                }
                subject {
                    pvid
                    name
                }
            }
            banner {
                path
                fileName
                durationSecs
            }
            otherPlatformList {
                pvid
                link
                otherPlatformPvid

                source {
                    pvid
                    name
                    iconLink
                }
            }
            avatar {
                path
                fileName
                durationSecs
            }
            videoIntroductionList {
                path
                fileName
                durationSecs
            }
            certificateList {
                pvid
                issuer
                issuedDate
                name
                relativePath
            }
            curriculumList {
                pvid
                source {
                    pvid
                    name
                }
            }
            completionUser {
                pvid
                completionPercent
            }
            completionDetail {
                pvid
                profileSection
            }
        }
    }
`

export const mutateUserProfileOnNavbar = gql`
    mutation mutateUserProfile {
        getProfile {
            pvid
            firstName
            lastName
        }
    }
`

//$filter: InputLogSearch

export const mutateUserProfileForReferral = gql`
    mutation mutateUserProfileForReferral($userPvid: Int!) {
        getProfile(userPvid: $userPvid) {
            firstName
            lastName
            avatar {
                path
                fileName
                durationSecs
            }
        }
    }
`

export const mutateUserByToken = gql`
    mutation mutateUserByToken($code: String!) {
        getProfile(code: $code) {
            userCode
            pvid
            firstName
            lastName
            dateModified
            dob
            nric
            phone
            gender
            race
            description
            address
            referralCode
            roleCode
            roleName

            skillList {
                title
                pvid
            }
            expertiseList {
                title
                pvid
            }
            experienceList {
                pvid
                fromDate
                toDate
                isPrivateTutor
                isPresent
                level
                institution
                curriculum
                subject
            }
            educationList {
                pvid
                fromDate
                toDate
                institution
                isPresent
            }
            subjectList {
                pvid
                orderList
                pricePerHour
                level {
                    pvid
                    name
                }
                subject {
                    pvid
                    name
                }
            }
            banner {
                path
                fileName
                durationSecs
            }
            otherPlatformList {
                pvid
                link
                otherPlatformPvid

                source {
                    pvid
                    name
                    iconLink
                }
            }
            avatar {
                path
                fileName
                durationSecs
            }
            videoIntroductionList {
                path
                fileName
                durationSecs
            }
            certificateList {
                pvid
                issuer
                issuedDate
                name
                relativePath
            }
            curriculumList {
                pvid
                source {
                    pvid
                    name
                }
            }
        }
    }
`

export const mutateUserById = gql`
    mutation mutateUserById($userPvid: Int!) {
        getProfile(userPvid: $userPvid) {
            userCode
            pvid
            firstName
            lastName
            dateModified
            dob
            nric
            phone
            gender
            race
            description
            address
            referralCode
            roleCode
            roleName

            skillList {
                title
                pvid
            }
            expertiseList {
                title
                pvid
            }
            experienceList {
                pvid
                fromDate
                toDate
                isPrivateTutor
                isPresent
                level
                institution
                curriculum
                subject
            }
            educationList {
                pvid
                fromDate
                toDate
                institution
                isPresent
            }
            subjectList {
                pvid
                orderList
                pricePerHour
                level {
                    pvid
                    name
                }
                subject {
                    pvid
                    name
                }
            }
            banner {
                path
                fileName
                durationSecs
            }
            otherPlatformList {
                pvid
                link
                otherPlatformPvid

                source {
                    pvid
                    name
                    iconLink
                }
            }
            avatar {
                path
                fileName
                durationSecs
            }
            videoIntroductionList {
                path
                fileName
                durationSecs
            }
            certificateList {
                pvid
                issuer
                issuedDate
                name
                relativePath
            }
            curriculumList {
                pvid
                source {
                    pvid
                    name
                }
            }
        }
    }
`

export const mutateUploadProfileBanner = gql`
    mutation mutateUploadProfileBanner($file: Upload!) {
        uploadProfileBanner(file: $file) {
            fileName
            path
        }
    }
`

export const mutateUploadProfilePicture = gql`
    mutation mutateUploadProfilePicture($file: Upload!) {
        uploadAvatar(file: $file) {
            fileName
            path
        }
    }
`

export const mutateUploadIntroVideo = gql`
    mutation mutateUploadIntroVideo($file: Upload!) {
        uploadVideoIntroduction(file: $file) {
            fileName
            path
        }
    }
`

export const mutateFetchTestimonials = gql`
    mutation mutateFetchTestimonials($limit: Int) {
        getTestimonialList(limit: $limit) {
            comment
            postedDate
            rating
            reviewer {
                pvid
                firstName
                lastName
            }
        }
    }
`

export const mutateFetchTestimonialsById = gql`
    mutation mutateFetchTestimonialsById($userPvid: Int!, $limit: Int) {
        getTestimonialList(userPvid: $userPvid, limit: $limit) {
            comment
            postedDate
            rating
            reviewer {
                pvid
                firstName
                lastName
            }
        }
    }
`

export const mutateFetchTestimonialsByToken = gql`
    mutation mutateFetchTestimonialsByToken($token: String!, $limit: Int!) {
        getTestimonialListByToken(token: $token, limit: $limit) {
            comment
            postedDate

            reviewer {
                pvid
                firstName
                lastName
            }
        }
    }
`

export const mutateAddTestimonial = gql`
    mutation mutateAddTestimonial(
        $programmeSubjectPvid: Int!
        $comment: String!
        $referralCode: String!
        $reviewerPvid: Int
        $rating: Float!
        $newUser: NewUserTestimonial
    ) {
        addTestimonial(
            programmeSubjectPvid: $programmeSubjectPvid
            comment: $comment
            referralCode: $referralCode
            reviewerPvid: $reviewerPvid
            rating: $rating
            newUser: $newUser
        ) {
            text
            success
        }
    }
`

export const mutateDeleteEducations = gql`
    mutation mutateDeleteEducations($pvidList: [String!]!) {
        deleteEducation(pvidList: $pvidList)
    }
`

export const mutateUpdateEducations = gql`
    mutation mutateUpdateEducations($list: [InputEducation]!) {
        updateEducation(list: $list) {
            pvid
        }
    }
`

export const mutateUpdateExperiences = gql`
    mutation mutateUpdateExperiences($list: [InputExperience]!) {
        updateExperience(list: $list) {
            pvid
        }
    }
`

export const mutateDeleteExperiences = gql`
    mutation mutateDeleteExperiences($pvidList: [String!]!) {
        deleteExperience(pvidList: $pvidList)
    }
`

export const mutateDeleteSubjects = gql`
    mutation mutateDeleteSubjects($pvidList: [String!]!) {
        deleteSubject(pvidList: $pvidList)
    }
`

export const mutateUpdateSubjects = gql`
    mutation mutateUpdateSubjects($list: [InputUserSubject]!) {
        updateSubject(list: $list) {
            pvid
        }
    }
`

export const mutateUpdateSkills = gql`
    mutation mutateUpdateSkills($list: [InputSkill]!) {
        updateSkill(list: $list) {
            pvid
        }
    }
`

export const mutateDeleteSkills = gql`
    mutation mutateDeleteSkills($pvidList: [String!]!) {
        deleteSkill(pvidList: $pvidList)
    }
`

export const mutateUploadCertification = gql`
    mutation mutateUploadCertification($file: Upload!) {
        uploadCertification(file: $file) {
            fileName
            path
        }
    }
`

export const mutateAddCertification = gql`
    mutation mutateAddCertification($relativePath: String!, $name: String!, $issuedDate: Date!, $issuer: String!) {
        addCertificate(relativePath: $relativePath, name: $name, issuedDate: $issuedDate, issuer: $issuer) {
            pvid
            name
            userPvid
            relativePath
            issuer
            issuedDate
        }
    }
`

export const mutateDeleteCertification = gql`
    mutation mutateDeleteCertification($pvid: Int!) {
        deleteCertificate(pvid: $pvid)
    }
`

export const mutateAddOtherPlatform = gql`
    mutation mutateAddOtherPlatform($list: [InputOtherPlatform]!) {
        updateOtherPlatform(list: $list) {
            pvid
        }
    }
`

export const mutateDeleteOtherPlatform = gql`
    mutation mutateDeleteOtherPlatform($pvidList: [String!]!) {
        deleteOtherPlatform(pvidList: $pvidList)
    }
`

export const subscribeNewNotification = gql`
    subscription subscribeNewNotification($id: String!, $userPvid: Int!) {
        notification(id: $id, userPvid: $userPvid) {
            pvid
            entityId
            code
            remarks
            postedDate
            archived
        }
    }
`

export const queryReferralLink = gql`
    query queryReferralLink {
        referralLink: getUserReferralLink
    }
`

export const mutateDeleteResource = gql`
    mutation mutateDeleteResource($relativePath: String!) {
        deleteResource(relativePath: $relativePath) {
            text
            success
        }
    }
`

export const mutatePriceSuggestion = gql`
    mutation mutatePriceSuggestion($programmeSubjectPvid:Int!,$programmeLevelPvid:Int!){
        getSuggestionPrice(programmeSubjectPvid:$programmeSubjectPvid,programmeLevelPvid:$programmeLevelPvid){
            programmeSubjectPvid
            programmeLevelPvid
            averagePrice
        }
    }
`