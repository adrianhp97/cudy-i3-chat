import gql from "graphql-tag"

export const mutateSendMessage = gql`
  mutation sendMessage(
    $chatParentPvid: Int!
    $message: String!
    $senderName: String!
  ) {
    addConversation(
      chatParentPvid: $chatParentPvid,
      message: $message,
      senderName: $senderName
    ){
      chatParentPvid
      name
      message
      created_date
    }
  }
`

export const queryFetchMessages = gql`
  query getMessages(
    $chatParentPvid: Int!
  ) {
    conversationList(
      chatParentPvid: $chatParentPvid
    ){
      userPvid
      name
      message
      created_date
    }
  }
`