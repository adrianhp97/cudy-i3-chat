import gql from "graphql-tag"

export const queryFetchCurriculums = gql`
    query queryFetchCurriculums {
        getProgrammeCurriculumList {
            pvid
            name
        }
    }
`

export const queryFetchSubjects = gql`
    query queryFetchSubjects {
        getProgrammeSubjectList {
            pvid
            name
        }
    }
`

export const queryOtherPlatforms = gql`
    query queryOtherPlatforms {
        getOtherPlatformMasterList {
            pvid
            name
            iconLink
        }
    }
`
