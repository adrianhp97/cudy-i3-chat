import gql from "graphql-tag"

export const mutateLogin = gql`
    mutation mutateLogin($email: String!, $password: String!) {
        signIn(email: $email, password: $password) {
            token
            firstTime
            user {
                pvid
                email
                dateJoined
                verified
                token
                isActive
                role {
                    pvid
                    code
                    name
                    isPrivate
                }
            }
        }
    }
`

export const mutateRegistration = gql`
    mutation mutateRegistration(
        $rolePvid: Int!
        $email: String!
        $password: String!
        $firstName: String
        $lastName: String
        $description: String
        $dob: Date
        $address: String
        $referralId: Int
        $referralCode: String
        $uplineId: Int
    ) {
        register(
            rolePvid: $rolePvid
            email: $email
            password: $password
            firstName: $firstName
            lastName: $lastName
            description: $description
            dob: $dob
            address: $address
            referralId: $referralId
            referralCode: $referralCode
            uplineId: $uplineId
        ) {
            userCode
            userPvid
        }
    }
`

export const queryLogout = gql`
    query queryLogout {
        signOut
    }
`

export const mutateForgotPassword = gql`
    mutation mutateForgotPassword($email: String!) {
        requestResetPassword(email: $email) {
            success
            text
        }
    }
`

export const mutateSendVerificationEmail = gql`
    mutation mutateSendVerificationEmail($email: String!) {
        sendActivationEmail(email: $email) {
            success
            text
        }
    }
`

export const mutateNewPassword = gql`
    mutation mutateNewPassword(
        $newPassword1: String!
        $newPassword2: String!
        $resetCode: String!
    ) {
        resetPassword(
            newPassword1: $newPassword1
            newPassword2: $newPassword2
            resetCode: $resetCode
        ) {
            success
            text
        }
    }
`

export const mutateUpdatePassword = gql`
    mutation mutateUpdatePassword(
        $newPassword1: String!
        $newPassword2: String!
        $oldPassword: String!
    ) {
        updatePassword(
            newPassword1: $newPassword1
            newPassword2: $newPassword2
            oldPassword: $oldPassword
        ) {
            token
            user {
                pvid
                email
                password
                dateJoined
                verified
                token
            }
        }
    }
`
