import { gql } from "apollo-boost"

export const mutateSearchTutors = gql`
    mutation mutateSearchTutors(
        $keywords: String!
        $subjects: Int
        $curriculums: Int
        $price_range: PriceRange!
        $limit: Int
        $recently: Int!
        $alphabet: Int!
    ) {
        findTutors(
            keywords: $keywords
            price_range: $price_range
            subjects: $subjects
            curriculums: $curriculums
            limit: $limit
            recently: $recently
            alphabet: $alphabet
        ) {
            token
            pvid
            firstName
            lastName
            dateModified
            dob
            nric
            phone
            gender
            race
            description
            address
            pricePerHour
            education
            experienceYears
            resource {
                path
            }

            testimonial
            rating
        }
    }
`

export const mutateSortTutors = gql`
    mutation mutateSortTutors($recently: Int!, $alphabet: Int!) {
        sortTutors(recently: $recently, alphabet: $alphabet) {
            pvid
            firstName
            lastName
            dateModified
            dob
            nric
            phone
            gender
            race
            description
            address
            pricePerHour
            education
            experienceYears
            resource {
                path
            }

            testimonial
            rating
        }
    }
`
