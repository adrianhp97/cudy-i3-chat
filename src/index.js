import React from "react"
import ReactDOM from "react-dom"
import { ApolloClient } from "apollo-client"
import { InMemoryCache } from "apollo-cache-inmemory"
import { createUploadLink } from "apollo-upload-client"
import socketClient from "socket.io-client"
import { split } from "apollo-link"
import { getMainDefinition } from "apollo-utilities"
import { WebSocketLink } from "apollo-link-ws"

import "./styles/index.less"
import App from "./App"
import * as serviceWorker from "./serviceWorker"
import { baseUrl } from "helpers"

const token = localStorage.getItem("token")

const uploadLink = createUploadLink({
	uri: baseUrl + "/profile",
	credentials: "include",
	headers: {
		"auth-token": token,
		"Content-Type": "application/graphql"
	}
})

// const wsLink = new WebSocketLink({
//     // uri: `ws://3.0.139.243:3977/subscription`,
//     uri: `wss://backend.cudy.co:3977/subscription`,
//     options: { reconnect: false }
// })

export const client = new ApolloClient({
	cache: new InMemoryCache({ addTypename: false }),
	link: uploadLink
	// link: split(
	//     ({ query }) => {
	//         const definition = getMainDefinition(query)
	//         return definition.kind === "OperationDefinition" && definition.operation === "subscription"
	//     },
	//     // wsLink,
	//     uploadLink
	// )
})

ReactDOM.render(<App />, document.getElementById("root"))

serviceWorker.unregister()
