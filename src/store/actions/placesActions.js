import { instance, googlePlacesApiKey } from "helpers"
import * as types from "../constants"

const sgLatLng = "1.358533,103.802258"

export const loadingPlaces = (condition = true) => ({
	type: types.LOADING_PLACES,
	payload: { condition }
})

// prettier-ignore
export const fetchNearbyPlaces = ({lat, lng}, categoryId, radius = 40000) => dispatch => {
    return instance
        .get(
            `/nearbysearch/study/places?latitude=${lat}&longitude=${lng}&radius=${radius}&categoryPvid=${categoryId}`
        )
        .then(({data}) => 
            dispatch({ type: types.FETCH_NEARBY_PLACES, payload: data.data})
        )
        .catch(err => console.error(err.response))
}

export const searchPlaces = (keyword, typeId) => dispatch => {
	dispatch(loadingPlaces())
	return instance
		.get(`/find/study/places?name=${keyword}&typePvid=${typeId}`)
		.then(({ data }) => {
			dispatch({
				type: types.SEARCH_PLACES,
				payload: data.data
			})
		})
		.catch(err => console.error(err.response))
}

export const fetchPlaceTypes = () => dispatch => {
	dispatch(loadingPlaces())
	return instance
		.get(`/get/type/study/places`)
		.then(({ data }) => {
			dispatch({
				type: types.FETCH_PLACE_TYPES,
				payload: data.data
			})
		})
		.catch(err => console.error(err.response))
}

export const fetchPlaceDetails = placeId => dispatch => {
	dispatch(loadingPlaces())
	return instance
		.get(`/get/detail/study/places?studyPlacePvid=${placeId}`)
		.then(({ data }) => {
			dispatch({
				type: types.FETCH_PLACE_DETAILS,
				payload: data.data
			})
		})
		.catch(err => console.error(err.response))
}

export const fetchTuitionCategories = () => dispatch => {
	dispatch(loadingPlaces())
	return instance
		.get(`/get/category/private/tuition`)
		.then(({ data }) => {
			dispatch({ type: types.FETCH_TUITION_CATEGORIES, payload: data.data })
		})
		.catch(err => console.error(err.response))
}

export const searchTuition = (keyword, catId) => dispatch => {
	dispatch(loadingPlaces())
	return instance
		.get(`/find/private/tuition?name=${keyword}&categoryPvid=${catId}`)
		.then(({ data }) => dispatch({ type: types.SEARCH_TUITION, payload: data.data }))
		.catch(err => console.error(err.response))
}

export const searchNearbyTuition = ({ lat, lng }, categoryId, radius = 20) => dispatch => {
	dispatch(loadingPlaces())
	return instance
		.get(
			`/nearbysearch/private/tuition?latitude=${lat}&longitude=${lng}&radius=${radius}&categoryPvid=${categoryId}`
		)
		.then(({ data }) => dispatch({ type: types.SEARCH_NEARBY_TUITION, payload: data.data }))
		.catch(err => console.error(err.response))
}

export const fetchTuitionById = tuitionId => dispatch => {
	dispatch(loadingPlaces())
	return instance
		.get(`/get/private/tuition?tuitionPvid=${tuitionId}`)
		.then(({ data }) => {
			console.log({ tuitionData: data.data })
			dispatch({ type: types.FETCH_TUITION_BY_ID, payload: data.data })
		})
		.catch(err => console.error(err.response))
}
