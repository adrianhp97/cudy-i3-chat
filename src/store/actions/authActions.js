import * as Types from "../constants"
import { newClient } from "helpers"
import { message } from "antd"
import useError from "helpers/hooks/useError"

export const loadingAuth = () => ({ type: Types.LOADING_AUTH })

export const authUserOptions = setError => ({
    client: newClient("account"),
    onCompleted: ({ signIn }) => {
        const token = (signIn || {}).token
        const firstTime = (signIn || {}).firstTime
        const role = (signIn.user || {}).role || {}
        const userDetails = signIn.user || {}
        const url =
            firstTime && role.code === "TTR0"
                ? "/profile/onboarding_steps"
                : firstTime && role.code === "STDN"
                ? "/profile/onboarding_students"
                : "/"

        localStorage.setItem("token", token)
        localStorage.setItem("userDetails", JSON.stringify(userDetails))
        localStorage.setItem("firstTime", firstTime)
        message.loading("Logging you in...").then(() => {
            window.location.replace(url)
        })
    },
    onError: (err, netError) => {
        console.log({ err, netError })
        const error = useError(err)
        setError({ code: error.code, message: error.message })
    }
})

export const unauthUser = {
    client: newClient("account"),
    onCompleted: data => {
        const userDetails = JSON.parse(localStorage.getItem("userDetails")) || {}

        if (data) {
            localStorage.clear()
            message.loading("Logging you out...").then(() => {
                window.location.replace("/")
            })
        }

        // socket.emit("leave_channel", { token: userDetails.token })
    },
    onError: err => useError(err)
}
