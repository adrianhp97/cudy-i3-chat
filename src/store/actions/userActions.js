import { client } from "helpers"
import useError from "helpers/hooks/useError"

export const fetchUserOptions = () => ({
    client: client.profileClient,
    onError: err => {
        const error = err.graphQLErrors[0] || {}
        console.log({ error })
        if (error.statusCode === 401) {
            localStorage.clear()
            window.location.reload(true)
        }
    }
})

export const educationOptions = setEditMode => ({
    client: client.profileClient,
    onCompleted: data => setEditMode(""),
    onError: err => useError(err)
})
