import * as types from "../constants"

const initialStates = {
	places: [],
	pageToken: "",
	loading: false,
	place: {},
	placeTypes: [],
	tuitionCategories: [],
	// nearbyTuitions: [],
	tuition: {}
}

function reducer(state = initialStates, { payload, type }) {
	switch (type) {
		case types.LOADING_PLACES:
			return { ...state, loading: true }
		case types.FETCH_ALL_PLACES:
			return { ...state, places: payload, loading: false }
		case types.FETCH_PLACE_TYPES:
			return { ...state, placeTypes: payload, loading: false }
		case types.FIND_PLACE:
			return { ...state, places: payload, loading: false }
		case types.SEARCH_PLACES:
			return { ...state, places: payload, loading: false }
		case types.FETCH_NEARBY_PLACES:
			return {
				...state,
				nearbyPlaces: payload,
				loading: false
			}
		case types.FETCH_PHOTO:
			return { ...state, photo: payload, loading: false }
		case types.FETCH_PLACE_DETAILS:
			return { ...state, place: payload, loading: false }
		case types.FETCH_TUITION_CATEGORIES:
			return { ...state, tuitionCategories: payload, loading: false }
		case types.SEARCH_NEARBY_TUITION:
			return { ...state, nearbyTuitions: payload, loading: false }
		case types.FETCH_TUITION_BY_ID:
			return { ...state, tuition: payload, loading: false }
		case types.SEARCH_TUITION:
			return { ...state, tuitions: payload, loading: false }
		default:
			return state
	}
}

export default reducer
