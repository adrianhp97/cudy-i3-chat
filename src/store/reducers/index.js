import { combineReducers } from "redux"
import userReducer from "./userReducer.js"
import authReducer from "./authReducer.js"
import placesReducer from "./placesReducer.js"

const appReducer = combineReducers({
    user: userReducer,
    auth: authReducer,
    place: placesReducer
})

export const rootReducer = (state, action) => appReducer(state, action)
