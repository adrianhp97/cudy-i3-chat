import * as Types from "../constants"

const initialState = {
    user: {}
}

function userReducer(state = initialState, action) {
    switch (action.type) {
        case Types.FETCH_USER:
            return { ...state, user: { name: "Gunawan Sasongko", age: 24 } }
        default:
            return state
    }
}

export default userReducer
