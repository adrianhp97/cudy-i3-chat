import { css } from "styled-components"
import { ApolloClient } from "apollo-client"
import { InMemoryCache } from "apollo-cache-inmemory"
import { createUploadLink } from "apollo-upload-client"
import { message } from "antd"
import axios from "axios"
import md5 from "md5"

const token = localStorage.getItem("token")
message.config({ duration: 1 })

export const baseUrl = `https://backend.cudy.co`
// export const baseUrl = `http://3.0.139.243:3901`
export const serverBaseUrl = `${baseUrl}/src`
export const googleApiKey = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}&libraries=places`
export const googlePlacesApiKey = process.env.REACT_APP_GOOGLE_PLACES_API
const googlePhotoUrl = `https://maps.googleapis.com/maps/api/place/photo`
export const getPhotoUrl = reference =>
	`${googlePhotoUrl}?maxwidth=800&photoreference=${reference}&key=${process.env.REACT_APP_GOOGLE_PLACES_API}`
export const placeholderImage =
	"https://gw.alipayobjects.com/mdn/miniapp_social/afts/img/A*pevERLJC9v0AAAAAAAAAAABjAQAAAQ/original"

const corsUrl = `https://cors-anywhere.herokuapp.com`

const instance = axios.create({
	baseURL: baseUrl
	// baseURL: `${corsUrl}/https://maps.googleapis.com/maps/api/place`
})

instance.interceptors.request.use(config => {
	config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`
	return config
})

export const renderAvatar = (data = {}, serverBaseUrl = serverBaseUrl) => {
	if (data.hasOwnProperty("getProfile")) data = data.getProfile || {}
	const { avatar = [] } = data
	const gravatar = `https://gravatar.com/avatar/${md5(data.email || "")}`
	const userAvatar = (avatar[0] && avatar[0].path) || ""
	const theAvatar = avatar.length > 0 ? serverBaseUrl + userAvatar : gravatar
	return theAvatar
}

export { instance }

export const newClient = (clientName = "profile") => {
	return new ApolloClient({
		cache: new InMemoryCache({ addTypename: false }),
		link: createUploadLink({
			uri: `${baseUrl}/${clientName}`,
			headers: { "auth-token": token }
		})
	})
}

export const client = {
	programmeClient: new ApolloClient({
		cache: new InMemoryCache({ addTypename: false }),
		link: createUploadLink({
			uri: `${baseUrl}/programme`,
			// credentials: "include",
			headers: {
				"auth-token": token
			}
		})
	}),
	accountClient: new ApolloClient({
		cache: new InMemoryCache({ addTypename: false }),
		link: createUploadLink({
			uri: `${baseUrl}/account`,
			// credentials: "include",
			headers: {
				"auth-token": token
			}
		})
	}),
	profileClient: new ApolloClient({
		cache: new InMemoryCache({ addTypename: false }),
		link: createUploadLink({
			uri: `${baseUrl}/profile`,
			headers: {
				"auth-token": token
			}
		})
	}),
	resourceClient: new ApolloClient({
		cache: new InMemoryCache({ addTypename: false }),
		link: createUploadLink({
			uri: `${baseUrl}/resource`,
			// credentials: "include",
			headers: {
				"auth-token": token
			}
		})
	})
}

export const randomColor = arr => arr.slice().sort(() => Math.random() - 0.58)[0]

export const pricer = price =>
	new Intl.NumberFormat("en-SG", {
		currency: "SGD"
	}).format(price)

export const upperCase = word => word && word[0].toUpperCase() + word.slice(1)

// Range from date/time
export const range = (from, to) => Array.from({ length: Math.ceil((to - from + 1) / 1) }, (_, idx) => from + idx * 1)

// Media queries Styled-Components
const sizes = {
	tablet: 767,
	mobile: 414
}

export const media = Object.keys(sizes).reduce((acc, label) => {
	acc[label] = (...args) => css`
		@media (max-width: ${sizes[label]}px) {
			${css(...args)};
		}
	`
	return acc
}, {})

export const mobile = window.innerWidth < 415
export const tablet = window.innerWidth > 414 && window.innerWidth < 769
