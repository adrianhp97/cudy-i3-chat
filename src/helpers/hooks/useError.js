import { message } from "antd"

export default function useError(error = {}) {
    const err = error.networkError || {}
    const theError = (err.result || {}).errors[0] || {}
    if (theError) {
        message.error(theError.message || "Uh oh, there's something bad happened")
        return { message: theError.message, code: theError.code || -1 }
    }
}
