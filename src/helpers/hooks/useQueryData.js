import { useQuery } from "react-apollo"
import { client } from "helpers"

export default function useQueryData(query = "", uri) {
    const { data, error, loading } = useQuery(query, { client: client[uri] || "" })

    return { data: data || {}, loading, error }
}
