import React, { useEffect } from "react"
import { Switch, BrowserRouter, Route, Redirect } from "react-router-dom"
import { ApolloProvider } from "@apollo/react-hooks"
import { Provider } from "react-redux"
import socketClient from "socket.io-client"

import Home from "./pages/Home"
import { createAppStore } from "./store"
import CourseDetails from "./pages/CourseDetails"
import Signup from "./pages/Signup"
import Marketplace from "./pages/Marketplace"
import ForgotPassword from "./pages/ForgotPassword"
import ForgotSent from "./pages/ForgotPassword/Sent"
import RegisterSuccess from "./pages/Signup/RegisterSuccess"
import TutorProfile from "./pages/Tutor/Profile"
import NewPasword from "./pages/ForgotPassword/NewPassword"
import PasswordChanged from "./pages/ForgotPassword/PasswordChanged"
// import Dashboard from "./pages/Dashboard"
import Signin from "./pages/Signin"
import AccountActive from "pages/AccountActive"
import ResendVerification from "pages/ResendVerification"
import Resent from "pages/ResendVerification/Resent"
import AdditionalInfoTutor from "pages/AdditionalInfoTutor"
import Thankyou from "pages/AdditionalInfoTutor/Thankyou"
import NewPassword from "./pages/ForgotPassword/NewPassword"
import AddTestimonials from "pages/Tutor/Profile/AddTestimonials"
import TestimonialSuccess from "pages/Tutor/Profile/TestimonialSuccess"
import Settings from "pages/Tutor/Settings"
import { client } from "index"
import ScrollToTop from "components/common/ScrollToTop"
import ConditionalPage from "components/common/ConditionalPage"
import OnboardingStudents from "pages/OnboardingStudents"
import WhoViewed from "pages/Tutor/Profile/WhoViewed"
import Assignments from "pages/Tutor/Assignments"
import Dashboard from "pages/Dashboard"
import PostAssignment from "pages/Tutor/Assignments/Post"
import { baseUrl } from "helpers"
import { notifyMe } from "helpers/notification"
import StudyPlaces from "pages/StudyPlaces"
import PlaceDetails from "pages/StudyPlaces/PlaceDetails"

const socket = socketClient(baseUrl)
// localStorage.setItem("socketStorage", socket)

function connectSocket() {
	socket.on("notification", function(msg) {
		console.log("notify running")
		console.log(msg)
		notifyMe(msg.reviewerName, msg.remarks)
	})
}

const userDetails = JSON.parse(localStorage.getItem("userDetails")) || {}

export default class App extends React.Component {
	constructor(props) {
		super(props)

		connectSocket()
		this.state = {
			join: false
		}
	}

	render() {
		if (localStorage.getItem("token") && !this.state.join) {
			this.setState({ join: true }, () => {
				console.log("join")
				socket.emit("join_channel", { channel: userDetails.token })
			})
		}
		return (
			<div className="main-wrapper">
				<Provider store={createAppStore()}>
					<ApolloProvider client={client}>
						<BrowserRouter>
							<ScrollToTop />
							<Switch>
								{/* <Redirect exact from="/" to="/maintenance" /> */}

								{/* <Redirect exact from="/" to={token ? "/" : "/login"} /> */}
								<Route exact path="/" component={Home} />
								<Route path="/course/1" component={CourseDetails} />
								<Route exact path="/register" component={Signup} />
								<Route path="/register/success" component={RegisterSuccess} />
								<Route path="/account/active" component={AccountActive} />
								<Route exact path="/account/resend_email" component={ResendVerification} />
								<Route path="/account/resend_email/success" component={Resent} />
								<Route path="/login" render={() => <Signin socket={socket} />} />

								<Route path="/profile/info/thankyou" component={Thankyou} />
								<Route path="/profile/add_testimonial/success" component={TestimonialSuccess} />
								<Route path="/profile/add_testimonials" component={AddTestimonials} />
								<Route path="/profile/onboarding_steps" component={AdditionalInfoTutor} />
								<Route path="/profile/onboarding_students" component={OnboardingStudents} />
								<Route path="/profile/settings" component={Settings} />
								<Route path="/profile/viewed" component={WhoViewed} />
								<Route exact path="/profile/me" component={TutorProfile} />
								<Route
									path="/:token-:firstName-:lastName"
									render={() => <TutorProfile peopleProfile={true} />}
								/>
								<Route path="/dashboard" component={Dashboard} />
								<Route path="/study_places" component={StudyPlaces} />
								{/* <Route exact path="/study_places/tuition/:name-:pvid" component={PlaceDetails} /> */}

								<Route exact path="/assignments" component={Assignments} />
								<Route path="/assignments/post_assignment" component={PostAssignment} />
								<Route path="/browse/tutors" component={Marketplace} />
								<Route path="/forgot/sent" component={ForgotSent} />
								<Route path="/forgot/new_password" component={NewPassword} />
								<Route path="/forgot/password_changed" component={PasswordChanged} />
								<Route path="/forgot" component={ForgotPassword} />

								<Route render={() => <ConditionalPage status="404" />} />
							</Switch>
						</BrowserRouter>
					</ApolloProvider>
				</Provider>
			</div>
		)
	}
}

// const App = () => {
//     const socket = socketClient(baseUrl)

//     // socket.connect()

//     useEffect(() => {
//         console.log({ socket })
//         socket.on("notification", msg => {
//             console.log("notify running")
//             console.log(msg)
//             notifyMe(msg.reviewerName, msg.remarks)
//         })
//     })

//     return (
//         <div className="main-wrapper">
//             <Provider store={createAppStore()}>
//                 <ApolloProvider client={client}>
//                     <BrowserRouter>
//                         <ScrollToTop />
//                         <Switch>
//                             {/* <Redirect exact from="/" to="/maintenance" /> */}

//                             {/* <Redirect exact from="/" to={token ? "/" : "/login"} /> */}
//                             <Route exact path="/" component={Home} />
//                             <Route path="/course/1" component={CourseDetails} />
//                             <Route exact path="/register" component={Signup} />
//                             <Route path="/register/success" component={RegisterSuccess} />
//                             <Route path="/account/active" component={AccountActive} />
//                             <Route exact path="/account/resend_email" component={ResendVerification} />
//                             <Route path="/account/resend_email/success" component={Resent} />
//                             <Route path="/login" component={Signin} />

//                             <Route path="/profile/info/thankyou" component={Thankyou} />
//                             <Route path="/profile/add_testimonial/success" component={TestimonialSuccess} />
//                             <Route path="/profile/add_testimonials" component={AddTestimonials} />
//                             <Route path="/profile/onboarding_steps" component={AdditionalInfoTutor} />
//                             <Route path="/profile/onboarding_students" component={OnboardingStudents} />
//                             <Route path="/profile/settings" component={Settings} />
//                             <Route path="/profile/viewed" component={WhoViewed} />
//                             <Route exact path="/profile/me" component={TutorProfile} />
//                             <Route
//                                 path="/:token-:firstName-:lastName"
//                                 render={() => <TutorProfile peopleProfile={true} />}
//                             />
//                             <Route path="/dashboard" component={Dashboard} />

//                             <Route exact path="/assignments" component={Assignments} />
//                             <Route path="/assignments/post_assignment" component={PostAssignment} />
//                             <Route path="/browse/tutors" component={Marketplace} />
//                             <Route path="/forgot/sent" component={ForgotSent} />
//                             <Route path="/forgot/new_password" component={NewPassword} />
//                             <Route path="/forgot/password_changed" component={PasswordChanged} />
//                             <Route path="/forgot" component={ForgotPassword} />

//                             <Route render={() => <ConditionalPage status="404" />} />
//                         </Switch>
//                     </BrowserRouter>
//                 </ApolloProvider>
//             </Provider>
//         </div>
//     )
// }

// export default App
