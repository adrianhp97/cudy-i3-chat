import React, { useEffect, useState } from "react"
import { Row, Col, Form, message } from "antd"
import { withRouter, Link } from "react-router-dom"
import { Section, Heading, Button, Alert } from "components"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import styled from "styled-components"
import { useMutation } from "@apollo/react-hooks"
import { validationSchema } from "./validation"
import { mutateLogin } from "queries/auth"
import { authUserOptions } from "store/actions/authActions"
import { mobile, baseUrl } from "helpers"

const BackgroundImage = styled.div`
    background-image: url("http://source.unsplash.com/random/");
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    position: relative;
    min-height: 150vh;
    img {
        position: absolute;
        left: 30px;
        top: 20px;
        cursor: pointer;
    }
    
`

const StyledRow = styled(Row)`
    background-color: #fff;
`

function Signin({ history, location, socket }) {
    const [theError, setError] = useState(null)
    const [setLogin, { loading }] = useMutation(mutateLogin, authUserOptions(setError, socket))

    const handleLogin = (values, { resetForm }) => {
        setLogin({ variables: { ...values } })
    }

    useEffect(() => {
        if (localStorage.getItem("token")) {
            history.push("/")
        }

        // if ((location.state || {}).loggedout) {
        //     message.error("Your session is expired. Please re-login to continue")
        // }
    }, [])

    return (
        <StyledRow >
            {!mobile && (
                <Col lg={12}>
                    <BackgroundImage>
                        <img
                            src="https://app.cudy.co/img/logo_live.png"
                            width="100"
                            onClick={() => history.push("/")}
                        />
                    </BackgroundImage>
                </Col>
            )}
            <Col lg={12}>
                {mobile && (
                    <Row type="flex" justify="center" style={{ paddingTop: "2em" }}>
                        <Col lg={12}>
                            <img
                                src="https://app.cudy.co/img/logo_live.png"
                                width="100"
                                onClick={() => history.push("/")}
                            />
                        </Col>
                    </Row>
                )}
                <Section style={{ marginTop: "3em" }}>
                    <Heading
                        content="Welcome back!"
                        subheader="Please login into your account to continue using Cudy"
                    />
                </Section>

                {/* <Section>
                    <Row gutter={16}>
                        <Col lg={12}>
                            <Button block social="facebook" icon="facebook">
                                Sign in with Facebook
                            </Button>
                        </Col>
                        <Col lg={12}>
                            <Button block social="google" icon="google">
                                Sign in with Google
                            </Button>
                        </Col>
                    </Row>
                </Section>
                <Section centered>OR</Section> */}

                <Section marginBottom={0}>
                    <Formik
                        onSubmit={handleLogin}
                        validationSchema={validationSchema}
                        initialValues={{ email: "", password: "" }}
                        render={({ handleSubmit }) => (
                            <Form onSubmit={handleSubmit} layout="vertical">
                                <TextInput
                                    type="email"
                                    label="Your email"
                                    name="email"
                                    placeholder="Enter your email"
                                />
                                <TextInput
                                    password
                                    type="password"
                                    label="Your password"
                                    name="password"
                                    placeholder="Enter your password"
                                />
                                {theError && theError.code === 10010 && (
                                    <Section paddingHorizontal={0}>
                                        <Alert
                                            message="Not verified"
                                            description={
                                                <span>
                                                    It looks like your account is not verified yet.{" "}
                                                    <Link to="/account/resend_email">
                                                        Send me another email verification
                                                    </Link>
                                                </span>
                                            }
                                            type="error"
                                            showIcon
                                        />
                                    </Section>
                                )}
                                <Section
                                    paddingHorizontal={0}
                                    textAlign="right"
                                    style={{ paddingTop: 0 }}
                                    marginBottom="very"
                                >
                                    <Link to="/forgot">Forgot you password?</Link>
                                </Section>
                                <Row gutter={24}>
                                    <Col lg={12} style={{ marginBottom: mobile && "1em" }}>
                                        <Button type="primary" htmlType="submit" loading={loading} block>
                                            Login now
                                        </Button>
                                    </Col>{" "}
                                    <Col lg={12}>
                                        <Button
                                            block
                                            type="ghost"
                                            htmlType="button"
                                            onClick={() => history.push("/register")}
                                        >
                                            Sign up
                                        </Button>
                                    </Col>
                                </Row>
                            </Form>
                        )}
                    />
                </Section>
            </Col>
        </StyledRow>
    )
}

export default withRouter(Signin)
