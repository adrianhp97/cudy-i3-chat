import * as yup from "yup"

export const validationSchema = yup.object().shape({
    email: yup
        .string()
        .required("This is required")
        .email("Please enter a valid email"),
    password: yup.string().required("This is required")
})
