import React from "react"
import { Section, Card, Heading, Button, Tick } from "components"
import { Row, Col } from "antd"
import { Link, withRouter } from "react-router-dom"

function AccountActive({ history, location }) {
    const params = new URLSearchParams(location.search)
    const success = params.get("success")

    return (
        <Section>
            <Row type="flex" justify="center" align="middle">
                <Col lg={12}>
                    <Section centered>
                        <Link to="/">
                            <img src="https://app.cudy.co/img/logo_live.png" width="100" />
                        </Link>
                    </Section>
                    <Card autoHeight noHover>
                        {success ? (
                            <Section centered>
                                <Tick />
                                <Heading
                                    content="Well done!"
                                    subheader="Your account is active. Now start logging in to browse best tutors around us."
                                    marginBottom="3em"
                                />
                                <Link to="/login">
                                    <Button type="ghost" icon="user">
                                        Go to login
                                    </Button>
                                </Link>
                            </Section>
                        ) : (
                            <Section centered>
                                <Heading
                                    content="Oops!"
                                    subheader="Something wrong happened. you might want to go back to the Home page"
                                    marginBottom="3em"
                                />
                                <Link to="/">
                                    <Button type="ghost" icon="home">
                                        Go to Home
                                    </Button>
                                </Link>
                            </Section>
                        )}
                    </Card>
                </Col>
            </Row>
        </Section>
    )
}

export default withRouter(AccountActive)
