import * as yup from "yup"

export const validationSchema = yup.object().shape({
    email: yup
        .string()
        .required("You have to tell your email")
        .email("Please provide a valid email address"),
    password: yup
        .string()
        .required()
        .min(8, "Eight characters minimum"),
    // address: yup.string().required("You have to tell your location"),
    tc: yup.string().required("You have to read the required documents first before continue"),
    repeat_password: yup
        .string()
        .required("Please repeat your password")
        .oneOf([yup.ref("password")], "Password should match")
})
