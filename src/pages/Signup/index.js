import React, { useEffect, useState } from "react"
import { Row, Col, Form } from "antd"
import { withRouter } from "react-router-dom"
import { Formik } from "formik"
import styled from "styled-components"

import { Section, Heading, Button, Alert, Avatar } from "components"
import TextInput from "components/forms/TextInput"
import RadioInput from "components/forms/RadioInput"
import CheckboxInput from "components/forms/CheckboxInput"
import { validationSchema } from "./validation"
import { useMutation } from "@apollo/react-hooks"
import { mobile, newClient, serverBaseUrl, baseUrl } from "helpers"
import useError from "helpers/hooks/useError"
import { mutateRegistration } from "queries/auth"
import { SubmitButton } from "formik-antd"
import { mutateUserProfileForReferral } from "queries/profile"

const BackgroundImage = styled.div`
    background-image: url("http://source.unsplash.com/random/");
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    position: relative;
    min-height: 170vh;
    img {
        position: absolute;
        left: 30px;
        top: 20px;
        cursor: pointer;
    }
`

const StyledRow = styled(Row)`
    background-color: #fff;
`

function Signup({ history, location }) {
    const [userProfile, setUserProfile] = useState({})
    const params = new URLSearchParams(location.search)

    const { avatar = [] } = userProfile

    const isFromReferral = params.get("referral") && params.get("code") && params.get("upline")
    const avatarUrl = serverBaseUrl + (avatar[0] || {}).path

    const [fetchProfile] = useMutation(mutateUserProfileForReferral, {
        client: newClient("profile"),
        onCompleted: data => {
            data = data.getProfile
            setUserProfile(data)
        }
    })

    const [handleRegis] = useMutation(mutateRegistration, {
        client: newClient("account"),
        onCompleted: data => {
            history.push({ pathname: "/register/success", state: { success: true } })
        },
        onError: err => useError(err)
    })

    const handleRegister = (values, { setSubmitting }) => {
        values = {
            ...values,
            firstName: "",
            lastName: "",
            description: "",
            address: "",
            dob: "",
            referralId: Number(params.get("referral")) || 0,
            referralCode: params.get("code") || "",
            uplineId: Number(params.get("upline")) || 0
        }
        console.log({ values })
        const { checkboxes, date, tc, month, year, repeat_password, ...rest } = values
        handleRegis({ variables: { ...rest, referralLink: "" } }).finally(() => {
            setSubmitting(false)
        })
    }

    const referMessage = (
        <span>
            You are being referred by &nbsp;
            <a
                href={`${baseUrl}/profile/${params.get("upline")}-${userProfile.firstName}-${
                    userProfile.lastName
                }`}
                target="_blank"
                rel="noopener noreferrer"
            >
                <Avatar src={avatarUrl} size="small" /> {userProfile.firstName}{" "}
                {userProfile.lastName}
            </a>
        </span>
    )

    useEffect(() => {
        if (localStorage.getItem("token")) history.push("/404")

        if (isFromReferral) fetchProfile({ variables: { userPvid: Number(params.get("upline")) } })
    }, [])

    return (
        <StyledRow>
            {!mobile && (
                <Col lg={12}>
                    <BackgroundImage>
                        <img
                            src="https://app.cudy.co/img/logo_live.png"
                            width="100"
                            onClick={() => history.push("/")}
                        />
                    </BackgroundImage>
                </Col>
            )}
            <Col lg={12}>
                {mobile && (
                    <Row type="flex" justify="center" style={{ paddingTop: "2em" }}>
                        <Col lg={12}>
                            <img
                                src="https://app.cudy.co/img/logo_live.png"
                                width="100"
                                onClick={() => history.push("/")}
                            />
                        </Col>
                    </Row>
                )}
                <Section style={{ marginTop: "3em" }}>
                    <Heading
                        content="Join Cudy today"
                        subheader="Join our learning platform to get more than just family"
                        bold
                    />
                </Section>

                {isFromReferral && (
                    <Section style={{ paddingTop: 0, paddingBottom: 0 }}>
                        <Alert type="info" showIcon message={referMessage} />
                    </Section>
                )}

                {/*
                    ███████╗ ██████╗  ██████╗██╗ █████╗ ██╗         ██╗      ██████╗  ██████╗ ██╗███╗   ██╗███████╗
                    ██╔════╝██╔═══██╗██╔════╝██║██╔══██╗██║         ██║     ██╔═══██╗██╔════╝ ██║████╗  ██║██╔════╝
                    ███████╗██║   ██║██║     ██║███████║██║         ██║     ██║   ██║██║  ███╗██║██╔██╗ ██║███████╗
                    ╚════██║██║   ██║██║     ██║██╔══██║██║         ██║     ██║   ██║██║   ██║██║██║╚██╗██║╚════██║
                    ███████║╚██████╔╝╚██████╗██║██║  ██║███████╗    ███████╗╚██████╔╝╚██████╔╝██║██║ ╚████║███████║                                                                    
                */
                /* <Section>
                    <Row gutter={16}>
                        <Col lg={12}>
                            <Button block social="facebook" icon="facebook">
                                Sign up with Facebook
                            </Button>
                        </Col>
                        <Col lg={12}>
                            <Button block social="google" icon="google">
                                Sign up with Google
                            </Button>
                        </Col>
                    </Row>
                </Section>
                <Section centered>OR</Section> */}

                <Section marginBottom={mobile ? "very" : 0} style={{ paddingBottom: !mobile && 0 }}>
                    <Formik
                        onSubmit={handleRegister}
                        validationSchema={validationSchema}
                        initialValues={{ rolePvid: 1 }}
                        render={({ handleSubmit }) => (
                            <Form onSubmit={handleSubmit} layout="vertical">
                                <Row gutter={16}>
                                    <Col lg={24}>
                                        <TextInput
                                            label="Your email"
                                            type="email"
                                            name="email"
                                            placeholder="Enter your email"
                                        />
                                    </Col>
                                </Row>
                                <Row gutter={16}>
                                    <Col lg={12}>
                                        <TextInput
                                            password
                                            label="Your password"
                                            name="password"
                                            placeholder="Enter your password"
                                        />
                                    </Col>
                                    <Col lg={12}>
                                        <TextInput
                                            password
                                            label="Repeat password"
                                            name="repeat_password"
                                            placeholder="Repeat your password"
                                        />
                                    </Col>
                                </Row>
                                <Section paddingHorizontal={0}>
                                    <Heading
                                        content="Select your role"
                                        level={4}
                                        subheader="Please choose your role"
                                    />
                                    <RadioInput
                                        name="rolePvid"
                                        options={[
                                            { label: "Tutor", value: 2 },
                                            { label: "Student/parent", value: 1 }
                                        ]}
                                    />
                                </Section>
                                <Section paddingHorizontal={0}>
                                    <CheckboxInput name="tc">
                                        <span>
                                            I have agreed to the{" "}
                                            <a href="/">terms and conditions</a> and{" "}
                                            <a href="/">privacy policy</a> of the site
                                        </span>
                                    </CheckboxInput>
                                </Section>
                                <Row gutter={24}>
                                    <Col lg={12} style={{ marginBottom: mobile && "1em" }}>
                                        <SubmitButton block size="large" type="primary">
                                            Create account
                                        </SubmitButton>
                                    </Col>
                                    <Col lg={12}>
                                        <Button
                                            block
                                            type="ghost"
                                            htmlType="button"
                                            onClick={() => history.push("/login")}
                                        >
                                            Login
                                        </Button>
                                    </Col>
                                </Row>
                            </Form>
                        )}
                    />
                </Section>
            </Col>
        </StyledRow>
    )
}

export default withRouter(Signup)
