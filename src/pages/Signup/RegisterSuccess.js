import React, { useEffect } from "react"
import { Row, Col, Icon } from "antd"
import { Link, withRouter } from "react-router-dom"
import { Section, Heading, Tick, Button, Card, Alert } from "components"
import styled from "styled-components"
import ShareSection from "components/common/ShareSection"
import { baseStyles } from "styles/base"

function RegisterSuccess({ location, history }) {
    const CutorsSection = styled(Section).attrs(() => ({
        paddingHorizontal: "0"
    }))`
        background: #1890ff;
        border-radius: 8px;
        padding: 2em;
        margin-bottom: 4em;
        h4.ant-typography {
            margin-bottom: 0.5em;
        }
        .ant-typography,
        a {
            color: #fff;
        }
    `

    useEffect(() => {
        const state = location.state || {}
        if (!state.success) history.push("/register")
    }, [])

    return (
        <Section style={{ marginTop: "2em" }}>
            <Row type="flex" justify="center" align="middle">
                <Col lg={12}>
                    <Card title="" autoHeight>
                        <Section centered>
                            <Tick />
                            <Heading
                                bold
                                content="Thank you! :)"
                                subheader="Hey, thank you for registering at Cudy! Now please take a look at your inbox and verify your account. Don't worry, won't take you long. We promise."
                                marginBottom="3em"
                            />
                            <Alert
                                message="Don't forget to check your spam/junk folder too :)"
                                type="info"
                                showIcon
                            />{" "}
                            <Row type="flex" justify="center" style={{ marginBottom: "2em" }}>
                                <Col lg={18} style={{ textAlign: "center" }}>
                                    <p style={{ color: baseStyles.greyColor }}>
                                        Mind to spread some words?
                                    </p>
                                    <ShareSection isNotLoggedin />
                                </Col>
                            </Row>
                            <CutorsSection>
                                <Heading
                                    content="Join Cutors!  🙋🏼‍♂️"
                                    subheader="If you are a tutor, don't forget to join our dedicated community of Tutors in Singapore as well. We've been waiting for you."
                                />
                                <a href="https://t.me/cutors/" target="__blank">
                                    Join now <Icon type="right" />
                                </a>
                            </CutorsSection>
                        </Section>
                        <Section centered paddingHorizontal="normal">
                            <Link to="/">
                                <Button type="ghost" icon="home">
                                    Back to Home
                                </Button>
                            </Link>
                        </Section>
                    </Card>
                </Col>
            </Row>
        </Section>
    )
}

export default withRouter(RegisterSuccess)
