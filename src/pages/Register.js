import React, { useState } from "react"
import { Form, Row, Col, Radio, Checkbox } from "antd"
import { Formik, Field } from "formik"
import TextInput from "components/forms/TextInput"
import Button from "components/Button"
import Heading from "components/Heading"
import Section from "components/Section"
import styled from "styled-components"
import SelectInput from "components/forms/SelectInput"
import { dates, yearListFrom1980, months } from "dummy"
import RadioInput from "components/forms/RadioInput"
import CheckboxInput from "components/forms/CheckboxInput"
import { Link, withRouter } from "react-router-dom"
import { useMutation } from "react-apollo"
import { newClient } from "helpers"
import useError from "helpers/hooks/useError"
import { mutateRegistration } from "queries/auth"

const StyledSection = styled(Section)`
    padding: 20px;
`

const checkboxOptions = [
    {
        label: "Be the first to receive discounts and deals from the Cudy newsletter",
        value: "newsletter"
    },
    {
        label: (
            <span>
                I have agreed to the <a href="/">terms and conditions</a> and{" "}
                <a href="/">privacy policy</a> of the site
            </span>
        ),
        value: "tc"
    }
]

const yearList = yearListFrom1980.map(item => ({ value: item, label: item }))
const monthOptions = months.map(item => ({ label: item.label, value: item.number }))

function Register({ onSetActive, history, ...props }) {
    const [handleRegis, { error, loading }] = useMutation(mutateRegistration, {
        client: newClient("account"),
        onCompleted: data => {
            if (data) {
                history.push({ pathname: "/register/success", state: { success: true } })
            }
        },
        onError: err => useError(err)
    })

    const handleRegister = (values, { resetForm }) => {
        values = {
            ...values,
            description: "",
            dob: `${values.year}/${values.month}/${values.date}`
        }
        const { checkboxes, date, month, year, repeat_password, ...rest } = values
        handleRegis({ variables: { ...rest, referralLink: "" } })
    }

    return (
        <>
            <StyledSection>
                <Formik
                    onSubmit={handleRegister}
                    validate={validate}
                    initialValues={{ date: 1, month: "01", year: 2000, rolePvid: 2 }}
                    render={({ handleSubmit }) => (
                        <Form onSubmit={handleSubmit} layout="vertical">
                            <Row gutter={16}>
                                <Col lg={12}>
                                    <TextInput
                                        label="Your first name"
                                        type="text"
                                        name="firstName"
                                        placeholder="Enter your first name"
                                    />
                                </Col>
                                <Col lg={12}>
                                    <TextInput
                                        label="Your last name"
                                        type="text"
                                        name="lastName"
                                        placeholder="Enter your last name"
                                    />
                                </Col>
                            </Row>
                            <TextInput
                                label="Your email"
                                type="email"
                                name="email"
                                placeholder="Enter your email"
                            />
                            <Row gutter={16}>
                                <Col lg={24} style={{ marginBottom: ".5em" }}>
                                    <label>Date of birth</label>
                                </Col>
                                <Col lg={6}>
                                    <SelectInput name="date" options={dates} />
                                </Col>
                                <Col lg={10}>
                                    <SelectInput name="month" options={monthOptions} />
                                </Col>
                                <Col lg={8}>
                                    <SelectInput name="year" options={yearList} />
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col lg={12}>
                                    <TextInput
                                        password
                                        label="Your password"
                                        name="password"
                                        placeholder="Enter your password"
                                    />
                                </Col>
                                <Col lg={12}>
                                    <TextInput
                                        password
                                        label="Repeat password"
                                        name="repeat_password"
                                        placeholder="Repeat your password"
                                    />
                                </Col>
                            </Row>
                            <TextInput
                                name="address"
                                placeholder="Your location..."
                                label="Your location in Singapore"
                            />
                            <Section paddingHorizontal={0}>
                                <Heading
                                    content="Select your role"
                                    level={4}
                                    subheader="Please choose your role"
                                />
                                <RadioInput
                                    name="rolePvid"
                                    options={[
                                        { label: "Tutor", value: 2 },
                                        { label: "Student/parent", value: 1 }
                                    ]}
                                />
                            </Section>
                            <Section paddingHorizontal={0}>
                                <CheckboxInput group name="checkboxes" options={checkboxOptions} />
                            </Section>
                            <Button block htmlType="submit" type="primary" loading={loading}>
                                Create account
                            </Button>
                            {/* <Col lg={12}>
                                    <Link to="/login">
                                        <Button block type="ghost" htmlType="button">
                                            Login
                                        </Button>
                                    </Link>
                                </Col> */}
                        </Form>
                    )}
                />
            </StyledSection>
            <StyledSection centered>
                <p>
                    Already have an account?{" "}
                    <a href="#" onClick={() => onSetActive("login")}>
                        Login
                    </a>
                </p>
            </StyledSection>
        </>
    )
}

const validate = values => {
    const error = {}

    if (!values.firstName) error.firstName = "This is required"
    if (!values.lastName) error.lastName = "This is required"
    if (!values.email) error.email = "This is required"
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email))
        error.email = "Please provide a valid email address"
    if (!values.password) error.password = "This is required"
    else if (values.password.length < 6) error.password = "At least 6 characters"
    if (!values.repeat_password) error.repeat_password = "This is required"
    else if (values.repeat_password !== values.password)
        error.repeat_password = "Password doesn't match"

    return error
}

export default withRouter(Register)
