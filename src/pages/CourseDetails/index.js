import React from "react"
import { withRouter } from "react-router-dom"
import { Typography, Rate, Row, Col, Icon, Divider, Skeleton, Affix } from "antd"
import Section from "../../components/Section"
import Layout from "../../components/Layout"
import playing from "../../assets/images/playing-computer.png"
import "styled-components/macro"
import styled from "styled-components"
import Card from "../../components/Card"
import Button from "../../components/Button"
import CourseCard from "../../components/CourseCard"
import List from "../../components/List"
import { lessons, reviews, courses } from "../../dummy"
import Heading from "../../components/Heading"
import CommentItem from "../../components/CommentItem"
import Carousel from "../../components/Carousel"

const WhiteSpan = styled.span`
    color: #fff;
`

const { Paragraph, Title } = Typography

class CourseDetails extends React.Component {
    constructor(props) {
        super(props)

        this.container = React.createRef()
    }

    render() {
        return (
            <Layout>
                <Section dark paddingHorizontal="padded">
                    <Row>
                        <Col span={14}>
                            <Section dark padding="20px 0">
                                <Rate disabled defaultValue={5} /> &nbsp;{" "}
                                <span>5/5 (230 reviews)</span>
                            </Section>
                            <Typography>
                                <Title level={2}>Python Programming for O Levels: Part 1/3</Title>
                                <Paragraph css="margin-bottom: 3em !important">
                                    <WhiteSpan>
                                        by <strong>Mr. Francis Goh</strong> &middot;{" "}
                                        <strong>Lower secondary</strong> &middot;{" "}
                                        <strong>Computing</strong>
                                    </WhiteSpan>
                                </Paragraph>
                                <Paragraph>
                                    <WhiteSpan>
                                        <Icon type="heart" /> Like this course
                                    </WhiteSpan>{" "}
                                    <Divider type="vertical" />
                                    &nbsp;
                                    <WhiteSpan>
                                        <Icon type="share-alt" /> Share this course
                                    </WhiteSpan>{" "}
                                    <Divider type="vertical" />
                                    &nbsp;
                                    <strong>320 views</strong>
                                </Paragraph>
                            </Typography>
                        </Col>
                        <Col span={10}>
                            <img src={playing} alt="Playing computer" width="100%" />
                        </Col>
                    </Row>
                </Section>

                <div
                    ref={node => (this.container = node)}
                    style={{ display: "flex", alignItems: "flex-start" }}
                >
                    <Row>
                        <Col span={14}>
                            <Section paddingHorizontal="padded">
                                <Row gutter={32}>
                                    <Col span={24}>
                                        <Typography>
                                            <Title level={3}>Summary</Title>
                                            <Paragraph>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Fusce imperdiet convallis sem, vitae lobortis
                                                nibh suscipit at. Aliquam laoreet, arcu a vehicula
                                                faucibus, justo arcu molestie mi, eu aliquet massa
                                                neque nec leo. Duis gravida est at dui lobortis bib{" "}
                                                <br />
                                                <br /> Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit. Fusce imperdiet convallis sem,
                                                vitae lobortis nibh suscipit at. Aliquam laoreet,
                                                arcu a vehicula faucibus, justo arcu molestie mi, eu
                                                aliquet massa neque nec leo. Duis gravida est at dui
                                            </Paragraph>
                                        </Typography>
                                    </Col>
                                </Row>
                            </Section>

                            <Section paddingHorizontal="padded">
                                <Row>
                                    <Col>
                                        <List data={lessons} />
                                    </Col>
                                </Row>
                            </Section>

                            <Section paddingHorizontal="padded">
                                <Row>
                                    <Col>
                                        <Heading content="Reviews" />
                                        {reviews.map(item => (
                                            <CommentItem
                                                key={item.id}
                                                photoUrl={item.photoUrl}
                                                author={item.author}
                                                content={item.content}
                                                datetime={item.datetime}
                                            />
                                        ))}
                                    </Col>
                                </Row>
                            </Section>
                        </Col>

                        <Col span={8} style={{ height: "160vh" }}>
                            <Section className="floating-container" noPadding>
                                <Row>
                                    <Card
                                        autoHeight
                                        title="$25 per hour"
                                        description="You qualify for a Free Lesson"
                                    >
                                        <Button shape="round" type="primary" block>
                                            Trial
                                        </Button>
                                    </Card>
                                </Row>
                                <Row>
                                    <Card autoHeight title="">
                                        <Heading
                                            icon="safety"
                                            iconSize={20}
                                            image="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                                            content="Ananta Pratama"
                                            subheader="Verified"
                                            marginBottom="2em"
                                        />
                                        <Typography>
                                            <Heading
                                                level={4}
                                                content="Qualification"
                                                subheader="MSc Diploma"
                                            />
                                            <Heading
                                                level={4}
                                                content="Experience"
                                                subheader={
                                                    <ul>
                                                        <li>Maths teacher at Glory School</li>
                                                        <li>Science teacher at Up School</li>
                                                    </ul>
                                                }
                                            />
                                            <Heading
                                                level={4}
                                                content="Specialization"
                                                subheader={
                                                    <ul>
                                                        <li>Primary Science</li>
                                                        <li>Lower Secondary Science</li>
                                                        <li>O Levels Chemistry</li>
                                                    </ul>
                                                }
                                            />
                                        </Typography>
                                        <Section padding="20px 0 0">
                                            <Button shape="round" type="primary" block>
                                                Ask a question
                                            </Button>
                                        </Section>
                                    </Card>
                                </Row>
                            </Section>
                        </Col>
                    </Row>
                </div>

                <Divider />

                <Section paddingHorizontal="padded">
                    <Row gutter={32}>
                        <Col>
                            <Heading content="Students also viewed" level={4} />
                        </Col>
                    </Row>
                    <Row gutter={32}>
                        <Carousel
                            autoplay
                            height="auto"
                            slidesToShow={4}
                            slidesToScroll={1}
                            background="transparent"
                            dots={false}
                        >
                            {courses.map(course => {
                                const { id, ...rest } = course

                                return (
                                    <Col key={course.id} span={6}>
                                        <CourseCard
                                            hoverable
                                            {...rest}
                                            onClick={() => this.props.history.push("/course/1")}
                                        />
                                    </Col>
                                )
                            })}
                        </Carousel>
                    </Row>
                </Section>

                <Section paddingHorizontal="padded">
                    <Row gutter={32}>
                        <Col>
                            <Heading content="More classes by Ananta Pratama" level={4} />
                        </Col>
                    </Row>
                    <Row gutter={32}>
                        <Carousel
                            autoplay
                            height="auto"
                            slidesToShow={4}
                            slidesToScroll={1}
                            background="transparent"
                            dots={false}
                        >
                            {courses.map(course => {
                                const { id, ...rest } = course

                                return (
                                    <Col key={course.id} span={6}>
                                        <CourseCard
                                            hoverable
                                            {...rest}
                                            onClick={() => this.props.history.push("/course/1")}
                                        />
                                    </Col>
                                )
                            })}
                        </Carousel>
                    </Row>
                </Section>
            </Layout>
        )
    }
}

export default withRouter(CourseDetails)
