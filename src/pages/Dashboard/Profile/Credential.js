import React from "react"
import Section from "../../../components/Section"
import UploadInput from "../../../components/forms/UploadInput"
import { Row, Col, Typography } from "antd"
import styled from "styled-components"
import Button from "../../../components/Button"
import Recaptcha from "react-google-recaptcha"
import { siteKey } from "../../../dummy"

const StyledRow = styled(Row)`
    // border-bottom: 1px solid #ddd;
    padding-bottom: 1em;
    margin-bottom: 1em;
`

function Credential({ asyncOnload }) {
    return (
        <Section paddingHorizontal={0}>
            <StyledRow type="flex" align="middle">
                <Col span={8}>
                    <Typography.Text strong>Award Certificates</Typography.Text>
                </Col>
                <Col span={8}>
                    <UploadInput draggable={false} name="award_certificates" />
                </Col>
            </StyledRow>
            <StyledRow type="flex" align="middle">
                <Col span={8}>
                    <Typography.Text strong>Course Completion Certificate</Typography.Text>
                </Col>
                <Col span={8}>
                    <UploadInput draggable={false} name="course_completion" />
                </Col>
            </StyledRow>
            <StyledRow type="flex" align="middle">
                <Col span={8}>
                    <Typography.Text strong>Resume / CV</Typography.Text>
                </Col>
                <Col span={8}>
                    <UploadInput draggable={false} name="resume" />
                </Col>
            </StyledRow>
            <Row>
                <Col>
                    <Recaptcha
                        style={{ marginBottom: "2em" }}
                        sitekey={siteKey}
                        theme="light"
                        asyncScriptOnLoad={asyncOnload}
                    />
                </Col>
            </Row>
            <Row>
                <Col>
                    <Button type="primary" size="default">
                        Submit attachments
                    </Button>
                </Col>
            </Row>
        </Section>
    )
}

export default Credential
