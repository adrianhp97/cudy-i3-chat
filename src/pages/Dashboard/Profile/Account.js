import React from "react"
import Section from "../../../components/Section"
import { Form } from "antd"
import TextInput from "../../../components/forms/TextInput"
import Button from "../../../components/Button"
import { Formik, Field } from "formik"
import styled from "styled-components"
import Recaptcha from "react-google-recaptcha"
import { siteKey } from "../../../dummy"

const StyledInput = styled(Field)`
    .ant-input {
        background: #fff;
    }
`

const formItemLayout = {
    layout: "horizontal",
    labelAlign: "left",
    colon: false,
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
    }
}

function Account({ asyncOnload }) {
    return (
        <Section paddingHorizontal={0} width="70%">
            <Formik
                initialValues={{}}
                render={() => (
                    <Form {...formItemLayout}>
                        <StyledInput
                            name="email"
                            type="email"
                            placeholder="Your email"
                            label="Email"
                            component={TextInput}
                        />
                        <StyledInput
                            name="phone"
                            type="number"
                            placeholder="Your phone number"
                            label="Phone number"
                            component={TextInput}
                        />
                        <StyledInput
                            name="password"
                            type="password"
                            placeholder="Minimum of 6 characters"
                            label="New password"
                            component={TextInput}
                        />
                        <StyledInput
                            name="repeat_password"
                            type="password"
                            placeholder="Confirm your new password"
                            label="Confirm new password"
                            component={TextInput}
                        />
                        <Recaptcha
                            style={{ marginBottom: "2em" }}
                            sitekey={siteKey}
                            theme="light"
                            asyncScriptOnLoad={asyncOnload}
                        />
                        <Form.Item>
                            <Button type="primary" icon="check" size="default">
                                Change details
                            </Button>
                        </Form.Item>
                    </Form>
                )}
            />
        </Section>
    )
}

export default Account
