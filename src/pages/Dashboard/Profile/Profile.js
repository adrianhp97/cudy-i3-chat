import React, { useState } from "react"
import Section from "../../../components/Section"
import { Formik, Field } from "formik"
import TextInput from "../../../components/forms/TextInput"
import Button from "../../../components/Button"
import { Form, Tag, Row, Col, Avatar } from "antd"
import { Input, DatePicker } from "formik-antd"
import styled from "styled-components"
import ProfileCover from "../../Tutor/Profile/ProfileCover"
import { profileInitialValues, gender, race, school } from "../../../dummy"
import SelectInput from "../../../components/forms/SelectInput"
import UploadInput from "../../../components/forms/UploadInput"

const ProfilePicture = styled(Avatar)`
    && {
        width: 70px;
        height: 70px;
        margin: ${({ role }) => (role === "student" ? "initial" : "-60px 0 3em 40px")};
        box-shadow: 0 0 0px 5px rgb(240, 242, 245);
    }
`

const formItemLayout = {
    layout: "horizontal",
    labelAlign: "left",
    colon: false,
    labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
    }
}

const getBaseImage = (image, callback) => {
    const reader = new FileReader()
    reader.addEventListener("load", () => callback(reader.result))
    reader.readAsDataURL(image)
}

function ProfileTab(props) {
    const { handlers, inputValue, states } = props
    const {
        handleChange,
        handleAddSpecializations,
        handleCloseTag,
        handleAddQualifications,
        handleAddExperiences
    } = handlers
    const { experiences, qualifications, specializations, userType } = states
    const [cover, setCover] = useState("")

    const handleChangeCover = info => {
        if (info.file.status === "done") {
            getBaseImage(info.file.originFileObj, imageUrl => setCover(imageUrl))
        }
    }

    const handleChangeDob = (setFieldValue, string) => {
        setFieldValue("dob", string)
        console.log(string)
    }

    return (
        <Section>
            {userType === "tutor" && (
                <>
                    <Row>
                        <Col span={22}>
                            <ProfileCover background={cover} onChangeCover={handleChangeCover} />
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <ProfilePicture
                                size="large"
                                src="https://i2.wp.com/float8ion.com/wp-content/uploads/2015/12/random-user-31.jpg?fit=300%2C300"
                            />
                        </Col>
                    </Row>
                </>
            )}

            {userType === "student" && (
                <Row style={{ marginBottom: "2em" }} type="flex" align="middle">
                    <Col span={4}>
                        <ProfilePicture
                            role="student"
                            size="large"
                            src="https://i2.wp.com/float8ion.com/wp-content/uploads/2015/12/random-user-31.jpg?fit=300%2C300"
                        />
                    </Col>
                    <Col span={5}>
                        <UploadInput
                            draggable={false}
                            name="profile_picture"
                            buttonType="primary"
                            buttonText="Upload new picture"
                        />{" "}
                    </Col>
                    <Col span={6}>
                        <Button size="default" type="ghost">
                            Delete
                        </Button>
                    </Col>
                </Row>

                //    <Row>
                //        <Col>
                //             <Formik
                //                 render={() => (
                //                     <Form>
                //                     </Form>
                //                 )}
                //             />
                //        </Col>
                //    </Row>
            )}

            {userType === "tutor" ? (
                <Formik
                    initialValues={{
                        ...profileInitialValues,
                        specializations,
                        qualifications,
                        experiences
                    }}
                    render={() => (
                        <Form {...formItemLayout}>
                            <Field
                                name="profile_link"
                                label="Profile link"
                                placeholder="Your profile link"
                                component={TextInput}
                            />

                            <Form.Item label="Specializations">
                                <Input
                                    type="text"
                                    name="specializations"
                                    placeholder="Enter your specializations"
                                    onChange={handleChange}
                                    onPressEnter={handleAddSpecializations}
                                    value={inputValue}
                                />
                                {specializations &&
                                    specializations.map(item => (
                                        <span key={item}>
                                            <Tag
                                                color="orange"
                                                closable
                                                onClose={() => handleCloseTag(item)}
                                            >
                                                {item}
                                            </Tag>
                                        </span>
                                    ))}
                            </Form.Item>

                            <Form.Item label="Qualifications">
                                <Input
                                    type="text"
                                    name="qualifications"
                                    placeholder="Enter your qualifications"
                                    onChange={handleChange}
                                    onPressEnter={handleAddQualifications}
                                    value={inputValue}
                                />
                                {qualifications &&
                                    qualifications.map(item => (
                                        <span key={item}>
                                            <Tag
                                                color="orange"
                                                closable
                                                onClose={() => handleCloseTag(item)}
                                            >
                                                {item}
                                            </Tag>
                                        </span>
                                    ))}
                            </Form.Item>

                            <Form.Item label="Experiences">
                                <Input
                                    type="text"
                                    name="experiences"
                                    placeholder="Enter your experiences"
                                    onChange={handleChange}
                                    onPressEnter={handleAddExperiences}
                                    value={inputValue}
                                />
                                {experiences &&
                                    experiences.map(item => (
                                        <span key={item}>
                                            <Tag
                                                color="orange"
                                                closable
                                                onClose={() => handleCloseTag(item)}
                                            >
                                                {item}
                                            </Tag>
                                        </span>
                                    ))}
                            </Form.Item>

                            <Field
                                name="biography"
                                label="Biography"
                                placeholder="Please tell us about yourself..."
                                component={TextInput}
                                textarea
                            />
                            <Button type="primary" icon="check" size="default">
                                Submit
                            </Button>
                        </Form>
                    )}
                />
            ) : (
                <Formik
                    initialValues={{ name: "Riva Yudha" }}
                    render={({ setFieldValue, values }) => (
                        <Form {...formItemLayout}>
                            <Field
                                name="school"
                                label="School name"
                                placeholder="Your school"
                                options={school}
                                component={SelectInput}
                            />
                            <Field
                                name="name"
                                component={TextInput}
                                label="Name"
                                placeholder="Your name..."
                            />
                            {/* <Field
                                name="dob"
                                type="date"
                                label="Date of birth"
                                placeholder="Your date of birth"
                                component={DateInput}
                            /> */}
                            <Form.Item name="dob" label="Date of birth">
                                <DatePicker
                                    onChange={(_, string) => handleChangeDob(setFieldValue, string)}
                                    style={{ display: "block" }}
                                    format={"DD/MM/YYYY"}
                                />
                            </Form.Item>
                            <Field
                                name="gender"
                                label="Gender"
                                placeholder="Your gender..."
                                component={SelectInput}
                                options={gender}
                            />
                            <Field
                                name="race"
                                label="Race"
                                placeholder="Your race..."
                                component={SelectInput}
                                options={race}
                            />
                            <Button type="primary" icon="check" size="default">
                                Submit
                            </Button>
                        </Form>
                    )}
                />
            )}
        </Section>
    )
}

export default ProfileTab
