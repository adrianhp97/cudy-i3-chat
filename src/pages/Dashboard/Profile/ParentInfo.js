import React from "react"
import Section from "../../../components/Section"
import Heading from "../../../components/Heading"
import { Formik, Field } from "formik"
import { Form } from "antd"
import Button from "../../../components/Button"
import TextInput from "../../../components/forms/TextInput"

const formItemLayout = {
    layout: "horizontal",
    labelAlign: "left",
    colon: false,
    labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
    }
}

function ParentInfo() {
    return (
        <Section>
            <Formik
                render={() => (
                    <Form {...formItemLayout}>
                        <Field
                            name="first_name"
                            type="text"
                            label="First name"
                            placeholder="Your parent/guardian first name"
                            component={TextInput}
                        />
                        <Field
                            name="last_name"
                            type="text"
                            label="Last name"
                            placeholder="Your parent/guardian last name"
                            component={TextInput}
                        />
                        <Field
                            name="email"
                            type="email"
                            label="Email"
                            placeholder="Your parent/guardian email"
                            component={TextInput}
                        />
                        <Field
                            name="contact"
                            type="text"
                            label="Contact"
                            placeholder="Your parent/guardian contact number"
                            component={TextInput}
                        />
                        <Button size="default" type="primary" htmlType="submit" icon="check">
                            Submit
                        </Button>
                    </Form>
                )}
            />
        </Section>
    )
}

export default ParentInfo
