import React from "react"
import Section from "../../../components/Section"
import Button from "../../../components/Button"
import { Form } from "antd"
import TextInput from "../../../components/forms/TextInput"
import { bankInfo, siteKey } from "../../../dummy"
import SelectInput from "../../../components/forms/SelectInput"
import { Formik, Field } from "formik"
import Recaptcha from "react-google-recaptcha"

const formItemLayout = {
    layout: "horizontal",
    labelAlign: "left",
    colon: false,
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
    }
}

function BankInfo({ asyncOnload }) {
    return (
        <Section paddingHorizontal={0} width="70%">
            <Formik
                render={() => (
                    <Form {...formItemLayout}>
                        <Field
                            name="account_number"
                            label="Account number"
                            placeholder="Your account number"
                            component={TextInput}
                        />
                        <Field
                            name="bank_name"
                            label="Bank name"
                            options={bankInfo}
                            component={SelectInput}
                        />
                        <Recaptcha
                            style={{ marginBottom: "2em" }}
                            sitekey={siteKey}
                            theme="light"
                            asyncScriptOnLoad={asyncOnload}
                        />
                        <Form.Item>
                            <Button type="primary" size="default">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                )}
            />
        </Section>
    )
}

export default BankInfo
