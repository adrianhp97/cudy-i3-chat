import React, { useState } from "react"
import Section from "../../../components/Section"
import { Row, Col, Tabs } from "antd"
import Tab from "../../../components/Tab"
import Heading from "../../../components/Heading"
import Credential from "./Credential"
import Account from "./Account"
import BankInfo from "./BankInfo"
import ProfileTab from "./Profile"
import ParentInfo from "./ParentInfo"

const userType = "tutor"

function Profile() {
    const asyncOnload = () => console.log("Script loaded")
    const [specializations, setSpecializations] = useState(["Math O Level"])
    const [qualifications, setQualifications] = useState([
        "Bachelor of Arts - Nan Yang Polytechnic"
    ])
    const [experiences, setExperiences] = useState(["SGBC Pte Ltd"])
    const [inputValue, setInputValue] = useState("")

    const handleChange = e => {
        const data = ["specializations", "qualifications", "experiences"]
        if (data.find(item => item === e.target.name)) {
            setInputValue(e.target.value)
        }
    }

    const handleAddSpecializations = () => {
        let specials = [...specializations]
        if (inputValue && specials.indexOf(inputValue)) {
            specials = [...specials, inputValue]
        }
        setSpecializations(specials)
        setInputValue("")
    }

    const handleAddQualifications = () => {
        let quali = [...qualifications]
        if (inputValue && quali.indexOf(inputValue)) {
            quali = [...quali, inputValue]
        }
        setQualifications(quali)
        setInputValue("")
    }

    const handleAddExperiences = () => {
        let exp = [...experiences]
        if (inputValue && exp.indexOf(inputValue)) {
            exp = [...exp, inputValue]
        }
        setExperiences(exp)
        setInputValue("")
    }

    const handleCloseTag = tag => {
        const updated = specializations.filter(item => item !== tag)
        setSpecializations(updated)
    }

    const handlers = {
        handleChange,
        handleAddSpecializations,
        handleCloseTag,
        handleAddQualifications,
        handleAddExperiences
    }

    const states = {
        specializations,
        qualifications,
        experiences,
        userType
    }

    return (
        <Section>
            <Heading content={userType === "tutor" ? "Tutor Profile" : "Student Profile"} />
            <Tab>
                <Tabs.TabPane tab="Profile" key="Profile">
                    <ProfileTab states={states} inputValue={inputValue} handlers={handlers} />
                </Tabs.TabPane>
                {userType === "tutor" && (
                    <Tabs.TabPane tab="Credential Verification" key="Credential Verification">
                        <Credential asyncOnload={asyncOnload} />
                    </Tabs.TabPane>
                )}
                <Tabs.TabPane tab="Account" key="Account">
                    <Account asyncOnload={asyncOnload} />
                </Tabs.TabPane>
                {userType === "tutor" ? (
                    <Tabs.TabPane tab="Bank Info" key="Bank Info">
                        <BankInfo asyncOnload={asyncOnload} />
                    </Tabs.TabPane>
                ) : (
                    <Tabs.TabPane tab="Parent Info" key="Parent Info">
                        <ParentInfo userType={userType} />
                    </Tabs.TabPane>
                )}
            </Tab>
        </Section>
    )
}

export default Profile
