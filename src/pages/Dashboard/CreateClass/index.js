import React, { useState } from "react"
import Section from "../../../components/Section"
import Heading from "../../../components/Heading"
import { Row, Col } from "antd"
import Card from "../../../components/Card"
import student from "../../../assets/images/student/student.svg"
import chat from "../../../assets/images/chat/chat.svg"
import styled from "styled-components"
import StructuredClass from "./StructuredClass"
import ConsultationalClass from "./ConsultationalClass"
import Button from "../../../components/Button"

const StyledCard = styled(Card)`
    && {
        cursor: pointer;
        .ant-card-cover {
            padding: 3em;
            max-height: 300px;
            text-align: center;
            img {
                width: 100%;
                height: 100%;
                object-fit: cover;
                display: inline-block;
            }
        }
        .ant-card-meta-detail {
            margin-bottom: 1em;
        }
    }
`

function CreateClass() {
    const [classType, setClassType] = useState(null)

    return (
        <Section>
            <Heading
                content="Create a new class"
                subheader={
                    classType !== null ? (
                        <span>
                            You are creating a <strong>{classType}</strong> class
                        </span>
                    ) : (
                        "Please choose which type of class do you want to create"
                    )
                }
                marginBottom="3em"
            />
            {classType === null ? (
                <Row gutter={32}>
                    <Col span={8}>
                        <StyledCard
                            autoHeight
                            title="Structured Class"
                            description="This is a structured class and you'll be happy with it back and forth"
                            src={student}
                            onClick={() => setClassType("structured")}
                        />
                    </Col>
                    <Col span={8}>
                        <StyledCard
                            autoHeight
                            title="Consultational Class"
                            description="This is a consultational class and you'll be happy with yourself"
                            src={chat}
                            onClick={() => setClassType("consultational")}
                        />
                    </Col>
                </Row>
            ) : (
                <Row>
                    <Col span={24}>
                        <Button
                            icon="left"
                            type="link"
                            size="default"
                            style={{ marginBottom: "2em", paddingLeft: 0 }}
                            onClick={() => setClassType(null)}
                        >
                            Back
                        </Button>
                        {classType === "structured" ? <StructuredClass /> : <ConsultationalClass />}
                    </Col>
                </Row>
            )}
        </Section>
    )
}

export default CreateClass
