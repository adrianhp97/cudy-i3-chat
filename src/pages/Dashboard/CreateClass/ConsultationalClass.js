import React, { useState } from "react"
import styled from "styled-components"
import { Formik, Field } from "formik"
import { Form, Slider, Popconfirm, Divider, Row, Col } from "antd"
import Section from "../../../components/Section"
import UploadInput from "../../../components/forms/UploadInput"
import SelectInput from "../../../components/forms/SelectInput"
import { curriculum, level, subjects, orderedNumber } from "../../../dummy"
import TextInput from "../../../components/forms/TextInput"
import Calendar from "../../../components/Calendar"
import Button from "../../../components/Button"
import Card from "../../../components/Card"
import Heading from "../../../components/Heading"
import Empty from "../../../components/Empty"
import moment from "moment"

const Cal = styled(Calendar)`
    && {
        .ant-select {
            width: initial;
        }
    }
`

const formItemLayout = {
    layout: "horizontal",
    labelAlign: "left",
    colon: false,
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 }
    }
}

const initialValues = {
    curriculum: curriculum[1].value,
    level: "",
    subject: "",
    student_capacity: 1,
    class_title: "",
    summary: "",
    price: 20,
    price_suggestion: [20, 50],
    lesson_slot: "22/05/2019"
}

function ConsultationalClass() {
    const [formData, setFormData] = useState(initialValues)

    const handlePanelChange = (value, mode) => console.log(value, mode)

    return (
        <Section paddingHorizontal={0}>
            <Row gutter={32}>
                <Col span={16}>
                    <Formik
                        initialValues={formData}
                        render={({ values, handleChange, setFieldValue }) => {
                            const handleForm = e => {
                                let name = e.target.name
                                setFormData({ ...formData, [name]: e.target.value })
                                return handleChange(e)
                            }

                            const handleSelect = (name, value) => {
                                setFormData({ ...formData, [name]: value })
                                setFieldValue(name, value)
                            }

                            return (
                                <Form {...formItemLayout}>
                                    <UploadInput name="file" />
                                    <Field
                                        name="curriculum"
                                        placeholder="Select the curriculum"
                                        label="Curriculum"
                                        component={SelectInput}
                                        options={curriculum}
                                        onChange={val => handleSelect("curriculum", val)}
                                    />
                                    <Field
                                        name="level"
                                        placeholder="Select your level"
                                        label="Level"
                                        component={SelectInput}
                                        options={level}
                                        onChange={val => handleSelect("level", val)}
                                    />
                                    <Field
                                        name="subject"
                                        placeholder="Select the subject"
                                        label="Subject"
                                        component={SelectInput}
                                        options={subjects}
                                        onChange={val => handleSelect("subject", val)}
                                    />
                                    <Field
                                        name="student_capacity"
                                        placeholder="Select how many students your class accept"
                                        label="Student capacity"
                                        component={SelectInput}
                                        options={orderedNumber}
                                        onChange={val => handleSelect("student_capacity", val)}
                                    />
                                    <Field
                                        name="class_title"
                                        placeholder="E.g. Mathematics Lesson for All"
                                        label="Class title"
                                        component={TextInput}
                                        onChange={handleForm}
                                    />
                                    <Field
                                        name="summary"
                                        placeholder="Please tell more about your class..."
                                        label="Summary"
                                        component={TextInput}
                                        onChange={handleForm}
                                        textarea
                                    />
                                    <Form.Item label="Price per hour">
                                        <Slider
                                            name="price"
                                            marks={{ 10: "$10", 100: "$100" }}
                                            tipFormatter={value => `$${value}`}
                                            step={5}
                                            min={10}
                                            max={100}
                                            onChange={value => {
                                                setFormData({ ...formData, price: value })
                                            }}
                                            value={formData.price}
                                        />
                                    </Form.Item>
                                    <Form.Item label="Price suggestion">
                                        <Slider
                                            name="price_suggestion"
                                            disabled
                                            range
                                            marks={{ 0: "$0", 50: "$50" }}
                                            tipFormatter={value => `$${value}`}
                                            step={5}
                                            max={50}
                                            value={formData.price_suggestion}
                                            onChange={value =>
                                                setFormData({
                                                    ...formData,
                                                    price_suggestion: value
                                                })
                                            }
                                        />
                                    </Form.Item>
                                    <Form.Item label="Add lesson slot">
                                        <Cal
                                            name="lesson_slot"
                                            onSelect={date => {
                                                console.log(date)
                                                setFieldValue(
                                                    "lesson_slot",
                                                    moment(date).format("DD/MM/YYYY")
                                                )
                                                setFormData({
                                                    ...formData,
                                                    lesson_slot: moment(date).format("DD/MM/YYYY")
                                                })
                                            }}
                                            fullscreen={false}
                                            onPanelChange={handlePanelChange}
                                            value={moment(values.lesson_slot, "DD/MM/YYYY")}
                                            bg="white"
                                        />
                                    </Form.Item>
                                    <Divider />
                                    <Form.Item
                                        wrapperCol={{ sm: 24 }}
                                        style={{ textAlign: "right", marginTop: "3em" }}
                                    >
                                        <Popconfirm
                                            title="Are you sure want to create this consultational class?"
                                            cancelText="Cancel"
                                            onConfirm={() => ({})}
                                            onCancel={() => ({})}
                                        >
                                            <Button
                                                icon="check"
                                                type="primary"
                                                size="default"
                                                htmlType="submit"
                                            >
                                                Create class...
                                            </Button>
                                        </Popconfirm>
                                    </Form.Item>
                                </Form>
                            )
                        }}
                    />
                </Col>
                <Col span={8} style={{ height: "150vh" }}>
                    <Section noPadding className="floating-container">
                        <Card autoHeight title="Your class summary">
                            {formData.curriculum || formData.level ? (
                                <>
                                    {formData.curriculum && (
                                        <Heading
                                            reverse
                                            content="Curriculum"
                                            level={4}
                                            subheader={formData.curriculum}
                                        />
                                    )}
                                    {formData.level && (
                                        <Heading
                                            reverse
                                            content="Level"
                                            level={4}
                                            subheader={formData.level}
                                        />
                                    )}
                                    {formData.subject && (
                                        <Heading
                                            reverse
                                            content="Subject"
                                            level={4}
                                            subheader={formData.subject}
                                        />
                                    )}
                                    {formData.student_capacity !== 0 && (
                                        <Heading
                                            reverse
                                            content="Student capacity"
                                            level={4}
                                            subheader={formData.student_capacity}
                                        />
                                    )}
                                    {formData.class_title && (
                                        <Heading
                                            reverse
                                            content="Class title"
                                            level={4}
                                            subheader={formData.class_title}
                                        />
                                    )}
                                    {formData.summary && (
                                        <Heading
                                            reverse
                                            content="Summary"
                                            level={4}
                                            subheader={formData.summary}
                                        />
                                    )}
                                    {formData.price && (
                                        <Heading
                                            reverse
                                            content="Price"
                                            level={4}
                                            subheader={formData.price}
                                        />
                                    )}
                                    {formData.lesson_slot && (
                                        <Heading
                                            reverse
                                            content="Lesson slot"
                                            level={4}
                                            subheader={formData.lesson_slot}
                                        />
                                    )}
                                </>
                            ) : (
                                <Empty />
                            )}
                        </Card>
                    </Section>
                </Col>
            </Row>
        </Section>
    )
}

export default ConsultationalClass
