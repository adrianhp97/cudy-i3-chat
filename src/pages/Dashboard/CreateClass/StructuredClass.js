import React, { useState } from "react"
import Section from "../../../components/Section"
import UploadInput from "../../../components/forms/UploadInput"
import { Formik, Field } from "formik"
import {
    Form,
    Divider,
    Icon,
    DatePicker,
    TimePicker,
    Row,
    Col,
    Input,
    Checkbox,
    Typography
} from "antd"
import SelectInput from "../../../components/forms/SelectInput"
import { curriculum, orderedNumber, subjects, level } from "../../../dummy"
import TextInput from "../../../components/forms/TextInput"
import Button from "../../../components/Button"
import Heading from "../../../components/Heading"
import moment from "moment"
import Card from "../../../components/Card"

const formItemLayout = {
    layout: "horizontal",
    labelAlign: "left",
    colon: false,
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 }
    }
}

const initialValues = {
    curriculum: undefined,
    level: undefined,
    subject: undefined,
    student_capacity: 1,
    class_title: "",
    summary: "",
    lessonInitialValues: [
        {
            id: 0,
            date: moment(),
            start_time: moment("12:01", "HH:mm"),
            end_time: moment("01:01", "HH:mm"),
            title: "",
            summary: "",
            include_description: false
        }
    ]
}

function StructuredClass() {
    const { lessonInitialValues } = initialValues

    const [viewLesson, setViewLesson] = useState(false)
    const [lessons, setLessons] = useState(lessonInitialValues)
    const [index, setIndex] = useState(0)
    const [formData, setFormData] = useState(initialValues)

    const handleAddNewLesson = () => {
        setIndex(index => index + 1)
        let newLesson = { ...lessonInitialValues[0], title: "", summary: "", id: index + 1 }
        setLessons([...lessons, newLesson])
    }

    const handleDeleteLesson = id => {
        setIndex(index => index - 1)
        const updatedLessons = lessons.filter(item => item.id !== id)
        setLessons(updatedLessons)
    }

    const handleChangeLesson = (e, id) => {
        let index = lessons.findIndex(item => item.id === id)
        if (index > -1) {
            const name = e.target.name
            let copied = [...lessons]
            copied[index][name] = e.target.value
            setLessons(copied)
        }
    }

    const lessonForm = () =>
        lessons.map((lesson, idx) => {
            const handleChangeTime = (time, name) => {
                let copied = [...lessons]
                copied[idx][name] = moment(time, "HH:mm")
                setLessons(copied)
            }

            return (
                <Section id={lesson.id} key={idx + 1} noPadding marginBottom={0}>
                    <Row gutter={32} type="flex" align="middle" justify="start">
                        <Col>
                            <Heading content={`Lesson ${idx + 1}`} level={4} />
                        </Col>
                        {idx !== 0 && (
                            <Col>
                                <Button
                                    icon="close"
                                    type="link"
                                    size="default"
                                    style={{ marginBottom: "1em" }}
                                    onClick={() => handleDeleteLesson(lesson.id)}
                                >
                                    Delete this lesson
                                </Button>
                            </Col>
                        )}
                    </Row>
                    <Form {...formItemLayout}>
                        <Form.Item label="Date">
                            <DatePicker
                                name="date"
                                format="DD/MM/YYYY"
                                disabledDate={current => current < moment()}
                                defaultValue={lesson.date}
                            />
                        </Form.Item>
                        <Form.Item label="Start time">
                            <TimePicker
                                name="start_time"
                                format="HH:mm"
                                value={lesson.start_time}
                                onChange={time => handleChangeTime(time, "start_time")}
                            />
                        </Form.Item>
                        <Form.Item label="End time">
                            <TimePicker
                                name="end_time"
                                format="HH:mm"
                                value={lesson.end_time}
                                onChange={time => handleChangeTime(time, "end_time")}
                            />
                        </Form.Item>
                        <Form.Item label="Lesson title">
                            <Input
                                name="title"
                                placeholder="Your lesson title"
                                value={lesson.title}
                                onChange={e => handleChangeLesson(e, lesson.id)}
                            />
                        </Form.Item>
                        <Form.Item label="Lesson summary">
                            <Input.TextArea
                                rows={4}
                                name="summary"
                                placeholder="Your lesson summary"
                                value={lesson.summary}
                                onChange={e => handleChangeLesson(e, lesson.id)}
                            />
                        </Form.Item>
                        <Form.Item>
                            <Checkbox
                                name="include_description"
                                checked={lesson.include_description}
                                onChange={() => ({})}
                            >
                                Include description
                            </Checkbox>
                        </Form.Item>
                    </Form>
                    <Divider />
                </Section>
            )
        })

    console.log(lessons)

    return (
        <Section paddingHorizontal={0}>
            <Row gutter={32}>
                <Col span={16}>
                    {viewLesson === false ? (
                        <Formik
                            onSubmit={values => console.log(values)}
                            initialValues={initialValues}
                            render={({ handleSubmit, handleChange, setFieldValue }) => {
                                const handleForm = e => {
                                    let name = e.target.name
                                    setFormData({ ...formData, [name]: e.target.value })
                                    return handleChange(e)
                                }

                                const handleSelect = (name, value) => {
                                    setFormData({ ...formData, [name]: value })
                                    setFieldValue(name, value)
                                }

                                return (
                                    <Form onSubmit={handleSubmit} {...formItemLayout}>
                                        <UploadInput name="file" />
                                        <Field
                                            name="curriculum"
                                            placeholder="Select the curriculum"
                                            label="Curriculum"
                                            component={SelectInput}
                                            options={curriculum.filter(x => x.value !== "All")}
                                            onChange={val => handleSelect("curriculum", val)}
                                        />
                                        <Field
                                            name="level"
                                            placeholder="Select your level"
                                            label="Level"
                                            component={SelectInput}
                                            options={level.filter(item => item.value !== "All")}
                                            onChange={val => handleSelect("level", val)}
                                        />
                                        <Field
                                            name="subject"
                                            placeholder="Select the subject"
                                            label="Subject"
                                            component={SelectInput}
                                            options={subjects.filter(item => item.value !== "All")}
                                            onChange={val => handleSelect("subject", val)}
                                        />
                                        <Field
                                            name="student_capacity"
                                            placeholder="Select how many students your class accept"
                                            label="Student capacity"
                                            component={SelectInput}
                                            options={orderedNumber}
                                            onChange={val => handleSelect("student_capacity", val)}
                                        />
                                        <Field
                                            name="class_title"
                                            placeholder="E.g. Mathematics Lesson for All"
                                            label="Class title"
                                            component={TextInput}
                                            onChange={handleForm}
                                        />
                                        <Field
                                            name="summary"
                                            placeholder="Please tell more about your class..."
                                            label="Summary"
                                            component={TextInput}
                                            onChange={handleForm}
                                            textarea
                                        />
                                        <Divider />
                                        <Form.Item
                                            wrapperCol={{ sm: 24 }}
                                            style={{ textAlign: "right", marginTop: "3em" }}
                                        >
                                            <Button
                                                type="primary"
                                                size="default"
                                                onClick={() => setViewLesson(true)}
                                            >
                                                Next &nbsp; <Icon type="right" />
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                )
                            }}
                        />
                    ) : (
                        <>
                            {lessonForm()}
                            <Button
                                icon="plus"
                                type="dashed"
                                size="default"
                                block
                                onClick={handleAddNewLesson}
                            >
                                Add another lesson
                            </Button>
                        </>
                    )}
                </Col>
                <Col span={8}>
                    <Card autoHeight title="Your class summary">
                        {formData.curriculum && (
                            <Heading reverse content="Curriculum" subheader={formData.curriculum} />
                        )}
                        {formData.level && (
                            <Heading reverse content="Level" level={4} subheader={formData.level} />
                        )}
                        {formData.subject && (
                            <Heading
                                reverse
                                content="Subject"
                                level={4}
                                subheader={formData.subject}
                            />
                        )}
                        {formData.student_capacity !== 0 && (
                            <Heading
                                reverse
                                content="Student capacity"
                                level={4}
                                subheader={formData.student_capacity}
                            />
                        )}
                        {formData.class_title && (
                            <Heading
                                reverse
                                content="Class title"
                                level={4}
                                subheader={formData.class_title}
                            />
                        )}
                        {formData.summary && (
                            <Heading
                                reverse
                                content="Summary"
                                level={4}
                                subheader={formData.summary}
                            />
                        )}
                        {viewLesson && lessons.length && <Divider />}
                        {viewLesson &&
                            lessons.map((item, idx) => (
                                <Section noPadding key={item.id} marginBottom="2em">
                                    <Heading
                                        reverse
                                        marginBottom="10px"
                                        content={`Lesson ${idx + 1}`}
                                    />
                                    <Typography.Paragraph>
                                        <ul>
                                            <li>
                                                <strong>When: </strong>
                                                {moment(item.date).format("dddd, DD MMMM YYYY")}
                                            </li>
                                            <li>
                                                <strong>Title: </strong> {item.title || "-"}
                                            </li>
                                            <li>
                                                <strong>Summary: </strong> {item.summary || "-"}
                                            </li>
                                            <li>
                                                <strong>Start: </strong>{" "}
                                                {moment(item.start_time).format("HH:mm")}
                                            </li>
                                            <li>
                                                <strong>End: </strong>{" "}
                                                {moment(item.end_time).format("HH:mm")}
                                            </li>
                                        </ul>
                                    </Typography.Paragraph>
                                </Section>
                            ))}
                    </Card>
                </Col>
            </Row>
        </Section>
    )
}

export default StructuredClass
