import React, { useState, useEffect } from "react"
import { Row, Col, Icon, Table, Switch, Calendar } from "antd"
import styled from "styled-components"
import Section from "../../components/Section"
import Statistic from "../../components/Statistic"
import Card from "../../components/Card"
import Timeline from "../../components/Timeline"
import { announcements, questions, classSchedule, courses } from "../../dummy"
import { Link } from "react-router-dom"
import List from "../../components/List"
import Heading from "../../components/Heading"
import Carousel from "../../components/Carousel"
import CourseCard from "../../components/CourseCard"

const StyledList = styled(List)`
    && {
        margin-bottom: 2em;
        .ant-list-item {
            box-shadow: none;
            border-radius: 0;
            margin-bottom: 0;
            .ant-list-item-meta-description {
                .ant-typography {
                    margin-bottom: 0.5em;
                }
                .ant-rate {
                    font-size: 16px;
                }
            }
        }
    }
`

const StyledRow = styled(Row)`
    margin-bottom: 1.5em;
`

const StyledCarousel = styled(Carousel)`
    .slick-slide {
        padding-left: 18px;
    }
`

function MainPage() {
    const [calendar, setCalendar] = useState(false)
    const cardHeight = React.createRef()
    const [height, setHeight] = useState(0)

    const sayHeight = () => setHeight(cardHeight.current.scrollHeight)

    useEffect(() => {
        sayHeight()
    })

    return (
        <Section>
            <StyledRow type="flex" justify="space-between" gutter={32}>
                <Col span={8}>
                    <Statistic
                        title="Upcoming class"
                        value={143}
                        link="Manage"
                        to="/dashboard/class/manage"
                    />
                </Col>
                <Col span={8}>
                    <Statistic
                        title="Total students"
                        value={1500}
                        link="Analytics"
                        to="/dashboard/analytics"
                    />
                </Col>
                <Col span={8}>
                    <Statistic
                        title="Available balance"
                        value="$2500"
                        link="Payout"
                        to="/dashboard/payout"
                    />
                </Col>
            </StyledRow>
            <StyledRow>
                <Col>
                    <Card autoHeight title="Upcoming lessons">
                        <StyledCarousel
                            background="transparent"
                            slidesToShow={4}
                            slidesToScroll={4}
                        >
                            {courses.map(item => (
                                <CourseCard {...item} key={item.id} />
                            ))}
                        </StyledCarousel>
                    </Card>
                </Col>
            </StyledRow>
            <StyledRow gutter={32}>
                <Col span={12}>
                    <Card style={{ height: height - 21 }} title="Announcements">
                        <Timeline data={announcements} />
                        <Section noPadding centered>
                            <Link to="/dashboard">
                                More announcements <Icon type="right" />
                            </Link>
                        </Section>
                    </Card>
                </Col>
                <Col span={12}>
                    <div ref={cardHeight}>
                        <Card autoHeight title="New questions" style={{ marginBottom: "2.5em" }}>
                            <StyledList
                                type="review"
                                itemLayout="horizontal"
                                data={questions}
                                noHover
                            />
                            <Section noPadding centered>
                                <Link to="/dashboard">
                                    More questions <Icon type="right" />
                                </Link>
                            </Section>
                        </Card>
                        <Card title="Today class" autoHeight>
                            <Heading
                                level={4}
                                content="A Level Math Class"
                                subheader="10:00 AM - 03:00 PM"
                            />
                            <Heading
                                level={4}
                                content="A Level Science Class"
                                subheader="02:00 PM - 04:00 PM"
                            />
                        </Card>
                    </div>
                </Col>
            </StyledRow>
            <StyledRow>
                <Col>
                    <Card title="Class schedule" autoHeight>
                        <Row style={{ marginBottom: "2em" }}>
                            <Col>
                                <Switch
                                    checked={calendar}
                                    onChange={() => setCalendar(!calendar)}
                                />{" "}
                                &nbsp; Calendar
                            </Col>
                        </Row>
                        {!calendar ? (
                            <Table
                                dataSource={classSchedule.data}
                                columns={classSchedule.columns}
                            />
                        ) : (
                            <Calendar onPanelChange={(value, mode) => console.log(value, mode)} />
                        )}
                        <Section noPadding centered>
                            <Link to="/dashboard">
                                Class history <Icon type="right" />
                            </Link>
                        </Section>
                    </Card>
                </Col>
            </StyledRow>
        </Section>
    )
}

export default MainPage
