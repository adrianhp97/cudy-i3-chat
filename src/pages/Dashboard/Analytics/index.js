import React from "react"
import Section from "../../../components/Section"
import { Row, Col, Tabs } from "antd"
import Statistic from "../../../components/Statistic"
import Heading from "../../../components/Heading"
import styled from "styled-components"
import * as Chart from "recharts"
import Tab from "../../../components/Tab"

const { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } = Chart

const Column = styled(Col)`
    width: 20%;
`

const data = [
    { name: "Jan", amount: 10, pv: 2400, uv: 2400 },
    { name: "Feb", amount: 20, pv: 1398, uv: 2210 },
    { name: "Mar", amount: 20, pv: 9800, uv: 2290 },
    { name: "Apr", amount: 35, pv: 3908, uv: 2000 },
    { name: "May", amount: 45, pv: 4800, uv: 2181 },
    { name: "Jun", amount: 65, pv: 3800, uv: 2500 },
    { name: "Jul", amount: 40, pv: 4300, uv: 2100 },
    { name: "Aug", amount: 50, pv: 1700, uv: 2100 },
    { name: "Sep", amount: 100, pv: 1290, uv: 2100 },
    { name: "Oct", amount: 25, pv: 1447, uv: 2100 },
    { name: "Nov", amount: 70, pv: 4300, uv: 2100 },
    { name: "Dec", amount: 80, pv: 1887, uv: 2100 }
]

function Analytics() {
    return (
        <Section>
            <Heading content="Analytics" level={4} marginBottom="2em" />
            <Row gutter={32} type="flex" justify="space-between" style={{ marginBottom: "2em" }}>
                <Column>
                    <Statistic title="Current balance" value={400} prefix="$" />
                </Column>
                <Column>
                    <Statistic title="Potential income" value={120} prefix="$" />
                </Column>
                <Column>
                    <Statistic title="Pay to date" value={360} prefix="$" />
                </Column>
                <Column>
                    <Statistic title="Trial conversion" value={40} suffix="%" />
                </Column>
                <Column>
                    <Statistic title="Total reach" value={360} />
                </Column>
            </Row>
            <Row>
                <Col span={24}>
                    <Tab>
                        <Tabs.TabPane key="Income" tab="Income">
                            <ResponsiveContainer width={1200} height={300}>
                                <AreaChart margin={{ top: 40 }} data={data}>
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <XAxis dataKey="name" />
                                    <YAxis
                                        label={{
                                            value: "In dollar ($)",
                                            angle: "-90",
                                            position: "insideLeft"
                                        }}
                                    />
                                    <Tooltip />
                                    <Area
                                        type="monotone"
                                        dataKey="amount"
                                        stroke="#fed501"
                                        fill="#FF9D00"
                                    />
                                </AreaChart>
                            </ResponsiveContainer>
                        </Tabs.TabPane>
                        <Tabs.TabPane key="Paying students" tab="Paying students">
                            <ResponsiveContainer width={1200} height={300}>
                                <AreaChart margin={{ top: 40 }} data={data}>
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <XAxis dataKey="name" />
                                    <YAxis
                                        label={{
                                            value: "In dollar ($)",
                                            angle: "-90",
                                            position: "insideLeft"
                                        }}
                                    />
                                    <Tooltip />
                                    <Area
                                        type="monotone"
                                        dataKey="amount"
                                        stroke="#fed501"
                                        fill="#FF9D00"
                                    />
                                </AreaChart>
                            </ResponsiveContainer>
                        </Tabs.TabPane>
                        <Tabs.TabPane key="Reach" tab="Reach">
                            <ResponsiveContainer width={1200} height={300}>
                                <AreaChart margin={{ top: 40 }} data={data}>
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <XAxis dataKey="name" />
                                    <YAxis
                                        label={{
                                            value: "In dollar ($)",
                                            angle: "-90",
                                            position: "insideLeft"
                                        }}
                                    />
                                    <Tooltip />
                                    <Area
                                        type="monotone"
                                        dataKey="amount"
                                        stroke="#fed501"
                                        fill="#FF9D00"
                                    />
                                </AreaChart>
                            </ResponsiveContainer>
                        </Tabs.TabPane>
                    </Tab>
                </Col>
            </Row>
        </Section>
    )
}

export default Analytics
