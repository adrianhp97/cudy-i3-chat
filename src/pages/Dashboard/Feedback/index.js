import React from "react"
import Section from "../../../components/Section"
import Heading from "../../../components/Heading"
import { Row, Col, Form, message } from "antd"
import Card from "../../../components/Card"
import { Formik, Field } from "formik"
import TextInput from "../../../components/forms/TextInput"
import Button from "../../../components/Button"
import SelectInput from "../../../components/forms/SelectInput"
import { feedbackQuery } from "../../../dummy"
import UploadInput from "../../../components/forms/UploadInput"
import { withRouter } from "react-router"

function Feedback(props) {
    const handleSubmit = values => {
        console.log(values)
        setTimeout(() => {
            props.history.push("/dashboard")
            message.success("Thanks for your time! We'll get back to you real shortly. :)")
        }, 2000)
    }

    return (
        <Section>
            <Row>
                <Col span={12}>
                    <Heading
                        content="Feedback"
                        subheader="Head over to our FAQs for immediate answers to your queries. If you are unable to find the information that you need, please fill in the form below and we'll get back to you as soon as we can!"
                    />
                </Col>
            </Row>
            <Row>
                <Col span={16}>
                    <Card autoHeight title="">
                        <Formik
                            onSubmit={values => handleSubmit(values)}
                            validate={validate}
                            initialValues={{ subject: "" }}
                            render={({ handleSubmit, isSubmitting }) => (
                                <Form onSubmit={handleSubmit} layout="vertical">
                                    <Field
                                        type="text"
                                        name="subject"
                                        label="Subject"
                                        placeholder="Subject of feedback"
                                        component={TextInput}
                                        background="#f3f3f3"
                                    />
                                    <Field
                                        name="query"
                                        label="Your query"
                                        placeholder="Choose your query"
                                        component={SelectInput}
                                        options={feedbackQuery}
                                        background="#f3f3f3"
                                    />
                                    <Field
                                        name="feedback"
                                        label="Feedback"
                                        placeholder="Your feedback"
                                        component={TextInput}
                                        background="#f3f3f3"
                                        textarea
                                    />
                                    <UploadInput draggable={false} name="file" /> <br />
                                    <Button
                                        size="default"
                                        htmlType="submit"
                                        type="primary"
                                        icon="check"
                                        disabled={isSubmitting}
                                    >
                                        Submit
                                    </Button>
                                </Form>
                            )}
                        />
                    </Card>
                </Col>
            </Row>
        </Section>
    )
}

const validate = values => {
    let errors = {}

    if (!values.subject) errors.subject = "This is required"
    if (!values.query) errors.query = "This is required"
    if (!values.feedback) errors.feedback = "This is required"

    return errors
}

export default withRouter(Feedback)
