import React, { useState } from "react"
import styled from "styled-components"
import Section from "../../../components/Section"
import Heading from "../../../components/Heading"
import List from "../../../components/common/ReviewList"
import { reviewsDashboard, sortBy } from "../../../dummy"
import { Row, Col, Typography, Rate, Tabs } from "antd"
import { Formik } from "formik"
import { Select, Form } from "formik-antd"
import Tab from "../../../components/Tab"

const StyledList = styled(List)`
    .ant-list-item {
        box-shadow: none;
        .ant-list-item-meta-description {
            .ant-typography {
                margin-bottom: 0.5em;
            }
            .ant-rate {
                font-size: 16px;
            }
        }
    }
`

const Rating = styled(Rate)`
    && {
        font-size: 16px;
    }
`

const ratingOptions = [
    { key: "all", value: "all", label: "All ratings" },
    {
        key: 1,
        value: 1,
        label: (
            <span>
                <Rating disabled defaultValue={1} /> (39)
            </span>
        )
    },
    {
        key: 2,
        value: 2,
        label: (
            <span>
                <Rating disabled defaultValue={2} /> (11)
            </span>
        )
    },
    {
        key: 3,
        value: 3,
        label: (
            <span>
                <Rating disabled defaultValue={3} /> (8)
            </span>
        )
    },
    {
        key: 4,
        value: 4,
        label: (
            <span>
                <Rating disabled defaultValue={4} /> (27)
            </span>
        )
    },
    {
        key: 5,
        value: 5,
        label: (
            <span>
                <Rating disabled defaultValue={5} /> (10)
            </span>
        )
    }
]

function Reviews() {
    const [edit, setEdit] = useState(false)
    const [item, setItem] = useState(-1)
    const [activeTab, setActiveTab] = useState("review-gained")

    const userType = "tutor"

    const listProps = {
        edit,
        userType,
        activeTab,
        itemId: item
    }

    return (
        <Section>
            <Heading content="View Reviews" level={2} />

            <Tab onChange={active => setActiveTab(active)}>
                <Tabs.TabPane tab="Review gained" key="review-gained">
                    <Section centered>
                        {userType === "tutor" && (
                            <Row>
                                <Col>
                                    <Typography.Title level={3}>
                                        Average: 4.5 / 5 stars &middot;{" "}
                                        <Typography.Text type="secondary">
                                            30 reviews
                                        </Typography.Text>
                                    </Typography.Title>
                                </Col>
                            </Row>
                        )}
                    </Section>

                    <Row gutter={32}>
                        <Col span={7}>
                            <Formik
                                render={() => (
                                    <Form.Item>
                                        <span>
                                            Sort by &nbsp;{" "}
                                            <Select
                                                name="sort"
                                                placeholder="Sort reviews"
                                                defaultValue={sortBy[0].value}
                                                style={{ width: 200 }}
                                            >
                                                {Select.renderOptions(sortBy)}
                                            </Select>
                                        </span>
                                    </Form.Item>
                                )}
                            />
                        </Col>
                        <Col span={8}>
                            <Formik
                                render={() => (
                                    <Form.Item>
                                        <span>
                                            Filter rating &nbsp;{" "}
                                            <Select
                                                name="rating"
                                                placeholder="Filter rating"
                                                defaultValue={sortBy[0].value}
                                                style={{ width: 200 }}
                                            >
                                                {Select.renderOptions(ratingOptions)}
                                            </Select>
                                        </span>
                                    </Form.Item>
                                )}
                            />
                        </Col>
                    </Row>
                    <StyledList
                        itemLayout="horizontal"
                        data={reviewsDashboard}
                        onCloseEdit={() => setItem(null)}
                        onShowTextInput={id => setItem(id)}
                        {...listProps}
                    />
                </Tabs.TabPane>

                <Tabs.TabPane tab="Not reviewed" key="not-reviewed">
                    <StyledList
                        itemLayout="horizontal"
                        data={reviewsDashboard}
                        onCloseEdit={() => setItem(null)}
                        onShowTextInput={id => setItem(id)}
                        {...listProps}
                    />
                </Tabs.TabPane>
            </Tab>
        </Section>
    )
}

export default Reviews
