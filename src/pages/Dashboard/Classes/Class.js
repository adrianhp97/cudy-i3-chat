import React, { useState } from "react"
import { Row, Col, Select } from "antd"
import Table from "antd/lib/table"
import Modal from "../../../components/Modal"
import ClassesCard from "./Card"
import Calendar from "../../../components/Calendar"
import Section from "../../../components/Section"


function Class({ data: { data, columns }, userType }) {
    const [modal, setModal] = useState(false)
    const [key, setKey] = useState(-1)

    const handleModal = (key) => {
        setModal(modal => !modal)
        setKey(key)
    }

    return (
        <>
            <Modal visible={modal} onCancel={() => setModal(false)} onOk={() => setModal(false)}>
                <Section marginBottom={0} noPadding>
                    <Calendar fullscreen={false} />
                </Section>
            </Modal>
            <Row style={{ marginBottom: "1.5em" }}>
                <Col>
                    <span>
                        Filter by &nbsp;
                        <Select defaultValue={["All"]} style={{ width: 180 }}>
                            {["All", "Completed", "Not completed"].map(item => (
                                <Select.Option key={item} value={item}>
                                    {item}
                                </Select.Option>
                            ))}
                        </Select>
                    </span>
                </Col>
            </Row>
            {userType === "tutor" ? (
                <Row gutter={32}>
                    {data.map(item => (
                        <Col key={item.id} span={6}>
                            <ClassesCard onHandleModal={handleModal} {...item} />
                        </Col>
                    ))}
                </Row>
            ) : (
                <Row>
                    <Table dataSource={data} columns={columns} />
                </Row>
            )}
        </>
    )
}

export default Class
