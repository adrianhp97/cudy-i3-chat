import React from "react"
import Section from "../../../components/Section"
import Heading from "../../../components/Heading"
import Tab from "../../../components/Tab"
import { Tabs } from "antd"
import { upcomingClasses, structuredClasses } from "../../../dummy"
import Class from "./Class"

const userType = "tutor"

function Classes() {
    return (
        <Section>
            <Heading content="Manage Classes" />
            {userType === "student" ? (
                <Tab>
                    <Tabs.TabPane tab="Upcoming classes" key="upcoming">
                        <Class userType={userType} type="upcoming" data={upcomingClasses} />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Past classes" key="past">
                        <Class userType={userType} type="past" data={upcomingClasses} />
                    </Tabs.TabPane>
                </Tab>
            ) : (
                <Tab>
                    <Tabs.TabPane tab="Structured classes" key="structured">
                        <Class userType={userType} type="structured" data={structuredClasses} />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Consultational classes" key="consultational">
                        <Class userType={userType} type="consultational" data={structuredClasses} />
                    </Tabs.TabPane>
                </Tab>
            )}
        </Section>
    )
}

export default Classes
