import React from "react"
import Card from "../../../components/Card"
import Button from "../../../components/Button"
import { Icon, Row, Col } from "antd"
import styled from "styled-components"
import Rating from "../../../components/Rating"
import Avatar from "../../../components/Avatar"

const StyledCard = styled(Card)`
    && {
        height: 470px;
        .ant-card-meta-detail {
            margin-bottom: 0;
        }
        .ant-card-cover {
            overflow: hidden;
            height: 200px;
            max-height: unset;
            > img {
                height: 100%;
                object-fit: cover;
            }
        }
        .ant-rate {
            margin-bottom: 0.5em;
            + p {
                color: #ff9d00;
            }
        }
    }
`

const CardContent = styled.div`
    margin-bottom: 1.5em;
`

function ClassesCard(props) {
    return (
        <StyledCard {...props} title={props.subject} src="https://source.unsplash.com/random/">
            <CardContent>
                <Row style={{ marginBottom: "1em" }}>
                    <Col>
                        <Rating size="small" color="primary" disabled defaultValue={props.rating} />
                        <p>${props.price} / hour</p>
                        <Icon type="user" /> {props.classType} <br />
                        <Icon type="team" /> Up to 4 person
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Avatar src="https://randomuser.me/api/portraits/women/46.jpg" /> &nbsp;
                        Rusmanto Akmal
                    </Col>
                </Row>
            </CardContent>
            <Button
                type="primary"
                size="default"
                icon="retweet"
                onClick={() => props.onHandleModal && props.onHandleModal(Number(props.id))}
            >
                Renew class
            </Button>
        </StyledCard>
    )
}

export default ClassesCard
