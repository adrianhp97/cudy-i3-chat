import React from "react"
import Section from "../../../components/Section"
import Heading from "../../../components/Heading"
import { Row, Col, Typography, Icon, Table } from "antd"
import Button from "../../../components/Button"
import { referral } from "../../../dummy"

function Referral() {
    return (
        <Section>
            <Heading content="Referral" />
            <Row>
                <Col>
                    <Section centered>
                        <Heading content="Share your link" level={4} />
                        <Section
                            centered
                            bg="#e9e9e9"
                            width="auto"
                            style={{ display: "inline-block" }}
                        >
                            Your link is{" "}
                            <Typography.Text type="warning" strong copyable>
                                http://www.cudy.com/wefuihwe89
                            </Typography.Text>
                        </Section>

                        <Section>
                            <Button type="default">
                                <Icon type="message" theme="filled" /> Whatsapp
                            </Button>{" "}
                            &nbsp;
                            <Button type="default">
                                <Icon type="facebook" theme="filled" /> Facebook
                            </Button>{" "}
                            &nbsp;
                            <Button type="default" icon="twitter">
                                Twitter
                            </Button>{" "}
                            &nbsp;
                            <Button type="default" icon="mail">
                                Email
                            </Button>{" "}
                            &nbsp;
                        </Section>
                    </Section>
                </Col>
            </Row>

            <Row>
                <Col>
                    <Section centered>
                        <Heading content="Referral History" />
                        <Table dataSource={referral.data} columns={referral.columns} />
                    </Section>
                </Col>
            </Row>

            <Row type="flex" justify="center">
                <Col span={16}>
                    <Section centered bg="#fff">
                        <Typography.Paragraph strong>Terms & Conditions</Typography.Paragraph>
                        <Typography.Paragraph ellipsis={{ rows: 3, expandable: true }}>
                            There are many variations of passages of Lorem Ipsum available, but the
                            majority have suffered alteration in some form, by injected humour, or
                            randomised words which don't look even slightly believable. If you are
                            going to use a passage of Lorem Ipsum, you need to be sure there isn't
                            anything embarrassing hidden in the middle of text. All the Lorem Ipsum
                            generators on the Internet tend to repeat predefined chunks as
                            necessary, making this the first true generator on the Internet. It uses
                            a dictionary of over 200 Latin words, combined with a handful of model
                            sentence structures, to generate Lorem Ipsum which looks reasonable. The
                            generated Lorem Ipsum is therefore always free from repetition, injected
                            humour, or non-characteristic words etc.
                        </Typography.Paragraph>
                    </Section>
                </Col>
            </Row>
        </Section>
    )
}

export default Referral
