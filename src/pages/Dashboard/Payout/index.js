import React, { useState, Suspense } from "react"
import Section from "../../../components/Section"
import Heading from "../../../components/Heading"
import Tab from "../../../components/Tab"
import { Tabs, message } from "antd"

const RequestPayout = React.lazy(() => import("./RequestPayout"))
const PayoutHistory = React.lazy(() => import("./PayoutHistory"))

function Payout() {
    const [modalRequest, setModalRequest] = useState(false)

    const handleToggleModal = () => setModalRequest(modalRequest => !modalRequest)
    const handleSendOtp = object => {
        console.log(object)
        setTimeout(() => {
            handleToggleModal()
            message.success("The OTP code has been sent. Please check your phone.")
        }, 2000)
    }

    return (
        <Section>
            <Heading content="Payout" />
            <Tab>
                <Tabs.TabPane tab="Request Payout" key="Request Payout">
                    <Suspense fallback="Loading...">
                        <RequestPayout
                            modalRequest={modalRequest}
                            onSendOtp={handleSendOtp}
                            onToggleModal={handleToggleModal}
                        />
                    </Suspense>
                </Tabs.TabPane>
                <Tabs.TabPane tab="Payout History" key="Payout History">
                    <Suspense fallback="Loading...">
                        <PayoutHistory />
                    </Suspense>
                </Tabs.TabPane>
            </Tab>
        </Section>
    )
}

export default Payout
