import React from "react"
import { Row, Col, Select, DatePicker, Table } from "antd"
import { payout } from "../../../dummy"
import styled from "styled-components"
import moment from "moment"
import Button from "../../../components/Button"
import Section from "../../../components/Section"

const Dropdown = styled(Select)`
    width: 140px;
`

const RangePicker = styled(DatePicker.RangePicker)`
    width: inherit;
`

function PayoutHistory() {
    const handleChange = (value) => console.log(value)

    return (
        <Section paddingHorizontal={0}>
            <Row type="flex" justify="space-between">
                <Col>
                    <Dropdown defaultValue={["All"]} onChange={handleChange}>
                        {payout.status.map(item => (
                            <Select.Option key={item.key}>{item.label}</Select.Option>
                        ))}
                    </Dropdown>{" "}
                    &nbsp;
                    <RangePicker
                        defaultValue={[moment(), moment().add("1 day")]}
                        format={"DD/MM/YYYY"}
                    />
                </Col>
                <Col>
                    <Button icon="download" type="primary" size="default">
                        Download as CSV
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Section paddingHorizontal={0}>
                        <Table dataSource={payout.data} columns={payout.columns} />
                    </Section>
                </Col>
            </Row>
        </Section>
    )
}

export default PayoutHistory
