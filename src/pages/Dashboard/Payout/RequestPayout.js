import React from "react"
import styled from "styled-components"
import Section from "../../../components/Section"
import { Col, Row, Table, Select, Form, Typography, Icon } from "antd"
import Statistic from "../../../components/Statistic"
import { requestPayout } from "../../../dummy"
import Button from "../../../components/Button"
import Modal from "../../../components/Modal"
import { Formik, Field } from "formik"
import TextInput from "../../../components/forms/TextInput"
import Heading from "../../../components/Heading"

const Dropdown = styled(Select)`
    && {
        width: 140px;
        margin-right: 1em;
    }
`

function RequestPayout(props) {
    const { modalRequest, onToggleModal, onSendOtp } = props

    return (
        <Section paddingHorizontal={0}>
            <Modal
                visible={modalRequest}
                onCancel={onToggleModal}
                onOk={onSendOtp}
                okText={
                    <span>
                        <Icon type="check" /> Send me the code
                    </span>
                }
                centered
            >
                <Section centered>
                    <Heading
                        content="Please key in your phone number for the one-time payment setup"
                        level={4}
                        marginBottom="2em"
                    />
                    <Formik
                        onSubmit={values => console.log(values)}
                        initialValues={{ phone: "" }}
                        render={({ handleSubmit }) => (
                            <Form onSubmit={handleSubmit}>
                                <Field
                                    name="phone"
                                    placeholder="Your phone number"
                                    component={TextInput}
                                    background="#f3f3f3"
                                />
                            </Form>
                        )}
                    />
                    <Typography.Text type="secondary">
                        Please include your country code
                    </Typography.Text>
                </Section>
            </Modal>
            <Row gutter={32} type="flex" justify="space-between" style={{ marginBottom: "2em" }}>
                <Col span={8}>
                    <Statistic title="Pending Balance" value={210} prefix="$" />
                </Col>
                <Col span={8}>
                    <Statistic title="Available Balance" value={500} prefix="$" />
                </Col>
                <Col span={8}>
                    <Statistic title="Withdrawn Balance" value={500} prefix="$" />
                </Col>
            </Row>
            <Row>
                <Col>
                    <Row type="flex" justify="space-between">
                        <Col>
                            <Dropdown defaultValue={["All"]}>
                                {requestPayout.status.map(item => (
                                    <Select.Option key={item.key}>{item.label}</Select.Option>
                                ))}
                            </Dropdown>
                            <Dropdown defaultValue={["All"]}>
                                {requestPayout.type.map(item => (
                                    <Select.Option key={item.key}>{item.label}</Select.Option>
                                ))}
                            </Dropdown>
                            <Dropdown defaultValue={["All"]}>
                                {requestPayout.payment.map(item => (
                                    <Select.Option key={item.key}>{item.label}</Select.Option>
                                ))}
                            </Dropdown>
                        </Col>
                        <Col>
                            <Button
                                type="primary"
                                icon="dollar"
                                size="default"
                                onClick={onToggleModal}
                            >
                                Request payout
                            </Button>
                        </Col>
                    </Row>
                    <Section paddingHorizontal={0}>
                        <Table dataSource={requestPayout.data} columns={requestPayout.columns} />
                    </Section>
                </Col>
            </Row>
        </Section>
    )
}

export default RequestPayout
