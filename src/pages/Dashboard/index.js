import React from "react"
import { Switch, Route, Redirect } from "react-router-dom"
import MainPage from "./MainPage"
import Profile from "./Profile"
import Layout from "../../components/Layout"
import Analytics from "./Analytics"
import Resources from "./Resources"
import Reviews from "./Reviews"
import Classes from "./Classes"
import Payout from "./Payout"
import Referral from "./Referral"
import Settings from "./Settings"
import Feedback from "./Feedback"
import CreateClass from "./CreateClass"
import CloudStorage from "./CloudStorage"
import SocialMedia from "./SocialMedia"

function Dashboard() {
    return (
        <Layout sidebar>
            <Switch>
                <Redirect exact from="/dashboard" to="/dashboard/main" />
                <Route path="/dashboard/main" component={MainPage} />
                <Route path="/dashboard/profile" component={Profile} />
                <Route path="/dashboard/analytics" component={Analytics} />
                <Route path="/dashboard/resources" component={Resources} />
                <Route path="/dashboard/reviews" component={Reviews} />
                <Route path="/dashboard/classes/manage" component={Classes} />
                <Route path="/dashboard/classes/create" component={CreateClass} />
                <Route path="/dashboard/payout" component={Payout} />
                <Route path="/dashboard/feedback" component={Feedback} />
                <Route path="/dashboard/referral" component={Referral} />
                <Route path="/dashboard/settings" component={Settings} />
                <Route path="/dashboard/storage" component={CloudStorage} />
                <Route path="/dashboard/social_media" component={SocialMedia} />
            </Switch>
        </Layout>
    )
}

export default Dashboard
