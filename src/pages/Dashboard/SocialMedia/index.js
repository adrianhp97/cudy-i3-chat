import React from "react"
import Section from "../../../components/Section"
import Heading from "../../../components/Heading"
import { Row, Col, Divider, Typography } from "antd"
import Card from "../../../components/Card"
import styled from "styled-components"
import Avatar from "../../../components/Avatar"
import { winners } from "../../../dummy"

const Image = styled.img`
    width: 100%;
    height: 100%;
    max-height: 180px;
    object-fit: cover;
`

const Ava = styled(Avatar)`
    && {
        width: 150px;
        height: 150px;
        margin-bottom: 1em;
    }
`

const { Paragraph, Text } = Typography

function SocialMedia() {
    return (
        <Section>
            <Heading content="Social Media Rewards" />
            <Row style={{ marginBottom: "3em" }}>
                <Col>
                    <Row>
                        <Col span={6}>
                            <Image src="http://source.unsplash.com/user/anotherlovely" />
                        </Col>
                        <Col span={6}>
                            <Image src="http://source.unsplash.com/user/erondu" />
                        </Col>
                        <Col span={6}>
                            <Image src="http://source.unsplash.com/user/gkt_k_" />
                        </Col>
                        <Col span={6}>
                            <Image src="http://source.unsplash.com/user/alicealinari" />
                        </Col>
                    </Row>
                    <Row>
                        <Col span={6}>
                            <Image src="https://source.unsplash.com/user/erondu" />
                        </Col>
                        <Col span={6}>
                            <Image src="https://source.unsplash.com/user/joshua_humphrey" />
                        </Col>
                        <Col span={6}>
                            <Image src="https://source.unsplash.com/user/brunovdkraan" />
                        </Col>
                        <Col span={6}>
                            <Image src="https://source.unsplash.com/user/anniespratt" />
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row gutter={32}>
                <Col span={10}>
                    <Section centered>
                        <Heading content="🔥 Weekly winner" level={4} marginBottom="2em" />
                        <Ava
                            src="https://randomuser.me/api/portraits/women/95.jpg"
                            size="large"
                            shape="circle"
                        />
                        <Paragraph strong>
                            Jenny Shalimar <br />{" "}
                            <Text type="secondary">Second week of May 2019</Text>
                        </Paragraph>
                    </Section>
                </Col>
                <Col span={14}>
                    <Section centered>
                        <Heading content="🔥 Past winners" level={4} marginBottom="2em" />
                        <Row gutter={32} type="flex" justify="center">
                            {winners.map(item => (
                                <Col span={5}>
                                    <Card autoHeight style={{ minWidth: 100 }}>
                                        <Avatar
                                            style={{ marginBottom: "1em" }}
                                            src={item.src}
                                            size="large"
                                            shape="circle"
                                        />{" "}
                                        <br />
                                        <p style={{ marginBottom: 0 }}>{item.name}</p>
                                    </Card>
                                </Col>
                            ))}
                        </Row>
                    </Section>
                </Col>
            </Row>
            <Divider />
            <Row>
                <Col>
                    <Heading content="Want to contribute?" level={4} />
                    {/* <Row type="flex" justify="center"> */}
                    {/* <Col span={16}> */}
                    <Section bg="#fff">
                        <Row gutter={32}>
                            <Col span={8}>
                                <Paragraph strong>Instructions</Paragraph>
                                <Paragraph type="secondary" style={{ fontSize: 12 }}>
                                    <ol>
                                        <li>Launch Instagram App</li>
                                        <li>Take a photo about Cudy</li>
                                        <li>
                                            Upload your photo on Instagram (follow the posting
                                            instructions below)
                                        </li>
                                        <li>
                                            Write your best caption (ensure it is relevant and
                                            exciting)
                                        </li>
                                    </ol>
                                </Paragraph>
                            </Col>
                            <Col span={8}>
                                <Paragraph strong>Terms</Paragraph>
                                <Paragraph type="secondary" style={{ fontSize: 12 }}>
                                    <ol>
                                        <li>
                                            Do not feature any other brand names or logos in the
                                            photo other than Cudy
                                        </li>
                                        <li>
                                            Do not feature other users in the photo on Cudy without
                                            their consent
                                        </li>
                                        <li>
                                            Photos should be natural and match your overall feed or
                                            mood
                                        </li>
                                        <li>Make sure that Cudy is clearly seen in the photo</li>
                                        <li>
                                            There are no themes to follow. Be your creative self!
                                        </li>
                                    </ol>
                                </Paragraph>
                            </Col>
                            <Col span={8}>
                                <Paragraph strong>
                                    Tag <br />{" "}
                                    <Text type="secondary" copyable>
                                        @cudy-sg
                                    </Text>
                                </Paragraph>
                                <Paragraph strong>
                                    Make sure you use hashtags <br />{" "}
                                    <Text type="secondary" copyable>
                                        #cudysg
                                    </Text>{" "}
                                    &nbsp;
                                    <Text copyable type="secondary">
                                        #singapore
                                    </Text>
                                </Paragraph>
                            </Col>
                        </Row>
                    </Section>
                    {/* </Col> */}
                    {/* </Row> */}
                </Col>
            </Row>
        </Section>
    )
}

export default SocialMedia
