import React, { useState } from "react"
import Section from "../../../components/Section"
import { Row, Col, List, Icon, message } from "antd"
import Statistic from "../../../components/Statistic"
import Button from "../../../components/Button"
import { cloudStorage } from "../../../dummy"
import Heading from "../../../components/Heading"
import Card from "../../../components/Card"

function CloudStorage() {
    const [cartItem, setCartItem] = useState([])

    const handleAddToCart = id => {
        const selectedItem = cloudStorage.find(item => item.id === id)
        if (selectedItem) {
            setCartItem([...cartItem, selectedItem])
        }
    }

    const handleBuy = () => {
        setTimeout(() => {
            message.success("You have successfully purchase some storage. Thanks! :)")
        }, 1500)
    }

    const totalPrice = cartItem.map(item => item.price).reduce((acc, curr) => acc + curr, 0)
    const totalCapacity = cartItem.map(item => item.capacity).reduce((acc, curr) => acc + curr, 0)

    return (
        <Section>
            <Row>
                <Col>
                    <Heading content="Cloud storage" />
                </Col>
            </Row>
            <Row type="flex" justify="space-between" gutter={32} style={{ marginBottom: "3em" }}>
                <Col span={8}>
                    <Statistic title="Storage used" value="100 MB" />
                </Col>
                <Col span={8}>
                    <Statistic title="Total storage" value="10 GB" />
                </Col>
                <Col span={8}>
                    <Statistic title="Used" value="20%" />
                </Col>
            </Row>
            <Row gutter={32}>
                <Col span={10}>
                    <Heading
                        content={
                            <h4>
                                <Icon type="hdd" /> &nbsp; Get more space!
                            </h4>
                        }
                    />
                    <List
                        dataSource={cloudStorage}
                        renderItem={item => (
                            <List.Item
                                actions={[
                                    <Button
                                        type="ghost"
                                        size="default"
                                        icon="plus"
                                        onClick={() => handleAddToCart(item.id)}
                                    >
                                        Add
                                    </Button>
                                ]}
                            >
                                <List.Item.Meta
                                    title={`${item.capacity} GB`}
                                    description={`$${item.price} / month`}
                                />
                            </List.Item>
                        )}
                    />
                </Col>
                <Col span={10}>
                    <Heading
                        content={
                            <h4>
                                <Icon type="shopping" /> &nbsp; Your storage cart
                            </h4>
                        }
                    />
                    <Row type="flex" justify="space-between" gutter={32}>
                        <Col span={12}>
                            <Card
                                autoHeight
                                title={
                                    <h2>
                                        {totalCapacity} <span style={{ fontSize: 12 }}>GB</span>
                                    </h2>
                                }
                                description="Storage added"
                            />
                        </Col>
                        <Col span={12}>
                            <Card autoHeight title={<h2>$ {totalPrice}</h2>} description="Price" />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button type="primary" size="default" icon="check" onClick={handleBuy}>
                                Buy now!
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Section>
    )
}

export default CloudStorage
