import React, { useState } from "react"
import { Formik, Field } from "formik"
import { Tabs, Form } from "antd"
import Section from "../../../components/Section"
import Heading from "../../../components/Heading"
import Tab from "../../../components/Tab"
import Card from "../../../components/Card"
import Button from "../../../components/Button"
import SelectInput from "../../../components/forms/SelectInput"
import { typeOfClass } from "../../../dummy"
import TextInput from "../../../components/forms/TextInput"
import UploadInput from "../../../components/forms/UploadInput"
import UploadedResources from "./UploadedResources"

function Resources() {
    const [uploadedResources, setUploadedResources] = useState([])
    const handleViewUploadedResources = value => {
        console.log(value)
        setUploadedResources(value)
    }

    return (
        <Section>
            <Heading content="Lessons Resource" />
            <Tab>
                <Tabs.TabPane tab="Upload resource" key="Upload resource">
                    <Section width="70%" paddingHorizontal={0}>
                        <Card autoHeight title="Upload your resource">
                            <Formik
                                onSubmit={values => {
                                    console.log(values)
                                }}
                                render={({ handleSubmit }) => (
                                    <Form onSubmit={handleSubmit} layout="vertical">
                                        <Field
                                            name="class"
                                            placeholder="Choose a class"
                                            label="Your class"
                                            options={typeOfClass}
                                            component={SelectInput}
                                        />
                                        <Field
                                            name="lessons"
                                            placeholder="Choose a lesson"
                                            label="Your lesson"
                                            options={typeOfClass}
                                            component={SelectInput}
                                        />
                                        <Field
                                            name="name"
                                            placeholder="Example: Diagrams XYZ"
                                            label="Resource name"
                                            component={TextInput}
                                        />
                                        <Field
                                            name="instructions"
                                            placeholder="Example: Take this resources as your learning preferences"
                                            label="Instructions"
                                            textarea
                                            component={TextInput}
                                        />
                                        <UploadInput name="file" />
                                        <Button type="primary" size="default" icon="check">
                                            Upload resource
                                        </Button>
                                    </Form>
                                )}
                            />
                        </Card>
                    </Section>
                </Tabs.TabPane>
                <Tabs.TabPane tab="View uploaded resource" key="View uploaded resource">
                    <UploadedResources
                        data={uploadedResources}
                        onViewUploadedResources={handleViewUploadedResources}
                    />
                </Tabs.TabPane>
                <Tabs.TabPane tab="View uploaded submissions" key="View uploaded submissions">
                    <UploadedResources
                        data={uploadedResources}
                        onViewUploadedResources={handleViewUploadedResources}
                    />
                </Tabs.TabPane>
            </Tab>
        </Section>
    )
}

export default Resources
