import React from "react"
import Section from "../../../components/Section"
import { Cascader, Form, Row, Col, Table } from "antd"
import { uploadedResources } from "../../../dummy"
import styled from "styled-components"
import Heading from "../../../components/Heading"

const Cascade = styled(Cascader)`
    display: block;
`


function UploadedResources(props) {
    const { data, onViewUploadedResources } = props
    const selectedResource = uploadedResources
        .filter(item => item.value === data[0])
        .map(item => item.children)[0]

    const selected =
        selectedResource &&
        selectedResource
            .filter((item) => item.value === data[1])
            .map((item) => item.dataSource)[0]

    const columns = [
        { title: "Name", dataIndex: "name", key: "name" },
        { title: "Instructions", dataIndex: "instructions", key: "instructions" },
        { title: "Attached file", dataIndex: "file", key: "file" }
    ]

    return (
        <Section paddingHorizontal={0}>
            <Row gutter={64}>
                <Col span={8}>
                    <Form layout="vertical">
                        <Form.Item label="Select your lesson">
                            <Cascade
                                style={{ display: "block" }}
                                expandTrigger="hover"
                                options={uploadedResources}
                                onChange={onViewUploadedResources}
                            />
                        </Form.Item>
                    </Form>
                </Col>
                <Col span={14}>
                    <Heading content={data[0] || ""} level={4} subheader={data[1]} />
                    <Table columns={columns} dataSource={selected || []} />
                </Col>
            </Row>
        </Section>
    )
}

export default UploadedResources
