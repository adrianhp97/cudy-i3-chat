import React, { useEffect, useState } from "react"
import { Row, Col } from "antd"
import { withRouter } from "react-router-dom"
import styled from "styled-components"

import { Section, Heading, Layout, ResultSection, Loading } from "components"
import FilteringPanel from "./FilteringPanel"
import TutorCard from "components/common/TutorCard"
import { media, mobile, client } from "helpers"
import useQueryData from "helpers/hooks/useQueryData"
import { queryFetchCurriculums, queryFetchSubjects } from "queries/data"
import SelectInput from "components/forms/SelectInput"
import { useMutation } from "react-apollo"
import { mutateSearchTutors } from "queries/user"

const CardColumn = styled(Col)`
    && {
        margin-bottom: 2.5em;
        ${media.mobile`
            margin-bottom: 2em;
        `}
    }
`

const initialValues = {
    keywords: "",
    curriculums: 0,
    subjects: 0,
    alphabet: 0,
    recently: 0,
    price_range: [10, 100]
}

function Marketplace({ location, history }) {
    const { data: curr = {} } = useQueryData(queryFetchCurriculums, "programmeClient")
    const { data: subj = {} } = useQueryData(queryFetchSubjects, "programmeClient")
    const [users, setUsers] = useState([])
    const [formValues, setFormValues] = useState(initialValues)

    const params = new URLSearchParams(location.search)
    const subjectParams = params.get("subjects")
    const keywordParams = params.get("keywords")

    const fromNavbar =
        (localStorage.getItem("fromNavbar") && JSON.parse(localStorage.getItem("fromNavbar"))) || {}

    const [searchUsers, { loading }] = useMutation(mutateSearchTutors, {
        client: client.profileClient,
        onCompleted: data => {
            data = data.findTutors || []
            setUsers(data)
        }
    })

    const curriculumsData = curr.getProgrammeCurriculumList || []
    const subjectsData = subj.getProgrammeSubjectList || []
    const currOptions = curriculumsData.map(item => ({
        value: Number(item.pvid),
        label: item.name
    }))
    const subjOptions = subjectsData.map(item => ({ value: Number(item.pvid), label: item.name }))

    const handleSortUsers = value => {
        const recently = value === "recently" ? 1 : 0
        const alphabet = value === "alphabet" ? 1 : 0
        const values = {
            ...formValues,
            price_range: {
                startPrice: formValues.price_range[0],
                endPrice: formValues.price_range[1]
            },
            recently,
            alphabet
        }
        searchUsers({ variables: values })
    }

    const handleClickTutor = tutor => {
        history.push(`/${tutor.token}-${tutor.pvid}-${tutor.firstName}-${tutor.lastName}`)
    }

    const renderUsers = () => {
        const userList = fromNavbar.data || users
        if (userList.length === 0) {
            return <ResultSection type="search" title="There's no tutors found" />
        }
        if (loading) return <Loading />
        return userList.map(tutor => {
            const { pvid, ...rest } = tutor

            return (
                <CardColumn key={pvid} lg={6} xs={12}>
                    <TutorCard {...rest} onClick={() => handleClickTutor(tutor)} />
                </CardColumn>
            )
        })
    }

    return (
        <Layout>
            <Section paddingHorizontal="padded">
                <Row gutter={48}>
                    <Col lg={6}>
                        <FilteringPanel
                            options={{ currOptions, subjOptions }}
                            onUsers={{
                                searchUsers,
                                fromNavbar,
                                initialValues
                            }}
                            onFormValues={{ formValues, setFormValues }}
                            params={{ subjectParams, keywordParams }}
                            location={{ location, history }}
                        />
                    </Col>
                    <Col lg={18}>
                        <Section noPadding style={{ marginBottom: "2em" }}>
                            <Row type="flex" justify="space-between" align="middle">
                                <Col lg={12}>
                                    <Heading
                                        content="Searches"
                                        subheader={
                                            <span>
                                                Showing&nbsp;
                                                <strong>{users.length} tutors</strong>
                                            </span>
                                        }
                                        level={4}
                                    />
                                </Col>
                                <Col lg={6} xs={12} style={{ textAlign: "right" }}>
                                    <SelectInput
                                        independent
                                        name="sort"
                                        defaultvalue="Sort by..."
                                        options={[
                                            { value: "Sort by...", label: "Sort by..." },
                                            { value: "recently", label: "Recently added" },
                                            { value: "alphabet", label: "Alphabet" }
                                        ]}
                                        onChange={handleSortUsers}
                                        placeholder="Sort by..."
                                    />
                                </Col>
                            </Row>
                        </Section>
                        <Section noPadding>
                            <Row gutter={mobile ? 16 : 32} type="flex">
                                {renderUsers()}
                            </Row>
                        </Section>
                    </Col>
                </Row>
            </Section>
        </Layout>
    )
}

export default withRouter(Marketplace)
