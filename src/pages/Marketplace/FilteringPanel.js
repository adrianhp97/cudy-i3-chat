import React, { useEffect } from "react"
import { Form, Radio, Slider, Input } from "antd"
import styled from "styled-components"
import debounce from "just-debounce-it"

import { Card, Heading, Section } from "components"

const StyledSection = styled(Section)`
	padding-bottom: 1.5em;
	&& {
		article {
			margin-bottom: 0.5em;
		}
	}
	.ant-checkbox-group-item {
		display: block;
	}
	h4.ant-typography {
		font-size: 1em;
	}
	.ant-radio-wrapper {
		display: block;
		span {
		}
	}
`

const StyledInput = styled(Input.Search).attrs({
	size: "large"
})`
	&& {
		margin-top: -40px;
		margin-bottom: 2em;
		display: block;
		input[name="keywords"] {
			border-radius: 8px;
		}
	}
`

function FilteringPanel({ options, onUsers, params = {}, location, onFormValues }) {
	const { subjOptions, currOptions } = options
	const { searchUsers, initialValues } = onUsers
	const { subjectParams = "", keywordParams = "" } = params
	const { formValues, setFormValues } = onFormValues
	const numberedSubject = Number(subjectParams)
	const { history } = location

	const handleSearchFilter = name => e => {
		// e.preventDefault()
		const { price_range } = formValues
		if (name === "keywords") setFormValues(prev => ({ ...prev, keywords: e }))
		else setFormValues(prev => ({ ...prev, [name]: e.target.value }))

		const values = {
			...formValues,
			price_range: { startPrice: price_range[0], endPrice: price_range[1] }
		}

		if (window.location.search !== "") history.push("/browse/tutors")
		// if (previousValues !== formValues) {
		debounce(() => searchUsers({ variables: values }), 500)
		// }
	}

	const handleChangeSlider = value => {
		setFormValues(prev => ({ ...prev, price_range: value }))
		const values = {
			...formValues,
			price_range: { startPrice: value[0], endPrice: value[1] }
		}

		if (window.location.search !== "") history.push("/browse/tutors")
		debounce(() => searchUsers({ variables: values }), 500)
	}

	useEffect(() => {
		let data = {}
		if (subjectParams) {
			data = {
				...formValues,
				price_range: { startPrice: 10, endPrice: 100 },
				subjects: numberedSubject
			}
		}

		if (keywordParams) {
			data = {
				...formValues,
				price_range: { startPrice: 10, endPrice: 100 },
				keywords: keywordParams || ""
			}
		}

		data = {
			...formValues,
			price_range: {
				startPrice: formValues.price_range[0] || 10,
				endPrice: formValues.price_range[1] || 100
			}
		}
		searchUsers({ variables: data })
	}, [searchUsers, formValues, subjectParams, keywordParams])

	return (
		<Card title="" autoHeight style={{ marginTop: "1em" }}>
			<Form layout="vertical">
				<StyledInput
					name="keywords"
					placeholder="E.g. Felix Ong..."
					onSearch={handleSearchFilter("keywords")}
					allowClear
				/>
				<StyledSection noPadding>
					<Heading content="Price per hour" level={4} />
					<Slider // from antd
						range
						name="price_range"
						marks={{ 0: "$0", 150: "$150" }}
						tipFormatter={value => `$${value}`}
						step={5}
						min={0}
						max={150}
						defaultValue={[formValues.price_range[0], formValues.price_range[1]]}
						onAfterChange={handleChangeSlider}
					/>
				</StyledSection>
				<StyledSection noPadding>
					<Heading content="Curriculum" level={4} />
					<Radio.Group // from antd
						name="curriculums"
						defaultValue={0}
						options={[{ value: 0, label: "All" }, ...currOptions]}
						onChange={handleSearchFilter("curriculums")}
					/>
				</StyledSection>
				<StyledSection noPadding>
					<Heading content="Subjects" level={4} />
					<Radio.Group // from antd
						name="subjects"
						defaultValue={numberedSubject}
						options={[{ value: 0, label: "All" }, ...subjOptions]}
						onChange={handleSearchFilter("subjects")}
					/>
				</StyledSection>
			</Form>
		</Card>
	)
}

export default FilteringPanel
