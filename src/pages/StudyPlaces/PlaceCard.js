import React, { useEffect } from "react"
import { motion } from "framer-motion"
import styled from "styled-components"
import { Heading, Rating } from "components"
import { baseStyles } from "styles/base"
import { connect } from "react-redux"
import { getPhotoUrl } from "helpers"
import { Row, Col, Icon } from "antd"

const StyledCard = styled(motion.div).attrs({
	whileTap: { scale: 0.94 },
	whileHover: { scale: 1.03 }
})`
	border-radius: 10px;
	box-shadow: ${baseStyles.boxShadow.main};
	background-color: #fff;
	height: 300px;
	max-height: 300px;
	.details {
		text-align: center;
		padding: 0;
		article.ant-typography {
			width: 90%;
			margin: 0 auto;
		}
		h4 {
			font-size: 1em;
			text-overflow: ellipsis;
			white-space: nowrap;
			overflow-x: hidden;
			margin-bottom: 0.5em;
		}
	}
	.image-wrapper {
		max-height: 200px;
		overflow-y: hidden;
		margin-bottom: 1.8em;
		> img {
			min-height: 200px;
			object-fit: cover;
			border-radius: 8px 8px 0 0;
		}
	}
	.rating-section,
	.price-section {
		.ant-rate-star {
			margin: 0;
		}
	}
	.rating-section {
		font-size: 0.8em;
		/* margin-bottom: 1em; */
	}
`

function PlaceCard({ data = {}, photo, ...props }) {
	const photoData = data.photos || {}
	const thePhoto =
		photoData.reference === null
			? "https://gw.alipayobjects.com/mdn/miniapp_social/afts/img/A*pevERLJC9v0AAAAAAAAAAABjAQAAAQ/original"
			: getPhotoUrl(photoData.reference)

	return (
		<StyledCard {...props}>
			<section className="details">
				<div className="image-wrapper">
					<img src={thePhoto} alt={data.name} width="100%" />
				</div>
				<Heading
					level={4}
					content={data.name}
					subheader={
						<Row type="flex" justify="center">
							<Col lg={20}>
								{data.userRatingTotal ? (
									<div className="rating-section">
										<Rating allowHalf disabled defaultValue={data.rating} />
										&nbsp; ({data.userRatingTotal})
									</div>
								) : (
									<div className="rating-section">No rating yet</div>
								)}
								{/* <div className="price-section">
                                    <Rating
                                        color="primary"
                                        disabled
                                        allowHalf
                                        defaultValue={data.price_level}
                                        character={<Icon type="dollar" />}
                                    />
                                </div> */}
							</Col>
						</Row>
					}
				/>
			</section>
		</StyledCard>
	)
}

const mapState = ({ place }) => ({ photo: place.photo })

export default connect(mapState)(PlaceCard)
