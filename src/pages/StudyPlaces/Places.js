import React, { useEffect, useState } from "react"
import { Section, Heading, Loading, Empty } from "components"
import { Row, Col, Input } from "antd"
import { Link } from "react-router-dom"
import PlaceCard from "./PlaceCard"
import { connect } from "react-redux"
import { fetchPlaceTypes, searchPlaces } from "store/actions/placesActions"
import styled from "styled-components"
import { motion } from "framer-motion"
import CategoryList from "components/common/CategoryList"

const FilterPanel = styled(motion.div)`
	margin-bottom: 1em;
	border-radius: 5px;
	padding: 1em 0;
	.right {
		text-align: right;
	}
`

const StyledInput = styled(Input.Search)`
	&& {
		margin-bottom: 1em;
		display: block;
		/* input[name="keyword"] {
			border-radius: 8px;
		} */
	}
`

const filterPanelVariants = {
	visible: { opacity: 1, y: 0, display: "block", transition: { staggerChildren: 0.2 } },
	hidden: { opacity: 0, y: -300, display: "none" }
}

function Places({ loading, fetchPlaceTypes, placeTypeOptions, searchPlaces, places }) {
	const [filterValues, setFilterValues] = useState({ type: 1, keyword: "" })
	const [latLng, setLatLng] = useState({ lat: 0, lng: 0 })

	const renderPlaces = () => {
		if (places.length === 0)
			return (
				<Section centered>
					<Empty description="Oops! No data found for now" />
				</Section>
			)

		if (loading)
			return (
				<Row>
					<Col lg={12}>
						<Loading />
					</Col>
				</Row>
			)

		return (
			<Row gutter={24}>
				{(places || []).map(item => (
					<Col lg={6} xs={12} key={item.pvid} style={{ marginBottom: "2em" }}>
						<Link to={`/study_places/places/${item.name}--${item.pvid}`}>
							<PlaceCard
								css={`
									cursor: pointer;
								`}
								data={item}
							/>
						</Link>
					</Col>
				))}
			</Row>
		)
	}

	const handleSearch = value => {
		setFilterValues({ ...filterValues, keyword: value })
		searchPlaces(value, filterValues.type)
	}

	const handleSelectType = type => {
		setFilterValues({ ...filterValues, type })
		searchPlaces(filterValues.keyword, type)
	}

	useEffect(() => {
		fetchPlaceTypes()
		searchPlaces("", 1)
	}, [])

	return (
		<Section>
			<FilterPanel variants={filterPanelVariants} animate="visible" initial="hidden" transition={{ delay: 2 }}>
				<Row type="flex" justify="space-between">
					<Col lg={8} className="right">
						<StyledInput name="keyword" placeholder="E.g. Eduhub..." onSearch={handleSearch} allowClear />
					</Col>
				</Row>
			</FilterPanel>
			<Heading
				bold
				level={4}
				marginBottom="1em"
				content="Places to study"
				subheader="School is not the only place to study. The list below proves it"
			/>
			<CategoryList options={placeTypeOptions} onClick={handleSelectType} type={filterValues.type} />
			{renderPlaces()}
		</Section>
	)
}

const mapState = ({ place }) => {
	const placeTypeOptions = (place.placeTypes || []).map(item => ({ value: item.pvid, label: item.name }))

	return {
		nearbyPlaces: place.nearbyPlaces,
		places: place.places,
		loading: place.loading,
		placeTypeOptions
	}
}

export default connect(mapState, { fetchPlaceTypes, searchPlaces })(Places)
