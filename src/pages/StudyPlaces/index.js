import React, { useEffect } from "react"
import { Row, Col, Menu, Icon } from "antd"
import { Section, Logo, Heading, Loading } from "components"
import styled from "styled-components/macro"
import { connect } from "react-redux"
import { searchPlaces, fetchNearbyPlaces } from "store/actions/placesActions"
import PlaceCard from "./PlaceCard"
import { Link, Switch, Route, Redirect, NavLink, useParams, useLocation } from "react-router-dom"
import { motion } from "framer-motion"
import StudyCenter from "./StudyCenter"
import PlaceDetails from "./PlaceDetails"
import Places from "./Places"

const MainSection = styled(Section).attrs({
	paddingHorizontal: 0
})`
	background-color: #fff;
`

const MainMenu = styled(Menu)`
	&& {
		height: 100vh;
		border-right: none;
		.ant-menu-item {
			padding-left: 2px;
			font-weight: bold;
			height: 25px;
			line-height: 1;
		}
	}
`

const TheMenu = styled(motion.ul)`
	&& {
		height: 100vh;
		border-right: none;
		.ant-menu-item {
			padding-left: 2px;
			font-weight: bold;
			height: 25px;
			line-height: 1;
			a:not(.active) {
				opacity: 0.3;
				&:hover {
					opacity: initial;
				}
			}
		}
	}
`

const MainRow = styled(Row)`
	.left-section {
		position: relative;
		height: 100vh;
		overflow-y: hidden;
	}
	.right-section {
		overflow-y: scroll;
		height: 100vh;
	}
`

const menuVariants = {
	visible: {
		opacity: 1,
		transition: {
			delay: 3,
			staggerChildren: 0.2
			// delayChildren: 0.15
		}
	}
}

const menuItemVariants = {
	visible: {
		y: 0,
		opacity: 1,
		transition: { stiffness: 1000, velocity: -100 }
	},
	hidden: { y: 50, opacity: 0 }
}

const menuItems = [
	{ key: "places", label: "Places" },
	{ key: "tuition", label: "Tuition center" }
]

const paramsPlaceDetails = JSON.parse(localStorage.getItem("paramsPlaceDetails"))

function StudyPlaces({ fetchTuitionCategories, places, loading, fetchNearbyPlaces, searchPlaces, photo }) {
	const { name, pvid } = useParams()
	const { pathname } = useLocation()
	const pathArray = pathname.split("/") || []
	const currentSection = pathArray[2] || ""
	const isDetailsPage = pathArray.length > 3

	useEffect(() => {
		// fetchNearbyPlaces(pageToken)
		// searchPlaces("cafe")
	}, [paramsPlaceDetails])

	return (
		<MainRow>
			{!isDetailsPage && (
				<Col lg={6} className="left-section">
					<MainSection>
						<Section marginBottom={0} style={{ paddingBottom: 0 }}>
							<Logo width={70} />
						</Section>
						<Section>
							{/* <MainMenu>
                            <Menu.Item key="popular">Most Popular</Menu.Item>
                            <Menu.Item key="nearby">Nearby</Menu.Item>
                            <Menu.Item key="all">All Places</Menu.Item>
                        </MainMenu> */}
							<TheMenu
								className="ant-menu ant-menu-light ant-menu-root ant-menu-vertical"
								variants={menuVariants}
								initial="hidden"
								animate="visible"
							>
								{menuItems.map(item => (
									<motion.li key={item.key} variants={menuItemVariants} className="ant-menu-item">
										<NavLink to={`/study_places/${item.key}`}>{item.label}</NavLink>
									</motion.li>
								))}
							</TheMenu>
						</Section>
					</MainSection>
				</Col>
			)}
			<Col lg={isDetailsPage ? 24 : 18} className="right-section">
				<Switch>
					<Redirect exact from="/study_places" to="/study_places/places" />
					<Route exact path="/study_places/places" component={Places} />
					<Route
						path="/study_places/places/:name--:pvid"
						render={() => <PlaceDetails currentSection={currentSection} />}
					/>
					<Route exact path="/study_places/tuition" component={StudyCenter} />
					<Route
						path="/study_places/tuition/:name--:pvid"
						render={() => <PlaceDetails currentSection={currentSection} />}
					/>
				</Switch>
			</Col>
		</MainRow>
	)
}

const mapState = ({ place }) => ({
	places: place.places || [],
	pageToken: place.pageToken,
	loading: place.loading,
	photo: place.photo
})

// prettier-ignore
export default connect( mapState, { searchPlaces, fetchNearbyPlaces } )(StudyPlaces)
