import React, { useState, useEffect } from "react"
import { Section, Rating, Heading, Tag, Avatar, Loading } from "components"
import { Row, Col, Icon, Collapse, List, Tooltip } from "antd"
import { motion, AnimatePresence } from "framer-motion"
import { baseStyles } from "styles/base"
import styled from "styled-components/macro"
import { useHistory, useParams } from "react-router-dom"
import { connect } from "react-redux"
import { fetchPlaceDetails, fetchTuitionById } from "store/actions/placesActions"
import { getPhotoUrl, media, mobile, placeholderImage } from "helpers"

const MainSection = styled(Section)`
	height: 100vh;
	padding-top: 0;
	padding-bottom: 0;
	margin-bottom: 0;
	overflow-y: hidden;
	.left-section {
		position: relative;
		height: 100vh;
	}
	.right-section {
		overflow-y: scroll;
		height: 100vh;
	}
	> .ant-row-flex {
		height: 100%;
		img {
			height: 100%;
			object-fit: cover;
		}
	}

	${media.mobile`
		&& {
			overflow-y: visible;
			padding-left: 0;
			padding-right: 0;
			.right-section {
				overflow-y: visible;
				> section {
					padding-left: 1em;
					padding-right: 1em;
				}
			}
		}
	`}
`

const StatCard = styled(motion.section)`
	padding: 2em;
	background-color: ${baseStyles.lightGrey.one};
	border-radius: 8px;
	margin-bottom: 2em;
	.ant-typography {
		margin-bottom: 0;
		overflow-x: hidden;
		text-overflow: ellipsis;
		h4.ant-typography {
			color: ${baseStyles.greyColor};
			font-size: 0.7em;
		}
	}
	.ant-rate {
		> li {
			padding-left: 0;
			margin-left: 0;
			margin-right: 4px;
		}
	}

	${media.mobile`
		article.ant-typography {
			margin-bottom: 1em;
		}
	`}
`

const ListItem = styled(List.Item)`
	.date {
		color: ${baseStyles.greyColor};
	}
`

const BackButton = styled(motion.button)`
    border-radius: 100px;
    position: absolute;
    bottom: -70px;
    left: 30px;
    padding: 1em 1.5em;
    background-color: #fff;
    box-shadow: ${baseStyles.boxShadow.main};
    z-index: 10;
    cursor: pointer;

	${media.mobile`
		bottom: unset;
		top: 20px;
	`}
    /* &:hover {
        box-shadow: ${baseStyles.boxShadow.hover};
        transform: translateY(-1px);
    } */
`

const PhotoSection = styled(motion.div)`
	padding: 0;
	display: flex;
	margin-bottom: 2em;
	flex-wrap: nowrap;
	overflow-x: scroll;
	width: auto;
	-webkit-overflow-scrolling: touch;
	&::-webkit-scrollbar {
		display: none;
	}
	.photo-wrapper {
		margin-right: 4px;
		width: 120px;
		height: 100px;
		filter: brightness(0.8);
		cursor: pointer;
		&:hover {
			filter: brightness(1);
		}
	}

	${media.mobile`
		width: 100vw;
		position: relative;
		margin-top: -100px;
	`}
`

const PlaceDetails = React.memo(({ fetchPlaceDetails, place, loading, fetchTuitionById, currentSection }) => {
	const [selectedPhoto, setSelectedPhoto] = useState("")
	const [initial, setInitial] = useState(true)
	const isTuition = currentSection === "tuition"

	const types = (place.types || []).map(item => {
		const type = item.includes("_") ? item.split("_").join(" ") : item
		return <Tag key={item}>{type}</Tag>
	})
	const description = item => (
		<div>
			<p>{item.review === "" ? "-" : item.review}</p>
			<Rating disabled allowHalf defaultValue={item.rating} />
		</div>
	)
	const history = useHistory()
	const { name, pvid } = useParams()
	const ratingTotal = !place.userRatingTotal ? "Not enough ratings" : place.userRatingTotal
	const reviewCount = (place.reviews || {}).length > 0 ? `(${place.reviews.length} reviews)` : "No reviews yet"
	let initialPhoto = (place.photos && place.photos[0]) || {}
	const photo = initial ? initialPhoto.reference : selectedPhoto

	const handleSelectPhoto = item => {
		setInitial(false)
		setSelectedPhoto(item)
	}

	useEffect(() => {
		if (isTuition) fetchTuitionById(pvid)
		else fetchPlaceDetails(pvid)
		if (name && pvid) {
			localStorage.setItem("paramsPlaceDetails", JSON.stringify({ name, pvid }))
		}
		return () => localStorage.removeItem("paramsPlaceDetails")
	}, [setSelectedPhoto, selectedPhoto, name, pvid])

	if (initial && loading) return <Loading />

	return (
		<MainSection paddingHorizontal={0}>
			<Row type="flex">
				<Col lg={14} className="left-section">
					<img
						src={(place.photos || []).length === 0 ? placeholderImage : getPhotoUrl(photo)}
						alt={place.name}
						width="100%"
					/>
					<BackButton
						animate={{ y: mobile ? 0 : -90 }}
						initial={{ y: mobile ? -90 : 90 }}
						transition={{ delay: 4 }}
						onClick={() => history.goBack()}
					>
						<Icon type="left" /> Back to places
					</BackButton>
				</Col>
				<Col lg={10} className="right-section">
					<Section
						css={`
							padding-top: 4em;
						`}
					>
						{mobile && (
							<PhotoSection onMobile>
								{(place.photos || []).map(({ reference: ref }) => (
									<div key={ref} className="photo-wrapper" onClick={() => handleSelectPhoto(ref)}>
										<img src={getPhotoUrl(ref)} alt="Heheh" width="120" />
									</div>
								))}
							</PhotoSection>
						)}
						<Rating disabled allowHalf defaultValue={place.rating} />
						&nbsp; <span>({ratingTotal})</span> <br />
						<Heading level={4} content={place.name} subheader={place.address} />
						<div style={{ marginBottom: "2em" }}>{types}</div>
						<StatCard>
							<Row gutter={16}>
								{!isTuition && (
									<Col lg={8} xs={12}>
										<Heading
											reverse
											level={4}
											content="Price"
											marginBottom={mobile && "1em"}
											subheader={
												<Rating
													color="primary"
													disabled
													allowHalf
													defaultValue={place.priceLevel}
													character={<Icon type="dollar" />}
												/>
											}
										/>
									</Col>
								)}
								<Col lg={isTuition ? 12 : 8} xs={12}>
									<Heading
										reverse
										level={4}
										content="Phone number"
										marginBottom={mobile && "1em"}
										subheader={
											<Tooltip title={place.phone}>
												<a
													href={`tel:${place.phone}`}
													target="_blank"
													// rel="noreferrer noopener"
												>
													{place.phone}
												</a>
											</Tooltip>
										}
									/>
								</Col>
								<Col lg={isTuition ? 12 : 8} xs={12}>
									<Heading
										reverse
										level={4}
										content="Website"
										subheader={
											<Tooltip title={place.website || "-"}>
												<a
													href={place.website || "-"}
													target="_blank"
													rel="noreferrer noopener"
												>
													{place.website || "-"}
												</a>
											</Tooltip>
										}
									/>
								</Col>
							</Row>
						</StatCard>
						{!mobile && (
							<PhotoSection>
								{(place.photos || []).map(({ reference: ref }) => (
									<div key={ref} className="photo-wrapper" onClick={() => handleSelectPhoto(ref)}>
										<img src={getPhotoUrl(ref)} alt="Heheh" width="120" />
									</div>
								))}
							</PhotoSection>
						)}
						<Collapse
							bordered={false}
							style={{ marginBottom: "2em" }}
							defaultActiveKey={(place.openingHours || []).length > 0 ? ["opening"] : []}
						>
							<Collapse.Panel header={<div>Opening hours</div>} key="opening">
								<List
									itemLayout="horizontal"
									dataSource={place.openingHours}
									renderItem={item => (
										<ListItem>
											<List.Item.Meta title={<div> {item.opening_hours} &middot; </div>} />
										</ListItem>
									)}
								/>
							</Collapse.Panel>
						</Collapse>
						<Collapse bordered={false}>
							<Collapse.Panel header={<div>Reviews {reviewCount}</div>} key="reviews">
								<List
									itemLayout="horizontal"
									dataSource={place.reviews}
									renderItem={item => (
										<ListItem>
											<List.Item.Meta
												avatar={<Avatar src={item.authorProfilePhoto} />}
												title={
													<div>
														{item.author} &middot;{" "}
														<span className="date">{item.relativeTime}</span>
													</div>
												}
												description={description(item)}
											/>
										</ListItem>
									)}
								/>
							</Collapse.Panel>
						</Collapse>
					</Section>
				</Col>
			</Row>
		</MainSection>
	)
})

const mapState = ({ place }, { currentSection }) => {
	const isTuition = currentSection === "tuition"

	return {
		place: isTuition ? place.tuition || {} : place.place || {},
		loading: place.loading
	}
}

export default connect(mapState, { fetchPlaceDetails, fetchTuitionById })(PlaceDetails)
