import React, { useEffect, useState } from "react"
import { Col, Row, Form, Input } from "antd"
import { Section, Heading, Loading, Empty } from "components"
import { connect } from "react-redux"
import { fetchTuitionCategories, searchNearbyTuition, searchTuition } from "store/actions/placesActions"
import styled from "styled-components"
import { motion } from "framer-motion"
import { baseStyles } from "styles/base"
import SelectInput from "components/forms/SelectInput"
import PlaceCard from "../PlaceCard"
import { Link } from "react-router-dom"
import CategoryList from "components/common/CategoryList"

const FilterPanel = styled(motion.div)`
	margin-bottom: 1em;
	border-radius: 5px;
	padding: 1em 0;
	/* box-shadow: ${baseStyles.boxShadow.main}; */
	.right {
		text-align: right;
	}
`

const StyledInput = styled(Input.Search).attrs({
	// size: "large"
})`
	&& {
		margin-bottom: 1em;
		display: block;
		/* input[name="keyword"] {
			border-radius: 8px;
		} */
	}
`

const filterPanelVariants = {
	visible: { opacity: 1, y: 0, display: "block", transition: { staggerChildren: 0.2 } },
	hidden: { opacity: 0, y: -300, display: "none" }
}

function StudyCenter({ tuitionOptions, loading, searchNearbyTuition, nearbyTuitions, ...props }) {
	const [filterValues, setFilterValues] = useState({ category: 1, keyword: "" })
	const [latLng, setLatLng] = useState({ lat: 0, lng: 0 })

	const { fetchTuitionCategories, tuitions, searchTuition } = props
	const theData = tuitions || nearbyTuitions || []

	const renderTuitions = () => {
		if (loading)
			return (
				<Row>
					<Col lg={12}>
						<Loading />
					</Col>
				</Row>
			)

		if (theData.length === 0)
			return (
				<Section centered>
					<Empty description="Oops! No data found for now" />
				</Section>
			)

		return (
			<Row gutter={24}>
				{(theData || []).map(item => (
					<Col lg={6} xs={12} key={item.pvid} style={{ marginBottom: "2em" }}>
						<Link to={`/study_places/tuition/${item.name}--${item.pvid}`}>
							<PlaceCard
								css={`
									cursor: pointer;
								`}
								data={item}
							/>
						</Link>
					</Col>
				))}
			</Row>
		)
	}

	const handleSelectCategory = category => {
		setFilterValues({ ...filterValues, category })
		searchTuition(filterValues.keyword, category)
		// searchNearbyTuition({ lat: latLng.lat, lng: latLng.lng }, category)
	}

	const handleSearchFilter = value => {
		setFilterValues({ ...filterValues, keyword: value })
		searchTuition(value, filterValues.category)
	}

	useEffect(() => {
		fetchTuitionCategories()
		searchTuition("", 1)
		// if ("geolocation" in navigator) {
		// 	navigator.geolocation.getCurrentPosition(({ coords }) => {
		// 		if (!nearbyTuitions) {
		// 			setLatLng({ lat: coords.latitude, lng: coords.longitude })
		// 			searchNearbyTuition({ lat: coords.latitude, lng: coords.longitude })
		// 		}
		// 	})
		// }
	}, [])

	return (
		<Section>
			<FilterPanel variants={filterPanelVariants} animate="visible" initial="hidden" transition={{ delay: 2 }}>
				<Row type="flex" justify="space-between">
					<Col lg={8} className="right">
						<StyledInput
							name="keyword"
							placeholder="E.g. Eduhub..."
							onSearch={handleSearchFilter}
							allowClear
						/>
					</Col>
				</Row>
			</FilterPanel>
			<Heading
				bold
				level={4}
				marginBottom="1em"
				content="Tuition center"
				subheader="List of tuition center in Singapore"
			/>
			<CategoryList options={tuitionOptions} type={filterValues.category} onClick={handleSelectCategory} />
			{renderTuitions()}
		</Section>
	)
}

const mapState = ({ place }) => {
	const tuitionOptions = (place.tuitionCategories || []).map(item => ({ value: item.pvid, label: item.name }))

	return {
		tuitions: place.tuitions,
		loading: place.loading,
		tuitionCategories: place.tuitionCategories,
		nearbyTuitions: place.nearbyTuitions,
		tuitionOptions
	}
}

export default connect(mapState, { fetchTuitionCategories, searchNearbyTuition, searchTuition })(StudyCenter)
