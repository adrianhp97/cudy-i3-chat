import React, { useState, useEffect } from "react"
import { Row, Col, Form, Icon, Popconfirm, Divider, message } from "antd"
import { Heading, Button, ButtonLink, Alert, Upload } from "components"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import { withRouter } from "react-router"
import { ResultSection } from "components"
import styled from "styled-components"
import SelectInput from "components/forms/SelectInput"
import { yearListFrom1980 } from "dummy"
import * as yup from "yup"
import { SubmitButton, FormikDebug } from "formik-antd"
import {
    mutateUploadCertification,
    mutateDeleteCertification,
    mutateAddCertification
} from "queries/profile"
import { client, mobile } from "helpers"
import { useMutation } from "react-apollo"
import useError from "helpers/hooks/useError"

const FieldRow = styled.div`
    position: relative;
    &:hover {
        .delete {
            display: flex;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
    }
    .delete {
        display: none;
        cursor: pointer;
        visibility: hidden;
        position: absolute;
        right: 0;
        border-radius: 50%;
        width: 25px;
        color: #ff9d00;
        height: 25px;
        z-index: 99;
        &:hover {
            background: #eee;
        }
    }
`

const Drag = styled.div`
    text-align: center;
    display: block;
    padding: 2em;
    background: #f3f3f3;
    border-radius: 8px;
    margin-bottom: 2em;
    h4 {
        font-size: 1.2em;
    }
    > p {
        font-size: 4em;
    }
`

const yearList = yearListFrom1980.map(item => ({ value: String(item), label: item }))

function Certifications({ onPrev, history, onCertifications, fetchUser }) {
    const { certifications = [], setCertifications } = onCertifications
    const [isAdding, setIsAdding] = useState(false)
    const [localFile, setLocalFile] = useState({ file: "", imagePreview: "" })

    const [uploadCertification] = useMutation(mutateUploadCertification, {
        client: client.resourceClient,
        onCompleted: data => {
            setLocalFile(prev => ({ ...prev, actual: (data.uploadCertification || {}).path }))
        }
    })

    const [addCertification] = useMutation(mutateAddCertification, {
        client: client.profileClient,
        onError: err => useError(err)
    })

    const [deleteCertification] = useMutation(mutateDeleteCertification, {
        client: client.profileClient,
        onCompleted: () => {
            fetchUser()
            message.success("Your certificate has been deleted")
        },
        onError: err => useError(err)
    })

    const handleWrapup = () => {
        history.push("/profile/info/thankyou")
    }

    const handleDelete = pvid => deleteCertification({ variables: { pvid: Number(pvid) } })

    const handleCancel = () => {
        setIsAdding(false)
        setLocalFile({})
    }

    const handleUploadToLocal = file => {
        const reader = new FileReader()
        reader.onloadend = () => {
            setLocalFile(prev => ({ ...prev, file, imagePreview: reader.result }))
        }
        reader.readAsDataURL(file)
        return false
    }

    const handleAddNewField = () => {
        setIsAdding(true)
    }

    const handleSave = (values, { setSubmitting }) => {
        values = {
            ...values,
            relativePath: localFile.actual,
            issuedDate: values.issuedDate + "/01/01"
        }
        addCertification({ variables: values })
            .then(() => {
                fetchUser()
                message.success("Your certificate has been added")
            })
            .finally(() => {
                setIsAdding(false)
                setSubmitting(false)
                setLocalFile({})
            })
    }

    const handleUploadCert = () => {
        uploadCertification({ variables: { file: localFile.file } })
    }

    // useEffect(() => {
    //     setCertifications(certifications)
    // }, [certifications])

    return (
        <Col lg={13} xs={24} style={{ padding: "2em" }}>
            <Heading
                content="Certifications"
                subheader="Please fill in your certifications"
                level={4}
                marginBottom="2em"
            />
            {/* <ResultSection title="This feature will be available soon" /> */}
            {isAdding ? (
                <Formik
                    onSubmit={handleSave}
                    initialValues={{ name: "", issuer: "", issuedDate: "" }}
                    validationSchema={validationSchema}
                    render={({ handleSubmit }) => (
                        <Form
                            layout="vertical"
                            onSubmit={handleSubmit}
                            style={{ marginBottom: "2em" }}
                        >
                            <FieldRow>
                                {localFile.file ? (
                                    localFile.actual ? (
                                        <Alert
                                            type="success"
                                            showIcon
                                            message="Your certificate is uploaded. Now please fill in the details of the certificate."
                                        />
                                    ) : (
                                        <Button
                                            block
                                            style={{ marginBottom: "2em" }}
                                            type="dashed"
                                            icon="upload"
                                            onClick={handleUploadCert}
                                        >
                                            Upload this certificate now
                                        </Button>
                                    )
                                ) : (
                                    <Upload
                                        beforeUpload={handleUploadToLocal}
                                        multiple={false}
                                        listType="picture"
                                        style={{ marginBottom: "2em" }}
                                    >
                                        <Drag>
                                            <p>
                                                <Icon type="inbox" />
                                            </p>
                                            <Heading
                                                content="Drag and drop your certification"
                                                subheader="Supported formats: .pdf, .doc, .docx, .png, .jpg, .jpeg"
                                                level={4}
                                            />
                                        </Drag>
                                    </Upload>
                                )}
                                <TextInput
                                    name="name"
                                    placeholder="Name of certification"
                                    label="Name of certification"
                                    disabled={!localFile.actual}
                                />
                                <Row gutter={16}>
                                    <Col lg={16}>
                                        <TextInput
                                            name="issuer"
                                            placeholder="Issuer"
                                            label="Issuer"
                                            disabled={!localFile.actual}
                                        />
                                    </Col>
                                    <Col lg={8}>
                                        <SelectInput
                                            name="issuedDate"
                                            placeholder="Year issued"
                                            options={yearList}
                                            label="Year issued"
                                            disabled={!localFile.actual}
                                        />
                                    </Col>
                                </Row>
                            </FieldRow>
                            <SubmitButton type="primary" size="large">
                                Save changes
                            </SubmitButton>{" "}
                            &nbsp;
                            <ButtonLink size="large" onClick={handleCancel}>
                                Cancel
                            </ButtonLink>
                        </Form>
                    )}
                />
            ) : (
                <>
                    {certifications.map((item, idx) => {
                        const { name = "", issuer = "", issuedDate = "" } = item
                        return (
                            <>
                                <Formik
                                    onSubmit={handleSave}
                                    initialValues={{
                                        name,
                                        issuer,
                                        issuedDate
                                    }}
                                    render={({ handleSubmit }) => (
                                        <Form
                                            layout="vertical"
                                            onSubmit={handleSubmit}
                                            style={{ marginBottom: "2em" }}
                                        >
                                            <FieldRow>
                                                <TextInput
                                                    name="name"
                                                    placeholder="Name of certification"
                                                    label="Name of certification"
                                                />
                                                <Row gutter={16}>
                                                    <Col lg={16}>
                                                        <TextInput
                                                            name="issuer"
                                                            placeholder="Issuer"
                                                            label="Issuer"
                                                        />
                                                    </Col>
                                                    <Col lg={8}>
                                                        <SelectInput
                                                            name="issuedDate"
                                                            placeholder="Year issued"
                                                            options={yearList}
                                                            label="Year issued"
                                                        />
                                                    </Col>
                                                </Row>
                                                <SubmitButton type="primary">
                                                    Update certificate
                                                </SubmitButton>{" "}
                                                &nbsp;{" "}
                                                <Popconfirm
                                                    title="Delete this certificate?"
                                                    onConfirm={() => handleDelete(item.pvid)}
                                                >
                                                    <ButtonLink icon="close">Delete</ButtonLink>
                                                </Popconfirm>
                                            </FieldRow>
                                        </Form>
                                    )}
                                />
                                {idx !== certifications.length - 1 && <Divider />}
                            </>
                        )
                    })}
                    <Button
                        block
                        type="dashed"
                        size="medium"
                        icon="plus"
                        onClick={handleAddNewField}
                        style={{ marginBottom: "2em" }}
                    >
                        Add a new certificate
                    </Button>
                    {/* <ButtonLink icon="close" onClick={() => setEditMode(false)}>
                        Cancel
                    </ButtonLink> */}
                </>
            )}
            <Row type="flex" justify="space-between">
                <Col lg={12} xs={24}>
                    <ButtonLink
                        type="primary"
                        block={mobile}
                        size="large"
                        onClick={() => onPrev("experiences")}
                    >
                        <Icon type="left" /> Back to Experiences
                    </ButtonLink>
                </Col>
                <Col lg={12} xs={24} style={{ textAlign: !mobile && "right" }}>
                    <Button type="primary" block={mobile} size="large" onClick={handleWrapup}>
                        Next <Icon type="right" />
                    </Button>
                </Col>
            </Row>
            {/* <Formik
                render={() => (
                    <>
                        <Form layout="vertical" style={{ marginBottom: "3em" }}>
                            <TextInput name="subject" placeholder="Subject" label="Subject" />
                            <TextInput name="level" placeholder="Level" label="Level" />
                            <TextInput name="price" placeholder="Price" label="Price per hour" />
                        </Form>
                        <Row type="flex" justify="space-between">
                            <Col>
                                <ButtonLink
                                    type="primary"
                                    size="large"
                                    onClick={() => onPrev("experiences")}
                                >
                                    <Icon type="left" /> Back to Experiences
                                </ButtonLink>
                            </Col>
                            <Col>
                                <Button type="primary" size="large" onClick={handleWrapup}>
                                    Next <Icon type="right" />
                                </Button>
                            </Col>
                        </Row>
                    </>
                )}
            /> */}
        </Col>
    )
}

const validationSchema = yup.object().shape({
    name: yup.string().required("You have to tell the name of the certificate"),
    issuer: yup.string().required("You have to tell the issuer"),
    issuedDate: yup.string().required("You have to tell when the certificate is issued")
})

export default withRouter(Certifications)
