import React, { useState, useEffect, Suspense } from "react"
import { Row, Col, Icon, Modal } from "antd"
import { Heading, Card, Section, ButtonLink, Steps, Donut, Loading } from "components"
import { Switch, Route, Redirect, withRouter } from "react-router-dom"
import styled from "styled-components"
import uuid from "uuid/v4"

import orangeEl from "assets/images/ellipse-orange.png"
import purpleEl from "assets/images/ellipse-purple.png"
import useQueryData from "helpers/hooks/useQueryData"
import { querySubjects, queryLevels, queryCurriculums } from "pages/Home/query"
import { mobile, media, newClient } from "helpers"
import AboutMe from "./AboutMe"
import Subjects from "./Subjects"
import Educations from "./Educations"
import Experiences from "./Experiences"
import Certifications from "./Certifications"
import { useMutation } from "react-apollo"
import { mutateUserProfile, mutateUpdateProfile } from "queries/profile"
import decorating from "assets/images/decorating.png"
import Name from "./Name"

// const Subjects = React.lazy(() => import("./Subjects"))
// const Educations = React.lazy(() => import("./Educations"))
// const Experiences = React.lazy(() => import("./Experiences"))
// const Certifications = React.lazy(() => import("./Certifications"))

const StyledCard = styled(Card).attrs({
    autoHeight: true,
    noHover: true
})`
    position: relative;
    .ant-card-body {
        padding: 0;
        .ant-card-meta {
            display: none;
            visibility: hidden;
        }
        img {
            border-radius: 10px 0 0 10px;
        }
    }

    ${media.mobile`
        && {
            margin-left: 1em;
        }
    `}
`

const BackgroundSection = styled(Section)`
    height: 100%;
    position: relative;
    background: url(${decorating}) no-repeat;
    background-size: cover;
    background-blend-mode: darken;
    border-radius: 10px 0 0 10px;
    && {
        .steps {
            padding-left: 2em;
            padding-top: 3em;
            display: flex;
            justify-content: center;
            align-items: center;
            color: #fff;
            .ant-steps-item-title {
                color: #fff;
            }
        }
    }
`

const StyledHeading = styled(Heading)`
    ${media.mobile`
        && {
            h2 {
                line-height: 1;
                margin-bottom: .5em;
            }
        }
    `}
`

const initialValues = {
    aboutme: {
        firstName: "",
        lastName: "",
        dob: Date.now(),
        nric: "",
        phone: "",
        gender: "",
        race: "",
        address: "",
        description: ""
    },
    subjects: [
        {
            pvid: uuid(),
            programmeLevelPvid: "",
            programmeSubjectPvid: "",
            pricePerHour: 0,
            orderList: 0
        },
        {
            pvid: uuid(),
            programmeLevelPvid: "",
            programmeSubjectPvid: "",
            pricePerHour: 0,
            orderList: 0
        }
    ],
    educations: [
        {
            id: uuid(),
            institution: "",
            monthFrom: "01",
            monthTo: "01",
            yearFrom: "2000",
            yearTo: "2000",
            isPresent: false
        },
        {
            id: uuid(),
            institution: "",
            monthFrom: "01",
            monthTo: "01",
            yearFrom: "2000",
            yearTo: "2000",
            isPresent: false
        }
    ],
    experiences: [
        {
            id: uuid(),
            curriculum: "",
            level: "",
            subject: "",
            monthFrom: "01",
            monthTo: "01",
            yearFrom: "2000",
            yearTo: "2000",
            institution: "",
            isPrivateTutor: false,
            isPresent: false
        },
        {
            id: uuid(),
            curriculum: "",
            level: "",
            subject: "",
            monthFrom: "01",
            monthTo: "01",
            yearFrom: "2000",
            yearTo: "2000",
            institution: "",
            isPrivateTutor: false,
            isPresent: false
        }
    ]
}

const stepsArray = pathname => {
    switch (pathname) {
        case "name":
            return 0
        case "aboutme":
            return 1
        case "subjects":
            return 2
        case "educations":
            return 3
        case "experiences":
            return 4
        case "certifications":
            return 5
        case "done":
            return 6
        default:
            return -1
    }
}

const firstTime = JSON.parse(localStorage.getItem("firstTime"))

function AdditionalInfoTutor({ history, location }) {
    const pathname = (location.pathname || "").split("/")[3]

    const { data: dataSubjects = {} } = useQueryData(querySubjects, "programmeClient")
    const { data: dataLevels = {} } = useQueryData(queryLevels, "programmeClient")
    const { data: dataCurriculums = {} } = useQueryData(queryCurriculums, "programmeClient")
    const [activeStep, setActiveStep] = useState(stepsArray(pathname))
    const [aboutme, setAboutme] = useState("")
    const [educations, setEducations] = useState(initialValues.educations)
    const [experiences, setExperiences] = useState(initialValues.experiences)
    const [certifications, setCertifications] = useState([])
    const [userData, setUserData] = useState({})

    const [fetchUser, { loading: loadingFetch }] = useMutation(mutateUserProfile, {
        client: newClient(),
        onCompleted: data => {
            data = data.getProfile || {}
            setUserData(data)
            setAboutme(data.description)
            setCertifications(data.certificateList)
        }
    })

    const [updateProfile] = useMutation(mutateUpdateProfile, {
        client: newClient("account"),
        onCompleted: data => {
            setUserData(data)
            fetchUser()
            handleNext("basic")
        }
    })

    const handleUpdateProfile = (values, section) => {
        updateProfile({ variables: { ...values } }).then(() => handleNext(section))
    }

    const handleNext = section => {
        setActiveStep(activeStep => activeStep + 1)
        history.push(`/profile/onboarding_steps/${section.toLowerCase()}`)
    }

    const handlePrev = section => {
        setActiveStep(activeStep => activeStep - 1)
        history.push(`/profile/onboarding_steps/${section.toLowerCase()}`)
    }

    const handleSkipSteps = () => {
        Modal.confirm({
            title: "Skip this?",
            content: "Are you sure want to skip this at the moment?",
            centered: true,
            onOk() {
                history.push("/")
            }
        })
    }

    const steps = stepsArray(pathname)

    useEffect(() => {
        if (!localStorage.getItem("token") /* || !firstTime */) history.push("/404")
        fetchUser()
        setActiveStep(steps)
    }, [steps])

    console.log({ userData })

    return (
        <Row type="flex" justify="center" align="middle" style={{ marginTop: !mobile && "2em" }}>
            <Col lg={16}>
                <Section centered width="75%" style={{ margin: "0 auto" }}>
                    <StyledHeading
                        content="You are almost done"
                        subheader="A better profile is the one that has a lot of informations to dig in. Therefore, it's recommended for you, as a tutor, to fill out additional informations you might have to complete your profile."
                    />
                </Section>
                <StyledCard>
                    <Donut src={orangeEl} bottom="-140px" left="-180px" style={{ zIndex: -1 }} />
                    <Donut src={purpleEl} width="200px" top="-90px" right="-90px" style={{ zIndex: -1 }} />
                    <Row gutter={32} type="flex">
                        <Col lg={10} xs={24}>
                            <BackgroundSection>
                                <div className="steps">
                                    <Steps
                                        progressDot
                                        current={activeStep}
                                        steps={[
                                            "Name",
                                            "About me",
                                            "Subjects",
                                            "Educations",
                                            "Experiences",
                                            "Certifications",
                                            "Done"
                                        ]}
                                    />
                                </div>
                            </BackgroundSection>
                        </Col>
                        <Suspense fallback={<Loading />}>
                            <Switch>
                                <Redirect exact from="/profile/onboarding_steps" to="/profile/onboarding_steps/name" />
                                <Route
                                    path="/profile/onboarding_steps/name"
                                    render={() => (
                                        <Name
                                            onActiveStep={{ activeStep, setActiveStep }}
                                            handlers={{ handleNext, handleUpdateProfile }}
                                            onAboutme={{ aboutme, setAboutme }}
                                            onData={{ userData, setUserData, initialValues: initialValues.aboutme }}
                                        />
                                    )}
                                />
                                <Route
                                    path="/profile/onboarding_steps/aboutme"
                                    render={() => (
                                        <AboutMe
                                            onActiveStep={{ activeStep, setActiveStep }}
                                            handlers={{ handleNext, handlePrev, handleUpdateProfile }}
                                            onAboutme={{ aboutme, setAboutme }}
                                            onData={{ userData }}
                                        />
                                    )}
                                />
                                <Route
                                    path="/profile/onboarding_steps/subjects"
                                    render={() => (
                                        <Subjects
                                            onActiveStep={{ activeStep, setActiveStep }}
                                            handlers={{ handleNext, handlePrev }}
                                            onData={{ userData }}
                                            loading={loadingFetch}
                                            fetchUser={fetchUser}
                                        />
                                    )}
                                />
                                <Route
                                    path="/profile/onboarding_steps/educations"
                                    render={() => (
                                        <Educations
                                            onActiveStep={{ activeStep, setActiveStep }}
                                            onNext={handleNext}
                                            onPrev={handlePrev}
                                            onData={{ userData }}
                                            onEducations={{ educations, setEducations }}
                                            loading={loadingFetch}
                                            fetchUser={fetchUser}
                                        />
                                    )}
                                />
                                <Route
                                    path="/profile/onboarding_steps/experiences"
                                    render={() => (
                                        <Experiences
                                            onActiveStep={{ activeStep, setActiveStep }}
                                            onNext={handleNext}
                                            onPrev={handlePrev}
                                            data={{ dataSubjects, dataCurriculums, dataLevels, userData }}
                                            onExperiences={{ experiences, setExperiences }}
                                            fetchUser={fetchUser}
                                            loading={loadingFetch}
                                        />
                                    )}
                                />
                                <Route
                                    path="/profile/onboarding_steps/certifications"
                                    render={() => (
                                        <Certifications
                                            onActiveStep={{ activeStep, setActiveStep }}
                                            onNext={handleNext}
                                            onPrev={handlePrev}
                                            fetchUser={fetchUser}
                                            onCertifications={{ certifications, setCertifications }}
                                        />
                                    )}
                                />
                            </Switch>
                        </Suspense>
                    </Row>
                </StyledCard>
                <Section centered>
                    <ButtonLink onClick={handleSkipSteps}>
                        Let me fill out everything later <Icon type="right" />{" "}
                    </ButtonLink>
                </Section>
            </Col>
        </Row>
    )
}

export default withRouter(AdditionalInfoTutor)
