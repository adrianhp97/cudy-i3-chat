import React, { useEffect } from "react"
import { Row, Col, Form, Icon } from "antd"
import { Heading, Button, Section, Loading } from "components"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import moment from "moment"

export default function Name({ handlers, onData, loading }) {
    const { handleNext, handleUpdateProfile } = handlers
    const { userData = {}, setUserData, initialValues } = onData

    const handleSubmitName = (values, { setSubmitting }) => {
        values = {
            ...initialValues,
            ...values,
            dob: moment(initialValues.dob).format("YYYY/MM/DD")
        }
        // updateProfile({ variables: { ...values } })
        handleUpdateProfile(values, "aboutme")
        setSubmitting(false)
    }

    if (Object.keys(userData).length === 0) return <Loading />

    return (
        <Col lg={14} xs={24} style={{ padding: "2em 3em" }}>
            <Heading content="Your name" subheader={<span>Tell us your name</span>} level={4} marginBottom="2em" />
            <Formik
                onSubmit={handleSubmitName}
                validate={validation}
                initialValues={{
                    firstName: userData.firstName || "",
                    lastName: userData.lastName || ""
                }}
                render={({ handleSubmit }) => (
                    <Form layout="vertical" onSubmit={handleSubmit}>
                        <Row gutter={32}>
                            <Col lg={12}>
                                <TextInput name="firstName" label="First name" placeholder="Your first name..." />
                            </Col>
                            <Col lg={12}>
                                <TextInput name="lastName" label="Last name" placeholder="Your last name..." />
                            </Col>
                        </Row>
                        <br />
                        <br />
                        <Section textAlign="right" noPadding>
                            <Button type="primary" htmlType="submit" loading={loading}>
                                {loading ? "Please wait..." : "Next"} <Icon type="right" />
                            </Button>
                        </Section>
                    </Form>
                )}
            />
        </Col>
    )
}

const validation = values => {
    let errors = {}
    if (!values.firstName) errors.firstName = "You have to tell us your first name"
    if (!values.lastName) errors.lastName = "You have to tell us your last name"
    return errors
}
