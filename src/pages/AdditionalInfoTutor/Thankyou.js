import React from "react"
import { Section, Card, Heading, Button, Tick, Alert } from "components"
import { Row, Col } from "antd"
import { Link } from "react-router-dom"

export default function Thankyou() {
    return (
        <Section>
            <Row type="flex" justify="center" align="middle">
                <Col lg={12}>
                    <Section centered>
                        <Link to="/">
                            <img src="https://app.cudy.co/img/logo_live.png" width="100" />
                        </Link>
                    </Section>
                    <Card autoHeight noHover>
                        <Section centered>
                            <Tick />
                            <Heading
                                content="You're all set!"
                                subheader="That's a great step! Now go to your profile to get started."
                                marginBottom="3em"
                            />
                            <Link to="/profile/me">
                                <Button type="primary" size="medium" icon="user">
                                    Go to your Profile
                                </Button>
                            </Link>
                        </Section>
                    </Card>
                </Col>
            </Row>
        </Section>
    )
}
