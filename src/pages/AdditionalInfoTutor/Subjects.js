import React, { useState, useEffect } from "react"
import { Row, Col, Form, Icon, message, Popconfirm } from "antd"
import { Heading, ButtonLink, Button, Empty, Loading } from "components"
import styled from "styled-components"
import uuid from "uuid/v4"

import TextInput from "components/forms/TextInput"
import SelectInput from "components/forms/SelectInput"
import { useMutation, useQuery } from "@apollo/react-hooks"
import { mutateAddSubjects, mutateDeleteSubjects } from "queries/profile"
import { client, mobile, newClient } from "helpers"
import { Formik } from "formik"
import { queryFetchSubjects, queryFetchCurriculums } from "queries/data"
import { SubmitButton } from "formik-antd"
import useError from "helpers/hooks/useError"

const FieldRow = styled(Row).attrs({
    gutter: 16
})`
    position: relative;
    margin-bottom: 2em;
    &:hover {
        .delete {
            display: flex;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
    }
    .delete {
        display: none;
        cursor: pointer;
        visibility: hidden;
        position: absolute;
        right: 0;
        top: -20px;
        border-radius: 50%;
        width: 25px;
        color: #ff9d00;
        height: 25px;
        z-index: 99;
        &:hover {
            background: #eee;
        }
    }
`

function Subjects({ handlers, onData, /*onSubjects, onLevels*/ fetchUser, loading }) {
    const [isAdding, setIsAdding] = useState(false)
    const { handleNext, handlePrev } = handlers
    const { userData = {} } = onData
    const [userSubjects, setUserSubjects] = useState([])

    const { data: dataSubjects = {} } = useQuery(queryFetchSubjects, {
        client: client.programmeClient
    })

    const { data: dataCurriculums = {} } = useQuery(queryFetchCurriculums, {
        client: client.programmeClient
    })

    const subjectOptions = (dataSubjects.getProgrammeSubjectList || []).map(item => ({
        value: Number(item.pvid),
        label: item.name
    }))

    const currOptions = (dataCurriculums.getProgrammeCurriculumList || []).map(item => ({
        value: Number(item.pvid),
        label: item.name
    }))

    const [addSubjects, { loading: loadingAdd }] = useMutation(mutateAddSubjects, {
        client: newClient(),
        onError: err => {
            if (err) {
                return
            }
        }
    })

    const [deleteSubjects, { loading: loadingDelete }] = useMutation(mutateDeleteSubjects, {
        client: newClient(),
        onCompleted: data => console.log({ data }),
        onError: err => useError(err)
    })

    const handleAddSubject = (values, { setSubmitting }) => {
        const hasPvid = values.hasOwnProperty("pvid")
        if (hasPvid) {
            const { source, ...rest } = values
            values = rest
        }
        addSubjects({ variables: { list: [{ ...values, orderList: 1 }] } })
            .then(() => fetchUser())
            .then(() => {
                message.success(`The subject item has been ${hasPvid ? "updated" : "added"}`, 2)
            })
            .finally(() => {
                setSubmitting(false)
                setIsAdding(false)
            })
    }

    const handleDelete = pvid => {
        const filtered = userSubjects.find(item => item.pvid === pvid) || {}
        deleteSubjects({ variables: { pvidList: [filtered.pvid] } })
            .then(() => fetchUser())
            .then(() => message.success("The subject item has been deleted", 2))
    }

    const handleCancel = () => {
        setIsAdding(false)
        setUserSubjects(userData.subjectList)
    }

    const handleAddNewField = () => {
        setIsAdding(true)
        setUserSubjects(prev => [
            ...prev,
            {
                id: uuid(),
                programmeSubjectPvid: "",
                pricePerHour: 0,
                subject: { pvid: null },
                level: { pvid: null },
                orderList: 0
            }
        ])
    }

    useEffect(() => {
        setUserSubjects(userData.subjectList)
    }, [userData.subjectList])

    const renderSubjects = () => {
        if ((userSubjects || []).length === 0) return <Empty />
        if (loading || loadingDelete || loadingAdd) return <Loading />
        if (isAdding) {
            return (
                <Formik
                    onSubmit={handleAddSubject}
                    render={({ handleSubmit }) => (
                        <Form layout="vertical" onSubmit={handleSubmit} style={{ marginBottom: "2em" }}>
                            <Row gutter={16}>
                                <Col lg={12}>
                                    <SelectInput
                                        name="programmeSubjectPvid"
                                        placeholder="Subject name"
                                        label="Subject"
                                        options={subjectOptions}
                                        marginBottom="5px"
                                    />
                                </Col>
                                <Col lg={12}>
                                    <SelectInput
                                        name="programmeLevelPvid"
                                        placeholder="Curriculum"
                                        label="Curriculum"
                                        options={currOptions}
                                        marginBottom="5px"
                                    />
                                </Col>
                                <Col lg={12}>
                                    <TextInput
                                        number
                                        label="Price per hour"
                                        name="pricePerHour"
                                        formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                        parser={value => value.replace(/\$\s?|(,*)/g, "")}
                                        step={5}
                                    />
                                </Col>
                            </Row>
                            <SubmitButton block>Save this subject</SubmitButton> &nbsp;{" "}
                            <ButtonLink block onClick={handleCancel}>
                                Cancel
                            </ButtonLink>
                        </Form>
                    )}
                />
            )
        }
        return (userSubjects || []).map(item => (
            <Formik
                onSubmit={handleAddSubject}
                initialValues={{
                    programmeSubjectPvid: Number(item.subject.pvid),
                    programmeLevelPvid: Number(item.level.pvid),
                    pricePerHour: item.pricePerHour
                }}
                render={({ handleSubmit }) => (
                    <FieldRow>
                        <Form layout="vertical" onSubmit={handleSubmit}>
                            <Row gutter={16}>
                                <Col lg={12}>
                                    <SelectInput
                                        name="programmeSubjectPvid"
                                        placeholder="Subject name"
                                        label="Subject"
                                        options={subjectOptions}
                                        marginBottom="5px"
                                    />
                                </Col>
                                <Col lg={12}>
                                    <SelectInput
                                        name="programmeLevelPvid"
                                        placeholder="Curriculum"
                                        label="Curriculum"
                                        options={currOptions}
                                        marginBottom="5px"
                                    />
                                </Col>
                                <Col lg={12}>
                                    <TextInput
                                        number
                                        name="pricePerHour"
                                        formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                        parser={value => value.replace(/\$\s?|(,*)/g, "")}
                                        step={5}
                                    />
                                </Col>
                            </Row>
                            <SubmitButton type="primary">Update</SubmitButton> &nbsp;{" "}
                            <Popconfirm title="Delete this subject?" onConfirm={() => handleDelete(item.pvid)}>
                                <ButtonLink icon="close">Delete</ButtonLink>
                            </Popconfirm>
                        </Form>
                    </FieldRow>
                )}
            />
        ))
    }

    console.log({ userData })

    return (
        <Col lg={13} xs={24} style={{ padding: "2em" }}>
            <Heading
                content="Subjects"
                subheader="Please fill the subjects you are teaching"
                level={4}
                marginBottom="2em"
            />

            {renderSubjects()}

            {!isAdding && (
                <div style={{ marginBottom: "2em" }}>
                    <Button
                        block
                        type="dashed"
                        size="medium"
                        icon="plus"
                        onClick={handleAddNewField}
                        style={{ marginBottom: "2em" }}
                    >
                        Add a new subject
                    </Button>
                    <ButtonLink block icon="close" onClick={() => ({})}>
                        Cancel
                    </ButtonLink>
                </div>
            )}

            {/* <Form layout="vertical" style={{ marginBottom: "3em" }} onSubmit={handleAddSubjects}> */}

            {/* {subjects.map((item, idx) => {
                    const handleChange = name => value => {
                        let newSubjects = [...subjects]
                        newSubjects[idx][name] = value
                        setSubjects(newSubjects)
                    }

                    return (
                        <FieldRow>
                            {idx > 0 && (
                                <span className="delete" onClick={() => handleDeleteSubject(item.pvid)}>
                                    <Icon type="close" />
                                </span>
                            )}
                            <Col lg={10}>
                                <SelectInput
                                    independent
                                    name="programmeSubjectPvid"
                                    placeholder="Subject"
                                    label="Subject"
                                    value={item.programmeSubjectPvid}
                                    options={subjectOptions}
                                    onChange={handleChange("programmeSubjectPvid")}
                                />
                            </Col>
                            <Col lg={8} xs={12}>
                                <SelectInput
                                    independent
                                    name="programmeLevelPvid"
                                    placeholder="Level"
                                    label="Level"
                                    value={item.programmeLevelPvid}
                                    options={levelOptions}
                                    onChange={handleChange("programmeLevelPvid")}
                                />
                            </Col>
                            <Col lg={6} xs={12}>
                                <TextInput
                                    number
                                    independent
                                    name="pricePerHour"
                                    label="Price per hour"
                                    formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                    parser={value => value.replace(/\$\s?|(,*)/g, "")}
                                    value={item.pricePerHour}
                                    step={5}
                                    onChange={handleChange("pricePerHour")}
                                />
                            </Col>
                        </FieldRow>
                    )
                })}
                <Button
                    block
                    type="dashed"
                    size="medium"
                    icon="plus"
                    onClick={handleAddNewField}
                    style={{ marginBottom: "2em" }}
                >
                    Add a new subject
                </Button> */}

            {/* </Form> */}
            <Row type="flex" justify="space-between">
                <Col lg={12} xs={24}>
                    <ButtonLink type="primary" size="large" block={mobile} onClick={() => handlePrev("aboutme")}>
                        <Icon type="left" /> Back to About me
                    </ButtonLink>
                </Col>
                <Col lg={12} xs={24} style={{ textAlign: !mobile && "right" }}>
                    <Button type="primary" size="large" block={mobile} onClick={() => handleNext("educations")}>
                        Go to Educations <Icon type="right" />
                    </Button>
                </Col>
            </Row>
        </Col>
    )
}

export default Subjects
