import React, { useState, useEffect } from "react"
import { Row, Col, Form, Icon, Checkbox, Divider, Popconfirm, message } from "antd"
import { Heading, Button, ButtonLink, Section, Empty } from "components"
import styled from "styled-components"
import uuid from "uuid/v4"

import TextInput from "components/forms/TextInput"
import SelectInput from "components/forms/SelectInput"
import { yearListFrom1980, months } from "dummy"
import { useMutation } from "@apollo/react-hooks"
import { mutateExperiences, mutateDeleteExperiences } from "queries/profile"
import { client, mobile, newClient } from "helpers"
import { Formik } from "formik"
import DateInput from "components/forms/DateInput"
import CheckboxInput from "components/forms/CheckboxInput"
import { SubmitButton } from "formik-antd"
import useError from "helpers/hooks/useError"
import moment from "moment"

const FieldRow = styled(Row).attrs({
    gutter: 16
})`
    position: relative;
    &:hover {
        .delete {
            display: flex;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
    }
    .delete {
        display: none;
        cursor: pointer;
        visibility: hidden;
        position: absolute;
        right: 0;
        top: -20px;
        border-radius: 50%;
        width: 25px;
        color: #ff9d00;
        height: 25px;
        z-index: 99;
        &:hover {
            background: #eee;
        }
    }
`

const StyledSelect = styled(SelectInput).attrs({
    marginBottom: "0"
})``

function Experiences({ onNext, onPrev, onExperiences, data, fetchUser, loading }) {
    const [userExperiences, setUserExperiences] = useState([])
    const [isAdding, setIsAdding] = useState(false)

    const [addExperience] = useMutation(mutateExperiences, {
        client: client.profileClient,
        onCompleted: data => console.log({ data }),
        onError: err => console.error(err)
    })

    const [deleteExperiences] = useMutation(mutateDeleteExperiences, {
        client: newClient(),
        onCompleted: () => {
            setIsAdding(false)
        },
        onError: err => useError(err)
    })

    const { experiences, setExperiences } = onExperiences
    const { dataSubjects = {}, dataCurriculums = {}, dataLevels = {}, userData = {} } = data

    const currOptions = (dataCurriculums.getProgrammeCurriculumList || []).map(item => ({
        value: item.name,
        label: item.name
    }))
    const subjectOptions = (dataSubjects.getProgrammeSubjectList || []).map(item => ({
        value: item.name,
        label: item.name
    }))

    const handleSubmit = e => {
        e.preventDefault()
        const updatedExp = experiences.map(item => {
            const fromDate = `${item.yearFrom}/${item.monthFrom}/01`
            const toDate = `${item.yearTo}/${item.monthTo}/01`
            const { id, monthFrom, yearFrom, monthTo, yearTo, ...rest } = item
            return { ...rest, fromDate, toDate }
        })
        addExperience({ variables: { list: updatedExp } }).then(() => onNext("certifications"))
    }

    const handleCancel = () => {
        setUserExperiences(userData.experienceList)
        setIsAdding(false)
    }

    const handleSave = (values, { setSubmitting }) => {
        const hasPvid = values.hasOwnProperty("pvid")
        values = {
            ...values,
            level: "0",
            fromDate: moment(values.fromDate).format("YYYY-MM-DD"),
            toDate: moment(values.toDate).format("YYYY-MM-DD")
        }
        if (hasPvid) {
            const { source, ...rest } = values
            values = rest
        }
        addExperience({ variables: { list: [values] } })
            .then(() => {
                fetchUser()
            })
            .then(() => message.success(`The experience item has been ${hasPvid ? "updated" : "added"}`, 2))
            .finally(() => {
                setIsAdding(false)
                setSubmitting(false)
            })
    }

    const handleDelete = pvid => {
        const filtered = userExperiences.find(item => item.pvid === pvid) || {}
        deleteExperiences({ variables: { pvidList: [filtered.pvid] } })
            .then(() => {
                fetchUser()
            })
            .then(() => message.success("The experience item has been deleted", 2))
    }

    const handleDeleteExp = id => {
        const filtered = [...experiences].filter(item => item.id !== id)
        setExperiences(filtered)
    }

    const handleAddNewField = () => setIsAdding(true)

    const renderExperiences = () => {
        if (isAdding)
            return (
                <Formik
                    onSubmit={handleSave}
                    initialValues={{ isPresent: false, isPrivateTutor: false }}
                    render={({ handleSubmit, values }) => (
                        <Form layout="vertical" onSubmit={handleSubmit} style={{ marginBottom: "2em" }}>
                            <Row gutter={16} type="flex">
                                <Col lg={8}>
                                    <TextInput
                                        name="institution"
                                        placeholder="Tuition's name"
                                        label="Tuition's name"
                                        disabled={values.isPrivateTutor}
                                    />
                                </Col>
                                <Col lg={8}>
                                    <SelectInput
                                        name="curriculum"
                                        label="Curriculum"
                                        placeholder="Curriculum"
                                        options={currOptions}
                                    />
                                </Col>
                                <Col lg={8}>
                                    <SelectInput
                                        name="subject"
                                        label="Subject"
                                        placeholder="Subject"
                                        options={subjectOptions}
                                    />
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col lg={8}>
                                    <DateInput
                                        monthly
                                        name="fromDate"
                                        label="Date joined"
                                        placeholder="Date joined"
                                        style={{ width: "100%" }}
                                    />
                                </Col>
                                <Col lg={8}>
                                    <DateInput
                                        monthly
                                        name="toDate"
                                        label="Date resigned"
                                        placeholder="Date resigned"
                                        style={{ width: "100%" }}
                                        disabled={values.isPresent}
                                    />
                                </Col>
                                <Col lg={8}>
                                    <CheckboxInput name="isPrivateTutor">I was a private tutor</CheckboxInput>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={12} style={{ marginBottom: "2em" }}>
                                    <CheckboxInput name="isPresent">I'm currently working here</CheckboxInput>
                                </Col>
                            </Row>
                            <SubmitButton block>Save this experience</SubmitButton> &nbsp;{" "}
                            <ButtonLink block onClick={handleCancel}>
                                Cancel
                            </ButtonLink>
                        </Form>
                    )}
                />
            )

        if ((userExperiences || []).length === 0) return <Empty />

        return (userExperiences || {}).map(item => (
            <Formik
                onSubmit={handleSave}
                initialValues={item}
                render={({ handleSubmit, values }) => (
                    <Form layout="vertical" onSubmit={handleSubmit} style={{ marginBottom: "2em" }}>
                        <FieldRow>
                            <Row gutter={16}>
                                <Col lg={8}>
                                    <TextInput
                                        name="institution"
                                        label="Tuition's name"
                                        placeholder="Tuition's name"
                                        disabled={values.isPrivateTutor || item.isPrivateTutor}
                                    />
                                </Col>
                                <Col lg={8}>
                                    <SelectInput
                                        name="curriculum"
                                        label="Curriculum"
                                        placeholder="Curriculum"
                                        options={currOptions}
                                    />
                                </Col>
                                <Col lg={8}>
                                    <SelectInput
                                        name="subject"
                                        label="Subject"
                                        placeholder="Subject"
                                        options={subjectOptions}
                                    />
                                </Col>
                            </Row>
                            <Row gutter={16} style={{ marginBottom: "2em" }}>
                                <Col lg={8}>
                                    <DateInput
                                        monthly
                                        name="fromDate"
                                        label="Date joined"
                                        placeholder="Date joined"
                                        style={{ width: "100%" }}
                                    />
                                </Col>
                                <Col lg={8}>
                                    <DateInput
                                        monthly
                                        name="toDate"
                                        label="Date resigned"
                                        placeholder="Date resigned"
                                        disabled={values.isPresent || item.isPresent}
                                        style={{ width: "100%" }}
                                    />
                                </Col>
                                <Col lg={8}>
                                    <CheckboxInput name="isPrivateTutor">I was a private tutor</CheckboxInput>
                                </Col>
                                <Col lg={12}>
                                    <CheckboxInput name="isPresent">I'm currently working here</CheckboxInput>
                                </Col>
                            </Row>
                            <SubmitButton type="primary">Update</SubmitButton> &nbsp;{" "}
                            <Popconfirm title="Delete this experience?" onConfirm={() => handleDelete(item.pvid)}>
                                <ButtonLink icon="close">Delete</ButtonLink>
                            </Popconfirm>
                        </FieldRow>
                    </Form>
                )}
            />
        ))
    }

    useEffect(() => {
        setUserExperiences(userData.experienceList)
    }, [userData.experienceList])

    return (
        <Col lg={13} xs={24} style={{ padding: "2em" }}>
            <Heading
                content="Tutoring experiences"
                subheader="Please fill in your tutoring experiences"
                level={4}
                marginBottom="2em"
            />

            {renderExperiences()}

            {!isAdding && (
                <div style={{ marginBottom: "2em" }}>
                    <Button
                        block
                        type="dashed"
                        size="medium"
                        icon="plus"
                        onClick={handleAddNewField}
                        style={{ marginBottom: "2em" }}
                    >
                        Add a new experience
                    </Button>
                    <ButtonLink block icon="close" onClick={() => ({})}>
                        Cancel
                    </ButtonLink>
                </div>
            )}

            <Row type="flex" justify="space-between">
                <Col lg={12} xs={24}>
                    <ButtonLink type="primary" block={mobile} size="large" onClick={() => onPrev("educations")}>
                        <Icon type="left" /> Back to Educations
                    </ButtonLink>
                </Col>
                <Col lg={12} xs={24} style={{ textAlign: !mobile && "right" }}>
                    <Button
                        type="primary"
                        block={mobile}
                        size="large"
                        htmlType="submit"
                        onClick={() => onNext("certifications")}
                    >
                        Go to Certifications <Icon type="right" />
                    </Button>
                </Col>
            </Row>
        </Col>
    )
}

export default Experiences
