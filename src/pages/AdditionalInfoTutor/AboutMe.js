import React, { useState, useEffect } from "react"
import { Row, Col, Form, Icon } from "antd"
import { Heading, Button, Section, ButtonLink, Loading } from "components"
import { Formik } from "formik"
import Script from "react-load-script"
import TextInput from "components/forms/TextInput"
import DateInput from "components/forms/DateInput"
import SelectInput from "components/forms/SelectInput"
import { race } from "dummy"
import { googleApiKey } from "helpers"
import RadioInput from "components/forms/RadioInput"
import moment from "moment"

let autocomplete = null

export default function AboutMe({ handlers, onData, loading }) {
    const { handleNext, handlePrev, handleUpdateProfile } = handlers
    const { userData = {} } = onData
    const [addressValue, setAddressValue] = useState("")
    const [address, setAddress] = useState({})

    const handleLoad = () => {
        const options = {
            types: [],
            componentRestrictions: { country: "SG" }
        }

        /*global google*/
        autocomplete = new google.maps.places.Autocomplete(document.getElementById("location"), options)
        autocomplete.addListener("place_changed", handlePlaceSelect)
    }

    const handlePlaceSelect = () => {
        const addressObject = autocomplete.getPlace()
        const userAddress = addressObject.address_components

        if (userAddress) {
            setAddress({
                location: addressObject.formatted_address
            })
            setAddressValue(addressObject.formatted_address)
        }
    }

    const handleSubmitBasic = (values, { setSubmitting }) => {
        const previousData = { firstName: userData.firstName, lastName: userData.lastName }
        values = {
            ...values,
            ...previousData,
            address: addressValue,
            phone: `+65${values.phone}`,
            dob: moment(values.dob).format("YYYY-MM-DD"),
            nric: ""
        }
        handleUpdateProfile(values, "subjects")
        setSubmitting(false)
    }

    useEffect(() => {
        setAddressValue(userData.address)
    }, [userData.address])

    if (Object.keys(userData).length < 1)
        return (
            <Col lg={14} xs={24}>
                <Loading />
            </Col>
        )

    return (
        <Col lg={14} xs={24} style={{ padding: "2em 3em" }}>
            <Heading
                content="About me"
                subheader={<span>Tell us some of your details</span>}
                level={4}
                marginBottom="2em"
            />
            <Formik
                onSubmit={handleSubmitBasic}
                initialValues={{
                    gender: (userData.gender || "").toLowerCase(),
                    race: userData.race,
                    dob: moment(userData.dob).format("YYYY-MM-DD"),
                    address: userData.address,
                    description: userData.description,
                    phone: (userData.phone || "").startsWith("+65") ? userData.phone.slice(3) : userData.phone
                }}
                render={({ handleSubmit }) => (
                    <Form layout="vertical" onSubmit={handleSubmit}>
                        <Row gutter={32}>
                            <Col lg={12}>
                                <DateInput name="dob" label="Date of birth" placeholder="Your date of birth..." />
                            </Col>
                            <Col lg={12}>
                                <TextInput
                                    name="phone"
                                    label="Your mobile"
                                    placeholder="E.g. 87888788"
                                    addonBefore="+65"
                                />
                            </Col>
                        </Row>
                        <Row gutter={32}>
                            <Col lg={12}>
                                <SelectInput name="race" label="Race" placeholder="Your race..." options={race} />
                            </Col>
                            <Col lg={12}>
                                <Script url={googleApiKey} onLoad={handleLoad} />
                                <TextInput
                                    special
                                    allowClear
                                    id="location"
                                    name="address"
                                    placeholder="Your location..."
                                    label="Your location in Singapore"
                                    value={addressValue}
                                    onChange={e => setAddressValue(e.target.value)}
                                />
                            </Col>
                        </Row>
                        <TextInput
                            textarea
                            rows={4}
                            name="description"
                            label="Short description about you"
                            placeholder="E.g. I am Jenny Ang, a 24-year old professional English tutor. I have 6-year experiences tutoring, both in freelance and worked under agency."
                        />
                        <RadioInput
                            name="gender"
                            label="Gender"
                            options={[{ value: "m", label: "Male" }, { value: "f", label: "Female" }]}
                        />
                        <br />
                        <br />
                        <Row type="flex" justify="space-between" align="middle">
                            <Col lg={8}>
                                <ButtonLink
                                    size="large"
                                    htmllType="button"
                                    icon="left"
                                    onClick={() => handlePrev("Name")}
                                >
                                    Back
                                </ButtonLink>
                            </Col>
                            <Col lg={8} style={{ textAlign: "right" }}>
                                <Button type="primary" htmlType="submit" loading={loading}>
                                    Next <Icon type="right" />
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                )}
            />
        </Col>
    )
}
