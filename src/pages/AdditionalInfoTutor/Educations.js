import React, { useState, useEffect } from "react"
import { Row, Col, Form, Icon, Checkbox, Divider, message, Popconfirm } from "antd"
import uuid from "uuid/v4"
import styled from "styled-components"
import TextInput from "components/forms/TextInput"
import SelectInput from "components/forms/SelectInput"
import { months, yearListFrom1980 } from "dummy"
import { Heading, Button, ButtonLink, Empty, Loading } from "components"
import { useMutation } from "@apollo/react-hooks"
import { mutateEducations, mutateDeleteEducations } from "queries/profile"
import { client, mobile, newClient } from "helpers"
import { Formik } from "formik"
import CheckboxInput from "components/forms/CheckboxInput"
import DateInput from "components/forms/DateInput"
import { SubmitButton, ResetButton } from "formik-antd"
import moment from "moment"

const FieldRow = styled(Row).attrs({
    gutter: 16
})`
    position: relative;
    &:hover {
        .delete {
            display: flex;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
    }
    .delete {
        display: none;
        cursor: pointer;
        visibility: hidden;
        position: absolute;
        right: 0;
        top: -20px;
        border-radius: 50%;
        width: 25px;
        color: #ff9d00;
        height: 25px;
        z-index: 99;
        &:hover {
            background: #eee;
        }
    }
`

function Educations({ onNext, onPrev, onEducations, onData, loading, fetchUser }) {
    const [userEducations, setUserEducations] = useState([])
    const [isAdding, setIsAdding] = useState(false)

    const { userData = {} } = onData
    const [addEducation] = useMutation(mutateEducations, {
        client: newClient()
    })
    const [deleteEducation] = useMutation(mutateDeleteEducations, { client: newClient() })
    const { educations, setEducations } = onEducations

    // const yearList = yearListFrom1980.map(item => ({ value: item, label: item }))
    // const monthOptions = months.map(item => ({ value: Number(item.number), label: item.label }))

    const handleDelete = pvid => {
        const filtered = userEducations.find(item => item.pvid === pvid) || {}
        deleteEducation({ variables: { pvidList: [filtered.pvid] } })
            .then(() => fetchUser())
            .then(() => message.success("The education item has been deleted", 2))
    }

    const handleAdd = (values, { setSubmitting }) => {
        values = {
            ...values,
            fromDate: moment(values.fromDate).format("YYYY-MM-DD"),
            toDate: moment(values.toDate).format("YYYY-MM-DD")
        }
        const hasPvid = values.hasOwnProperty("pvid")
        if (hasPvid) {
            const { source, ...rest } = values
            values = rest
        }
        addEducation({ variables: { list: [values] } })
            .then(() => fetchUser())
            .then(() => message.success("The education has been updated"))
            .then(() => {
                setSubmitting(false)
                setIsAdding(false)
            })
    }

    const handleCancel = () => {
        setIsAdding(false)
        setUserEducations(userData.educationList)
    }

    const handleAddNewField = () => setIsAdding(true)
    // set(prev => [
    //     ...prev,
    //     {
    //         id: uuid(),
    //         institution: "",
    //         monthFrom: "01",
    //         monthTo: "01",
    //         yearFrom: "2000",
    //         yearTo: "2000",
    //         isPresent: false
    //     }
    // ])

    const renderEducations = () => {
        if (loading) return <Loading />
        if (isAdding) {
            return (
                <Formik
                    onSubmit={handleAdd}
                    initialValues={{ isPresent: false }}
                    render={({ handleSubmit, values }) => (
                        <Form layout="vertical" onSubmit={handleSubmit} style={{ marginBottom: "2em" }}>
                            <TextInput
                                name="institution"
                                placeholder="E.g. Nanyang Polytechnic"
                                label="Your school/college"
                            />
                            <Row style={{ marginBottom: "1em" }}>
                                <Col lg={14}>
                                    <CheckboxInput name="isPresent">I'm currently studying here</CheckboxInput>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col lg={12} xs={12}>
                                    <DateInput
                                        monthly
                                        name="fromDate"
                                        label="From"
                                        placeholder="Choose the date"
                                        disabled={values.isPresent}
                                    />
                                </Col>
                                <Col lg={12} xs={12}>
                                    <DateInput
                                        monthly
                                        name="toDate"
                                        label="To"
                                        placeholder="Choose the date"
                                        disabled={values.isPresent}
                                    />
                                </Col>
                            </Row>
                            <SubmitButton block>Save this education</SubmitButton> &nbsp;{" "}
                            <ButtonLink block onClick={handleCancel}>
                                Cancel
                            </ButtonLink>
                        </Form>
                    )}
                />
            )
        }
        if ((userEducations || []).length === 0) return <Empty />

        return (userEducations || []).map(item => (
            <Formik
                initialValues={{
                    institution: item.institution,
                    fromDate: moment(item.fromDate).format("YYYY-MM-DD"),
                    toDate: moment(item.toDate).format("YYYY-MM-DD"),
                    isPresent: item.isPresent
                }}
                render={({ handleSubmit, values }) => (
                    <Form layout="vertical" onSubmit={handleSubmit} style={{ marginBottom: "2em" }}>
                        <TextInput
                            name="institution"
                            placeholder="E.g. Nanyang Polytechnic"
                            label="Your school/college"
                        />
                        <Row style={{ marginBottom: "1em" }}>
                            <Col lg={14}>
                                <CheckboxInput name="isPresent">I'm currently studying here</CheckboxInput>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col lg={12} xs={12}>
                                <DateInput
                                    monthly
                                    name="fromDate"
                                    label="From"
                                    placeholder="Choose the date"
                                    disabled={values.isPresent}
                                />
                            </Col>
                            <Col lg={12} xs={12}>
                                <DateInput
                                    monthly
                                    name="toDate"
                                    label="To"
                                    placeholder="Choose the date"
                                    disabled={values.isPresent}
                                />
                            </Col>
                        </Row>
                        <SubmitButton>Save changes</SubmitButton> &nbsp;{" "}
                        <Popconfirm title="Delete this subject?" onConfirm={() => handleDelete(item.pvid)}>
                            <ButtonLink icon="close">Delete</ButtonLink>
                        </Popconfirm>
                    </Form>
                )}
            />
        ))
    }

    useEffect(() => {
        setUserEducations(userData.educationList)
    }, [userData.educationList])

    return (
        <Col lg={13} xs={24} style={{ padding: "2em" }}>
            <Heading
                content="Educational background"
                subheader="Please fill in your educational background"
                level={4}
                marginBottom="2em"
            />

            {renderEducations()}

            {!isAdding && (
                <div style={{ marginBottom: "2em" }}>
                    <Button
                        block
                        type="dashed"
                        size="medium"
                        icon="plus"
                        onClick={handleAddNewField}
                        style={{ marginBottom: "2em" }}
                    >
                        Add a new education
                    </Button>
                    <ButtonLink block icon="close" onClick={() => ({})}>
                        Cancel
                    </ButtonLink>
                </div>
            )}

            <Row type="flex" justify="space-between">
                <Col lg={12} xs={24}>
                    <ButtonLink type="primary" block={mobile} size="large" onClick={() => onPrev("subjects")}>
                        <Icon type="left" /> Back to Subjects
                    </ButtonLink>
                </Col>
                <Col lg={12} xs={24} style={{ textAlign: !mobile && "right" }}>
                    <Button type="primary" block={mobile} size="large" onClick={() => onNext("experiences")}>
                        Go to Experiences <Icon type="right" />
                    </Button>
                </Col>
            </Row>
        </Col>
    )
}

export default Educations
