import gql from "graphql-tag"

export const queryAddSubjects = gql`
    mutation queryAddSubjects(
        $programmeLevelPvid: String!
        $programmeSubjectPvid: String!
        $pricePerHour: Int!
        $orderList: Int!
    ) {
        addSubject(
            programmeLevelPvid: $programmeLevelPvid
            programmeSubjectPvid: $programmeSubjectPvid
            pricePerHour: $pricePerHour
            orderList: $orderList
        ) {
            pvid
            userPvid
            programmeLevelPvid
            programmeSubjectPvid
            orderList

            level {
                pvid
                name
            }

            subject {
                pvid
                name
            }
        }
    }
`
