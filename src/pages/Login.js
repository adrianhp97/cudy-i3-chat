import React, { useEffect } from "react"
import { Formik } from "formik"
import { Form, message } from "antd"
import { useMutation } from "react-apollo"
import styled from "styled-components"

import TextInput from "../components/forms/TextInput"
import Button from "../components/Button"
import Section from "../components/Section"
import { Link, withRouter } from "react-router-dom"
import { newClient } from "helpers"
import useError from "helpers/hooks/useError"
import { mutateLogin } from "queries/auth"

const StyledSection = styled(Section)`
    padding: 20px;
`

function Login({ history, ...props }) {
    const { onSetActive, onSetModal } = props
    const [setLogin, { data, error, loading }] = useMutation(mutateLogin, {
        client: newClient("account"),
        onCompleted: data => {
            const token = (data.signIn || {}).token
            if (data) {
                localStorage.setItem("token", token)
                onSetModal(false)
                window.location.reload(true)
            }
        },
        onError: err => {
            if (err) useError(err)
        }
    })

    const handleLogin = (values, { resetForm }) => {
        setLogin({ variables: { ...values } })
        resetForm({})
    }

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <StyledSection>
            <Formik
                onSubmit={handleLogin}
                initialValues={{ email: "", password: "" }}
                validate={validate}
                render={({ handleSubmit, values }) => (
                    <>
                        <Form
                            onSubmit={handleSubmit}
                            layout="vertical"
                            style={{ marginBottom: "2em" }}
                        >
                            <TextInput
                                type="email"
                                label="Your email"
                                name="email"
                                placeholder="Enter your email"
                            />
                            <TextInput
                                password
                                type="password"
                                label="Your password"
                                name="password"
                                placeholder="Enter your password"
                            />
                            <StyledSection textAlign="right">
                                <Link to="/forgot">Forgot you password?</Link>
                            </StyledSection>
                            <Button type="primary" htmlType="submit" loading={loading} block>
                                Login now
                            </Button>
                        </Form>
                        <Section centered>
                            <p>
                                Need to create an account?{" "}
                                <a href="#" onClick={() => onSetActive && onSetActive("register")}>
                                    Register
                                </a>
                            </p>
                        </Section>
                    </>
                )}
            />
        </StyledSection>
    )
}

const validate = values => {
    const error = {}

    if (!values.email) error.email = "This is required"
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email))
        error.email = "Please provide a valid email address"
    if (!values.password) error.password = "This is required"

    return error
}

// prettier-ignore
export default withRouter(Login)
