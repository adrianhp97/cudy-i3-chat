import React from "react"
import { Card, Icon, Row, Col, Rate } from "antd"
import styled from "styled-components"

const Rating = styled(Rate)`
    && {
        margin-bottom: 1em;
    }
`

const StyledCard = styled(Card)`
    && {
        margin-bottom: 0.8em;
        .ant-card-cover {
            position: absolute;
            width: 100%;
            z-index: 1;
            height: 100%;
            img {
                display: block;
                width: 100%;
                height: 100%;
                object-fit: cover;
                filter: brightness(0.6);
            }
        }

        .ant-card-body {
            z-index: 2;
            position: relative;
            .ant-card-meta-detail {
                margin-bottom: 0;
                .ant-card-meta-title {
                    color: #fff;
                }
                .ant-card-meta-description {
                    color: #ddd;
                }
            }
        }

        .ant-card-actions {
            background: transparent;
            z-index: 2;
            position: relative;
            border-top: none;
            li,
            li span {
                color: #fff;
            }
        }
    }
`

function Classes() {
    return (
        <Row gutter={8}>
            <Col span={12}>
                <StyledCard
                    cover={
                        <img
                            alt="example"
                            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                    }
                    actions={[
                        <span>
                            <Icon type="dollar" /> &nbsp; 250.50
                        </span>,
                        <span>
                            <Icon type="user" /> &nbsp; 25 students
                        </span>,
                        <span>
                            <Icon type="eye" theme="filled" /> &nbsp; 760 views
                        </span>
                    ]}
                >
                    <Rating disabled defaultValue={4} />
                    <Card.Meta title="Card title" description="This is the description" />
                </StyledCard>
            </Col>
            <Col span={12}>
                <StyledCard
                    cover={
                        <img
                            alt="example"
                            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                    }
                    actions={[
                        <span>
                            <Icon type="dollar" /> &nbsp; 250.50
                        </span>,
                        <span>
                            <Icon type="user" /> &nbsp; 25 students
                        </span>,
                        <span>
                            <Icon type="eye" theme="filled" /> &nbsp; 760 views
                        </span>
                    ]}
                >
                    <Rating disabled defaultValue={4} />
                    <Card.Meta title="Card title" description="This is the description" />
                </StyledCard>
            </Col>
            <Col span={12}>
                <StyledCard
                    cover={
                        <img
                            alt="example"
                            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                    }
                    actions={[
                        <span>
                            <Icon type="dollar" /> &nbsp; 250.50
                        </span>,
                        <span>
                            <Icon type="user" /> &nbsp; 25 students
                        </span>,
                        <span>
                            <Icon type="eye" theme="filled" /> &nbsp; 760 views
                        </span>
                    ]}
                >
                    <Rating disabled defaultValue={4} />
                    <Card.Meta title="Card title" description="This is the description" />
                </StyledCard>
            </Col>
            <Col span={12}>
                <StyledCard
                    cover={
                        <img
                            alt="example"
                            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                    }
                    actions={[
                        <span>
                            <Icon type="dollar" /> &nbsp; 250.50
                        </span>,
                        <span>
                            <Icon type="user" /> &nbsp; 25 students
                        </span>,
                        <span>
                            <Icon type="eye" theme="filled" /> &nbsp; 760 views
                        </span>
                    ]}
                >
                    <Rating disabled defaultValue={4} />
                    <Card.Meta title="Card title" description="This is the description" />
                </StyledCard>
            </Col>
        </Row>
    )
}

export default Classes
