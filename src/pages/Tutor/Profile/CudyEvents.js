import React from "react"
import { Calendar } from "components"
import { Badge, Popover, Row, Col } from "antd"
import moment from "moment"
import styled from "styled-components"

const StyledCalendar = styled(Calendar)`
    .ant-fullcalendar-header {
        padding-right: 0;
        > .ant-radio-group {
            display: none;
            visibility: hidden;
        }
    }
`

export default function CudyEvents({ eventData }) {
    const renderDateCell = value => {
        return eventData.map(item => {
            if (item.startDate === moment(value).format("YYYY-MM-DD")) {
                return (
                    <Popover
                        title={item.title}
                        content={
                            <Row>
                                <Col lg={24}>
                                    <p>
                                        From <br /> <span>{item.startDate}</span>
                                    </p>
                                </Col>
                                <Col lg={24}>
                                    <p>
                                        To <br /> <span>{item.endDate}</span>
                                    </p>
                                </Col>
                                <Col lg={24}>
                                    <p>
                                        Location <br /> <span>{item.location}</span>
                                    </p>
                                </Col>
                            </Row>
                        }
                    >
                        <Badge status="success" />
                    </Popover>
                )
            }
        })
    }

    return <StyledCalendar fullscreen={false} dateCellRender={renderDateCell} />
}
