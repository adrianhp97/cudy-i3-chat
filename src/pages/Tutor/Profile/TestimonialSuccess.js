import React, { useEffect } from "react"
import { Row, Col } from "antd"
import { Link, withRouter } from "react-router-dom"
import { Section, Heading, Tick, Button, Card, Alert } from "components"

function TestimonialSuccess({ location }) {
    useEffect(() => {
        const state = location.state || {}
        if (!state.success) location.push("/profile/add_testimonial")
    }, [])

    return (
        <Section style={{ marginTop: "4em" }}>
            <Row type="flex" justify="center" align="middle">
                <Col lg={8}>
                    <Card title="" autoHeight>
                        <Section centered>
                            <Tick />
                            <Heading
                                content="Thank you! :)"
                                subheader="Your testimonial has been submitted. And you are also successfully registered. See the email we've just sent you to get started."
                                marginBottom="3em"
                            />
                            <Alert
                                message="Don't forget to check your spam/junk folder too :)"
                                type="info"
                                showIcon
                            />
                        </Section>
                        <Section centered paddingHorizontal="normal">
                            <Link to="/">
                                <Button type="ghost" icon="home">
                                    Back to Home
                                </Button>
                            </Link>
                        </Section>
                    </Card>
                </Col>
            </Row>
        </Section>
    )
}

export default withRouter(TestimonialSuccess)
