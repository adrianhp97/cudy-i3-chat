import React, { useState } from "react"
import { Section, Calendar, Empty, Button, ButtonLink } from "components"
import { Badge, Popover, Row, Col, Divider, Form } from "antd"
import moment from "moment"
import styled from "styled-components"
import { baseStyles } from "styles/base"
import { Formik } from "formik"
import DateInput from "components/forms/DateInput"
import TextInput from "components/forms/TextInput"
import SelectInput from "components/forms/SelectInput"
import { SubmitButton } from "formik-antd"
import useQueryData from "helpers/hooks/useQueryData"
import { queryFetchSubjects } from "queries/data"

const StyledBadge = styled(Badge)`
    .ant-badge-status-text {
        > p {
            p {
                font-size: 0.9em;
                color: ${baseStyles.greyColor};
                line-height: 1;
            }
        }
    }
`

const PopoverRow = styled(Row)`
    && {
        p {
            font-weight: bold;
            span {
                font-weight: initial;
            }
        }
    }
`

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 }
    }
}

const tailLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 6
        }
    }
}

export default function TutorCalendar({ teachingDate, unauthorizedUser, onEditMode }) {
    const { data = {} } = useQueryData(queryFetchSubjects, "programmeClient")

    const subjectOptions = (data.getProgrammeSubjectList || []).map(item => ({ value: item.pvid, label: item.name }))

    const { editMode, setEditMode } = onEditMode

    const handleAddNew = () => {
        setEditMode("Schedule")
    }

    const renderDateCell = value => {
        return teachingDate.map(item => {
            if (item.startDate === moment(value).format("YYYY-MM-DD")) {
                return (
                    <Popover
                        title={`Teaching ${item.subject}`}
                        content={
                            <PopoverRow>
                                <Col lg={24}>
                                    <p>
                                        From <br /> <span>{item.startDate}</span>
                                    </p>
                                </Col>
                                <Col lg={24}>
                                    <p>
                                        To <br /> <span>{item.endDate}</span>
                                    </p>
                                </Col>
                                <Col lg={24}>
                                    <p>
                                        Location <br /> <span>{item.location}</span>
                                    </p>
                                </Col>
                            </PopoverRow>
                        }
                    >
                        <StyledBadge
                            status="success"
                            text={
                                <p>
                                    {item.subject} <br /> <p>{item.location}</p>
                                </p>
                            }
                        />
                    </Popover>
                )
            }
        })
    }

    const renderEvents = () => {
        if (editMode === "Schedule") {
            return (
                <div>
                    <Row type="flex" justify="center" style={{ marginBottom: "2em" }}>
                        <Col>
                            <Button social="google" icon="google">
                                Import events from GCal
                            </Button>
                        </Col>
                    </Row>
                    <Divider>or</Divider>
                    <Formik
                        render={({ handleSubmit, values }) => (
                            <Form {...formItemLayout} onSubmit={handleSubmit}>
                                <SelectInput
                                    name="subject"
                                    label="Subject you are teaching"
                                    placeholder="Select the subject..."
                                    options={subjectOptions}
                                />
                                <DateInput name="startDate" label="From" placeholder="Choose the date..." />
                                <DateInput
                                    name="endDate"
                                    label="To"
                                    placeholder="Choose the date..."
                                    disabledDate={current => current < moment(values.startDate)}
                                    defaultPickerValue={moment(values.startDate).add(1, "day")}
                                />
                                <TextInput
                                    name="location"
                                    label="Location you are teaching"
                                    placeholder="E.g. Toa Payoh, Dhoby Ghaut, etc..."
                                    extra="It doesn't have to be precise"
                                />
                                <Form.Item {...tailLayout}>
                                    <SubmitButton type="primary" size="large" icon="plus">
                                        Add new event
                                    </SubmitButton>{" "}
                                    &nbsp;{" "}
                                    <ButtonLink size="large" onClick={() => setEditMode("")}>
                                        Cancel
                                    </ButtonLink>
                                </Form.Item>
                            </Form>
                        )}
                    />
                </div>
            )
        }

        if (teachingDate.length === 0) {
            return (
                <Section centered width="50%" style={{ margin: "0 auto" }}>
                    <Empty
                        description={
                            unauthorizedUser
                                ? "This tutor has not set any teaching schedule yet"
                                : "You haven't set any teaching schedule yet"
                        }
                    />
                    <Button type="primary" icon="plus" onClick={handleAddNew}>
                        Add new schedule
                    </Button>
                    <Divider>or</Divider>
                    <Button social="google" icon="google">
                        Import events from GCal
                    </Button>
                </Section>
            )
        }

        return <Calendar dateCellRender={renderDateCell} />
    }

    return <Section paddingHorizontal={0}>{renderEvents()}</Section>
}
