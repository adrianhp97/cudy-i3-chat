import React, { useState } from "react"
import styled from "styled-components"
// prettier-ignore
import { Section, VideoCard, Modal, Heading, Button, Upload, ButtonLink, Empty, Loading } from "components"
import { Col, Row, Icon, message } from "antd"
import ReactPlayer from "react-player"
import { useMutation } from "react-apollo"
import { mutateUploadIntroVideo, mutateDeleteResource } from "queries/profile"
import { client, serverBaseUrl, newClient } from "helpers"
import useError from "helpers/hooks/useError"
import octopus from "assets/images/octopus.png"

const Drag = styled.div`
    text-align: center;
    display: block;
    padding: 2em;
    background: #f3f3f3;
    border-radius: 8px;
    > p {
        font-size: 4em;
    }
`

const HorizontalRow = styled(Row).attrs(() => ({
    type: "flex",
    align: "middle",
    gutter: 16
}))`
    height: 250px;
    margin-bottom: 1.5em;
    flex-wrap: nowrap;
    overflow-x: scroll;
    width: auto;
    -webkit-overflow-scrolling: touch;
    &::-webkit-scrollbar {
        display: none;
    }
`

const StyledModal = styled(Modal).attrs(() => ({
    footer: false,
    closable: false,
    centered: true
}))`
    .ant-modal-body {
        padding: 0;
    }
    .ant-modal-content {
        background: transparent;
        box-shadow: none;
    }
`

const UploadModal = styled(Upload).attrs(() => ({
    centered: true,
    footer: false
}))`
    .ant-upload.ant-upload-select {
        display: block;
    }
`

const StyledButton = styled(Button)`
    &&& {
        span {
            color: rgba(0, 0, 0, 0.25);
        }
    }
`

const EmptyMessage = styled.p`
    span.primary {
        cursor: pointer;
    }
`

function IntroVideos({ data = [], fetchUser, unauthorizedUser, loading, handlers }) {
    const [selectedItem, setSelectedItem] = useState({})
    const [modal, setModal] = useState(false)
    const [isPlaying, setPlaying] = useState(false)
    const [uploadModal, setUploadModal] = useState(false)
    const [localFile, setLocalFile] = useState({ file: "", imagePreview: "" })
    const { setProfileCompletionModal } = handlers

    const [uploadVideo, { loading: loadingUpload }] = useMutation(mutateUploadIntroVideo, {
        client: newClient("resource"),
        onCompleted: () => fetchUser(),
        onError: err => useError(err)
    })

    const [deleteVideo, { loading: loadingDelete }] = useMutation(mutateDeleteResource, {
        client: newClient("resource"),
        onCompleted: () => fetchUser(),
        onError: err => useError(err)
    })

    const handleSelectItem = path => {
        setSelectedItem(path)
        setModal(true)
        setPlaying(true)
    }

    const handleCloseModal = () => {
        setModal(false)
        setPlaying(false)
    }

    const handleUploadToLocal = file => {
        const reader = new FileReader()
        reader.onloadend = () => {
            setLocalFile(prev => ({ ...prev, file, imagePreview: reader.result }))
        }
        reader.readAsDataURL(file)
    }

    const handleUploadVideo = () => {
        uploadVideo({ variables: { file: localFile.file } }).then(() => {
            message.success("Your video has been successfully uploaded!")
        })
    }

    const handleBeforeUpload = file => {
        handleUploadToLocal(file)
        return false
    }

    const handleCloseUploadModal = () => {
        setUploadModal(false)
        setLocalFile({})
    }

    const handleDeleteVideo = item => deleteVideo({ variables: { relativePath: item.path } })

    const renderVideos = () => {
        if (data.length === 0) {
            if (unauthorizedUser) {
                return (
                    <Section centered width="60%" style={{ paddingTop: 0, paddingBottom: 0 }}>
                        <Empty
                            description={
                                <EmptyMessage>
                                    This tutor didn't upload any introduction videos yet. Ask them to{" "}
                                    <span className="primary" onClick={() => setProfileCompletionModal(true)}>
                                        complete this section
                                    </span>
                                </EmptyMessage>
                            }
                        />
                    </Section>
                )
            } else {
                return (
                    <Col lg={24}>
                        <Row type="flex" align="middle" gutter={32}>
                            <Col lg={8} style={{ textAlign: "center" }}>
                                <img src={octopus} width="100%" alt="Upload your introduction videos" />
                            </Col>
                            <Col lg={16}>
                                <Heading
                                    content="Upload your introductions"
                                    subheader="By uploading your introduction videos, there will be a higher chance for you to attract other students/parents."
                                    level={4}
                                    marginBottom="2em"
                                />
                                <Button type="primary" icon="upload" onClick={() => setUploadModal(true)}>
                                    Upload your video now
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                )
            }
        }

        return (
            <>
                <HorizontalRow>
                    {data.map(item => (
                        <Col key={item.id} lg={6}>
                            <VideoCard
                                {...item}
                                // onClick={() => handleSelectItem(item)}
                                onSelectItem={handleSelectItem}
                                onDeleteVideo={handleDeleteVideo}
                                setModal={setModal}
                            />
                        </Col>
                    ))}
                </HorizontalRow>
                <Row>
                    <Col lg={8}>
                        <Button type="dashed" icon="upload" onClick={() => setUploadModal(true)} block>
                            Upload another video
                        </Button>
                    </Col>
                </Row>
            </>
        )
    }

    if (loading || loadingDelete || loadingUpload) return <Loading />

    return (
        <Section paddingHorizontal={0} style={{ overflowX: "hidden" }}>
            <Modal visible={uploadModal} footer={false} onCancel={handleCloseUploadModal}>
                <Heading content="Upload your introduction video" level={4} />
                <div style={{ marginBottom: "2em" }}>
                    {localFile.imagePreview ? (
                        <video controls width="100%">
                            <source src={localFile.imagePreview || ""} />
                        </video>
                    ) : (
                        <UploadModal
                            beforeUpload={handleBeforeUpload}
                            multiple={false}
                            listType="picture"
                            accept="video/*"
                            showUploadList={{ showPreviewIcon: true }}
                            onRemove={() => setLocalFile(prev => ({ ...prev, file: "" }))}
                        >
                            <Drag>
                                <p>
                                    <Icon type="inbox" />
                                </p>
                                <Heading
                                    content="Drag and drop your video to this area"
                                    subheader="Supported formats: .flv, .mp4, .3gp, .avi, .mov, .wmv"
                                    level={4}
                                />
                            </Drag>
                        </UploadModal>
                    )}
                </div>
                <StyledButton
                    type="primary"
                    icon="upload"
                    disabled={!localFile.file}
                    onClick={handleUploadVideo}
                    loading={loadingUpload}
                >
                    {loadingUpload ? "Please wait..." : "Upload this video now"}
                </StyledButton>{" "}
                &nbsp;{" "}
                {localFile.file && (
                    <ButtonLink
                        size="large"
                        icon="close"
                        onClick={() => setLocalFile(prev => ({ ...prev, imagePreview: "" }))}
                    >
                        Change video
                    </ButtonLink>
                )}
            </Modal>
            <StyledModal visible={modal && selectedItem} onCancel={handleCloseModal}>
                <ReactPlayer
                    controls
                    playing={isPlaying}
                    url={serverBaseUrl + selectedItem.path}
                    width="100%"
                    loop={false}
                />
                {/* <video controls width="100%" muted> */}
                {/* <source src="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" /> */}
                {/* <source src={serverBaseUrl + selectedItem.path} /> */}
                {/* </video> */}
            </StyledModal>

            {renderVideos()}
        </Section>
    )
}

export default IntroVideos
