import React, { useState, useEffect } from "react"
import { Section, Heading, ButtonLink, Empty, Loading } from "components"
import { Row, Col, Typography, Form, Input, Button, List, Icon, Checkbox, DatePicker } from "antd"
import Script from "react-load-script"
import styled from "styled-components"
import uuid from "uuid/v4"

import { baseStyles } from "styles/base"
import moment from "moment"
import { useMutation } from "react-apollo"
import { mutateUpdateEducations, mutateBasicProfile, mutateDeleteEducations } from "queries/profile"
import { googleApiKey, mobile } from "helpers"
import TextInput from "components/forms/TextInput"
import { educationOptions } from "store/actions/userActions"

const { Paragraph } = Typography

let autocomplete = null

export default function AboutMe({ data = {}, onEditMode, onUserBio, unauthorizedUser, ...props }) {
    const { fetchUser, loading } = props
    const { location = "", description = "", education = [] } = data
    const { editMode, setEditMode } = onEditMode
    const { userBio = {}, setUserBio } = onUserBio
    const [usersEducation, setUsersEducation] = useState([])
    const [deletedEducations, setDeletedEducations] = useState([])
    const [address, setAddress] = useState({})
    const [addressValue, setAddressValue] = useState(location)

    const [updateEducations] = useMutation(mutateUpdateEducations, educationOptions(setEditMode))
    const [updateDescription] = useMutation(mutateBasicProfile, educationOptions(setEditMode))
    const [deleteEducations] = useMutation(mutateDeleteEducations, educationOptions(setEditMode))

    const handleChange = e => {
        const name = e.target.name
        let theData = { ...userBio }
        theData[name] = e.target.value
        setUserBio(theData)
    }

    const handleDeleteOnLocal = (id, pvid) => {
        const filteredItems = usersEducation.filter(item =>
            item.id ? item.id !== id : item.pvid !== pvid
        )
        const deletedHasPvid = usersEducation
            .filter(item => (id ? item.id === id : item.pvid === pvid))
            .filter(item => item.hasOwnProperty("pvid"))
            .map(item => item.pvid)

        setDeletedEducations(items => [...items, ...deletedHasPvid])
        setUsersEducation(filteredItems)
    }

    const handleAddNewField = () => {
        const defaulty = {
            id: uuid(),
            institution: "",
            fromDate: "2019-01",
            toDate: "2019-01",
            isPresent: false
        }
        setUsersEducation(educations => [...educations, defaulty])
    }

    const handleSubmit = e => {
        e.preventDefault()
        const updatedEd = usersEducation
            .filter(item => item.institution !== "")
            .map(item => {
                const fromDate =
                    (item.fromDate || "").split("-").length < 3
                        ? item.fromDate + "-01"
                        : item.fromDate
                const toDate =
                    (item.toDate || "").split("-").length < 3 ? item.toDate + "-01" : item.toDate
                const { id, ...rest } = item
                return { ...rest, fromDate, toDate }
            })

        deleteEducations({ variables: { pvidList: deletedEducations } }).then(() => fetchUser())
        updateEducations({ variables: { list: updatedEd } }).then(() => fetchUser())
        updateDescription({ variables: { desc: userBio.description, address: addressValue } }).then(
            () => fetchUser()
        )
    }

    const handleCancel = e => {
        e.preventDefault()
        setEditMode("")
        // setUsersEducation(usersEducation)
    }

    const charAllowed = (
        <div>
            <span style={{ color: description && description.length > 680 && baseStyles.redColor }}>
                {description && description.length}
            </span>{" "}
            / 700 characters allowed
        </div>
    )

    const handleLoad = () => {
        const options = {
            types: [],
            componentRestrictions: { country: "SG" }
        }

        /*global google*/
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById("location"),
            options
        )
        autocomplete.addListener("place_changed", handlePlaceSelect)
    }

    const handlePlaceSelect = () => {
        const addressObject = autocomplete.getPlace()
        const userAddress = addressObject.address_components

        if (userAddress) {
            setAddress({
                location: addressObject.formatted_address
            })
            setAddressValue(addressObject.formatted_address)
        }
    }

    const EducationList = ({ unauthorizedUser }) => {
        if (usersEducation.length === 0)
            return (
                <Empty
                    description={
                        unauthorizedUser
                            ? "This tutor haven't set their education yet"
                            : "You haven't set your education yet"
                    }
                />
            )

        return (
            <List
                itemLayout="horizontal"
                dataSource={usersEducation}
                renderItem={item => {
                    const fromDate = `${moment(item.fromDate).format("MMM YYYY")}`
                    const toDate = `${moment(item.toDate).format("MMM YYYY")}`

                    return (
                        <StyledListItem>
                            <List.Item.Meta
                                title={item.institution}
                                description={`${fromDate} - ${item.isPresent ? "present" : toDate}`}
                            />
                        </StyledListItem>
                    )
                }}
            />
        )
    }

    useEffect(() => {
        setUsersEducation(education)
    }, [education])

    if (loading) return <Loading />

    return (
        <Section noPadding marginBottom={0}>
            {editMode === "About me" && !unauthorizedUser ? (
                <>
                    <Row gutter={32}>
                        <Col lg={8}>
                            <Form layout="vertical">
                                <Script url={googleApiKey} onLoad={handleLoad} />
                                <TextInput
                                    independent
                                    allowClear
                                    id="location"
                                    name="address"
                                    placeholder="Your location..."
                                    label="Your location in Singapore"
                                    error={!addressValue}
                                    defaultValue={location}
                                    value={addressValue}
                                    onChange={e => setAddressValue(e.target.value)}
                                />

                                {/* <Form.Item name="qualification" label="Qualification">
                                    <Input
                                        name="qualification"
                                        placeholder="Your qualification"
                                        value={qualification}
                                        onChange={handleChange}
                                    />
                                </Form.Item> */}

                                {usersEducation.map((item, idx, arr) => {
                                    const { fromDate, toDate, institution, pvid, isPresent } = item

                                    const handleChangeValue = name => e => {
                                        let newEducation = [...arr]
                                        if (name === "isPresent") {
                                            newEducation[idx][name] = e.target.checked
                                        } else {
                                            newEducation[idx][name] = e.target.value
                                        }
                                        setUsersEducation(newEducation)
                                        // setUserBio({ ...userBio, education: newEducation })
                                    }

                                    const handleChangeFromTo = name => (value, string) => {
                                        let newEducation = [...arr]
                                        if (name === "toDate" || name === "fromDate")
                                            newEducation[idx][name] = string + "-01"
                                        else newEducation[idx][name] = value
                                        setUsersEducation(newEducation)
                                        // setUserBio({ ...userBio, education: newEducation })
                                    }

                                    return (
                                        <FieldRow key={pvid}>
                                            <span
                                                className="delete"
                                                onClick={() => handleDeleteOnLocal(item.id, pvid)}
                                            >
                                                <Icon type="close" />
                                            </span>
                                            <Form.Item
                                                name="institution"
                                                label="School/college"
                                                style={{ marginBottom: 0 }}
                                            >
                                                <Input
                                                    name="institution"
                                                    placeholder="Your school/college"
                                                    value={institution}
                                                    onChange={handleChangeValue("institution")}
                                                />
                                            </Form.Item>
                                            <Row style={{ marginBottom: "1em" }}>
                                                <Col lg={24}>
                                                    <Checkbox
                                                        name="isPresent"
                                                        onChange={handleChangeValue("isPresent")}
                                                        checked={isPresent}
                                                    >
                                                        I'm currently studying here
                                                    </Checkbox>
                                                </Col>
                                            </Row>

                                            <Row gutter={16}>
                                                <Col lg={12} xs={12}>
                                                    <DatePicker.MonthPicker
                                                        name="fromDate"
                                                        placeholder="Date joined"
                                                        value={moment(fromDate)}
                                                        onChange={handleChangeFromTo("fromDate")}
                                                    />
                                                </Col>
                                                <Col lg={12} xs={12}>
                                                    <DatePicker.MonthPicker
                                                        name="toDate"
                                                        placeholder="Until"
                                                        value={moment(toDate)}
                                                        disabled={isPresent}
                                                        onChange={handleChangeFromTo("toDate")}
                                                    />
                                                </Col>
                                                {/* <Col lg={10} xs={12}>
                                                    <SelectInput
                                                        independent
                                                        name="yearFrom"
                                                        placeholder="Year"
                                                        label="Start year"
                                                        options={yearList}
                                                        value={fromYear}
                                                        marginBottom="0"
                                                        disabled={isPresent}
                                                        onChange={handleChangeFromTo("yearFrom")}
                                                    />
                                                </Col> */}
                                            </Row>
                                        </FieldRow>
                                    )
                                })}
                                <Button
                                    block
                                    type="dashed"
                                    size="medium"
                                    icon="plus"
                                    onClick={handleAddNewField}
                                    style={{ marginBottom: "2em" }}
                                >
                                    Add a new education
                                </Button>
                            </Form>
                        </Col>
                        <Col lg={16}>
                            <Form layout="vertical">
                                <Form.Item
                                    name="description"
                                    label="Tell something short (and sweet) about yourself"
                                    extra={charAllowed}
                                >
                                    <Input.TextArea
                                        rows={10}
                                        name="description"
                                        value={description}
                                        maxLength={700}
                                        placeholder="Short description about me"
                                        onChange={handleChange}
                                    />
                                </Form.Item>
                            </Form>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button type="primary" size="medium" onClick={handleSubmit}>
                                Save changes
                            </Button>{" "}
                            &nbsp; <ButtonLink onClick={handleCancel}>Cancel</ButtonLink>
                        </Col>
                    </Row>
                </>
            ) : (
                <Row type="flex" gutter={16}>
                    <Col lg={8}>
                        <BioHeading
                            content="Location"
                            subheader={addressValue || location || "-"}
                        />
                        {/* <BioHeading content="Qualifications" subheader={qualification} /> */}
                        <BioHeading
                            content="Education"
                            subheader={<EducationList unauthorizedUser={unauthorizedUser} />}
                        />
                    </Col>
                    <Col lg={16}>
                        {unauthorizedUser ? (
                            <Paragraph style={{ lineHeight: 1.8 }}>
                                {description || "This tutor didn't set their description yet"}
                            </Paragraph>
                        ) : (
                            <Paragraph style={{ lineHeight: 1.8 }}>
                                {description || "You haven't set your description yet"}
                            </Paragraph>
                        )}
                    </Col>
                </Row>
            )}
        </Section>
    )
}

const BioHeading = styled(Heading).attrs(() => ({
    reverse: true,
    marginBottom: "2em"
}))``

const StyledListItem = styled(List.Item)`
    && {
        margin: 1em 0 0.5em 0;
        padding-left: 0;
        .ant-list-item-meta-content {
            .ant-list-item-meta-title {
                color: ${baseStyles.greyColor};
                font-size: 14px;
                margin-bottom: 0;
            }
            .ant-list-item-meta-description {
                margin-bottom: 0.5em;
            }
        }
    }
`

const FieldRow = styled.div`
    position: relative;
    margin-bottom: 2em;
    &:hover {
        .delete {
            display: flex;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
    }
    .delete {
        display: none;
        cursor: pointer;
        visibility: hidden;
        position: absolute;
        right: 0;
        border-radius: 50%;
        width: 25px;
        color: #ff9d00;
        height: 25px;
        z-index: 99;
        &:hover {
            background: #eee;
        }
    }
`
