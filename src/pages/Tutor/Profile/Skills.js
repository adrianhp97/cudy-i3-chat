import React, { useState, useEffect } from "react"
import { Section, Button, ButtonLink, Empty, Loading } from "components"
import { Form, Tag, Select } from "antd"
import styled from "styled-components"

import { randomColor, client } from "helpers"
import { coolColors, dummySkills } from "dummy"
import SelectInput from "components/forms/SelectInput"
import { useMutation } from "react-apollo"
import { mutateUpdateSkills, mutateDeleteSkills } from "queries/profile"
import useError from "helpers/hooks/useError"

function Skills({ onEditMode, onSkills, fetchUser, loadingSkills, unauthorizedUser }) {
    const { editMode, setEditMode } = onEditMode
    const { skills = [], setSkills } = onSkills
    const [selectedItems, setSelectedItems] = useState(skills)
    const [deletedItems, setDeletedItems] = useState([])
    const [addedItems, setAddedItems] = useState([])

    const [updateSkills] = useMutation(mutateUpdateSkills, {
        client: client.profileClient,
        onCompleted: data => fetchUser(),
        onError: err => useError(err)
    })
    const [deleteSkills] = useMutation(mutateDeleteSkills, {
        client: client.profileClient,
        onError: err => useError(err)
    })

    const handleChange = value => {
        setAddedItems(arr => [{ title: value, pvid: "" }, ...arr])
    }

    const handleDelete = value => {
        const getItem = skills.find(item => item.title === value) || {}
        setDeletedItems(arr => [getItem.pvid, ...arr])
    }

    const handleSaveSkills = () => {
        setEditMode(false)
        updateSkills({ variables: { list: [...selectedItems, ...addedItems] } })
        deleteSkills({ variables: { pvidList: deletedItems } })
        setSkills(selectedItems)
    }

    const skillOptions = dummySkills.map(item => ({ value: item.title, label: item.title }))
    const filteredOptions = skillOptions.filter(
        item => !selectedItems.map(item => item.title).includes(item.value)
    )

    useEffect(() => {
        setSelectedItems(skills)
    }, [skills])

    // prettier-ignore
    if (loadingSkills) return <Loading />

    return (
        <Section paddingHorizontal={0} marginBottom={0}>
            {editMode === "Skills" && !unauthorizedUser ? (
                <Form layout="vertical">
                    <SelectInput
                        independent
                        name="skills"
                        placeholder="Your skills"
                        mode="multiple"
                        defaultValue={selectedItems.map(item => item.title)}
                        onSelect={handleChange}
                        options={filteredOptions}
                        label="Enter your skills"
                        onDeselect={handleDelete}
                    />
                    <Button type="primary" onClick={handleSaveSkills}>
                        Save changes
                    </Button>{" "}
                    &nbsp;
                    <ButtonLink size="small" onClick={() => setEditMode(false)}>
                        Cancel
                    </ButtonLink>
                </Form>
            ) : (
                <>
                    {unauthorizedUser ? (
                        <Section paddingHorizontal={0} marginBottom={0} centered>
                            {skills.length === 0 ? (
                                <Empty description="This tutor didn't specify any skills yet" />
                            ) : (
                                skills.map(({ pvid, title }) => {
                                    const color = randomColor(coolColors)
                                    return (
                                        <Tag
                                            key={pvid}
                                            color={color}
                                            style={{ marginBottom: ".7em" }}
                                        >
                                            {title}
                                        </Tag>
                                    )
                                })
                            )}
                        </Section>
                    ) : (
                        <>
                            {loadingSkills ? (
                                <Loading />
                            ) : skills.length === 0 ? (
                                <Section paddingHorizontal={0} marginBottom={0} centered>
                                    <Empty description="You didn't specify any skills yet" />
                                    <Button
                                        type="primary"
                                        icon="plus"
                                        onClick={() => setEditMode("Skills")}
                                    >
                                        Add one
                                    </Button>
                                </Section>
                            ) : (
                                skills.map(({ pvid, title }) => {
                                    const color = randomColor(coolColors)
                                    return (
                                        <Tag
                                            key={pvid}
                                            color={color}
                                            style={{ marginBottom: ".7em" }}
                                        >
                                            {title}
                                        </Tag>
                                    )
                                })
                            )}
                        </>
                    )}
                </>
            )}
        </Section>
    )
}

export default Skills
