import React from "react"
import { Modal, Heading, Section } from "components"
import { Formik } from "formik"
import { Form } from "antd"
import SelectInput from "components/forms/SelectInput"
import TextInput from "components/forms/TextInput"
import { SubmitButton } from "formik-antd"
import { profileSections } from "dummy"

export default function _CompleteProfileModal({ section, userData = {}, onModal, ...props }) {
    const { profileCompletionModal, setProfileCompletionModal } = onModal
    const sectionIndex = profileSections.findIndex(item => item.value === section)

    const initialValues = {
        section: (profileSections[sectionIndex] || {}).value,
        message: `Hi`
    }

    const isMale = userData.gender === "M" || userData.gender === "m"

    return (
        <Modal
            {...props}
            visible={profileCompletionModal}
            footer={false}
            onCancel={() => setProfileCompletionModal(false)}
        >
            <Section style={{ paddingLeft: "2em", paddingRight: "2em" }}>
                <Heading
                    bold
                    level={4}
                    marginBottom="2em"
                    content="Request to complete"
                    subheader={`Help ${userData.firstName} to improve ${isMale ? "his" : "her"} Profile by requesting ${
                        isMale ? "him" : "her"
                    } to complete ${isMale ? "his" : "her"} Profile immediately`}
                />
                <Formik
                    initialValues={initialValues}
                    render={({ handleSubmit }) => (
                        <Form layout="vertical" onSubmit={handleSubmit}>
                            <SelectInput
                                name="section"
                                label="Choose section"
                                placeholder="Choose the section..."
                                options={profileSections}
                            />
                            <TextInput
                                textarea
                                name="message"
                                rows={4}
                                label="Your message to them"
                                placeholder="Describe how important their Profile is once it's completed"
                            />
                            <SubmitButton size="large" type="primary" icon="plus">
                                Submit now
                            </SubmitButton>
                        </Form>
                    )}
                />
            </Section>
        </Modal>
    )
}
