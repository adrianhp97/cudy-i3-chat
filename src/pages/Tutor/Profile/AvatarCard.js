import React from "react"
import styled, { css } from "styled-components"
import { Card, Button, Tooltip, ButtonLink, Loading } from "components"
import avatar from "assets/images/avatar.png"
import Cropper from "react-easy-crop"
import { useMutation } from "react-apollo"
import { mutateUploadProfilePicture } from "queries/profile"
import { client, serverBaseUrl, renderAvatar } from "helpers"
import useError from "helpers/hooks/useError"
import { message } from "antd"

const ProfileCard = styled(Card)`
	position: relative;
	.change-picture {
		display: none;
		position: absolute;
		top: 45%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
	.ant-card-cover {
		background-color: #f3f3f3;
		height: 300px;
		max-height: 300px;
		overflow: hidden;
		object-fit: cover;
		height: 100%;
		width: 100%;
		border-radius: 8px 8px 0 0;
	}
	.ant-card-body {
		padding: 2.5em 1.5em;
		text-align: center;
		.ant-card-meta-detail {
			.ant-card-meta-title {
				font-weight: 700;
			}
		}
	}
	&:hover {
		.upload-zone {
			opacity: 1;
			visibility: visible;
		}
	}
`

const UploadButtons = styled.span`
	position: absolute;
	bottom: -10%;
	left: 50%;
	transform: translate(-50%, -50%);
`

const UploadZone = styled.span`
	width: 100%;
	opacity: 0;
	visibility: hidden;
	position: absolute;
	top: 45%;
	left: 50%;
	transform: translate(-50%, -50%);
	input[type="file"] {
		width: 0.1px;
		height: 0.1px;
		opacity: 0;
		overflow: hidden;
		z-index: -1;
		+ label {
			background: #fff;
			cursor: pointer;
			padding: 10px;
			border-radius: 5px;
			transition: all 0.3s ease;
			span {
				font-size: 1.2em;
			}
			&:hover {
				box-shadow: 1px 3px 9px rgba(0, 0, 0, 0.25);
			}
		}
	}
`

const UploadArea = styled.div`
	/* height: 200px; */
	width: 100%;
`

export default function AvatarCard({ data, unauthorizedUser, onShareModal, ...props }) {
	const { userData = {}, theTotal } = data
	const { loading, onSettings, onLocalFile } = props
	const { localFile, setLocalFile } = onLocalFile
	const { shareModal, setShareModal } = onShareModal
	const { pictureSettings, setPictureSettings } = onSettings
	// const [isEditing, setIsEditing] = useState(false)
	// const [zoom, setZoom] = useState(pictureSettings.zoom)
	// const [croppedPixels, setCroppedPixels] = useState({})

	const [uploadAvatar] = useMutation(mutateUploadProfilePicture, {
		client: client.resourceClient,
		onCompleted: data => setLocalFile({}),
		onError: err => useError(err)
	})

	const avatarData = userData.avatar || []
	const address = userData.address ? (
		<a href={`https://www.google.com/maps/dir/?api=1&destination=${userData.address}`} target="_blank">
			{userData.address} &nbsp; &#8599;
		</a>
	) : (
		"No location specified"
	)

	const totalExperience = theTotal === "0.0" ? "No experience yet" : `${parseFloat(theTotal)}y exp `

	// prettier-ignore
	const infoDetail = (
        <span className="info-detail">
            <p css={` margin-bottom: 0; `}>
                {address}
            </p>{" "}
            {totalExperience}
        </span>
    )

	const handleCrop = crop => {
		setPictureSettings(prev => ({ ...prev, crop }))
	}
	const handleZoom = zoom => {
		setPictureSettings(prev => ({ ...prev, zoom }))
	}
	const handleCropComplete = (area, pixels) => {
		// const canvas = document.createElement("canvas")
		// const ctx = canvas.getContext("2d")
		// const image = document.querySelector("#avatar")
		// canvas.setAttribute("width", pixels.width)
		// canvas.setAttribute("height", pixels.height)
		// ctx.drawImage(
		//     image,
		//     pixels.x,
		//     pixels.y,
		//     pixels.width,
		//     pixels.height,
		//     0,
		//     0,
		//     pixels.width,
		//     pixels.height
		// )
		// setCroppedPixels(pixels)
	}

	const handleUpload = () => {
		uploadAvatar({ variables: { file: localFile.file } }).then(() => {
			message.loading("Please wait...").then(() => props.fetchUser())
		})
	}

	const src = localFile.actual ? serverBaseUrl + localFile.actual : localFile.imagePreview || ""
	const avatarSrc = avatarData.length > 0 ? src : avatar

	if (loading)
		return (
			<Card>
				<Loading />
			</Card>
		)

	return (
		<ProfileCard
			height="auto"
			src={avatarSrc}
			title={`${userData.firstName} ${userData.lastName}`}
			description={infoDetail}
			id="avatar"
		>
			{!unauthorizedUser && (
				<UploadArea>
					{localFile.file && localFile.imagePreview ? (
						<>
							<Cropper
								image={localFile.imagePreview}
								crop={pictureSettings.crop}
								zoom={pictureSettings.zoom}
								aspect={pictureSettings.aspect}
								onCropChange={handleCrop}
								onCropComplete={handleCropComplete}
								onZoomChange={handleZoom}
								// onImageLoaded={imageSize => setZoom(300 / imageSize.naturalHeight)}
								restrictPosition={false}
							/>
							<UploadButtons className="upload-buttons">
								<Button type="primary" icon="image" onClick={handleUpload}>
									Upload profile picture
								</Button>{" "}
								&nbsp;{" "}
								<ButtonLink size="large" onClick={() => setLocalFile("")}>
									Cancel
								</ButtonLink>
							</UploadButtons>
						</>
					) : (
						<UploadZone
							className="upload-zone"
							onClick={() => setLocalFile(prev => ({ ...prev, isAvatar: true }))}
						>
							<input
								type="file"
								name="picture"
								id="picture"
								placeholder="Upload..."
								onChange={props.onUploadImage}
							/>
							<label htmlFor="picture">
								<span role="img" aria-label="cowboy">
									🤠
								</span>{" "}
								Change profile picture
							</label>
						</UploadZone>
					)}
				</UploadArea>
			)}
			{!localFile.file && !localFile.imagePreview && (
				<>
					<Tooltip title="This feature will be available soon">
						<Button type="primary" icon="mail" style={{ marginBottom: "1em" }}>
							Send message
						</Button>
					</Tooltip>
					<ButtonLink icon="share-alt" onClick={() => setShareModal(true)}>
						Share {userData.firstName}'s profile
					</ButtonLink>
				</>
			)}
		</ProfileCard>
	)
}
