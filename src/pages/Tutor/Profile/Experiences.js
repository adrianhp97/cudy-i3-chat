import React, { useState, useEffect } from "react"
import { Section, ButtonLink, Empty, Loading } from "components"
import { Form, Button, Row, Col, message, Popconfirm } from "antd"
import uuid from "uuid/v4"
import styled from "styled-components"
import DataList from "./DataList"
import { subjects } from "dummy"
import moment from "moment"
import { useMutation, useQuery } from "@apollo/react-hooks"
import { mutateUpdateExperiences, mutateDeleteExperiences } from "queries/profile"
import { newClient, mobile, client } from "helpers"
import useError from "helpers/hooks/useError"
import { queryFetchCurriculums } from "queries/data"
import SelectInput from "components/forms/SelectInput"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import { DatePicker, SubmitButton } from "formik-antd"
import CheckboxInput from "components/forms/CheckboxInput"

const FieldRow = styled.div`
    position: relative;
    margin-bottom: 1.5em;
    &:hover {
        .delete {
            display: flex;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
    }
    .delete {
        display: none;
        cursor: pointer;
        visibility: hidden;
        position: absolute;
        right: 0;
        border-radius: 50%;
        width: 25px;
        color: #ff9d00;
        height: 25px;
        z-index: 99;
        &:hover {
            background: #eee;
        }
    }
`

export default function Experiences({ onEditMode, onExperiences, unauthorizedUser, ...props }) {
    const { fetchUser, loading } = props
    const { editMode, setEditMode } = onEditMode
    const { experiences, setExperiences } = onExperiences
    const [userExperiences, setUserExperiences] = useState([])
    const [isAdding, setIsAdding] = useState(false)

    const { data: dataCurriculums = {} } = useQuery(queryFetchCurriculums, {
        client: client.programmeClient
    })

    const currOptions = (dataCurriculums.getProgrammeCurriculumList || []).map(item => ({
        value: item.name,
        label: item.name
    }))

    const [updateExperiences] = useMutation(mutateUpdateExperiences, {
        client: newClient(),
        onCompleted: () => {
            setIsAdding(false)
            setEditMode("")
        },
        onError: err => useError(err)
    })

    const [deleteExperiences] = useMutation(mutateDeleteExperiences, {
        client: newClient(),
        onCompleted: () => {
            setEditMode("")
            setIsAdding(false)
        },
        onError: err => useError(err)
    })

    const details = item => {
        const { fromDate = "", toDate = "", institution, isPrivateTutor } = item
        const monthFrom = fromDate.split("-")[1]
        const yearFrom = fromDate.split("-")[0]
        const monthTo = toDate.split("-")[1]
        const yearTo = toDate.split("-")[0]
        const totalExp = moment(`${yearTo} ${monthTo}`).to(`${yearFrom} ${monthFrom}`, true)

        return (
            <section>
                <p style={{ marginBottom: 0 }}>
                    {moment(monthFrom).format("MMM")} {yearFrom} - {moment(monthTo).format("MMM")}{" "}
                    {yearTo} &middot; <span style={{ color: "#999" }}>{totalExp} experience</span>
                </p>
                <span style={{ color: "#999" }}>
                    {isPrivateTutor ? "Private tutor" : institution}
                </span>
            </section>
        )
    }

    const subjectOptions = subjects.filter(item => item.value !== "All")

    const handleAddNewField = () => {
        setIsAdding(true)
        setUserExperiences(exps => [
            ...exps,
            {
                id: uuid(),
                isPrivateTutor: false,
                isPresent: false,
                institution: "",
                level: "Select your level",
                subject: "Select your subject",
                fromDate: "2019-01",
                toDate: "2019-01"
            }
        ])
    }

    const handleSave = (values, { setSubmitting }) => {
        const hasPvid = values.hasOwnProperty("pvid")
        values = {
            ...values,
            level: "0",
            fromDate: moment(values.fromDate).format("YYYY-MM-DD"),
            toDate: moment(values.toDate).format("YYYY-MM-DD")
        }
        if (hasPvid) {
            const { source, ...rest } = values
            values = rest
        }
        updateExperiences({ variables: { list: values } }).then(() => {
            fetchUser()
            message.success(`The experience item has been ${hasPvid ? "updated" : "added"}`, 2)
        })
        setSubmitting(false)
    }

    const handleCancel = () => {
        setUserExperiences(experiences)
        setEditMode("")
        setIsAdding(false)
    }

    const handleDelete = pvid => {
        const filtered = userExperiences.find(item => item.pvid === pvid) || {}
        deleteExperiences({ variables: { pvidList: [filtered.pvid] } }).then(() => {
            fetchUser()
            message.success("The experience item has been deleted", 2)
        })
    }

    useEffect(() => {
        setUserExperiences(experiences)
    }, [experiences])

    if (loading) return <Loading />

    return (
        <Section paddingHorizontal={0} noPadding={mobile}>
            {editMode === "Tutoring experiences" ? (
                isAdding ? (
                    <Formik
                        onSubmit={handleSave}
                        initialValues={{ isPresent: false, isPrivateTutor: false }}
                        render={({ handleSubmit, values }) => (
                            <Form layout="vertical" onSubmit={handleSubmit}>
                                <Row gutter={16} type="flex">
                                    <Col lg={8}>
                                        <TextInput
                                            name="institution"
                                            placeholder="Tuition's name"
                                            label="Tuition's name"
                                            disabled={values.isPrivateTutor}
                                        />
                                    </Col>
                                    <Col lg={8}>
                                        <SelectInput
                                            name="curriculum"
                                            label="Curriculum"
                                            placeholder="Curriculum"
                                            options={currOptions}
                                        />
                                    </Col>
                                    <Col lg={8}>
                                        <SelectInput
                                            name="subject"
                                            label="Subject"
                                            placeholder="Subject"
                                            options={subjectOptions}
                                        />
                                    </Col>
                                </Row>
                                <Row gutter={16} style={{ marginBottom: "2em" }}>
                                    <Col lg={4}>
                                        <DatePicker.MonthPicker
                                            name="fromDate"
                                            label="Date joined"
                                            placeholder="Date joined"
                                            style={{ width: "100%" }}
                                        />
                                    </Col>
                                    <Col lg={4}>
                                        <DatePicker.MonthPicker
                                            name="toDate"
                                            label="Date resigned"
                                            placeholder="Date resigned"
                                            style={{ width: "100%" }}
                                            disabled={values.isPresent}
                                        />
                                    </Col>
                                    <Col lg={8}>
                                        <CheckboxInput name="isPrivateTutor">
                                            I was a private tutor
                                        </CheckboxInput>
                                    </Col>
                                    <Col lg={8}>
                                        <CheckboxInput name="isPresent">
                                            I'm currently working here
                                        </CheckboxInput>
                                    </Col>
                                </Row>
                                <SubmitButton block>Save this experience</SubmitButton> &nbsp;{" "}
                                <ButtonLink block onClick={handleCancel}>
                                    Cancel
                                </ButtonLink>
                            </Form>
                        )}
                    />
                ) : (
                    <>
                        {userExperiences.map(item => (
                            <Formik
                                onSubmit={handleSave}
                                initialValues={item}
                                render={({ handleSubmit, values }) => (
                                    <Form
                                        layout="vertical"
                                        onSubmit={handleSubmit}
                                        style={{ marginBottom: "2em" }}
                                    >
                                        <FieldRow>
                                            <Row gutter={16}>
                                                <Col lg={8}>
                                                    <TextInput
                                                        name="institution"
                                                        label="Tuition's name"
                                                        placeholder="Tuition's name"
                                                        disabled={
                                                            values.isPrivateTutor ||
                                                            item.isPrivateTutor
                                                        }
                                                    />
                                                </Col>
                                                <Col lg={8}>
                                                    <SelectInput
                                                        name="curriculum"
                                                        label="Curriculum"
                                                        placeholder="Curriculum"
                                                        options={currOptions}
                                                    />
                                                </Col>
                                                <Col lg={8}>
                                                    <SelectInput
                                                        name="subject"
                                                        label="Subject"
                                                        placeholder="Subject"
                                                        options={subjectOptions}
                                                    />
                                                </Col>
                                            </Row>
                                            <Row gutter={16} style={{ marginBottom: "2em" }}>
                                                <Col lg={4}>
                                                    <DatePicker.MonthPicker
                                                        name="fromDate"
                                                        label="Date joined"
                                                        placeholder="Date joined"
                                                        style={{ width: "100%" }}
                                                    />
                                                </Col>
                                                <Col lg={4}>
                                                    <DatePicker.MonthPicker
                                                        name="toDate"
                                                        label="Date resigned"
                                                        placeholder="Date resigned"
                                                        disabled={
                                                            values.isPresent || item.isPresent
                                                        }
                                                        style={{ width: "100%" }}
                                                    />
                                                </Col>
                                                <Col lg={8}>
                                                    <CheckboxInput name="isPrivateTutor">
                                                        I was a private tutor
                                                    </CheckboxInput>
                                                </Col>
                                                <Col lg={8}>
                                                    <CheckboxInput name="isPresent">
                                                        I'm currently working here
                                                    </CheckboxInput>
                                                </Col>
                                            </Row>
                                            <SubmitButton type="primary">Update</SubmitButton>{" "}
                                            &nbsp;{" "}
                                            <Popconfirm
                                                title="Delete this experience?"
                                                onConfirm={() => handleDelete(item.pvid)}
                                            >
                                                <ButtonLink icon="close">Delete</ButtonLink>
                                            </Popconfirm>
                                        </FieldRow>
                                    </Form>
                                )}
                            />
                        ))}
                        <Button
                            block
                            type="dashed"
                            size="medium"
                            icon="plus"
                            onClick={handleAddNewField}
                            style={{ marginBottom: "2em" }}
                        >
                            Add a new platform
                        </Button>
                        <ButtonLink block icon="close" onClick={() => setEditMode(false)}>
                            Cancel
                        </ButtonLink>
                    </>
                )
            ) : (
                <>
                    {unauthorizedUser ? (
                        userExperiences.length === 0 ? (
                            <Empty description="This tutor didn't set any tutoring experiences yet" />
                        ) : (
                            userExperiences.map(item => {
                                // prettier-ignore
                                const { institution, curriculum, subject } = item

                                return (
                                    <DataList
                                        key={institution}
                                        title={`${curriculum} (${subject})`}
                                        description={details(item)}
                                    />
                                )
                            })
                        )
                    ) : userExperiences.length === 0 ? (
                        <Empty description="You haven't set any tutoring experiences yet" />
                    ) : (
                        userExperiences.map(item => {
                            // prettier-ignore
                            const { institution, curriculum, subject } = item

                            return (
                                <DataList
                                    key={institution}
                                    title={`${curriculum} (${subject})`}
                                    description={details(item)}
                                />
                            )
                        })
                    )}
                </>
            )}
        </Section>
    )
}
