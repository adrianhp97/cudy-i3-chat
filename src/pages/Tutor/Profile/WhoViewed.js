import React from "react"
import { Layout, Section, Heading, Card, Avatar } from "components"
import { List } from "antd"
import { dummyWhoViewed } from "dummy"
import moment from "moment"
import styled from "styled-components"
import { Link } from "react-router-dom"
import { baseStyles } from "styles/base"
import { mobile } from "helpers"

const StyledList = styled(List)`
    .ant-list-item-meta {
        .ant-list-item-meta-title {
            .name {
                color: ${baseStyles.greyColor};
                span {
                    color: ${baseStyles.greyColor};
                }
            }
            p {
                margin-bottom: 0;
            }
        }
    }
`

export default function WhoViewed() {
    // prettier-ignore
    const startWeek = moment().startOf("isoWeek")
    const endWeek = moment().endOf("isoWeek")

    const getDaysOfWeek = () => {
        let days = []
        let day = startWeek

        while (day <= endWeek) {
            days.push(day.toDate())
            day = day.clone().add(1, "d")
        }

        return days.map(item => moment(item).format("DD MMM YYYY"))
    }

    const thisWeek = getDaysOfWeek()

    return (
        <Layout>
            <Section width={mobile ? "100%" : "50%"} style={{ margin: "0 auto" }}>
                <Heading
                    bold
                    content="Who's viewed you"
                    subheader="See who has viewed your Profile recently."
                    marginBottom="3em"
                    level={4}
                />
                <Card autoHeight>
                    <Section paddingHorizontal={0} marginBottom="padded">
                        <Heading content="Today" level={4} />
                        <StyledList
                            itemLayout="horizontal"
                            dataSource={dummyWhoViewed.filter(
                                item => moment(item.time).format("DD MMM YYYY") === moment().format("DD MMM YYYY")
                            )}
                            renderItem={item => (
                                <List.Item>
                                    <List.Item.Meta
                                        avatar={<Avatar src={item.avatar} />}
                                        title={
                                            <p className="name">
                                                <Link to="/">{item.name}</Link> &nbsp;&bull;&nbsp;{" "}
                                                <span>{moment(item.time).format("DD MMM YYYY")}</span>
                                            </p>
                                        }
                                        description={
                                            <p>
                                                {item.role === "STDN" ? "Student" : "Tutor"} &nbsp;&bull;&nbsp;{" "}
                                                <span>{item.location}</span>
                                            </p>
                                        }
                                    />
                                </List.Item>
                            )}
                        />
                    </Section>

                    <Section paddingHorizontal={0} marginBottom="padded">
                        <Heading content="Yesterday" level={4} />
                        <StyledList
                            itemLayout="horizontal"
                            dataSource={dummyWhoViewed.filter(
                                item =>
                                    moment(item.time).format("DD MMM YYYY") ===
                                    moment()
                                        .subtract(1, "day")
                                        .format("DD MMM YYYY")
                            )}
                            renderItem={item => (
                                <List.Item>
                                    <List.Item.Meta
                                        avatar={<Avatar src={item.avatar} />}
                                        title={
                                            <p className="name">
                                                <Link to="/">{item.name}</Link> &nbsp;&bull;&nbsp;{" "}
                                                <span>{moment(item.time).format("DD MMM YYYY")}</span>
                                            </p>
                                        }
                                        description={
                                            <p>
                                                {item.role === "STDN" ? "Student" : "Tutor"} &nbsp;&bull;&nbsp;{" "}
                                                <span>{item.location}</span>
                                            </p>
                                        }
                                    />
                                </List.Item>
                            )}
                        />
                    </Section>

                    <Section paddingHorizontal={0} marginBottom="padded">
                        <Heading content="This week" level={4} />
                        <StyledList
                            itemLayout="horizontal"
                            dataSource={dummyWhoViewed.filter(item =>
                                thisWeek.includes(moment(item.time).format("DD MMM YYYY"))
                            )}
                            renderItem={item => (
                                <List.Item>
                                    <List.Item.Meta
                                        avatar={<Avatar src={item.avatar} />}
                                        title={
                                            <p className="name">
                                                <Link to="/">{item.name}</Link> &nbsp;&bull;&nbsp;{" "}
                                                <span>{moment(item.time).format("DD MMM YYYY")}</span>
                                            </p>
                                        }
                                        description={
                                            <p>
                                                {item.role === "STDN" ? "Student" : "Tutor"} &nbsp;&bull;&nbsp;{" "}
                                                <span>{item.location}</span>
                                            </p>
                                        }
                                    />
                                </List.Item>
                            )}
                        />
                    </Section>
                </Card>
            </Section>
        </Layout>
    )
}
