import React, { useState } from "react"
import styled from "styled-components"
import Button from "../../../components/Button"
import { message } from "antd"
import { ButtonLink, Loading, Section } from "components"
import { useMutation } from "react-apollo"
import { mutateUploadProfileBanner } from "queries/profile"
import { client, serverBaseUrl, media } from "helpers"
import useError from "helpers/hooks/useError"

const bgUrl = background =>
    background && background.includes(";base64") ? background : serverBaseUrl + background

const Hero = styled.div`
    background: ${({ background }) =>
        background
            ? `url('${bgUrl(background)}') no-repeat`
            : `linear-gradient(45deg, #fed501, #ff9d00);`};
    background-size: cover;
    background-position: center;
    height: 250px;
    margin-bottom: 1em;
    position: relative;
    transition: all 0.2s ease;
    &:hover {
        opacity: 0.8;
        .ant-upload-select,
        .upload-zone,
        .upload-buttons {
            opacity: 1;
            visibility: visible;
        }
    }
    .ant-upload-select {
        right: 110px;
        bottom: 70px;
        position: absolute;
        opacity: 0;
    }
`

const UploadButtons = styled.span`
    position: absolute;
    right: 13%;
    bottom: 28%;
    transform: translateX(30%);
`

const UploadZone = styled.span`
    opacity: 0;
    visibility: hidden;
    position: absolute;
    right: 12%;
    bottom: 28%;
    transform: translateX(30%);
    input[type="file"] {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        z-index: -1;
        + label {
            background: #fff;
            cursor: pointer;
            padding: 10px;
            border-radius: 5px;
            transition: all 0.3s ease;
            span {
                font-size: 1.2em;
            }
            &:hover {
                box-shadow: 1px 3px 9px rgba(0, 0, 0, 0.25);
            }
        }
    }

    ${media.mobile`
        right: 30%;
        transform: translateX(50%);
        width: 100%;
    `}
`

function ProfileCover({
    onChangeCover,
    background,
    listType = "picture",
    accept = "image/*",
    unauthorizedUser,
    fetchUser,
    ...props
}) {
    const { onFile } = props
    const { profileBanner = {}, setProfileBanner } = onFile
    const [uploadBanner] = useMutation(mutateUploadProfileBanner, {
        client: client.resourceClient,
        onCompleted: () => setProfileBanner({}),
        onError: err => useError(err)
    })

    const theBanner = profileBanner.imagePreview || profileBanner.actual

    const handleUpdateBanner = () => {
        uploadBanner({ variables: { file: profileBanner.file } }).then(() => {
            message.loading("Changing your profile banner...").then(() => fetchUser())
        })
    }

    if (props.loading)
        return (
            <Section centered style={{ minHeight: 240 }}>
                <p>Please wait...</p>
            </Section>
        )

    return (
        <Hero background={theBanner} className={background ? "uploaded-image" : "eheheh"}>
            {!unauthorizedUser && (
                <>
                    {profileBanner.file && profileBanner.imagePreview ? (
                        <UploadButtons className="upload-buttons">
                            <Button type="primary" icon="image" onClick={handleUpdateBanner}>
                                Upload profile banner
                            </Button>{" "}
                            &nbsp;{" "}
                            <ButtonLink size="large" onClick={() => setProfileBanner("")}>
                                Cancel
                            </ButtonLink>
                        </UploadButtons>
                    ) : (
                        <UploadZone className="upload-zone">
                            <input
                                type="file"
                                name="picture"
                                id="picture"
                                placeholder="Upload..."
                                onChange={onChangeCover}
                            />
                            <label htmlFor="picture">
                                <span role="img" aria-label="cowboy">
                                    🏞
                                </span>{" "}
                                &nbsp; Change your profile banner
                            </label>
                        </UploadZone>
                    )}
                </>
            )}
        </Hero>
    )
}

export default ProfileCover
