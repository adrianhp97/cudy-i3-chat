import React, { useState, useEffect } from "react"
import { Section, Button, ButtonLink, Empty, Upload, Heading, Alert, Loading } from "components"
import { Form, Row, Col, Input, Select, Icon, Divider, Popconfirm } from "antd"
import * as yup from "yup"
import styled from "styled-components"

import DataList from "./DataList"
import { yearListFrom1980 } from "dummy"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import SelectInput from "components/forms/SelectInput"
import { FormikDebug, SubmitButton } from "formik-antd"
import { useMutation } from "react-apollo"
import {
    mutateAddCertification,
    mutateUploadCertification,
    mutateDeleteCertification
} from "queries/profile"
import { client, mobile } from "helpers"
import useError from "helpers/hooks/useError"

const FieldRow = styled.div`
    position: relative;
    &:hover {
        .delete {
            display: flex;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
    }
    .delete {
        display: none;
        cursor: pointer;
        visibility: hidden;
        position: absolute;
        right: 0;
        border-radius: 50%;
        width: 25px;
        color: #ff9d00;
        height: 25px;
        z-index: 99;
        &:hover {
            background: #eee;
        }
    }
`

const Drag = styled.div`
    text-align: center;
    display: block;
    padding: 2em;
    background: #f3f3f3;
    border-radius: 8px;
    margin-bottom: 2em;
    h4 {
        font-size: 1.2em;
    }
    > p {
        font-size: 4em;
    }
`

function Certifications({ onEditMode, onCertifications, unauthorizedUser, fetchUser, loading }) {
    const { editMode, setEditMode } = onEditMode
    const { certifications, setCertifications } = onCertifications
    const [userCertifications, setUserCertifications] = useState([])
    const [isAdding, setIsAdding] = useState(false)
    const [localFile, setLocalFile] = useState({})

    const yearList = yearListFrom1980.map(item => ({ value: item, label: item }))

    const [addCertification] = useMutation(mutateAddCertification, {
        client: client.profileClient,
        onError: err => useError(err)
    })

    const [uploadCertification, { loading: loadingUpload }] = useMutation(
        mutateUploadCertification,
        {
            client: client.resourceClient,
            onCompleted: data => {
                setLocalFile(prev => ({ ...prev, actual: (data.uploadCertification || {}).path }))
            }
        }
    )

    const [deleteCertification] = useMutation(mutateDeleteCertification, {
        client: client.profileClient,
        onCompleted: () => fetchUser(),
        onError: err => useError(err)
    })

    const handleDelete = pvid => deleteCertification({ variables: { pvid: Number(pvid) } })

    const handleSave = (values, { setSubmitting }) => {
        values = {
            ...values,
            relativePath: localFile.actual,
            issuedDate: values.issuedDate + "/01/01"
        }
        addCertification({ variables: values }).finally(() => {
            setEditMode("")
            setSubmitting(false)
        })
    }

    const handleAddNewField = () => {
        setUserCertifications(certs => [...certs, { name: "", issuer: "", issuedDate: 2019 }])
        setIsAdding(true)
    }

    const handleCancel = () => {
        setEditMode("")
        setIsAdding(false)
        setLocalFile({})
        setUserCertifications(certifications)
    }

    const handleUploadToLocal = file => {
        const reader = new FileReader()
        reader.onloadend = () =>
            setLocalFile(prev => ({ ...prev, file, imagePreview: reader.result }))

        reader.readAsDataURL(file)
        return false
    }

    const handleUploadCert = () => uploadCertification({ variables: { file: localFile.file } })

    useEffect(() => {
        setUserCertifications(certifications)
    }, [certifications])

    if (loading) return <Loading />

    return (
        <Section paddingHorizontal={0} noPadding={mobile} marginBottom={0}>
            {editMode === "Certifications" ? (
                !isAdding ? (
                    <>
                        {userCertifications.map((item, idx) => {
                            const { name, issuer, issuedDate } = item
                            return (
                                <>
                                    <Formik
                                        initialValues={{
                                            name,
                                            issuer,
                                            issuedDate: (issuedDate || "").slice(0, 4)
                                        }}
                                        render={({ handleSubmit }) => (
                                            <Form
                                                layout="vertical"
                                                onSubmit={handleSubmit}
                                                style={{ marginBottom: "2em" }}
                                            >
                                                <FieldRow>
                                                    <TextInput
                                                        name="name"
                                                        placeholder="Name of certification"
                                                        label="Name of certification"
                                                    />
                                                    <Row gutter={16}>
                                                        <Col lg={16}>
                                                            <TextInput
                                                                name="issuer"
                                                                placeholder="Issuer"
                                                                label="Issuer"
                                                            />
                                                        </Col>
                                                        <Col lg={8}>
                                                            <SelectInput
                                                                name="issuedDate"
                                                                placeholder="Year issued"
                                                                options={yearList}
                                                                label="Year issued"
                                                            />
                                                        </Col>
                                                    </Row>
                                                    <SubmitButton type="primary">
                                                        Update certificate
                                                    </SubmitButton>{" "}
                                                    &nbsp;{" "}
                                                    <Popconfirm
                                                        title="Delete this certificate?"
                                                        onConfirm={() => handleDelete(item.pvid)}
                                                    >
                                                        <ButtonLink icon="close">Delete</ButtonLink>
                                                    </Popconfirm>
                                                </FieldRow>
                                            </Form>
                                        )}
                                    />
                                    {idx !== userCertifications.length - 1 && <Divider />}
                                </>
                            )
                        })}
                        <Button
                            block
                            type="dashed"
                            size="medium"
                            icon="plus"
                            onClick={handleAddNewField}
                            style={{ marginBottom: "2em" }}
                        >
                            Add a new certificate
                        </Button>
                        <ButtonLink icon="close" onClick={() => setEditMode(false)}>
                            Cancel
                        </ButtonLink>
                    </>
                ) : (
                    <Formik
                        onSubmit={handleSave}
                        initialValues={{ name: "", issuer: "" }}
                        validationSchema={validationSchema}
                        render={({ handleSubmit }) => (
                            <Form layout="vertical" onSubmit={handleSubmit}>
                                <FieldRow>
                                    {localFile.file ? (
                                        localFile.actual ? (
                                            <Alert
                                                type="success"
                                                showIcon
                                                message="Your certificate is uploaded. Now please fill in the details of the certificate."
                                            />
                                        ) : (
                                            <Button
                                                block
                                                style={{ marginBottom: "2em" }}
                                                type="dashed"
                                                icon="upload"
                                                loading={loadingUpload}
                                                onClick={handleUploadCert}
                                            >
                                                Upload this certificate now
                                            </Button>
                                        )
                                    ) : (
                                        <Upload
                                            beforeUpload={handleUploadToLocal}
                                            multiple={false}
                                            listType="picture"
                                            style={{ marginBottom: "2em" }}
                                        >
                                            <Drag>
                                                <p>
                                                    <Icon type="inbox" />
                                                </p>
                                                <Heading
                                                    content="Drag and drop your certification"
                                                    subheader="Supported formats: .pdf, .doc, .docx, .png, .jpg, .jpeg"
                                                    level={4}
                                                />
                                            </Drag>
                                        </Upload>
                                    )}
                                    <TextInput
                                        name="name"
                                        placeholder="Name of certification"
                                        label="Name of certification"
                                        disabled={!localFile.actual}
                                    />
                                    <Row gutter={16}>
                                        <Col lg={16}>
                                            <TextInput
                                                name="issuer"
                                                placeholder="Issuer"
                                                label="Issuer"
                                                disabled={!localFile.actual}
                                            />
                                        </Col>
                                        <Col lg={8}>
                                            <SelectInput
                                                name="issuedDate"
                                                placeholder="Year issued"
                                                options={yearList}
                                                label="Year issued"
                                                disabled={!localFile.actual}
                                            />
                                        </Col>
                                    </Row>
                                </FieldRow>
                                <SubmitButton type="primary" size="large">
                                    Save changes
                                </SubmitButton>{" "}
                                &nbsp;
                                <ButtonLink size="large" onClick={handleCancel}>
                                    Cancel
                                </ButtonLink>
                            </Form>
                        )}
                    />
                )
            ) : unauthorizedUser ? (
                userCertifications.length === 0 ? (
                    <Empty description="This tutor didn't specify any certifications yet" />
                ) : (
                    userCertifications.map(({ name, issuer, issuedDate, relativePath }) => (
                        <DataList
                            download={relativePath}
                            key={name}
                            title={`${name} (${issuedDate})`}
                            description={issuer}
                            unauthorizedUser={unauthorizedUser}
                        />
                    ))
                )
            ) : userCertifications.length === 0 ? (
                <Empty description="You haven't set any certifications yet" />
            ) : (
                userCertifications.map(({ name, issuer, issuedDate, relativePath }) => (
                    <DataList
                        download={relativePath}
                        key={name}
                        title={`${name} (${issuedDate})`}
                        description={issuer}
                    />
                ))
            )}
        </Section>
    )
}

const validationSchema = yup.object().shape({
    name: yup.string().required("You have to tell the name of the certificate"),
    issuer: yup.string().required("You have to tell the issuer"),
    issuedDate: yup.string().required("You have to tell when the certificate is issued")
})

export default Certifications
