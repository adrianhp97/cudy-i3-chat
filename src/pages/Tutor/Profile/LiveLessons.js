import React from "react"
import Section from "../../../components/Section"
import { Col, Row, Tag } from "antd"
import Card from "../../../components/Card"
import styled from "styled-components"

const StyledCard = styled(Card)`
    .ant-card-meta-detail {
        margin-bottom: 0;
    }
    .ant-card-cover {
        overflow: hidden;
    }

    .ant-card-body {
        .ant-tag {
            position: absolute;
            right: 0;
            top: 55%;
        }
    }
`

function LiveLessons(props) {
    return (
        <Section noPadding>
            <Row gutter={16}>
                {props.lessons.map(item => (
                    <Col span={8} key={item.id}>
                        <StyledCard hoverable {...item}>
                            <Tag color="black">{item.duration}</Tag>
                        </StyledCard>
                    </Col>
                ))}
            </Row>
        </Section>
    )
}

export default LiveLessons
