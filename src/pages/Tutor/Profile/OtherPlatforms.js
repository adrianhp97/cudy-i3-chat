import React, { useState, useEffect } from "react"
import { Section, Avatar, Button, ButtonLink, ResultSection, Empty, Loading } from "components"
import { Form, List, Popconfirm, message } from "antd"
import styled from "styled-components"
import useQueryData from "helpers/hooks/useQueryData"
import { queryOtherPlatforms } from "queries/data"
import SelectInput from "components/forms/SelectInput"
import { useMutation } from "react-apollo"
import { mutateAddOtherPlatform, mutateDeleteOtherPlatform } from "queries/profile"
import { client, mobile } from "helpers"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import { SubmitButton } from "formik-antd"
import useError from "helpers/hooks/useError"

const FieldRow = styled.div`
    position: relative;
    margin-bottom: 2em;
    &:hover {
        .delete {
            display: flex;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
    }
    .delete {
        display: none;
        cursor: pointer;
        visibility: hidden;
        position: absolute;
        right: 0;
        border-radius: 50%;
        width: 25px;
        color: #ff9d00;
        height: 25px;
        z-index: 99;
        &:hover {
            background: #eee;
        }
    }
`

export default function OtherPlatforms({ onOtherPlat, onEditMode, unauthorizedUser, ...props }) {
    const { loading, fetchUser } = props
    const { editMode, setEditMode } = onEditMode
    const { userOtherPlat, setUserOtherPlat } = onOtherPlat
    const [otherPlats, setOtherPlats] = useState([])
    const [isAdding, setIsAdding] = useState(false)

    const {
        data: { getOtherPlatformMasterList = [] }
    } = useQueryData(queryOtherPlatforms, "profileClient")

    const [addOtherPlatform] = useMutation(mutateAddOtherPlatform, {
        client: client.profileClient,
        onCompleted: () => {
            setIsAdding(false)
            setEditMode(false)
        },
        onError: (err, network) => {
            useError(err)
            console.log({ network })
        }
    })

    const [deleteOtherPlatform] = useMutation(mutateDeleteOtherPlatform, {
        client: client.profileClient,
        onCompleted: data => {
            setIsAdding(false)
            setEditMode(false)
        },
        onError: (err, network) => {
            useError(err)
            console.log({ network })
        }
    })

    const otherPlatOptions = getOtherPlatformMasterList.map(item => ({
        value: item.pvid,
        label: item.name
    }))

    const handleCancel = () => {
        setIsAdding(false)
        setEditMode(false)
        setOtherPlats(userOtherPlat)
    }

    const handleAddOtherPlatform = (values, { setSubmitting }) => {
        const hasPvid = values.hasOwnProperty("pvid")
        if (hasPvid) {
            const { source, ...rest } = values
            values = rest
        }
        addOtherPlatform({ variables: { list: values } }).then(() => {
            fetchUser()
            message.success(`The platform item has been ${hasPvid ? "updated" : "added"}`, 2)
        })
        setSubmitting(false)
    }

    const handleDelete = pvid => {
        const filtered = otherPlats.find(item => item.pvid === pvid) || {}
        deleteOtherPlatform({ variables: { pvidList: [filtered.pvid] } }).then(() => {
            fetchUser()
            message.success("The platform item has been deleted", 2)
        })
    }

    const renderData = () => {
        if (unauthorizedUser) {
            return otherPlats.length === 0 ? (
                <Empty description="This tutor is exclusive to Cudy" />
            ) : (
                <List
                    itemLayout="horizontal"
                    dataSource={otherPlats}
                    renderItem={({ source = {}, link }) => (
                        <List.Item>
                            <List.Item.Meta
                                avatar={<Avatar src={source.iconLink || ""} size="large" />}
                                title={source.name || ""}
                                description={
                                    <a href={link} target="__blank">
                                        See my profile &#8599;
                                    </a>
                                }
                            />
                        </List.Item>
                    )}
                />
            )
        }

        return otherPlats.length === 0 ? (
            <Empty description="You haven't set any platforms yet" />
        ) : (
            <List
                itemLayout="horizontal"
                dataSource={otherPlats}
                renderItem={({ source = {}, link }) => (
                    <List.Item>
                        <List.Item.Meta
                            avatar={<Avatar src={source.iconLink || ""} size="large" />}
                            title={source.name || ""}
                            description={
                                <a href={link} target="__blank">
                                    See my profile &#8599;
                                </a>
                            }
                        />
                    </List.Item>
                )}
            />
        )
    }

    const handleAddNewField = () => {
        setIsAdding(true)
        setOtherPlats(otherPlatforms => [...otherPlatforms, { otherPlatformPvid: "", link: "" }])
    }

    useEffect(() => {
        setOtherPlats(userOtherPlat)
    }, [userOtherPlat])

    if (loading) return <Loading />

    return (
        <Section paddingHorizontal={0} noPadding={mobile} marginBottom={0}>
            {/* <NotAvailable title="This feature will be available soon" /> */}

            {editMode === "Other Platforms" ? (
                isAdding ? (
                    <Formik
                        onSubmit={handleAddOtherPlatform}
                        render={({ handleSubmit }) => (
                            <Form
                                layout="vertical"
                                onSubmit={handleSubmit}
                                style={{ marginBottom: "2em" }}
                            >
                                <SelectInput
                                    name="otherPlatformPvid"
                                    label="Name of platform"
                                    placeholder="Name of platform"
                                    options={otherPlatOptions}
                                    marginBottom="10px"
                                />
                                <TextInput
                                    name="link"
                                    label="Profile's URL"
                                    placeholder="Your profile's URL"
                                />
                                <SubmitButton block>Save this platform</SubmitButton> &nbsp;{" "}
                                <ButtonLink block onClick={handleCancel}>
                                    Cancel
                                </ButtonLink>
                            </Form>
                        )}
                    />
                ) : (
                    <>
                        {otherPlats.map(item => (
                            <Formik
                                onSubmit={handleAddOtherPlatform}
                                initialValues={item}
                                render={({ handleSubmit }) => (
                                    <FieldRow>
                                        <Form layout="vertical" onSubmit={handleSubmit}>
                                            <SelectInput
                                                name="otherPlatformPvid"
                                                label="Name of platform"
                                                placeholder="Name of platform"
                                                options={otherPlatOptions}
                                                marginBottom="10px"
                                            />
                                            <TextInput
                                                name="link"
                                                label="Profile's URL"
                                                placeholder="Your profile's URL"
                                            />
                                        </Form>
                                        <SubmitButton type="primary">Update</SubmitButton> &nbsp;{" "}
                                        <Popconfirm
                                            title="Delete this platform?"
                                            onConfirm={() => handleDelete(item.pvid)}
                                        >
                                            <ButtonLink icon="close">Delete</ButtonLink>
                                        </Popconfirm>
                                    </FieldRow>
                                )}
                            />
                        ))}
                        <Button
                            block
                            type="dashed"
                            size="medium"
                            icon="plus"
                            onClick={handleAddNewField}
                            style={{ marginBottom: "2em" }}
                        >
                            Add a new platform
                        </Button>
                        <ButtonLink block icon="close" onClick={() => setEditMode(false)}>
                            Cancel
                        </ButtonLink>
                    </>
                )
            ) : (
                renderData()
            )}
        </Section>
    )
}
