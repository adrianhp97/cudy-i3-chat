import React from "react"
import { Heading, ButtonLink } from "components"
import { Row, Col } from "antd"
import styled from "styled-components"
import { mobile, serverBaseUrl } from "helpers"
import { baseStyles } from "styles/base"

const StyledHeading = styled(Heading)`
    &:not(:last-child) {
        border-bottom: 1px solid #eee;
        padding-bottom: 1em;
    }
`

const StyledRow = styled(Row)`
    &:not(:last-child) {
        border-bottom: 1px solid #eee;
        margin-bottom: 1em;
    }
`

const HeadingExtra = styled(Heading)`
    && {
        h4 {
            font-size: 1em;
            color: ${baseStyles.greyColor};
        }
    }
`

const token = localStorage.getItem("token")

export default function DataList({ title = "", description = "", download = "", extra = "" }) {
    if (download) {
        return (
            <StyledRow type="flex" justify="space-between">
                <Col xs={24} lg={12}>
                    <Heading reverse content={title} subheader={description} />
                </Col>
                <Col xs={24} lg={12} style={{ textAlign: mobile ? "left" : "right" }}>
                    {token && (
                        <a href={serverBaseUrl + download} target="_blank">
                            <ButtonLink icon="eye" style={{ marginRight: 0 }}>
                                View
                            </ButtonLink>
                        </a>
                    )}
                </Col>
            </StyledRow>
        )
    }

    if (extra)
        return (
            <StyledRow type="flex" justify="space-between" align="middle">
                <Col xs={24} lg={12}>
                    <Heading reverse content={title} subheader={description} />
                </Col>
                <Col xs={24} lg={12} style={{ textAlign: mobile ? "left" : "right" }}>
                    <HeadingExtra content={extra} level={4} />
                </Col>
            </StyledRow>
        )

    return <StyledHeading reverse content={title} subheader={description} />
}
