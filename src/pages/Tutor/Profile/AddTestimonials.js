import React, { useState, useEffect } from "react"
import { Section, Heading, ButtonLink, Logo, Modal } from "components"
import { Form, Row, Col, message } from "antd"
import TextInput from "components/forms/TextInput"
import SelectInput from "components/forms/SelectInput"
import { useQuery, useMutation } from "@apollo/react-hooks"
import { queryFetchSubjects } from "queries/data"
import { newClient, client, baseUrl } from "helpers"
import { Formik } from "formik"
import socketClient from "socket.io-client"
import { Rate, SubmitButton, FormItem } from "formik-antd"
import { withRouter } from "react-router"
import CheckboxInput from "components/forms/CheckboxInput"
import { mutateAddTestimonial } from "queries/profile"
import useError from "helpers/hooks/useError"
import { notifyMe } from "helpers/notification"

const token = localStorage.getItem("token")
const userDetails = JSON.parse(localStorage.getItem("userDetails")) || {}

const AddTestimonials = React.memo(({ location, history }) => {
    const [registerModal, setRegisterModal] = useState(false)
    const [tempInfo, setTempInfo] = useState({})

    const { data: dataSubjects = {} } = useQuery(queryFetchSubjects, {
        client: client.programmeClient
    })

    const subjectOptions = (dataSubjects.getProgrammeSubjectList || []).map(item => ({
        value: Number(item.pvid),
        label: item.name
    }))

    const [addTestimonial] = useMutation(mutateAddTestimonial, { client: newClient() })

    const params = new URLSearchParams(location.search)
    const tutorCode = params.get("tutor_code") || ""
    const referralCode = params.get("referral_code") || ""

    const handleAddTestimonial = (values, { setSubmitting }) => {
        setSubmitting(true)
        const { tc, ...newUserValues } = values
        values = {
            ...newUserValues,
            referralCode
        }
        if (!token) {
            setRegisterModal(true)
            setTempInfo(values)
        } else {
            addTestimonial({
                variables: { ...values, email: userDetails.email, reviewerPvid: Number(userDetails.pvid) }
            })
                .then(() => {
                    message.loading("Please wait...")

                    // .then(() => {
                    //     history.push({
                    //         pathname: "/profile/add_testimonial/success",
                    //         state: { success: true }
                    //     })
                    // })
                })
                .catch(err => useError(err))
        }
        setSubmitting(false)
    }

    const handleSubmitTestimonial = (values, { setSubmitting }) => {
        setSubmitting(true)
        const { tc, ...newUserValues } = values
        values = {
            ...tempInfo,
            reviewerPvid: Number(userDetails.pvid) || 0,
            newUser: newUserValues
        }
        addTestimonial({ variables: values })
            .then(() => {
                message.loading("Please wait...").then(() => {
                    history.push({
                        pathname: "/profile/add_testimonial/success",
                        state: { success: true }
                    })
                })
            })
            .catch(err => useError(err))
        setSubmitting(false)
    }

    return (
        <Section paddingHorizontal={0}>
            <Modal centered visible={registerModal} onCancel={() => setRegisterModal(false)} footer={null}>
                <Section paddingHorizontal={0}>
                    <Heading
                        content="One more step..."
                        subheader="We need a few details about yourself"
                        level={4}
                        marginBottom="3em"
                    />
                    <Formik
                        onSubmit={handleSubmitTestimonial}
                        initialValues={{ firstName: "", lastName: "" }}
                        validate={validateRegister}
                        render={({ handleSubmit }) => (
                            <Form layout="vertical" onSubmit={handleSubmit}>
                                <Row gutter={16}>
                                    <Col lg={12}>
                                        <TextInput
                                            name="firstName"
                                            placeholder="Your first name"
                                            label="Your first name"
                                        />
                                    </Col>
                                    <Col lg={12}>
                                        <TextInput
                                            name="lastName"
                                            placeholder="Your last name"
                                            label="Your last name"
                                        />
                                    </Col>
                                </Row>
                                <Row style={{ marginBottom: "3em" }}>
                                    <Col lg={24}>
                                        <TextInput
                                            name="email"
                                            type="email"
                                            placeholder="E.g. jennysalim@example.com"
                                            label="Your email"
                                        />
                                        <CheckboxInput name="tc">
                                            I have read and agreed to the{" "}
                                            <a href="https://www.cudy.co/tnc" target="_blank" rel="noreferrer noopener">
                                                terms and conditions
                                            </a>
                                            , as well as the{" "}
                                            <a
                                                href="https://www.cudy.co/privacy"
                                                target="_blank"
                                                rel="noreferrer noopener"
                                            >
                                                privacy policy
                                            </a>{" "}
                                            of the site.
                                        </CheckboxInput>
                                    </Col>
                                </Row>
                                <SubmitButton size="large">Register and add testimonial</SubmitButton> &nbsp;{" "}
                                <ButtonLink size="large" onClick={() => setRegisterModal(false)}>
                                    Cancel
                                </ButtonLink>
                            </Form>
                        )}
                    />
                </Section>
            </Modal>
            <Row type="flex" justify="center" style={{ marginTop: "3em" }}>
                <Col lg={10}>
                    <Logo onClick={() => history.push("/")} mb="3em" />
                    <Heading
                        content="Add testimonials"
                        subheader="Say what you'd like to say about the tutor"
                        marginBottom="3em"
                    />
                    <Formik
                        onSubmit={handleAddTestimonial}
                        validate={validate}
                        initialValues={{ programmeSubjectPvid: "", comment: "" }}
                        render={({ handleSubmit }) => (
                            <Form layout="vertical" onSubmit={handleSubmit} style={{ marginBottom: "3em" }}>
                                <SelectInput
                                    name="programmeSubjectPvid"
                                    label="Subject they teached"
                                    placeholder="Choose subject they teached"
                                    options={[{ value: "", label: "Select one option..." }, ...subjectOptions]}
                                />
                                <FormItem name="rating" label="Rate your experience with the tutor">
                                    <Rate name="rating" allowHalf={true} allowClear={true} />
                                </FormItem>
                                <TextInput
                                    textarea
                                    name="comment"
                                    label="Your testimonials"
                                    placeholder="Please be good, and give them a good comment/review"
                                    rows={5}
                                />
                                <SubmitButton type="primary" size="large">
                                    Add your testimonial
                                </SubmitButton>
                                &nbsp;{" "}
                            </Form>
                        )}
                    />
                    <Section centered>
                        <ButtonLink size="large" onClick={() => history.push("/")}>
                            Cancel and go to Home
                        </ButtonLink>
                    </Section>
                </Col>
            </Row>
        </Section>
    )
})

const validate = values => {
    let errors = {}

    if (!values.programmeSubjectPvid) errors.programmeSubjectPvid = "You have to choose the subject"
    if (!values.rating) errors.rating = "Don't forget the rating"
    if (!values.comment) errors.comment = "Don't forget the review"

    return errors
}

const validateRegister = values => {
    let errors = {}

    if (!values.firstName) errors.firstName = "Don't forget your first name"
    if (!values.lastName) errors.lastName = "Don't forget your last name"
    if (!values.email) errors.email = "Don't forget the email"
    if (!values.tc) errors.tc = "You have to agree first"

    return errors
}

export default withRouter(AddTestimonials)
