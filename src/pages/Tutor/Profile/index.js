import React, { useState, useEffect } from "react"
// prettier-ignore
import { Layout, Section, Card, Heading, ButtonLink, List, Tooltip, Empty, Button, Loading } from "components"
import { Row, Col, Icon, Typography } from "antd"
import ProfileCover from "./ProfileCover"
import styled from "styled-components"
import IntroVideos from "./IntroVideos"
import Subjects from "./Subjects"
import AboutMe from "./AboutMe"
import Skills from "./Skills"
import OtherPlatforms from "./OtherPlatforms"
import Certifications from "./Certifications"
import Experiences from "./Experiences"
import moment from "moment"
import { useMutation } from "react-apollo"
import {
    mutateUserProfile,
    mutateFetchTestimonials,
    mutateFetchTestimonialsById,
    queryReferralLink,
    mutateUserByToken,
    mutateFetchTestimonialsByToken
} from "queries/profile"
import { mobile, newClient, media } from "helpers"
import { withRouter, Link } from "react-router-dom"
import ShareModal from "./ShareModal"
import AvatarCard from "./AvatarCard"
import decorating from "assets/images/decorating.png"
import { dummyTestimonials, teachingDate, eventData } from "dummy"
import referralMail from "assets/images/referral_mail.png"
import whoViewed from "assets/images/who-viewed.png"
import useQueryData from "helpers/hooks/useQueryData"
import TutorCalendar from "./Calendar"
import CudyEvents from "./CudyEvents"
import CompleteProfileModal from "./_CompleteProfileModal"
import Chat from "components/common/Chat"

const userDetails = JSON.parse(localStorage.getItem("userDetails")) || {}
const token = localStorage.getItem("token")

const url = "https://sg.cudy.co"

const CardTitle = ({ title, description = "", onEditMode, noEdit, unauthorizedUser }) => (
    <StyledCardTitle className="card-title">
        <Heading content={title} subheader={description} level={4} />
        {unauthorizedUser || noEdit ? null : (
            <span className="icon" onClick={() => onEditMode(title)}>
                <Icon type="edit" />
            </span>
        )}
    </StyledCardTitle>
)

function TutorProfile({ match, history, peopleProfile }) {
    const [editMode, setEditMode] = useState("")
    const [subjects, setSubjects] = useState([])
    const [userBio, setUserBio] = useState({})
    const [skills, setSkills] = useState([])
    const [userOtherPlat, setUserOtherPlat] = useState([])
    const [certifications, setCertifications] = useState([])
    const [experiences, setExperiences] = useState([])
    const [testimonials, setTestimonials] = useState([])
    const [introVideos, setIntroVideos] = useState([])
    const [shareModal, setShareModal] = useState(false)
    const [profileCompletionModal, setProfileCompletionModal] = useState(false)
    const [unauthorizedUser, setUnauthorizedUser] = useState(false)
    const [profileBanner, setProfileBanner] = useState({ file: "", imagePreview: "", actual: "" })
    const [localFile, setLocalFile] = useState({
        file: "",
        imagePreview: "",
        isAvatar: false,
        actual: ""
    })
    const [pictureSettings, setPictureSettings] = useState({
        crop: { x: 0, y: 0 },
        zoom: 1,
        aspect: 3 / 3
    })
    const [chatParentPvid, setChatParentPvid] = useState(1)

    const userParams = match.params || {}

    const [fetchUser, { data = {}, loading }] = useMutation(mutateUserProfile, {
        client: newClient(),
        onCompleted: data => {
            data = data.getProfile || {}
            const userBio = {
                education: data.educationList,
                location: data.address,
                description: data.description
            }
            setSubjects(data.subjectList)
            setUserBio(userBio)
            setExperiences(data.experienceList)
            setSkills(data.skillList)
            setCertifications(data.certificateList)
            setIntroVideos(data.videoIntroductionList)
            setUserOtherPlat(data.otherPlatformList)
            setProfileBanner(prev => ({ ...prev, actual: (data.banner[0] || {}).path }))
            setLocalFile(prev => ({ ...prev, actual: (data.avatar[0] || {}).path }))
        },
        onError: err => {
            const error = err.graphQLErrors[0] || {}
            if (error.statusCode === 401) {
                setUnauthorizedUser(true)
            }
        }
    })

    const [fetchUserByToken, { data: dataByToken = {}, loading: loadingByToken }] = useMutation(mutateUserByToken, {
        client: newClient(),
        onCompleted: data => {
            data = data.getProfile || {}
            const userBio = {
                education: data.educationList,
                location: data.address,
                description: data.description
            }
            setSubjects(data.subjectList)
            setUserBio(userBio)
            setExperiences(data.experienceList)
            setSkills(data.skillList)
            setCertifications(data.certificateList)
            setIntroVideos(data.videoIntroductionList)
            setUserOtherPlat(data.otherPlatformList)
            setProfileBanner(prev => ({ ...prev, actual: (data.banner[0] || {}).path }))
            setLocalFile(prev => ({ ...prev, actual: (data.avatar[0] || {}).path }))
        },
        onError: err => {
            const error = err.graphQLErrors[0] || {}
            if (error.statusCode === 401) {
                setUnauthorizedUser(true)
            }
        }
    })

    const [fetchTestimonials] = useMutation(mutateFetchTestimonials, {
        client: newClient(),
        onCompleted: (data = {}) => {
            data = data.getTestimonialList || []
            setTestimonials(data)
        }
    })

    const [fetchTestiByToken] = useMutation(mutateFetchTestimonialsByToken, {
        client: newClient(),
        onCompleted: (data = {}) => {
            data = data.getTestimonialList || []
            setTestimonials(data)
        }
    })

    const {
        data: { referralLink }
    } = useQueryData(queryReferralLink, "profileClient")

    const userData = peopleProfile ? dataByToken.getProfile || {} : data.getProfile || {}

    const handleEditMode = title => setEditMode(title)

    const handleUploadImage = e => {
        e.preventDefault()

        const reader = new FileReader()
        const file = e.target.files[0]

        const isAvatar = localFile.isAvatar
        const handlers = isAvatar ? setLocalFile : setProfileBanner

        reader.onloadend = () => {
            handlers(prev => ({ ...prev, file, imagePreview: reader.result }))
        }

        reader.readAsDataURL(file)
    }

    const experienceTotal = experiences.map(item => {
        const fromYear = (item.fromDate || "").split("-")[0]
        const fromMonth = (item.fromDate || "").split("-")[1]
        const toYear = (item.toDate || "").split("-")[0]
        const toMonth = (item.toDate || "").split("-")[1]

        let startTime = moment(`${Number(fromYear)} ${Number(fromMonth)}`)
        let endTime = moment(`${Number(toYear)} ${Number(toMonth)}`)

        let totalSec = endTime.diff(startTime, "months")

        return totalSec
    })

    const theTotal = (experienceTotal.reduce((acc, curr) => acc + curr, 0) / 12).toFixed(1)
    const experiencesDescription = theTotal === "0.0" ? "No experience yet" : `${parseFloat(theTotal)}y exp `

    const tutorTestimonials = (token ? testimonials : dummyTestimonials).map(({ reviewer = {}, ...item }) => ({
        title: reviewer.firstName + " " + reviewer.lastName,
        description: item.comment,
        extra: item.postedDate
    }))

    const renderTestimonials = () => {
        if (!token)
            return (
                <>
                    <BlurredCard autoHeight noHover>
                        <Row type="flex" justify="center" align="middle">
                            <Col lg={12} xs={24}>
                                <img
                                    src={decorating}
                                    width={mobile ? "100%" : "130"}
                                    alt="Register first to see testimonials"
                                />
                            </Col>
                            <Col lg={12} xs={24}>
                                <Heading
                                    level={4}
                                    content="Login to see"
                                    subheader="You can see testimonials of this tutor, if you are already logged in. Go login now."
                                    marginBottom="2em"
                                />
                                <Link to="/login">
                                    <Button type="primary">Login now</Button>
                                </Link>
                            </Col>
                        </Row>
                    </BlurredCard>
                    <StyledList data={tutorTestimonials} type="review" />
                </>
            )

        if (testimonials.length === 0)
            return (
                <Empty
                    description={
                        unauthorizedUser
                            ? "This tutor has no testimonials yet"
                            : "You haven't gained any testimonials yet"
                    }
                />
            )

        return <StyledList data={tutorTestimonials} type="review" />
    }

    useEffect(() => {
        if (token && userDetails.token === userData.token) {
            history.push("/profile/me")
            if (unauthorizedUser) setUnauthorizedUser(false)
            fetchUser()
            fetchTestimonials({ variables: { limit: 3 } })
        }

        if (peopleProfile) {
            if (!unauthorizedUser) setUnauthorizedUser(true)
            fetchUserByToken({ variables: { code: userParams.token || 0 } })
            fetchTestiByToken({ variables: { token: userParams.token || "", limit: 3 } })
        }

        if ((!peopleProfile && !token) || (!token && (userDetails.role || {}).code === "STDN")) {
            history.push("/404")
        }

        if (unauthorizedUser) setUnauthorizedUser(false)
        fetchUser()
        fetchTestimonials({ variables: { limit: 3 } })
    }, [])

    return (
        <Layout customBread={peopleProfile && userData.firstName + " " + userData.lastName}>
            <CompleteProfileModal onModal={{ profileCompletionModal, setProfileCompletionModal }} userData={userData} />
            <ShareModal userData={userData} onModal={{ shareModal, setShareModal }} />
            <Chat
                data={{
                    receiver: userData,
                    sender: userDetails,
                    chatParentPvid
                }}
            />
            <Row>
                <Col lg={24}>
                    <ProfileCover
                        onChangeCover={handleUploadImage}
                        unauthorizedUser={unauthorizedUser}
                        onFile={{ profileBanner, setProfileBanner }}
                        fetchUser={fetchUser}
                        loading={loadingByToken || loading}
                    />
                </Col>
            </Row>
            <Section paddingHorizontal="very" style={{ position: "relative", top: "-80px" }}>
                <Row gutter={32}>
                    <Col lg={6} order={mobile ? 2 : 1}>
                        <AvatarCard
                            data={{ userData, theTotal }}
                            unauthorizedUser={unauthorizedUser}
                            onUploadImage={{ handleUploadImage }}
                            onShareModal={{ shareModal, setShareModal }}
                            onLocalFile={{ localFile, setLocalFile }}
                            onSettings={{ pictureSettings, setPictureSettings }}
                            fetchUser={fetchUser}
                            loading={loadingByToken || loading}
                        />

                        {!unauthorizedUser && (
                            <MainCard title={<CardTitle noEdit />}>
                                <Row gutter={16} type="flex" align="middle">
                                    <Col lg={6}>
                                        <img src={whoViewed} alt="Who's viewed me?" width="100%" />
                                    </Col>
                                    <Col lg={18}>
                                        <Link to="/profile/viewed">
                                            <ButtonLink type="default">Who's viewed me?</ButtonLink>
                                        </Link>
                                    </Col>
                                </Row>
                            </MainCard>
                        )}

                        {!unauthorizedUser && (
                            <MainCard>
                                {loadingByToken || loading ? (
                                    <Loading />
                                ) : (
                                    <>
                                        <img
                                            src={referralMail}
                                            width="100%"
                                            alt="Refer your friend to join Cudy"
                                            style={{ marginBottom: "2em" }}
                                        />
                                        <Heading
                                            content="Refer it!"
                                            subheader="You can refer your profile to your friend and get some extra
                                    benefits. Copy and share your referral link below to get started"
                                            level={4}
                                        />
                                        <Typography.Paragraph strong copyable={{ text: referralLink }}>
                                            <Tooltip title={referralLink}>Copy link</Tooltip>
                                        </Typography.Paragraph>
                                    </>
                                )}
                            </MainCard>
                        )}

                        {!unauthorizedUser && (
                            <MainCard title={<CardTitle title="Testimonials link" noEdit />}>
                                <p style={{ marginBottom: "2em" }}>
                                    Get your past students to give you a testimonial. Copy the link and share to them.
                                    Let them speak! 🙋🏼‍♂️
                                </p>
                                <Typography.Paragraph
                                    strong
                                    copyable={{
                                        text: `${url}/profile/add_testimonials?referral_code=${userData.referralCode}`
                                    }}
                                >
                                    <Tooltip
                                        title={`${url}/profile/add_testimonials?referral_code=${userData.referralCode}`}
                                    >
                                        Copy link
                                    </Tooltip>
                                </Typography.Paragraph>
                            </MainCard>
                        )}

                        <MainCard
                            title={
                                <CardTitle
                                    title="Skills"
                                    onEditMode={handleEditMode}
                                    unauthorizedUser={unauthorizedUser}
                                />
                            }
                        >
                            <Skills
                                onEditMode={{ editMode, setEditMode }}
                                onSkills={{ skills, setSkills }}
                                fetchUser={fetchUser}
                                loadingSkills={loadingByToken || loading}
                                unauthorizedUser={unauthorizedUser}
                            />
                        </MainCard>

                        <MainCard
                            title={
                                <CardTitle
                                    title="Other platforms"
                                    onEditMode={handleEditMode}
                                    unauthorizedUser={unauthorizedUser}
                                />
                            }
                        >
                            <OtherPlatforms
                                onEditMode={{ editMode, setEditMode }}
                                onOtherPlat={{ userOtherPlat, setUserOtherPlat }}
                                unauthorizedUser={unauthorizedUser}
                                fetchUser={fetchUser}
                                loading={loadingByToken || loading}
                            />
                        </MainCard>

                        <MainCard
                            title={
                                <CardTitle
                                    noEdit
                                    title="Cudy Events"
                                    onEditMode={handleEditMode}
                                    unauthorizedUser={unauthorizedUser}
                                />
                            }
                        >
                            <CudyEvents
                                onEditMode={{ editMode, setEditMode }}
                                unauthorizedUser={unauthorizedUser}
                                fetchUser={fetchUser}
                                loading={loadingByToken || loading}
                                eventData={eventData}
                            />
                        </MainCard>
                    </Col>

                    <Col lg={18} order={mobile ? 1 : 2}>
                        <MainCard
                            title={
                                <CardTitle
                                    noEdit
                                    title="Introduction videos"
                                    onEditMode={handleEditMode}
                                    unauthorizedUser={unauthorizedUser}
                                />
                            }
                        >
                            <IntroVideos
                                data={introVideos}
                                fetchUser={fetchUser}
                                unauthorizedUser={unauthorizedUser}
                                loading={loadingByToken || loading}
                                handlers={{ setProfileCompletionModal }}
                            />
                        </MainCard>

                        <Card autoHeight noHover>
                            <Section noPadding>
                                <Row type="flex" justify="space-between" style={{ marginBottom: "1.5em" }}>
                                    <Col lg={8}>
                                        <Heading content="Testimonials" level={4} />
                                    </Col>
                                    {token && (
                                        <Col lg={8} style={{ textAlign: "right" }}>
                                            <ButtonLink>
                                                See more <Icon type="right" />
                                            </ButtonLink>
                                        </Col>
                                    )}
                                </Row>
                                {loadingByToken || loading ? (
                                    <Loading />
                                ) : (
                                    <Row>
                                        <Col lg={24} style={{ position: "relative" }}>
                                            {renderTestimonials()}
                                        </Col>
                                    </Row>
                                )}
                            </Section>
                        </Card>

                        <MainCard
                            title={
                                <CardTitle
                                    title="About me"
                                    onEditMode={handleEditMode}
                                    unauthorizedUser={unauthorizedUser}
                                />
                            }
                        >
                            <AboutMe
                                data={userBio}
                                onUserBio={{ userBio, setUserBio }}
                                fetchUser={fetchUser}
                                onEditMode={{ editMode, setEditMode }}
                                unauthorizedUser={unauthorizedUser}
                                loading={loadingByToken || loading}
                            />
                        </MainCard>

                        <MainCard
                            title={
                                <CardTitle
                                    title="Tutoring experiences"
                                    description={experiencesDescription}
                                    onEditMode={handleEditMode}
                                    unauthorizedUser={unauthorizedUser}
                                />
                            }
                        >
                            <Experiences
                                onExperiences={{ experiences, setExperiences, theTotal }}
                                onEditMode={{ editMode, setEditMode }}
                                unauthorizedUser={unauthorizedUser}
                                fetchUser={fetchUser}
                                loading={loadingByToken || loading}
                            />
                        </MainCard>

                        <MainCard
                            title={
                                <CardTitle
                                    title="Schedule"
                                    onEditMode={handleEditMode}
                                    unauthorizedUser={unauthorizedUser}
                                />
                            }
                        >
                            <TutorCalendar
                                onEditMode={{ editMode, setEditMode }}
                                unauthorizedUser={unauthorizedUser}
                                teachingDate={teachingDate}
                                fetchUser={fetchUser}
                                loading={loadingByToken || loading}
                            />
                        </MainCard>

                        <Row type="flex" gutter={32}>
                            <Col xs={24} lg={12} style={{ marginBottom: mobile && "1em" }}>
                                <MainCard
                                    title={
                                        <CardTitle
                                            title="Subjects"
                                            onEditMode={handleEditMode}
                                            unauthorizedUser={unauthorizedUser}
                                        />
                                    }
                                    editMode={editMode}
                                >
                                    <Subjects
                                        onSubjects={{ subjects, setSubjects }}
                                        onEditMode={{ editMode, setEditMode }}
                                        fetchUser={fetchUser}
                                        unauthorizedUser={unauthorizedUser}
                                        loading={loadingByToken || loading}
                                    />
                                </MainCard>
                            </Col>

                            <Col xs={24} lg={12}>
                                <MainCard
                                    editMode={editMode}
                                    title={
                                        <CardTitle
                                            title="Certifications"
                                            onEditMode={handleEditMode}
                                            unauthorizedUser={unauthorizedUser}
                                        />
                                    }
                                >
                                    <Certifications
                                        onEditMode={{ editMode, setEditMode }}
                                        onCertifications={{ certifications, setCertifications }}
                                        unauthorizedUser={unauthorizedUser}
                                        fetchUser={fetchUser}
                                        loading={loadingByToken || loading}
                                    />
                                </MainCard>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Section>
        </Layout>
    )
}

export default withRouter(TutorProfile)

/*
███████╗████████╗██╗   ██╗██╗     ███████╗███████╗
██╔════╝╚══██╔══╝╚██╗ ██╔╝██║     ██╔════╝██╔════╝
███████╗   ██║    ╚████╔╝ ██║     █████╗  ███████╗
╚════██║   ██║     ╚██╔╝  ██║     ██╔══╝  ╚════██║
███████║   ██║      ██║   ███████╗███████╗███████║
                                                  
*/

const BlurredCard = styled(Card)`
    && {
        width: 60%;
        height: 200px;
        left: 50%;
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%);
        z-index: 2;
        &:hover {
            transform: translate(-50%, -50%);
        }

        ${media.mobile`
            width: 100%;
            height: auto;
        `}
    }
`

const StyledList = styled(List).attrs(() => ({
    noHover: true
}))`
    filter: ${!token && "blur(6px)"};
    user-select: ${!token && "none"};
    .ant-list-item {
        box-shadow: none;
        border-radius: 0;
    }
`

const MainCard = styled(Card).attrs(() => ({
    height: "100%",
    noHover: true
}))`
    background-color: ${({ bg }) => bg || ""};
    .ant-card-meta-detail {
        margin-bottom: 1em;
    }
    &:hover {
        .card-title {
            .icon {
                display: flex;
                justify-content: center;
                align-items: center;
                visibility: visible;
            }
        }
    }
`

const StyledCardTitle = styled.div`
    display: flex;
    align-items: center;
    > .ant-typography {
        margin-right: 1em;
        margin-bottom: 0;
    }
    .icon {
        position: absolute;
        right: 20px;
        cursor: pointer;
        display: none;
        visibility: hidden;
        color: #ff9d00;
        font-size: 0.9em;
        border-radius: 50%;
        width: 30px;
        height: 30px;
        &:hover {
            background: #eee;
        }
    }
`
