import React from "react"
import { Modal, Section } from "components"
import { Form, Row, Col } from "antd"
import TextInput from "components/forms/TextInput"
import ShareSection from "components/common/ShareSection"

const url = "https://sg.cudy.co"

export default function ShareModal({ userData, onModal }) {
    const { shareModal, setShareModal } = onModal
    const profileUrl = `${url}/${userData.userCode}-${userData.firstName}-${userData.lastName}`

    return (
        <Modal
            title={`Share ${userData.firstName}'s profile`}
            visible={shareModal}
            onCancel={() => setShareModal(false)}
            footer={false}
        >
            <Section paddingHorizontal={0}>
                <Form layout="vertical">
                    <TextInput
                        independent
                        name="share_profile"
                        placeholder="Share profile..."
                        label="Copy the profile's URL"
                        value={profileUrl}
                    />
                    <Row gutter={32} type="flex" align="middle">
                        <Col lg={6}>Share to</Col>
                        <Col lg={18}>
                            <ShareSection userData={userData} />
                        </Col>
                    </Row>
                </Form>
            </Section>
        </Modal>
    )
}
