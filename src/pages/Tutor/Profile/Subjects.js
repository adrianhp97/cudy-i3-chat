import React, { useState, useEffect } from "react"
import { Section, Button, ButtonLink, Empty, Loading } from "components"
import { Form, Row, Col, Popconfirm, message } from "antd"
import DataList from "./DataList"
import styled from "styled-components/macro"
import { useQuery, useMutation } from "@apollo/react-hooks"
import uuid from "uuid/v4"

import { queryFetchCurriculums, queryFetchSubjects } from "queries/data"
import { newClient, client, mobile } from "helpers"
import TextInput from "components/forms/TextInput"
import SelectInput from "components/forms/SelectInput"
import { mutateUpdateSubjects, mutateDeleteSubjects,mutatePriceSuggestion } from "queries/profile"
import useError from "helpers/hooks/useError"
import { Formik } from "formik"
import { SubmitButton, FormikDebug } from "formik-antd"

const FieldRow = styled(Row)`
    position: relative;
    margin-bottom: 2em;
    &:hover {
        .delete {
            display: flex;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
    }
    .delete {
        display: none;
        cursor: pointer;
        visibility: hidden;
        position: absolute;
        right: 0;
        border-radius: 50%;
        width: 25px;
        color: #ff9d00;
        height: 25px;
        z-index: 99;
        &:hover {
            background: #eee;
        }
    }
`

const StyleWrapper = styled.div``;

export default function Subjects({ onEditMode, onSubjects, fetchUser, unauthorizedUser, loading }) {
    const { editMode, setEditMode } = onEditMode
    const { subjects = [] } = onSubjects
    const [userSubjects, setUserSubjects] = useState([])
    const [isAdding, setIsAdding] = useState(false)
    const [formValues, setFormValues] = useState({})
    const [suggestionPrice,setSuggestionPrice] = useState({});

    const [updateSubjects] = useMutation(mutateUpdateSubjects, {
        client: newClient(),
        onCompleted: data => setEditMode(""),
        onError: err => useError(err)
    })

    const [deleteSubjects, { loading: loadingDelete }] = useMutation(mutateDeleteSubjects, {
        client: newClient(),
        onCompleted: data => setEditMode(""),
        onError: err => useError(err)
    })

    const [getSuggestionPrice] = useMutation(mutatePriceSuggestion,{
        client: newClient(),
        onCompleted:data=> setSuggestionPrice(data.getSuggestionPrice),
        onError: err => console.log(err)// useError(err)
    })

    const { data: dataSubjects = {} } = useQuery(queryFetchSubjects, {
        client: client.programmeClient
    })

    const { data: dataCurriculums = {} } = useQuery(queryFetchCurriculums, {
        client: client.programmeClient
    })

    const subjectOptions = (dataSubjects.getProgrammeSubjectList || []).map(item => ({
        value: Number(item.pvid),
        label: item.name
    }))

    const currOptions = (dataCurriculums.getProgrammeCurriculumList || []).map(item => ({
        value: Number(item.pvid),
        label: item.name
    }))

    const handleEditSubject = (values, { setSubmitting }) => {
        const hasPvid = values.hasOwnProperty("pvid")
        if (hasPvid) {
            const { source, ...rest } = values
            values = rest
        }
        updateSubjects({ variables: { list: { ...values, orderList: 1 } } })
            .then(() => fetchUser())
            .then(() => message.success(`The subject item has been ${hasPvid ? "updated" : "added"}`, 2))
            .finally(() => setSubmitting(false))
    }
    const handleAddSubject = (event) => {

        const hasPvid = formValues.hasOwnProperty("pvid")
        let values = formValues;
        if (hasPvid) {
            const { source, ...rest } = formValues
            values = rest
        }


        
        updateSubjects({ variables: { list: { ...values, orderList: 1 } } })
            .then(() => fetchUser())
            .then(() => message.success(`The subject item has been ${hasPvid ? "updated" : "added"}`, 2))
            //.finally(() => setSubmitting(false))

        // const hasPvid = values.hasOwnProperty("pvid")
        // if (hasPvid) {
        //     const { source, ...rest } = values
        //     values = rest
        // }
        // updateSubjects({ variables: { list: { ...values, orderList: 1 } } })
        //     .then(() => fetchUser())
        //     .then(() => message.success(`The subject item has been ${hasPvid ? "updated" : "added"}`, 2))
        //     .finally(() => setSubmitting(false))
    }

    const handleDelete = pvid => {
        const filtered = userSubjects.find(item => item.pvid === pvid) || {}
        deleteSubjects({ variables: { pvidList: [filtered.pvid] } })
            .then(() => fetchUser())
            .then(() => message.success("The subject item has been deleted", 2))
    }

    const handleCancel = () => {
        setIsAdding(false)
        setUserSubjects(subjects)
        setEditMode("")
    }

    const handleAddNewField = () => {
        setIsAdding(true)
        setUserSubjects(prev => [
            ...prev,
            {
                id: uuid(),
                programmeSubjectPvid: "",
                pricePerHour: 0,
                subject: { pvid: null },
                level: { pvid: null },
                orderList: 0
            }
        ])
    }

    const onLocalChange = name =>  value => {
        
        setFormValues({...formValues, [name]: value});
        if(!Object.keys(formValues).length )return;
        
        const obj = {
            ...formValues,
            [name]:value
        }
        
        getSuggestionPrice({variables:{...obj}})
    }

    useEffect(() => {
        setUserSubjects(subjects)
    }, [subjects])

    if (loading) return <Loading />

    return (
        <Section paddingHorizontal={0} noPadding={mobile} marginBottom={0}>
            {editMode === "Subjects" ? (
                isAdding ? (
                    // <Formik
                    //     onSubmit={handleAddSubject}
                    //     render={({ handleSubmit, values }) => {
                            <Form layout="vertical" >
                                <Row gutter={16}>
                                    <Col lg={12}>
                                        <SelectInput
                                            independent
                                            name="programmeSubjectPvid"
                                            placeholder="Subject name"
                                            label="Subject"
                                            options={subjectOptions}
                                            marginBottom="5px"
                                            onChange={onLocalChange('programmeSubjectPvid')}
                                        />
                                    </Col>
                                    <Col lg={12}>
                                        <SelectInput
                                            independent
                                            name="programmeLevelPvid"
                                            placeholder="Curriculum"
                                            label="Curriculum"
                                            options={currOptions}
                                            marginBottom="5px"
                                            onChange={onLocalChange('programmeLevelPvid')}
                                        />
                                    </Col>

                                    <Col lg={12} className="ant-form ant-form-item-label">
                                        <label css={`margin-bottom:5px;display:block`}>Average Price /hour </label>
                                        <label css={`font-size:30px!important`}>{suggestionPrice.averagePrice <= 0  || !suggestionPrice.averagePrice? 'N/A' : `$ ${suggestionPrice.averagePrice}`}</label>
                                    </Col>

                                    <Col lg={12}>
                                        <TextInput
                                            independent
                                            number
                                            label="Price per hour"
                                            name="pricePerHour"
                                            formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                            parser={value => value.replace(/\$\s?|(,*)/g, "")}
                                            step={5}
                                            onChange = {onLocalChange('pricePerHour')}
                                        />
                                    </Col>
                                </Row>
                                <Button block onClick={handleAddSubject}>Save this subject</Button> &nbsp;{" "}
                                <ButtonLink block onClick={handleCancel}>
                                    Cancel
                                </ButtonLink>
                            </Form>
                    //     }}
                    // />
                ) : (
                    <>
                        {userSubjects.map(item => (
                            
                            <Formik
                                onSubmit={handleEditSubject}
                                // validate = {}
                                initialValues={{
                                    pvid:item.pvid,
                                    programmeSubjectPvid: Number(item.subject.pvid),
                                    programmeLevelPvid: Number(item.level.pvid),
                                    pricePerHour: item.pricePerHour
                                }}
                                render={({ handleSubmit }) => (
                                    <FieldRow>
                                        <Form layout="vertical" onSubmit={handleSubmit}>
                                            <Row gutter={16}>
                                                <Col lg={12}>
                                                    <SelectInput
                                                        name="programmeSubjectPvid"
                                                        placeholder="Subject name"
                                                        label="Subject"
                                                        options={subjectOptions}
                                                        marginBottom="5px"
                                                    />
                                                </Col>
                                                <Col lg={12}>
                                                    <SelectInput
                                                        name="programmeLevelPvid"
                                                        placeholder="Curriculum"
                                                        label="Curriculum"
                                                        options={currOptions}
                                                        marginBottom="5px"
                                                    />
                                                </Col>
                                                <Col lg={12}>
                                                    <TextInput
                                                        number
                                                        name="pricePerHour"
                                                        formatter={value =>
                                                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                                                        }
                                                        parser={value => value.replace(/\$\s?|(,*)/g, "")}
                                                        step={5}
                                                    />
                                                </Col>
                                            </Row>
                                            <SubmitButton type="primary">Update</SubmitButton> &nbsp;{" "}
                                            <Popconfirm
                                                title="Delete this subject?"
                                                onConfirm={() => handleDelete(item.pvid)}
                                            >
                                                <ButtonLink icon="close" loading={loadingDelete}>
                                                    Delete
                                                </ButtonLink>
                                            </Popconfirm>
                                        </Form>
                                    </FieldRow>
                                )}
                            />
                        ))}
                        <Button
                            block
                            type="dashed"
                            size="medium"
                            icon="plus"
                            onClick={handleAddNewField}
                            style={{ marginBottom: "2em" }}
                        >
                            Add a new subject
                        </Button>
                        <ButtonLink block icon="close" onClick={() => setEditMode(false)}>
                            Cancel
                        </ButtonLink>
                    </>
                )
            ) : unauthorizedUser ? (
                userSubjects.length === 0 ? (
                    <Empty description="This tutor didn't set any subjects yet" />
                ) : (
                    userSubjects.map(({ subject, level, pricePerHour, pvid }) => {
                        return (
                            <DataList
                                key={pvid}
                                title={subject.name}
                                description={level.name}
                                extra={`$${pricePerHour} per hour`}
                            />
                        )
                    })
                )
            ) : userSubjects.length === 0 ? (
                <Empty description="You haven't set any subjects yet" />
            ) : (
                userSubjects.map(({ subject, level, pricePerHour, pvid }) => {
                    return (
                        <DataList
                            key={pvid}
                            title={subject.name}
                            description={level.name}
                            extra={`$${pricePerHour} per hour`}
                        />
                    )
                })
            )}
        </Section>
    )
}
