import React, { useEffect, useState } from "react"
import { withRouter } from "react-router"
import { Layout, Section, Heading, Loading, Alert } from "components"
import { Row, Col, Tabs, Form, message } from "antd"
import styled from "styled-components"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import { useMutation } from "react-apollo"
import { mutateUserProfile, mutateUpdateProfile, mutateUpdateCurriculum } from "queries/profile"
import { mobile, newClient } from "helpers"
import { SubmitButton, DatePicker, FormItem } from "formik-antd"
import { race as races } from "dummy"
import moment from "moment"
import RadioInput from "components/forms/RadioInput"
import useError from "helpers/hooks/useError"
import { mutateUpdatePassword } from "queries/auth"
import SelectInput from "components/forms/SelectInput"
import useQueryData from "helpers/hooks/useQueryData"
import { queryFetchCurriculums } from "queries/data"
import DynamicIcon from "components/common/DynamicIcon"

const StyledTab = styled(Tabs)`
    && {
        .ant-tabs-left-content {
            padding-left: 4em;
        }
    }
`

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 }
    }
}

const tailLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 6
        }
    }
}

const userDetails = (localStorage.getItem("userDetails") && JSON.parse(localStorage.getItem("userDetails"))) || {}
const isStudent = (userDetails.role || {}).code === "STDN"

function Settings() {
    const [userProfile, setUserProfile] = useState({})

    const { firstName, lastName, dob, phone, race, gender } = userProfile
    const initials = { firstName, lastName, dob, phone, race, gender }
    const curriculumList = userProfile.curriculumList || []
    const curriculum = (curriculumList && curriculumList[0] && curriculumList[0].source) || {}

    const { data = {} } = useQueryData(queryFetchCurriculums, "programmeClient")
    const currOptions = (data.getProgrammeCurriculumList || []).map(item => ({
        value: Number(item.pvid),
        label: item.name
    }))

    const [fetchUser] = useMutation(mutateUserProfile, {
        client: newClient(),
        onCompleted: data => {
            data = data.getProfile || {}
            setUserProfile(data)
        }
    })

    const [updateCurriculum] = useMutation(mutateUpdateCurriculum, {
        client: newClient("profile"),
        onCompleted: () => {
            message.success("Your curriculum has been updated")
            fetchUser()
        }
    })

    const [updateProfile] = useMutation(mutateUpdateProfile, {
        client: newClient("account"),
        onCompleted: () => message.success("Your profile has been updated"),
        onError: err => useError(err)
    })

    const [changePassword] = useMutation(mutateUpdatePassword, {
        client: newClient("account"),
        onCompleted: data => {
            message.loading("Resetting your password...").then(() => {
                message.success("Your password has been changed")
            })
        },
        onError: err => useError(err)
    })

    const handleUpdateProfile = (values, { setSubmitting }) => {
        values = { ...values, dob: moment(values.dob).format("YYYY-MM-DD") }
        updateProfile({ variables: values }).finally(() => setSubmitting(false))
    }

    const handleChangePassword = (values, { setSubmitting }) => {
        changePassword({ variables: values }).finally(() => setSubmitting(false))
    }

    const handleUpdateCurriculum = (values, { setSubmitting }) => {
        values = curriculumList.length > 0 ? { pvid: curriculumList[0].pvid, ...values } : values
        updateCurriculum({ variables: { list: [{ ...values }] } }).finally(() => {
            setSubmitting(false)
        })
    }

    const roleIcon = () => {
        let icon = ""
        if (userProfile.gender === "F".toUpperCase()) {
            if (userProfile.roleCode === "STDN") icon = "icon--student"
            else icon = "icon--businesswoman"
        }
        if (userProfile.gender === "M".toUpperCase()) {
            if (userProfile.roleCode === "STDN") icon = "icon--basicpeople"
            else icon = "icon--businessman"
        }
        return icon
    }

    const iconForRole = roleIcon()

    useEffect(() => {
        fetchUser()
    }, [userProfile.gender, userProfile.roleCode])

    console.log({ iconForRole })

    if (Object.keys(userProfile).length < 1)
        return (
            <Section paddingHorizontal="very" style={{ paddingTop: "4em" }}>
                <Row type="flex" justify="center">
                    <Loading />
                </Row>
            </Section>
        )

    return (
        <Layout breadcrumb={false}>
            <Section paddingHorizontal="very" style={{ paddingTop: "4em" }}>
                <Row type="flex" justify="center">
                    <Col lg={18}>
                        <StyledTab tabPosition={mobile ? "top" : "left"}>
                            <Tabs.TabPane tab="Basic" key="basic">
                                <Row type="flex" align="middle" justify="space-between" style={{ marginBottom: "2em" }}>
                                    <Col lg={8}>
                                        <Heading content="Basic" level={4} subheader="Your basic profile details" />
                                    </Col>
                                    <Col lg={8}>
                                        <DynamicIcon type={iconForRole} /> &nbsp; You are a{" "}
                                        <strong className="primary">{userProfile.roleName}</strong>
                                    </Col>
                                </Row>
                                <Formik
                                    onSubmit={handleUpdateProfile}
                                    initialValues={initials}
                                    render={({ handleSubmit }) => (
                                        <Form {...formItemLayout} onSubmit={handleSubmit}>
                                            <TextInput name="firstName" placeholder="First name" label="First name" />
                                            <TextInput name="lastName" placeholder="Last name" label="Last name" />
                                            <RadioInput
                                                name="gender"
                                                label="Gender"
                                                options={[
                                                    { value: "M", label: "Male" },
                                                    { value: "F", label: "Female" }
                                                ]}
                                            />
                                            <FormItem name="dob" label="DOB">
                                                <DatePicker name="dob" placeholder="Your date of birth" label="DOB" />
                                            </FormItem>
                                            <SelectInput name="race" placeholder="Race" label="Race" options={races} />
                                            <TextInput
                                                name="phone"
                                                placeholder="Mobile number"
                                                label="Mobile number"
                                                addonBefore="+65"
                                                marginBottom="3em"
                                            />
                                            <Form.Item {...tailLayout}>
                                                <SubmitButton size="large">Save changes</SubmitButton>
                                            </Form.Item>
                                        </Form>
                                    )}
                                />
                            </Tabs.TabPane>

                            {isStudent && (
                                <Tabs.TabPane tab="Education" key="education">
                                    <Heading
                                        content="Education"
                                        level={4}
                                        subheader="Some educational details of you"
                                        marginBottom="3em"
                                    />
                                    <Formik
                                        initialValues={{
                                            programmeCurriculumPvid: Number(curriculum.pvid)
                                        }}
                                        onSubmit={handleUpdateCurriculum}
                                        render={({ handleSubmit }) => (
                                            <Form {...formItemLayout} onSubmit={handleSubmit}>
                                                <SelectInput
                                                    name="programmeCurriculumPvid"
                                                    label="Curriculum and level"
                                                    placeholder="Select one..."
                                                    options={currOptions}
                                                />
                                                <Form.Item {...tailLayout}>
                                                    <SubmitButton size="large">Save changes</SubmitButton>
                                                </Form.Item>
                                            </Form>
                                        )}
                                    />
                                </Tabs.TabPane>
                            )}

                            <Tabs.TabPane tab="Change password" key="password">
                                <Heading
                                    content="Change password"
                                    level={4}
                                    subheader="If you don't satisfy with your current password"
                                    marginBottom="3em"
                                />
                                <Formik
                                    onSubmit={handleChangePassword}
                                    validate={validate}
                                    render={({ handleSubmit }) => (
                                        <Form {...formItemLayout} onSubmit={handleSubmit}>
                                            <TextInput
                                                password
                                                name="newPassword1"
                                                placeholder="Your new password..."
                                                label="Your new password"
                                            />
                                            <TextInput
                                                password
                                                name="newPassword2"
                                                placeholder="Repeat your new password..."
                                                label="Repeat password"
                                            />
                                            <TextInput
                                                password
                                                name="oldPassword"
                                                placeholder="Your old password..."
                                                label="Your old password"
                                            />
                                            <Form.Item {...tailLayout}>
                                                <SubmitButton size="large">Save changes</SubmitButton>
                                            </Form.Item>
                                        </Form>
                                    )}
                                />
                            </Tabs.TabPane>
                        </StyledTab>
                    </Col>
                </Row>
            </Section>
        </Layout>
    )
}

const validate = values => {
    const error = {}

    if (!values.newPassword1) error.newPassword1 = "You have to provide your new password"
    else if (values.newPassword1.length < 8) error.newPassword1 = "At least 8 characters"
    if (!values.newPassword2) error.newPassword2 = "Please repeat your new password"
    else if (values.newPassword2 !== values.newPassword1) error.newPassword2 = "The password doesn't match"

    return error
}

export default withRouter(Settings)
