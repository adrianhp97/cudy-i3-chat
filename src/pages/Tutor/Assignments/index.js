import React from "react"
import { Layout, Heading, Card, Empty, Button, Avatar, Section } from "components"
import { Row, Col, List, Tag } from "antd"
import { Link } from "react-router-dom"
import moment from "moment"
import styled, { css } from "styled-components"

let assignments = [
    {
        id: 1,
        subject: "English",
        curriculum: "Primary 2",
        gender: "Female",
        location: "21 Paya Lebar",
        notes: "I need this assignment asap",
        price_range: [20, 100],
        postedDate: "21 September 2019",
        status: "Active"
    }
]

const AssignmentRow = styled(Row)`
    .ant-typography {
        h2 {
            font-size: 1em;
        }
    }
`

export default function Assignments() {
    const renderAssignments = () => {
        if (assignments.length === 0) return <Empty />
        return (
            <List
                itemLayout="horizontal"
                dataSource={assignments}
                renderItem={item => (
                    <List.Item>
                        <List.Item.Meta
                            avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title={
                                <p>
                                    You posted an assignment {moment(item.postedDate).fromNow()} &nbsp;{" "}
                                    <Tag color="#87d068">{item.status}</Tag>
                                </p>
                            }
                            description={
                                <Row gutter={32}>
                                    <Col lg={8}>
                                        <Heading reverse content="Subject" subheader={item.subject} />
                                        <Heading reverse content="Curriculum" subheader={item.curriculum} />
                                    </Col>
                                    <Col lg={8}>
                                        <Heading reverse content="Gender" subheader={item.gender} />
                                        <Heading reverse content="Location" subheader={item.location} />
                                    </Col>
                                    <Col lg={8}>
                                        <Heading
                                            reverse
                                            content="Price range"
                                            subheader={`${item.price_range[0]} - ${item.price_range[1]}`}
                                        />
                                        <Heading reverse content="Additional notes" subheader={item.notes} />
                                    </Col>
                                </Row>
                            }
                        />
                    </List.Item>
                )}
            />
        )
    }

    return (
        <Layout>
            <Section paddingHorizontal={0}>
                <Row type="flex" justify="center">
                    <Col lg={12}>
                        <Row type="flex" justify="space-between" align="middle" style={{ marginBottom: "2em" }}>
                            <Col lg={12}>
                                <Heading
                                    bold
                                    content="Your assignments"
                                    subheader="The list of your posted assignments"
                                    level={4}
                                />
                            </Col>
                            <Col lg={8} style={{ textAlign: "right" }}>
                                <Link to="/assignments/post_assignment">
                                    <Button type="primary" size="medium" icon="plus">
                                        Post new assignment
                                    </Button>
                                </Link>
                            </Col>
                        </Row>
                        <Card autoHeight>{renderAssignments()}</Card>
                    </Col>
                </Row>
            </Section>
        </Layout>
    )
}
