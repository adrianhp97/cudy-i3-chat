import React, { useState } from "react"
import { Layout, Heading, Button, Section } from "components"
import TimeInput from "components/forms/TimeInput"
import { Row, Col, Form, Slider, message } from "antd"
import SelectInput from "components/forms/SelectInput"
import Script from "react-load-script"
import { useHistory } from "react-router-dom"
import useQueryData from "helpers/hooks/useQueryData"
import { queryFetchCurriculums, queryFetchSubjects } from "queries/data"
import RadioInput from "components/forms/RadioInput"
import { preferredDays, winners } from "dummy"
import styled from "styled-components"
import moment from "moment"
import MentionInput from "components/forms/MentionInput"
import ToggleInput from "components/forms/ToggleInput"
import { googleApiKey } from "helpers"
import TextInput from "components/forms/TextInput"
import SliderInput from "components/forms/SliderInput"
import { Formik } from "formik"

const StyledDay = styled(Col)`
	.ant-time-picker {
		width: 100%;
	}
	p {
		font-weight: bold;
		margin-bottom: 0;
	}
`

const formItemLayout = {
	labelCol: {
		xs: { span: 24 },
		sm: { span: 6 }
	},
	wrapperCol: {
		xs: { span: 24 },
		sm: { span: 14 }
	}
}

const tailLayout = {
	wrapperCol: {
		xs: {
			span: 24,
			offset: 0
		},
		sm: {
			span: 16,
			offset: 6
		}
	}
}

const experienceOptions = [
	{ value: "> 1", label: "> 1 year" },
	{ value: "> 2", label: "> 2 year" },
	{ value: "> 3", label: "> 3 year" },
	{ value: "> 5", label: "> 5 year" }
]

let autocomplete = null

export default function PostAssignment() {
	const [selectedDay, setSelectedDay] = useState("")
	const [allowGroups, setAllowGroups] = useState(false)
	const [formValues, setFormValues] = useState({})
	const [addressValue, setAddressValue] = useState("")
	const [address, setAddress] = useState({})
	const [groups, setGroups] = useState([])
	const history = useHistory()

	const { data = {} } = useQueryData(queryFetchCurriculums, "programmeClient")
	const { data: subject = {} } = useQueryData(queryFetchSubjects, "programmeClient")
	const currOptions = (data.getProgrammeCurriculumList || []).map(item => ({
		value: Number(item.pvid),
		label: item.name
	}))
	const subjectOptions = (subject.getProgrammeSubjectList || []).map(item => ({
		value: Number(item.pvid),
		label: item.name
	}))

	const handleLoad = () => {
		const options = {
			types: [],
			componentRestrictions: { country: "SG" }
		}

		/*global google*/
		autocomplete = new google.maps.places.Autocomplete(document.getElementById("location"), options)
		autocomplete.addListener("place_changed", handlePlaceSelect)
	}

	const handlePlaceSelect = () => {
		const addressObject = autocomplete.getPlace()
		const userAddress = addressObject.address_components

		if (userAddress) {
			setAddress({
				location: addressObject.formatted_address
			})
			setAddressValue(addressObject.formatted_address)
		}
	}

	const renderTime = day => {
		let timeList = []
		if (selectedDay === day) {
			timeList = [...timeList, <TimeInput name="time" format="HH:mm" />]
		}
		return timeList
	}

	const handleSelectGroup = options => setGroups(opt => [options, ...opt])
	const handleSelect = name => e => {
		const isSpecialFields = name === "tutor_experience" || name === "gender" || name === "notes"
		if (isSpecialFields) setFormValues(val => ({ ...val, [name]: e.target.value }))
		else setFormValues(val => ({ ...val, [name]: e }))
	}

	const handleSubmit = e => {
		e.preventDefault()
		// const { curriculum, subject, price_range, tutor_experience, gender } = formValues
		const values = {
			...formValues,
			groups,
			location: addressValue
		}
		message.loading("Please wait...").then(() => {
			history.push("/assignments")
		})
	}

	return (
		<Layout>
			<Section paddingHorizontal={0}>
				<Row type="flex" justify="center">
					<Col lg={14}>
						<Heading
							bold
							content="Post an assignment"
							subheader="Need to get tutors fast for your needs? Post an assignment, and they'll notice"
							marginBottom="2em"
							level={4}
						/>
						<Formik
							render={({ handleSubmit }) => (
								<Form {...formItemLayout} onSubmit={handleSubmit}>
									<SelectInput
										name="curriculum"
										label="Select curriculum"
										placeholder="Select curriculum"
										onChange={handleSelect("curriculum")}
										options={currOptions}
									/>
									<SelectInput
										name="subject"
										label="Select subject"
										placeholder="Select subject"
										onChange={handleSelect("subject")}
										options={subjectOptions}
									/>
									<SliderInput
										range
										label="Preferred price"
										extra={
											<span>
												Suggestion price to set: <strong>$50</strong> - <strong>$100</strong>
											</span>
										}
										name="price_range"
										marks={{ 10: "$10", 150: "$150" }}
										tipFormatter={value => `$${value}`}
										defaultValue={[10, 100]}
										step={5}
										min={0}
										max={150}
										onAfterChange={handleSelect("price_range")}
									/>
									{/* <Form.Item
                                name="price_range"
                                label="Preferred price"
                                extra={
                                    <span>
                                        Suggestion price to set: <strong>$50</strong> - <strong>$100</strong>
                                    </span>
                                }
                            >
                                <Slider
                                    range
                                    name="price_range"
                                    marks={{ 10: "$10", 150: "$150" }}
                                    tipFormatter={value => `$${value}`}
                                    defaultValue={[10, 100]}
                                    step={5}
                                    min={0}
                                    max={150}
                                    onAfterChange={handleSelect("price_range")}
                                />
                            </Form.Item> */}
									<RadioInput
										group
										name="gender"
										label="Preferred gender"
										options={[
											{ value: "M", label: "Male" },
											{ value: "F", label: "Female" }
										]}
										onChange={handleSelect("gender")}
									/>
									<RadioInput
										group
										name="tutor_experience"
										label="Tutor's experience"
										options={experienceOptions}
										onChange={handleSelect("tutor_experience")}
									/>
									<Script url={googleApiKey} onLoad={handleLoad} />
									<TextInput
										allowClear
										id="location"
										name="address"
										placeholder="Choose your preferred location..."
										label="Preferred location"
										value={addressValue}
										onChange={e => setAddressValue(e.target.value)}
									/>
									{/* <Form.Item name="schedule" label="Preferred time">
                                    <Row gutter={32}>
                                        {preferredDays.map(item => (
                                            <StyledDay lg={12} key={item.value}>
                                                <p>{item.label}</p>
                                                <Row gutter={8}>
                                                    <Col lg={12}>
                                                        <TimeInput
                                                            use12Hours
                                                            name="fromTime"
                                                            label="From"
                                                            format="HH:mm"
                                                            minuteStep={15}
                                                            defaultOpenValue={moment("01:15")}
                                                        />
                                                    </Col>
                                                    <Col lg={12}>
                                                        <TimeInput
                                                            use12Hours
                                                            name="toTime"
                                                            label="To"
                                                            format="HH:mm"
                                                            minuteStep={15}
                                                            defaultOpenValue={moment("01:15")}
                                                        />
                                                    </Col>
                                                </Row>
                                            </StyledDay>
                                        ))}
                                    </Row>
                                </Form.Item> */}
									<ToggleInput
										name="multiple"
										label="Allow others to join the lesson"
										onChange={value => setAllowGroups(value)}
									>
										Allow heheh
									</ToggleInput>
									<MentionInput
										independent
										name="group"
										label="Choose friends to join"
										placeholder="Enter your friends' name"
										disabled={!allowGroups}
										onSelect={handleSelectGroup}
										options={winners.map(item => ({ value: item.name, label: item.name }))}
									/>
									<TextInput
										textarea
										rows={4}
										name="notes"
										placeholder="Some special notes regarding this assignment"
										label="Additional notes"
										onChange={handleSelect("notes")}
									/>
									<Form.Item {...tailLayout}>
										<Button type="primary" htmlType="submit">
											Post the assignment now
										</Button>
									</Form.Item>
								</Form>
							)}
						/>
					</Col>
				</Row>
			</Section>
		</Layout>
	)
}
