import gql from "graphql-tag"

export const queryCurriculums = gql`
    query queryCurriculums {
        getProgrammeCurriculumList {
            pvid
            name
        }
    }
`

export const queryLevels = gql`
    query queryLevels {
        getProgrammeLevelList {
            pvid
            name
        }
    }
`

export const querySubjects = gql`
    query querySubjects {
        getProgrammeSubjectList {
            pvid
            name
        }
    }
`
