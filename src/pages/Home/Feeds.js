import React, { useState } from "react"
import { Avatar, Heading, ButtonLink } from "components"
import { Row, Col, Card, Form, Comment, Tooltip, Icon, Divider } from "antd"
import styled from "styled-components"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import { SubmitButton, ResetButton } from "formik-antd"
import { serverBaseUrl, renderAvatar } from "helpers"
import moment from "moment"
import { feedSample } from "dummy"
import { baseStyles } from "styles/base"

const NewFeedFCard = styled(Card).attrs({
	autoHeight: true
})`
	&& {
		margin-bottom: 2em;
		background-color: transparent;
		border-color: transparent;
		.ant-card-body {
			padding: 0;
		}
	}
`

const ActionItem = styled.span`
	margin-right: 1.4em;
`

const StyledComment = styled(Comment)`
	padding: 2em;
	background-color: #fff;
	border-radius: 10px;
	padding-bottom: 1em;
	margin-bottom: 1.5em;
	transition: all 0.2s ease;
	&:hover {
		box-shadow: ${baseStyles.boxShadow.main};
	}
`

const StyledTextarea = styled(TextInput)`
	&& {
		border: none;
		resize: none;
	}
`

export default function Feeds({ userDetails = {} }) {
	const [action, setAction] = useState("")
	const [isCommenting, setIsCommenting] = useState(false)
	const [selectedItem, setSelectedItem] = useState({})
	const [feedData, setFeedData] = useState(feedSample)

	const avatar = (userDetails.avatar || [])[0] || {}

	const handleSubmitFeed = (values, { setSubmitting }) => {
		setSubmitting(false)
	}

	const handleReactions = (item, action) => {
		let feed = [...feedData]
		const selectedItemIndex = feed.findIndex(feed => feed.id === item.id)
		if (selectedItemIndex > -1) {
			feed[selectedItemIndex][action] = feed[selectedItemIndex][action] + 1
			setFeedData(feed)
		}
	}

	const handleShowCommentForm = item => {
		setSelectedItem(item)
		setIsCommenting(true)
	}

	return (
		<Row gutter={32} type="flex" justify="center">
			{/* <Col lg={12}>
                
            </Col> */}
			<Col lg={12}>
				<NewFeedFCard>
					<Row gutter={16}>
						<Col lg={4}>
							<Avatar src={serverBaseUrl + avatar.path} size="large" />
						</Col>
						<Col lg={20}>
							<Formik
								onSubmit={handleSubmitFeed}
								initialValues={{ content: "" }}
								render={({ handleSubmit }) => (
									<Form layout="vertical" onSubmit={handleSubmit}>
										<TextInput
											textarea
											rows={4}
											name="content"
											placeholder="What's on your thought?"
										/>
										<SubmitButton type="primary" size="large" icon="plus">
											Send feed
										</SubmitButton>{" "}
										&nbsp;{" "}
										<ResetButton size="large" type="link">
											Reset
										</ResetButton>
									</Form>
								)}
							/>
						</Col>
					</Row>
				</NewFeedFCard>

				<Divider />

				<Heading content="Your feeds" level={4} />
				{feedData.map(item => {
					const actions = [
						<ActionItem key="comment-basic-like">
							<Tooltip title="Like">
								<Icon
									type="like"
									theme={action === "liked" ? "filled" : "outlined"}
									onClick={() => handleReactions(item, "likes")}
								/>
							</Tooltip>
							<span style={{ paddingLeft: 5, cursor: "auto" }}>{item.likes}</span>
						</ActionItem>,
						<ActionItem key="comment-basic-dislike">
							<Tooltip title="Dislike">
								<Icon
									type="dislike"
									theme={action === "disliked" ? "filled" : "outlined"}
									onClick={() => handleReactions(item, "dislikes")}
								/>
							</Tooltip>
							<span style={{ paddingLeft: 5, cursor: "auto" }}>{item.dislikes}</span>
						</ActionItem>,
						<ActionItem key="comment-basic-reply-to" onClick={() => handleShowCommentForm(item)}>
							Reply to
						</ActionItem>
					]

					return (
						<StyledComment
							key={item.id}
							actions={actions}
							author={<a>{item.name}</a>}
							avatar={<Avatar src={item.avatar} alt={item.name} />}
							content={<p>{item.content}</p>}
							datetime={
								<Tooltip title={moment(item.date).format("YYYY-MM-DD HH:mm:ss")}>
									<span>{moment(item.date).fromNow()}</span>
								</Tooltip>
							}
						>
							{isCommenting && selectedItem.id === item.id ? (
								<Row gutter={8} style={{ marginBottom: "1.5em" }}>
									<Col lg={3}>
										<Avatar src={serverBaseUrl + avatar} size="large" />
									</Col>
									<Col lg={21}>
										<Formik
											onSubmit={handleSubmitFeed}
											initialValues={{ content: "" }}
											render={({ handleSubmit }) => (
												<Form layout="vertical" onSubmit={handleSubmit}>
													<StyledTextarea
														textarea
														name="comment"
														rows={4}
														placeholder="What's on your thought?"
													/>
													<SubmitButton type="primary" icon="plus">
														Send comment
													</SubmitButton>{" "}
													&nbsp;{" "}
													<ButtonLink onClick={() => setIsCommenting(false)}>
														Cancel
													</ButtonLink>
												</Form>
											)}
										/>
									</Col>
								</Row>
							) : null}
						</StyledComment>
					)
				})}
			</Col>
		</Row>
	)
}
