import React, { useState, useEffect } from "react"
import { Row, Col, BackTop, Form, Divider, Icon } from "antd"
import styled from "styled-components"
import { Heading, Layout, Section, Button, ResultSection, Loading, ButtonLink } from "components"
import { withRouter } from "react-router"
import { Formik } from "formik"

import { baseStyles } from "styles/base"
import TutorCard from "components/common/TutorCard"
import { mobile, media, newClient, baseUrl } from "helpers"
import SelectInput from "components/forms/SelectInput"
import orangeEl from "assets/images/ellipse-orange.png"
import purpleEl from "assets/images/ellipse-purple.png"
import { queryCurriculums, querySubjects } from "./query"
import useQueryData from "helpers/hooks/useQueryData"
import TextInput from "components/forms/TextInput"
import books from "assets/images/books.png"
import { mutateSearchTutors } from "queries/user"
import { useMutation, withApollo } from "react-apollo"
import Feeds from "./Feeds"
import { mutateUserProfileHome } from "queries/profile"

const initialValues = {
    keywords: "",
    subjects: 0,
    curriculums: 0,
    recently: 0,
    alphabet: 0,
    price_range: {
        startPrice: 0,
        endPrice: 150
    }
}

const token = localStorage.getItem("token")
const detailsFromStorage = JSON.parse(localStorage.getItem("userDetails")) || {}

const Home = React.memo(({ history }) => {
    const { data: curriculums } = useQueryData(queryCurriculums, "programmeClient")
    const { data: subjects } = useQueryData(querySubjects, "programmeClient")
    const [selectedSubject, setSelectedSubject] = useState({ value: -1, label: "" })
    const [keywords, setKeywords] = useState("")
    const [users, setUsers] = useState([])
    const [userDetails, setUserDetails] = useState([])

    const curriculumOptions = (curriculums.getProgrammeCurriculumList || []).map(({ name, pvid }) => ({
        value: Number(pvid),
        label: name
    }))

    const subjectOptions = (subjects.getProgrammeSubjectList || []).map(({ name, pvid }) => ({
        value: Number(pvid),
        label: name
    }))

    const [fetchUser] = useMutation(mutateUserProfileHome, {
        client: newClient(),
        onCompleted: data => {
            data = data.getProfile || {}
            setUserDetails(data)
        }
    })

    const [searchUsers, { loading }] = useMutation(mutateSearchTutors, {
        client: newClient(),
        onCompleted: data => {
            data = data.findTutors || []
            setUsers(data)
        }
    })

    const handleSearch = values => {
        setKeywords(values.keywords)
        window.location.replace(`/browse/tutors/?keywords=${values.keywords}`)
    }

    const handleSearchTutors = (values, { setSubmitting }) => {
        values = { ...initialValues, ...values, recently: 0, alphabet: 0 }
        searchUsers({ variables: values }).then(() => setSubmitting(false))
    }

    const handleSearchBySubject = selected => {
        setSelectedSubject({ value: selected.value, label: selected.label })
        window.location.replace(`/browse/tutors/?subjects=${selected.value}&subjectLabel=${selected.label}`)
    }

    const handleClickTutor = tutor => {
        history.push(`/${tutor.token}-${tutor.firstName}-${tutor.lastName}`)
    }

    const renderUsers = () => {
        if (users.length === 0) {
            return <ResultSection type="search" title="There's no tutors found" />
        }
        if (loading)
            return (
                <Col lg={24}>
                    <Section centered>
                        <Loading />
                    </Section>
                </Col>
            )
        return users.map(tutor => {
            const { pvid, ...rest } = tutor

            return (
                <CardColumn key={pvid} lg={6} xs={12}>
                    <TutorCard {...rest} onClick={() => handleClickTutor(tutor)} />
                </CardColumn>
            )
        })
    }

    useEffect(() => {
        searchUsers({ variables: initialValues })
        fetchUser()
    }, [])

    return (
        <Layout breadcrumb={false}>
            <BackTop />

            <Row>
                <Col lg={24}>
                    <HeroImage>
                        <Col lg={10}>
                            <Section>
                                <BoldHeading
                                    content={
                                        <p>
                                            Let's be part of the learning revolution in{" "}
                                            <span className="primary">Singapore</span>
                                        </p>
                                    }
                                    subheader="Find the best tutors across Singapore, today."
                                    color="white"
                                />
                                <Formik
                                    onSubmit={handleSearch}
                                    initialValues={{ keywords: "" }}
                                    render={({ handleSubmit }) => (
                                        <Form layout="vertical" onSubmit={handleSubmit}>
                                            <TextInput
                                                name="keywords"
                                                placeholder="Search for tutors..."
                                                width="100%"
                                                size="large"
                                            />
                                        </Form>
                                    )}
                                />
                            </Section>
                        </Col>
                        <Col lg={10}></Col>
                    </HeroImage>
                </Col>
            </Row>

            {token && (
                <Section>
                    <MainRow>
                        <Col lg={24}>
                            <Feeds userDetails={userDetails} />
                        </Col>
                    </MainRow>
                </Section>
            )}

            <Section style={{ position: "relative" }}>
                <Donut src={orangeEl} alt="Orange donut" width="45%" bottom="-150px" right="-200px" />
                <Donut src={purpleEl} alt="Purple donut" left="-200px" />
                <MainRow gutter={32} marginBottom="6em">
                    <Col lg={12}>
                        <video controls width="100%" muted>
                            <source src="https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" />
                        </video>
                    </Col>
                    <Col lg={12}>
                        <BoldHeadPar
                            content={
                                <p>
                                    Find <span className="primary">best tutors</span>
                                </p>
                            }
                            subheader="See the tutors that match your criteria, and make an assignment"
                        />
                    </Col>
                </MainRow>

                <MainRow gutter={48}>
                    <Col lg={12}>
                        <BoldHeading content="Find subjects that are suitable the most to you" />
                        {/* <Search name="search" width="100%" placeholder="Search subjects..." /> */}
                        <Section centered>
                            <img src={books} alt="Find subject" width="100%" />
                        </Section>
                    </Col>
                    <Col lg={12}>
                        <Row gutter={32}>
                            {subjectOptions.map(item => (
                                <SubjectColumn>
                                    <Button
                                        block
                                        size="large"
                                        key={item.value}
                                        icon={item.icon}
                                        onClick={() => handleSearchBySubject(item)}
                                    >
                                        {item.label}
                                    </Button>
                                </SubjectColumn>
                            ))}
                        </Row>
                    </Col>
                </MainRow>
            </Section>

            <Divider />

            <MainRow>
                <Col lg={22}>
                    <Row style={{ marginBottom: "2em" }} type="flex" justify="center">
                        <Col lg={18} style={{ textAlign: "center" }}>
                            <Heading content="Find your tutor" />
                            <Formik
                                onSubmit={handleSearchTutors}
                                initialValues={{ curriculums: 0, subjects: 0 }}
                                render={({ handleSubmit }) => (
                                    <Form layout="vertical" onSubmit={handleSubmit}>
                                        <Row gutter={16} type="flex" justify="center">
                                            <Col lg={8}>
                                                <SelectInput
                                                    name="curriculums"
                                                    placeholder="Based on curriculums..."
                                                    options={[
                                                        { value: 0, label: "All curriculums" },
                                                        ...curriculumOptions
                                                    ]}
                                                    marginBottom="0"
                                                />
                                            </Col>
                                            <Col lg={8}>
                                                <SelectInput
                                                    name="subjects"
                                                    placeholder="Based on subjects..."
                                                    options={[{ value: 0, label: "All subjects" }, ...subjectOptions]}
                                                    marginBottom="0"
                                                />
                                            </Col>
                                            <Col lg={16}>
                                                <Button htmlType="submit" size="medium" type="primary" block>
                                                    Search tutors
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Form>
                                )}
                            />
                        </Col>
                    </Row>
                    <Row gutter={mobile ? 16 : 32} type="flex" style={{ marginBottom: "2em" }}>
                        {renderUsers()}
                    </Row>
                    {users.length > 0 && (
                        <Row type="flex" justify="center">
                            <Col lg={24}>
                                <ButtonLink block onClick={() => history.push("/browse/tutors")}>
                                    See all other tutors &nbsp; <Icon type="right" />
                                </ButtonLink>
                            </Col>
                        </Row>
                    )}
                </Col>
            </MainRow>
        </Layout>
    )
})

// prettier-ignore
export default withRouter(withApollo(Home))

/*
███████╗████████╗██╗   ██╗██╗     ███████╗███████╗
██╔════╝╚══██╔══╝╚██╗ ██╔╝██║     ██╔════╝██╔════╝
███████╗   ██║    ╚████╔╝ ██║     █████╗  ███████╗
╚════██║   ██║     ╚██╔╝  ██║     ██╔══╝  ╚════██║
███████║   ██║      ██║   ███████╗███████╗███████║
╚══════╝   ╚═╝      ╚═╝   ╚══════╝╚══════╝╚══════╝
*/

const CardColumn = styled(Col)`
    && {
        margin-bottom: 2.5em;
        ${media.mobile`
            margin-bottom: 2em;
        `}
    }
`

const HeroImage = styled(Row).attrs(() => ({
    type: "flex",
    justify: "center",
    align: "middle"
}))`
    height: 350px;
    /* background: violet; */
    margin-bottom: 4em;
    background: linear-gradient(45deg, violet, blueviolet);
    h2.ant-typography {
        color: #fff;
        line-height: 1;
        font-weight: bold;
        margin-bottom: 0.8em;
    }
`

const SubjectColumn = styled(Col).attrs(() => ({
    lg: 12,
    xs: 12
}))`
    margin-bottom: 1.5em;
    .ant-btn {
        height: 60px;
        border: none;
        box-shadow: ${baseStyles.boxShadow.main};
        > span {
            text-overflow: ellipsis;
            overflow-x: hidden;
            max-width: 150px;
        }
        &:hover {
            transform: translateY(-3px);
            box-shadow: ${baseStyles.boxShadow.hover};
        }
    }
    ${media.mobile`
        .ant-btn {
            > span {
                max-width: 120px;
            }
        }
    `}
`

const BoldHeading = styled(Heading)`
    && {
        margin-bottom: 2em;
        h2 {
            font-weight: bold;
            line-height: 1.2;
        }
    }
`

const BoldHeadPar = styled(BoldHeading)`
    && {
        h2 {
            p {
                margin-bottom: 0;
            }
            .primary {
                color: ${baseStyles.primaryColor};
            }
        }
        .ant-typography:not(h2) {
            font-size: 1.4em;
        }
    }
`

const MainRow = styled(Row).attrs(() => ({
    type: "flex",
    justify: "center",
    align: "middle"
}))`
    padding-left: 8rem;
    padding-right: 8rem;
    margin-bottom: ${({ marginBottom }) => marginBottom || "4em"};
    ${media.mobile`
        padding-left: 1em;
        padding-right: 1em;
        > .ant-col:first-child {
            margin-bottom: 3em;
        }
    `}
`

const Donut = styled.img.attrs(({ src, alt, width }) => ({
    src,
    alt,
    width: width || "40%"
}))`
    position: absolute;
    top: ${({ top }) => top && top};
    bottom: ${({ bottom }) => bottom && bottom};
    left: ${({ left }) => left && left};
    right: ${({ right }) => right && right};
    ${media.mobile`
        display: none;
    `}
`
