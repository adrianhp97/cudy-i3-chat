import React from "react"
import { Section, Card, Heading, ButtonLink } from "components"
import { Row, Col, Form } from "antd"
import { Link, withRouter } from "react-router-dom"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import { SubmitButton } from "formik-antd"
import { useMutation } from "@apollo/react-hooks"
import { mutateSendVerificationEmail } from "queries/auth"
import { newClient } from "helpers"
import useError from "helpers/hooks/useError"

function ResendVerification({ history }) {
    const [setSend] = useMutation(mutateSendVerificationEmail, {
        client: newClient("account"),
        onCompleted: data => {
            if (data)
                history.push({
                    pathname: "/account/resend_email/success",
                    state: { success: true }
                })
        },
        onError: err => {
            if (err) useError(err)
        }
    })

    const handleSend = (values, { setSubmitting }) => {
        setSend({ variables: { email: values.email } })
    }

    return (
        <Row type="flex" justify="center" align="middle">
            <Col lg={10}>
                <Section centered>
                    <Link to="/">
                        <img src="https://app.cudy.co/img/logo_live.png" width="100" />
                    </Link>
                </Section>
                <Card autoHeight noHover style={{ padding: "1.5em 2em" }}>
                    <Heading
                        level={4}
                        content="Resend email"
                        subheader="If you didn't see any email verification we sent to you, then please enter your email address once again, and we'll send a copy"
                        marginBottom="3em"
                    />
                    <Formik
                        onSubmit={handleSend}
                        validate={validate}
                        initialValues={{ email: "" }}
                        render={({ handleSubmit }) => (
                            <Form
                                onSubmit={handleSubmit}
                                layout="vertical"
                                style={{ marginBottom: "2em" }}
                            >
                                <TextInput
                                    name="email"
                                    type="email"
                                    placeholder="E.g. jennysalim@example.com"
                                    label="Your email"
                                />
                                <SubmitButton type="primary" size="medium" icon="check">
                                    Send me the email once again
                                </SubmitButton>
                            </Form>
                        )}
                    />
                </Card>
                <Section centered>
                    <Link to="/login">
                        <ButtonLink icon="user">Go to login</ButtonLink>
                    </Link>
                </Section>
            </Col>
        </Row>
    )
}

const validate = values => {
    const error = {}

    if (!values.email) error.email = "This is required"
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email))
        error.email = "Please provide a valid email address"

    return error
}

export default withRouter(ResendVerification)
