import React, { useEffect } from "react"
import { Section, Card, Heading, Button, Tick, Alert } from "components"
import { Row, Col } from "antd"
import { Link } from "react-router-dom"

export default function Resent({ location = {}, history }) {
    useEffect(() => {
        const state = location.state || {}
        if (!state.success) history.push("/login")
    }, [])

    return (
        <Row type="flex" justify="center" align="middle">
            <Col lg={8}>
                <Section centered>
                    <Link to="/">
                        <img src="https://app.cudy.co/img/logo_live.png" width="100" />
                    </Link>
                </Section>
                <Card autoHeight noHover>
                    <Section centered>
                        <Tick />
                        <Heading
                            content="It's been sent!"
                            subheader="The verification email has been sent. Please check your inbox and click on it to get your account activated."
                            marginBottom="3em"
                        />
                        <Alert
                            showIcon
                            message="Don't forget to check your spam/junk folder too :)"
                            type="info"
                            style={{ marginBottom: "3em" }}
                        />
                        <Link to="/login">
                            <Button type="primary" size="medium" icon="user">
                                Go to login
                            </Button>
                        </Link>
                    </Section>
                </Card>
            </Col>
        </Row>
    )
}
