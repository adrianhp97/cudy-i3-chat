import React from "react"
import { Row, Col } from "antd"
import { Link } from "react-router-dom"
import Button from "../components/Button"

function About() {
    return (
        <Row>
            <Col>
                Hey! This is <strong>About</strong>!
                <section>
                    <Link to="/">
                        <Button type="ghost">Sip lagi!</Button>
                    </Link>
                </section>
            </Col>
        </Row>
    )
}

export default About
