import React from "react"
import { Link, withRouter } from "react-router-dom"
import Section from "../../components/Section"
import { Row, Col, Form, Icon, message } from "antd"
import Card from "../../components/Card"
import Heading from "../../components/Heading"
import { Formik } from "formik"
import TextInput from "../../components/forms/TextInput"
import Button from "../../components/Button"
import { useMutation } from "@apollo/react-hooks"
import { mutateNewPassword } from "queries/auth"
import { newClient } from "helpers"
import useError from "helpers/hooks/useError"

function NewPassword({ history, location }) {
    const [handleNewPassword] = useMutation(mutateNewPassword, {
        client: newClient("account"),
        onCompleted: data => {
            if (data) {
                message.loading("Resetting your password...").then(() => {
                    history.push({ pathname: "/forgot/password_changed", state: { success: true } })
                })
            }
        },
        onError: err => {
            if (err) useError(err)
        }
    })

    const params = new URLSearchParams(location.search)
    const code = params.get("code") || ""

    const handleSubmit = values => {
        values = { ...values, resetCode: code }
        handleNewPassword({ variables: { ...values } })
    }

    return (
        <Section>
            <Row type="flex" justify="center" align="middle">
                <Col lg={12}>
                    <Section centered>
                        <Link to="/">
                            <img src="https://app.cudy.co/img/logo_live.png" width="100" />
                        </Link>
                    </Section>
                    <Card title="" autoHeight noHover>
                        <Section>
                            <Heading
                                content="Enter new password"
                                subheader="Now you can enter a new password. Please don't use your pet name."
                                marginBottom="2em"
                            />
                            <Formik
                                onSubmit={handleSubmit}
                                validate={validate}
                                render={({ handleSubmit }) => (
                                    <Form onSubmit={handleSubmit} layout="vertical">
                                        <TextInput
                                            password
                                            name="newPassword1"
                                            type="password"
                                            placeholder="Enter your new password..."
                                            label="New password"
                                        />
                                        <TextInput
                                            password
                                            name="newPassword2"
                                            type="password"
                                            placeholder="Repeat your password..."
                                            label="Repeat password"
                                        />
                                        <Button type="primary" htmlType="submit">
                                            Change my password
                                        </Button>
                                    </Form>
                                )}
                            />
                        </Section>
                    </Card>
                    <Section centered>
                        <Link to="/">
                            <Icon type="home" theme="filled" /> &nbsp; Back to Home
                        </Link>
                    </Section>
                </Col>
            </Row>
        </Section>
    )
}

const validate = values => {
    const error = {}

    if (!values.newPassword1) error.newPassword1 = "You have to provide your new password"
    else if (values.newPassword1.length < 8) error.newPassword1 = "At least 8 characters"
    if (!values.newPassword2) error.newPassword2 = "Please repeat your new password"
    else if (values.newPassword2 !== values.newPassword1)
        error.newPassword2 = "The password doesn't match"

    return error
}

export default withRouter(NewPassword)
