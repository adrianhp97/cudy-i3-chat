import React, { useEffect } from "react"
import { Row, Col, Popconfirm, message } from "antd"
import { Link } from "react-router-dom"
import { Heading, Tick, Button, Card, Section, Alert, ButtonLink } from "components"
import { mutateForgotPassword } from "queries/auth"
import { newClient } from "helpers"
import useError from "helpers/hooks/useError"
import { useMutation } from "@apollo/react-hooks"

function ForgotSent({ location }) {
    const [handleForgot] = useMutation(mutateForgotPassword, {
        client: newClient("account"),
        onCompleted: data => {
            if (data) {
                window.location.replace("/forgot/sent")
                message.success("The email has been sent once again.")
            }
        },
        onError: err => {
            console.error(err)
            useError(err)
        }
    })

    const emailAddress = (location.state || {}).email || ""

    const handleResendForgot = e => {
        e.preventDefault()
        handleForgot({ variables: { email: location.state.email } })
    }

    useEffect(() => {
        if (!(location.state || {}).success) {
            window.location.replace("/login")
        }
    }, [])

    return (
        <Section style={{ marginTop: "4em" }}>
            <Row type="flex" justify="center" align="middle">
                <Col lg={8}>
                    <Card title="" autoHeight>
                        <Section centered>
                            <Tick />
                            <Heading
                                content="Instructions sent!"
                                subheader="Great! Now please take a look at your inbox. We've sent you a detail on how to reset your
                            password."
                            />
                        </Section>
                        <Row type="flex" justify="center">
                            <Col lg={18}>
                                <Alert
                                    message="Don't forget to check your spam/junk folder too :)"
                                    type="info"
                                    showIcon
                                />
                            </Col>
                        </Row>
                        <Section centered>
                            <Popconfirm
                                title="Send another copy of email?"
                                onConfirm={handleResendForgot}
                            >
                                <ButtonLink>I didn't get the email</ButtonLink>
                            </Popconfirm>
                        </Section>
                        <Section centered paddingHorizontal="normal">
                            <Link to="/">
                                <Button type="ghost" icon="home">
                                    Back to Home
                                </Button>
                            </Link>
                        </Section>
                    </Card>
                </Col>
            </Row>
        </Section>
    )
}

export default ForgotSent
