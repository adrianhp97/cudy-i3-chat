import React, { useState } from "react"
import { Formik } from "formik"
import { Form, Row, Col, Icon, message } from "antd"
import { Heading, Card, Section, Button } from "components"
import TextInput from "components/forms/TextInput"
import { Link, withRouter } from "react-router-dom"
import { useMutation } from "@apollo/react-hooks"
import { mutateForgotPassword } from "queries/auth"
import { client, newClient } from "helpers"
import useError from "helpers/hooks/useError"

function ForgotPassword({ history }) {
    const [email, setEmail] = useState("")

    const [handleForgot] = useMutation(mutateForgotPassword, {
        client: newClient("account"),
        onCompleted: data => {
            message.loading("Please wait a second...").then(() => {
                if (data) {
                    history.push({
                        pathname: "/forgot/sent",
                        state: { success: true, email }
                    })
                }
            })
        },
        onError: err => {
            console.error(err)
            useError(err)
        }
    })

    const handleSubmitForgot = values => {
        setEmail(values.email)
        handleForgot({ variables: { email: values.email } })
    }

    return (
        <Section>
            <Row type="flex" justify="center" align="middle">
                <Col lg={12}>
                    <Section centered>
                        <Link to="/">
                            <img src="https://app.cudy.co/img/logo_live.png" width="100" />
                        </Link>
                    </Section>
                    <Card title="" autoHeight>
                        <Section paddingHorizontal="normal">
                            <Heading
                                content="Forgot password"
                                subheader="Forgot your password? Don't panic. Just tell us your email here."
                                marginBottom="2em"
                            />
                            <Formik
                                validate={validate}
                                initialValues={{ email: "" }}
                                onSubmit={handleSubmitForgot}
                                render={({ handleSubmit, values }) => (
                                    <Form onSubmit={handleSubmit} layout="vertical">
                                        <TextInput
                                            type="email"
                                            name="email"
                                            label="Your email"
                                            placeholder="Enter the email you used to access Cudy"
                                            background="#f3f3f3"
                                        />
                                        <Button
                                            htmlType="submit"
                                            type="primary"
                                            disabled={!values.email}
                                        >
                                            Recover my password
                                        </Button>
                                    </Form>
                                )}
                            />
                        </Section>
                    </Card>
                    <Section centered>
                        <Link to="/">
                            <Icon type="home" theme="filled" /> &nbsp; Back to Home
                        </Link>
                    </Section>
                </Col>
            </Row>
        </Section>
    )
}

const validate = values => {
    const error = {}

    if (!values.email) error.email = "Please enter your email address"
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email))
        error.email = "Please provide a valid email address"

    return error
}

export default withRouter(ForgotPassword)
