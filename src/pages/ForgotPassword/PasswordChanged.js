import React, { useEffect } from "react"
import Section from "../../components/Section"
import { Row, Col } from "antd"
import { Link, withRouter } from "react-router-dom"
import { Heading, Tick, Button, Card } from "components"

function PasswordChanged({ history, location = {} }) {
    useEffect(() => {
        const state = location.state || {}
        if (!state.success) history.push("/")
    }, [])

    return (
        <Section style={{ marginTop: "4em" }}>
            <Row type="flex" justify="center" align="middle">
                <Col span={8}>
                    <Card title="" autoHeight>
                        <Section centered>
                            <Tick />
                            <Heading
                                content="It's changed!"
                                subheader="Great! Now you can try to login using your brand new password. Have fun! :)"
                            />
                        </Section>
                        <Section centered paddingHorizontal="normal">
                            <Link to="/login">
                                <Button type="ghost" icon="home">
                                    Go to login
                                </Button>
                            </Link>
                        </Section>
                    </Card>
                </Col>
            </Row>
        </Section>
    )
}

export default withRouter(PasswordChanged)
