import React, { useState, useEffect } from "react"
import { Row, Col, Icon, Modal } from "antd"
import { Section, Heading, Card, Steps, ButtonLink } from "components"
import { Switch, Route, Redirect, useHistory, useLocation } from "react-router"
import styled from "styled-components"

import Name from "./Name"
import decorating from "assets/images/decorating.png"
import Basic from "./Basic"
import Curriculum from "./Curriculum"
import useQueryData from "helpers/hooks/useQueryData"
import { queryFetchCurriculums } from "queries/data"
import { mobile, newClient } from "helpers"
import { useMutation } from "react-apollo"
import { mutateUserProfile, mutateUpdateProfile } from "queries/profile"

const BackgroundSection = styled(Section)`
    height: 100%;
    position: relative;
    background: url(${decorating}) no-repeat;
    background-size: cover;
    background-blend-mode: darken;
    border-radius: 10px 0 0 10px;
    && {
        .steps {
            padding-left: 2em;
            padding-top: 3em;
            display: flex;
            justify-content: center;
            align-items: center;
            color: #fff;
            .ant-steps-item-title {
                color: #fff;
            }
        }
    }
`

const StyledCard = styled(Card)`
    && {
        .ant-card-body {
            padding: 0;
        }
    }
`

const stepsArray = pathname => {
    switch (pathname) {
        case "name":
            return 0
        case "basic":
            return 1
        case "curriculum":
            return 2
        case "done":
            return 3
        default:
            return -1
    }
}

export const initialValues = {
    firstName: "",
    lastName: "",
    dob: Date.now(),
    nric: "",
    phone: "",
    gender: "",
    race: "",
    address: "",
    description: ""
}

const firstTime = JSON.parse(localStorage.getItem("firstTime"))

export default function OnboardingStudents() {
    const [activeStep, setActiveStep] = useState("name")
    const [userData, setUserData] = useState({})
    const history = useHistory()
    const location = useLocation()
    const pathname = (location.pathname || "").split("/")[3]

    const { data: curriculumData = {} } = useQueryData(queryFetchCurriculums, "programmeClient")
    const [fetchUser] = useMutation(mutateUserProfile, {
        client: newClient("profile"),
        onCompleted: data => {
            data = data.getProfile || {}
            setUserData(data)
        }
    })

    const [updateProfile, { loading }] = useMutation(mutateUpdateProfile, {
        client: newClient("account"),
        onCompleted: data => {
            setUserData(data)
            fetchUser()
            handleNext("basic")
        }
    })

    const handleUpdateProfile = (values, section) => {
        updateProfile({ variables: { ...values } }).then(() => handleNext(section))
    }

    const handleNext = section => {
        section = section.toLowerCase()
        setActiveStep(activeStep => activeStep + 1)
        history.push(`/profile/onboarding_students/${section}`)
    }

    const handlePrev = section => {
        section = section.toLowerCase()
        setActiveStep(activeStep => activeStep - 1)
        history.push(`/profile/onboarding_students/${section}`)
    }

    const handleSkipSteps = () => {
        Modal.confirm({
            title: "Skip this?",
            content: "Are you sure want to skip this at the moment?",
            centered: true,
            onOk() {
                history.push("/")
            }
        })
    }

    const steps = stepsArray(pathname)

    useEffect(() => {
        if (!localStorage.getItem("token")) history.push("/404")
        fetchUser()
        setActiveStep(steps)
    }, [steps])

    return (
        <Section width={mobile ? "100%" : "75%"} style={{ margin: "0 auto", marginTop: "3em" }}>
            <Heading
                content="You are almost done!"
                subheader="It's time to complete your profile so other people could recognize you better"
                marginBottom="3em"
                style={{ textAlign: "center" }}
            />
            <StyledCard noHover autoHeight>
                <Row gutter={32} type="flex">
                    <Col lg={8} xs={24}>
                        <BackgroundSection noPadding>
                            <div className="steps">
                                <Steps
                                    progressDot
                                    current={activeStep}
                                    steps={["Name", "Basic", "Curriculum", "Done"]}
                                />
                            </div>
                        </BackgroundSection>
                    </Col>
                    <Switch>
                        <Redirect exact from="/profile/onboarding_students" to="/profile/onboarding_students/name" />
                        <Route
                            path="/profile/onboarding_students/name"
                            render={() => (
                                <Name
                                    handlers={{ handleNext, handlePrev, handleUpdateProfile }}
                                    fetchUser={fetchUser}
                                    onData={{ userData, setUserData }}
                                    loading={loading}
                                />
                            )}
                        />
                        <Route
                            path="/profile/onboarding_students/basic"
                            render={() => (
                                <Basic
                                    handlers={{ handleNext, handlePrev, handleUpdateProfile }}
                                    onData={{ userData, setUserData }}
                                    loading={loading}
                                />
                            )}
                        />
                        <Route
                            path="/profile/onboarding_students/curriculum"
                            render={() => (
                                <Curriculum
                                    handlers={{ handleNext, handlePrev }}
                                    curriculumData={curriculumData.getProgrammeCurriculumList}
                                    onData={{ userData, setUserData }}
                                    fetchUser={fetchUser}
                                />
                            )}
                        />
                    </Switch>
                </Row>
            </StyledCard>
            <Section centered>
                <ButtonLink onClick={handleSkipSteps}>
                    Let me fill out everything later <Icon type="right" />{" "}
                </ButtonLink>
            </Section>
        </Section>
    )
}
