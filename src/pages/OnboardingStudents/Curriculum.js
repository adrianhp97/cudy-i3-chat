import React from "react"
import { Row, Col, Form, Icon } from "antd"
import { Heading, Button, ButtonLink } from "components"
import { Formik } from "formik"
import { useHistory } from "react-router"
import SelectInput from "components/forms/SelectInput"
import { useMutation } from "react-apollo"
import { mutateUpdateCurriculum } from "queries/profile"
import { newClient } from "helpers"

export default function Curriculum({ handlers, curriculumData = [], onData, fetchUser }) {
    const { handlePrev } = handlers
    const { setUserData } = onData
    const history = useHistory()

    const [updateCurriculum, { loading }] = useMutation(mutateUpdateCurriculum, {
        client: newClient(),
        onCompleted: data => {
            setUserData(data)
            fetchUser()
            history.push("/profile/info/thankyou")
        }
    })
    const handleSubmitCurriculum = (values, { setSubmitting }) => {
        updateCurriculum({ variables: { list: [{ ...values }] } })
        setSubmitting(false)
    }

    const currOptions = curriculumData.map(item => ({ value: Number(item.pvid), label: item.name }))

    return (
        <Col lg={16} xs={24} style={{ padding: "2em 3em" }}>
            <Heading
                content="Your curriculum and level"
                subheader="What curriculum and level are you currently on"
                level={4}
                marginBottom="2em"
            />
            <Formik
                onSubmit={handleSubmitCurriculum}
                render={({ handleSubmit }) => (
                    <Form layout="vertical" onSubmit={handleSubmit}>
                        <SelectInput
                            name="programmeCurriculumPvid"
                            label="Please choose one"
                            placeholder="Your current curriculum and level..."
                            options={currOptions}
                        />
                        <br />
                        <br />
                        <Row type="flex" justify="space-between" align="middle">
                            <Col lg={8}>
                                <ButtonLink
                                    size="large"
                                    htmllType="button"
                                    icon="left"
                                    onClick={() => handlePrev("Basic")}
                                >
                                    Back
                                </ButtonLink>
                            </Col>
                            <Col lg={8} style={{ textAlign: "right" }}>
                                <Button type="primary" htmlType="submit" loading={loading}>
                                    Finish <Icon type="right" />
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                )}
            />
        </Col>
    )
}
