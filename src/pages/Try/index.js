import React from "react"
import gql from "graphql-tag"
import { Query } from "react-apollo"
import Section from "../../components/Section"
import Heading from "../../components/Heading"
import { Col, Row } from "antd"
import Card from "../../components/Card"
import moment from "moment"
import { Link } from "react-router-dom"

function Try() {
    return (
        <Section>
            <Heading content="This is a page for testing things out" />
            <Section paddingHorizontal={0}>
                <Heading content="Your posts" level={4} />
                <Row gutter={32}>
                    <Query query={POST_QUERY}>
                        {({ loading, data: { posts } }) => {
                            if (loading) return "Loading..."
                            return posts.map(item => (
                                <Col key={item.id} span={8}>
                                    <Card>
                                        <Link to={`/post/${item.id}`}>
                                            <Heading
                                                content={item.title}
                                                subheader={
                                                    <span>
                                                        {item.author} &middot;{" "}
                                                        {moment(item.createdAt).format(
                                                            "DD/MM/YYYY"
                                                        )}
                                                    </span>
                                                }
                                            />
                                            <p>{item.content}</p>
                                        </Link>
                                    </Card>
                                </Col>
                            ))
                        }}
                    </Query>
                </Row>
            </Section>
        </Section>
    )
}

const POST_QUERY = gql`
    {
        posts {
            id
            title
            content
            author
            createdAt
        }
    }
`

export default Try
