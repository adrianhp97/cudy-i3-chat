import React from "react"
import Section from "../../components/Section"
import { Query } from "react-apollo"
import gql from "graphql-tag"
import { withRouter } from "react-router"
import { Row, Col } from "antd"
import Heading from "../../components/Heading"
import moment from "moment"

function Post(props) {
    // prettier-ignore
    const { match: { params } } = props

    return (
        <Section>
            <Query query={POST_SINGLE_QUERY} variables={{ id: params.id }}>
                {({ loading, data: { post } }) => {
                    if (loading) return "Loading..."
                    return (
                        <Row type="flex" justify="center">
                            <Col span={12}>
                                <Heading
                                    content={post.title}
                                    subheader={
                                        <span>
                                            {post.author} &middot;{" "}
                                            {moment(post.createdAt).format("DD/MM/YYYY")}
                                        </span>
                                    }
                                />
                                <p>{post.content}</p>
                            </Col>
                        </Row>
                    )
                }}
            </Query>
        </Section>
    )
}

const POST_SINGLE_QUERY = gql`
    query Post($id: ID!) {
        post(where: { id: $id }) {
            id
            title
            content
            author
            createdAt
        }
    }
`

export default withRouter(Post)
