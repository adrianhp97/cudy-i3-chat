import React from "react"
import { Upload as Uploadee } from "antd"

export default function Upload(props) {
    return <Uploadee {...props} />
}
