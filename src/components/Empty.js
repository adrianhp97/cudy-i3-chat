import React from "react"
import NoData from "antd/lib/empty"

function Empty({ description, ...props }) {
	return <NoData {...props} description={description} image={NoData.PRESENTED_IMAGE_SIMPLE} />
}

export default Empty
