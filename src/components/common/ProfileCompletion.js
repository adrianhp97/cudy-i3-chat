import React from "react"
import { Drawer, Icon } from "antd"
import styled from "styled-components"

import Heading from "components/Heading"
import Section from "components/Section"
import Percentage from "components/Percentage"
import { Button, Empty } from "components"
import { mobile } from "helpers"
import { Link } from "react-router-dom"

const StyledDrawer = styled(Drawer)`
    .ant-drawer-content {
        padding: 1.5em;
    }
`

const ListItem = styled.div`
    margin-bottom: 1em;
    .anticon {
        color: ${({ selected }) => !selected && "#ddd"};
    }
    span {
        color: ${({ selected }) => !selected && "#ddd"};
    }
`

const entities = ["Subjects", "Education", "Experience", "Certificate", "Other platform", "Basic profile", "Skills"]

export default function ProfileCompletion({ data, ...props }) {
    const { completionUser = {}, completionDetail = [] } = data

    return (
        <StyledDrawer {...props} width={mobile ? "100%" : 320}>
            <Section paddingHorizontal={0}>
                {completionDetail.length === 0 ? (
                    <Empty />
                ) : (
                    <Percentage type="dashboard" percent={completionUser.completionPercent} />
                )}
            </Section>
            <Heading content="You're almost there" subheader="Keep up the good progress" level={4} />
            <Section paddingHorizontal={0} marginBottom="padded">
                {entities.map(item => {
                    const selected = completionDetail.find(ent => ent.profileSection === item)

                    return (
                        <ListItem selected={selected}>
                            <Icon type="check-circle" theme={!selected ? "filled" : "twoTone"} twoToneColor="#52c41a" />{" "}
                            &nbsp; <span>{item}</span>
                        </ListItem>
                    )
                })}
            </Section>
            <Link to="/profile/me">
                <Button type="primary" block>
                    Complete my profile now
                </Button>
            </Link>
        </StyledDrawer>
    )
}
