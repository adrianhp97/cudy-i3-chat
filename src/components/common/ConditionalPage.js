import React from "react"
import { Result, Row, Col } from "antd"
import styled from "styled-components"
import { Link } from "react-router-dom"
import Button from "components/Button"

const StyledResult = styled(Result)`
    .ant-result-subtitle {
        width: 80%;
        margin: 0 auto;
    }
`

function ConditionalPage({ status = "500", ...props }) {
    if (status === "500") {
        return (
            <Row type="flex" justify="center">
                <Col lg={12}>
                    <StyledResult
                        {...props}
                        status="500"
                        title="Maintenance"
                        subTitle="Currently we are under maintenance. We'll get back to you very soon. Thanks! :)"
                    />
                </Col>
            </Row>
        )
    }

    if (status === "404") {
        return (
            <Row type="flex" justify="center">
                <Col lg={12}>
                    <StyledResult
                        {...props}
                        status="404"
                        title="404"
                        subTitle="You're playing too far. It's time to turn around and go back home."
                        extra={
                            <Link to="/">
                                <Button type="primary" icon="home">
                                    Back home
                                </Button>
                            </Link>
                        }
                    />
                </Col>
            </Row>
        )
    }
}

export default ConditionalPage
