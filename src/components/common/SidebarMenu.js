import React from "react"
import { Menu, Icon, Divider } from "antd"
import styled from "styled-components"

const StyledMenu = styled(Menu)`
    height: 100%;
    li {
        .anticon {
            color: #ff9d00;
        }
        &.ant-menu-item {
            padding: 0 25px;
        }
        .ant-menu-submenu-title {
            color: #ff9d00;
            padding: 0 25px;
        }
    }
`

function SidebarMenu(props) {
    const handleClick = ({ key }) => {
        props.history.push(`/dashboard/${key}`)
    }

    return (
        <StyledMenu theme="light" mode="vertical" onClick={handleClick}>
            <Menu.Item key="main">
                <Icon type="appstore" /> &nbsp;
                <span>Dashboard</span>
            </Menu.Item>
            <Menu.Item key="profile">
                <Icon type="user" /> &nbsp;
                <span>View profile</span>
            </Menu.Item>
            <Menu.Item key="classes/manage">
                <Icon type="fund" /> &nbsp;
                <span>Manage classes</span>
            </Menu.Item>
            <Menu.Item key="classes/create">
                <Icon type="thunderbolt" /> &nbsp;
                <span>Create class</span>
            </Menu.Item>
            <Menu.Item key="reviews">
                <Icon type="message" /> &nbsp;
                <span>Reviews</span>
            </Menu.Item>
            <Menu.Item key="resources">
                <Icon type="cloud-download" /> &nbsp;
                <span>Resources</span>
            </Menu.Item>
            <Menu.Item key="analytics">
                <Icon type="dot-chart" /> &nbsp;
                <span>Analytics</span>
            </Menu.Item>
            <Menu.Item key="payout">
                <Icon type="dollar" /> &nbsp;
                <span>Payout</span>
            </Menu.Item>
            <Menu.Item key="referral">
                <Icon type="team" /> &nbsp;
                <span>Referral</span>
            </Menu.Item>
            <Menu.Item key="feedback">
                <Icon type="api" /> &nbsp;
                <span>Feedback</span>
            </Menu.Item>
            <Divider />
            <Menu.Item key="storage">
                <Icon type="cloud" /> &nbsp;
                <span>Cloud storage</span>
            </Menu.Item>
            <Menu.Item key="social_media">
                <Icon type="fire" /> &nbsp;
                <span>Social media rewards</span>
            </Menu.Item>
        </StyledMenu>
    )
}

export default SidebarMenu
