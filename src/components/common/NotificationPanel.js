import React, { useEffect } from "react"
import { Drawer, Tabs, Comment, Icon, Divider } from "antd"
import uuid from "uuid/v4"
import socketClient from "socket.io-client"

import CommentItem from "../CommentItem"
import Tab from "../Tab"
import styled from "styled-components"
import Section from "../Section"
import Empty from "../Empty"
import Button from "../Button"
import { mobile, baseUrl } from "helpers"
import { withApollo, Subscription } from "react-apollo"
import { subscribeNewNotification } from "queries/profile"
import moment from "moment"
import { baseStyles } from "styles/base"
import ResultSection from "components/ResultSection"

const StyledDrawer = styled(Drawer)`
    .ant-drawer-body {
        padding: 0;
    }
`

const StyledSection = styled(Section)`
    padding: 20px;
`

const userDetails = (localStorage.getItem("userDetails") && JSON.parse(localStorage.getItem("userDetails"))) || {}

function NotificationPanel(props) {
    return (
        <StyledDrawer
            placement="right"
            closable={false}
            width={mobile ? "80%" : 450}
            onClose={props.onClose}
            visible={props.visible}
        >
            <Tab>
                <Tabs.TabPane tab="Notifications" key="Notifications">
                    <StyledSection>
                        {/* <Subscription
                            subscription={subscribeNewNotification}
                            variables={{ id: uuid(), userPvid: Number(userDetails.pvid) }}
                        >
                            {({ loading, data = {}, error }) => {
                                const { notification = [] } = data
                                if (error) return "Error"
                                if (loading) return "Loading"
                                if (notification.length === 0)
                                    return <Empty description="Your notification is empty" />
                                return notification.map(item => (
                                    <NotificationItem {...item} key={item.pvid} />
                                ))
                            }}
                        </Subscription> */}

                        <ResultSection title="We've been working hard for the notifications. Please stay tuned." />
                    </StyledSection>
                </Tabs.TabPane>
                {/* <Tabs.TabPane tab="Archived" key="Archived">
                    <StyledSection>
                        <Empty />
                    </StyledSection>
                </Tabs.TabPane> */}
            </Tab>
        </StyledDrawer>
    )
}

export default withApollo(NotificationPanel)

const StyledComment = styled(Comment)`
    cursor: pointer;
    border-bottom: 1px solid #eee;
    padding-bottom: 2em;
    &:hover {
        background: ${baseStyles.lightGrey.one};
    }
    .ant-comment-inner {
        padding-bottom: 0;
        .ant-comment-content-detail {
            > p {
                margin-bottom: 0;
            }
        }
        .ant-comment-avatar {
            font-size: 1.8em;
            margin-right: 1em;
        }
    }
`

function NotificationItem(props) {
    const { title = "", code, postedDate, pvid, remarks, entityId } = props

    const ava = code === "TESTI" ? "container" : "contacts"

    const renderTitle = () => {
        if (code === "TESTI") {
            return <p>You have a new testimonial from {title}</p>
        }
    }

    return (
        <StyledComment
            author={renderTitle()}
            avatar={<Icon type={ava} theme="twoTone" twoToneColor={baseStyles.primaryColor} />}
            datetime={moment(postedDate).format("DD MMMM YYYY")}
            content={<p>{remarks || ""}</p>}
        />
    )
}
