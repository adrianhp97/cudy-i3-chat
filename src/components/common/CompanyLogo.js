import React from "react"
import Section from "../Section"
import Heading from "../Heading"
import { Row, Col } from "antd"
import { companyLogo } from "../../dummy"

function CompanyLogo() {
    return (
        <Section centered>
            <Heading content="Featured On" level={3} />
            <Row>
                {companyLogo.map(src => (
                    <Col className="company-logo" key={src}>
                        <img src={src} width={100} />
                    </Col>
                ))}
            </Row>
        </Section>
    )
}

export default CompanyLogo
