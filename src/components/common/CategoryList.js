import React from "react"
import styled from "styled-components"
import { motion } from "framer-motion"
import { baseStyles } from "styles/base"
import { media } from "helpers"

const TypeSection = styled(motion.section)`
	display: flex;
	padding: 1em 0;
	margin-bottom: 2em;
	flex-wrap: nowrap;
	overflow-x: scroll;
	width: auto;
	-webkit-overflow-scrolling: touch;
	&::-webkit-scrollbar {
		display: none;
	}
	.type-button {
		color: ${baseStyles.greyColor};
		margin-right: 1em;
		min-width: 180px;
		text-align: center;
		background-color: #fff;
		box-shadow: ${baseStyles.boxShadow.hover};
		display: flex;
		justify-content: center;
		align-items: center;
		padding: 1em;
		line-height: 1;
		border-radius: 5px;
		cursor: pointer;
		&:hover,
		&.active {
			background-color: ${baseStyles.primaryColor};
			color: #fff;
		}
	}

	${media.mobile`
        .type-button {
            min-width: 120px;
        }
    `}
`

const typeSectionVariants = {
	visible: { transition: { staggerChildren: 0.1, when: "beforeChildren" } },
	hidden: { transition: { when: "afterChildren" } }
}

const typeSectionItems = {
	visible: { opacity: 1, y: 0 },
	hidden: { opacity: 0, y: -50 }
}

export default function CategoryList({ options, onClick, type }) {
	return (
		<TypeSection currentType={type} variants={typeSectionVariants} animate="visible" initial="hidden">
			{(options || []).map(item => (
				<motion.div
					key={item.value}
					value={item.value}
					variants={typeSectionItems}
					whileTap={{ scale: 0.95 }}
					className={type === item.value ? "type-button active" : "type-button"}
					onClick={() => onClick(item.value)}
				>
					{item.label}
				</motion.div>
			))}
		</TypeSection>
	)
}
