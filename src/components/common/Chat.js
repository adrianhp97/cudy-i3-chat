import React, { useState, useEffect, useRef } from "react"
import styled from "styled-components/macro"
import { motion } from "framer-motion"
import { Row, Col, Icon, Form } from "antd"
import Avatar from "components/Avatar"
import Heading from "components/Heading"
import { baseStyles } from "styles/base"
import Button from "../Button"
import { Formik } from "formik"
import TextInput from "components/forms/TextInput"
import { baseUrl, client } from "helpers"
import { queryFetchMessages, mutateSendMessage } from "queries/chat"
import socketClient from "socket.io-client"
import { useMutation, useLazyQuery } from "react-apollo"
import { newClient } from "helpers"
import moment from 'moment';
import { send } from "q"

const StyledChat = styled(motion.div)`
	position: fixed;
	left: 2rem;
	bottom: 1rem;
	border-radius: 8px;
	padding: 0;
	height: 360px;
	background-color: #fff;
	width: 250px;
	z-index: 2;
	box-shadow: ${baseStyles.boxShadow.main};
	.receiver-bar {
		border-bottom: 1px solid #ddd;
		padding: 1em 1.5em;
		margin-left: 0px !important;
		margin-right: 0px !important;
		background: #ffffff;
		width: 250px;
		max-height: 5em;
	}
	.chat-input {
		margin-left: 0px !important;
		margin-right: 0px !important;
		/* height: 60px; */
		padding: 4px;
	}
	.chat-body {
		word-break: break-word;
		margin-left: 0px !important;
		margin-right: 0px !important;
		padding: 1em 1em;
		overflow-y: scroll;
		height: calc(100% - 5em - 60px);
		::-webkit-scrollbar-track {
			background-color: #f5f5f5;
		}
		::-webkit-scrollbar {
			width: 10px;
			background-color: #f5f5f5;
		}
		::-webkit-scrollbar-thumb {
			border-radius: 10px;
			background-image: -webkit-gradient(
				linear,
				left bottom,
				left top,
				color-stop(0.44, #fed501),
				color-stop(0.72, #fcb43f),
				color-stop(0.86, #ff9d00)
			);
		}
	}
`

const HidingIcon = styled.span`
	position: absolute;
	right: 10px;
	top: 5px;
	color: ${baseStyles.greyColor};
`

const ReceiverHeading = styled(Heading).attrs({
	level: 4,
	marginBottom: "0"
})`
	h4.ant-typography {
		font-size: 1em;
	}
`

const BubbleWrapper = styled.div`
	width: 100%;
	margin-bottom: 10px;
	text-align: ${props => (props.receiver ? "left" : "right")};
`
const Bubble = styled.div`
	position: relative;
	display: inline-block;
	text-align: left;
	background: ${props => (props.receiver ? "#f0f2f5" : "#FED501")};
	border-radius: .4em;
	padding: 2px 10px;
	max-width: 70%;
	&:after {
		content: '';
		position: absolute;
		${props => (props.receiver ? "left" : "right")}: 0;
		top: 50%;
		width: 0;
		height: 0;
		border: 6px solid transparent;
		border-${props => (props.receiver ? "right" : "left")}-color: ${props => (props.receiver ? "#f0f2f5" : "#FED501")};
		border-${props => (props.receiver ? "left" : "right")}: 0;
		border-top: 0;
		margin-top: -3px;
		margin-${props => (props.receiver ? "left" : "right")}: -6px;
	}
	span {
		position: absolute;
    ${props => (props.receiver ? "right" : "left")}: -40px;
    bottom: 0;
    font-size: 0.5rem;
	}
`

const cardVariants = {
	hidden: { opacity: 0, y: 100 },
	visible: { opacity: 1, y: 0, transition: { delay: 4 } }
}

const socket = socketClient(baseUrl)
const chatClient = newClient("chat")

export default function Chat({ data = {} }) {
	const [typing, setTyping] = useState(false)
	const [typingName, setTypingName] = useState("")
	const [to, setTo] = useState(null)
	const { receiver, sender, chatParentPvid } = data
	const [messages, setMessages] = useState([])
	const [messageQueue, setMessageQueue] = useState([])
  const [inRoom, setInRoom] = useState(false)
  const messagesEndRef = useRef(null)

  const scrollToBottom = () => {
		console.log('reference here', messagesEndRef.current.scrollIntoView)
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
  }
  
  const joinChatRoom = () => {
		socket.emit("join_chat_room", {
			chatParentPvid
		})
  }
  
	const [getMessages, { loading }] = useLazyQuery(queryFetchMessages, {
		client: chatClient,
		fetchPolicy: "network-only",
		pollInterval: 0,
		onCompleted: data => {
			if (data && data.conversationList) {
				setMessages(data.conversationList)
			}
			scrollToBottom()
		},
		onError: err => {
			const error = err.graphQLErrors[0] || {}
			console.log(error)
		}
  })
  
	const [sendMessage] = useMutation(mutateSendMessage, {
		client: newClient("chat"),
		onCompleted: data => {
			socket.emit("new_message", data.message)
		},
		onError: err => {
			const error = err.graphQLErrors[0] || {}
			console.log(error)
		}
  })
  
	const handleSubmitMessage = async (values, { resetForm }) => {
		resetForm({})
		const { message } = values

		// setMessageQueue([
		// 	...messageQueue,
		// 	{
		// 		userPvid: sender.pvid,
		// 		message: message
		// 	}
		// ])
		scrollToBottom()
		await sendMessage({
			variables: {
				chatParentPvid,
				message,
				senderName: sender && `${sender.firstName || ""} ${sender.lastName || ""}`
			}
		})

		getMessages({
			variables: {
				chatParentPvid: data.chatParentPvid
			}
		})
		// setMessageQueue(messageQueue.filter((item, idx) => idx !== 0))
  }
  
	const sendTypingAction = () => {
		socket.emit("typing_in_chat_room", {
			chatParentPvid,
			firstName: sender.firstName || "",
			lastName: sender.lastName || "",
			name: `${sender.firstName || ""} ${sender.lastName || ""}`
		})
  }
  
	const timeoutFunction = () => {
		setTyping(false)
		socket.emit("typing_in_chat_room", false)
  }
  
	const handleTyping = e => {
		setTyping(true)
		sendTypingAction()
		clearTimeout(to)
		setTo(setTimeout(timeoutFunction, 200))
  }
  
	useEffect(() => {
		if (!inRoom) {
			joinChatRoom()
			getMessages({
				variables: {
					chatParentPvid: data.chatParentPvid
				}
			})
			setInRoom(true)
		}
	
		socket.on("new_message", () => {
			console.log('get new message')
			getMessages({
				variables: {
					chatParentPvid
				}
			})
		})
		socket.on("typing_in_chat_room", data => {
			if (data.name != false) {
				setTypingName(data.name)
			} else {
				setTypingName("")
			}
		})
	}, [])

  return (
		<StyledChat variants={cardVariants} animate="visible" initial="hidden">
			<Row gutter={16} type="flex" align="middle" className="receiver-bar">
				<Col lg={6}>
					<Avatar src="https://source.unsplash.com/random/" />
				</Col>
				<Col lg={18}>
					<ReceiverHeading
						content={(typingName) || (receiver && `${receiver.firstName} ${receiver.lastName}`)}
						subheader="44 minutes ago"
					/>
				</Col>
				<HidingIcon>
					<Icon type="down" />
				</HidingIcon>
			</Row>
			<Row gutter={16} type="flex" className="chat-body">
        {
          messages.map(item => {
            return (
              <BubbleWrapper receiver={parseInt(item.userPvid) !== parseInt(sender.pvid)}>
                <Bubble receiver={parseInt(item.userPvid) !== parseInt(sender.pvid)}>
                  {item.message}
                  <span>{moment(item.created_date).format('hh:mmA')}</span>
                </Bubble>
              </BubbleWrapper>
            )
          })
        }
				{
          messageQueue.map(item => {
            return (
              <BubbleWrapper>
                <Bubble>
                  {item.message}
                </Bubble>
              </BubbleWrapper>
            )
          })
        }
        <div ref={messagesEndRef} style={{height:0, width: '100%'}} />
			</Row>
			<Row gutter={16} className="chat-input">
				<Col lg={24}>
					<Formik // Untuk handle segala jenis form fields, kita pake library namanya Formik -- lebih ringkas dan efisien
						onSubmit={handleSubmitMessage}
						render={({ handleSubmit }) => (
							<Form layout="vertical" onSubmit={handleSubmit}>
								<TextInput // Component TextInput nya udah disiapin pake custom component kita sendiri (components/forms/TextInput.js)
									name="message"
									placeholder="Type your message..."
									style={{ marginBottom: 0 }}
									onKeyUp={handleTyping}
								/>
							</Form>
						)}
					/>
				</Col>
			</Row>
		</StyledChat>
	)
}