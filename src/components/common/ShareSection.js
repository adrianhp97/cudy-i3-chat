import React from "react"
import { Row, Col } from "antd"
import styled from "styled-components"
import { Tooltip } from "components"
import DynamicIcon from "./DynamicIcon"

const StyledShareIcon = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0 auto;
    width: 30px;
    height: 30px;
    border-radius: 12px;
    background-color: ${({ bg }) => bg};
    cursor: pointer;
`

const url = "https://sg.cudy.co"
const windowFeatures = "height=350,width=600,left=300,top=200"

const handleShareWhatsapp = (profileUrl, text) => {
    window.open(`whatsapp://send?text=${text} ${profileUrl}`, "whatsapp-popup", windowFeatures)
}

const handleShareFacebook = (profileUrl, text) =>
    window.open(
        "https://www.facebook.com/sharer/sharer.php?u=" + profileUrl + `&quote=${text}`,
        "facebook-popup",
        windowFeatures
    )

const handleShareTwitter = (profileUrl, text) =>
    window.open("https://twitter.com/share?url=" + profileUrl + `&text=${text}`, "twitter-popup", windowFeatures)

// const handleShareMessenger = () =>
//     window.open(
//         "fb-messenger://share?link=" +
//             encodeURIComponent(url) +
//             "&app_id=" +
//             encodeURIComponent("cudy_marketplace"),
//         "messenger-popup",
//         windowFeatures
//     )

const handleShareLinkedin = (profileUrl, text) => {
    window.open(
        `https://www.linkedin.com/shareArticle?mini=true&url=${encodeURIComponent(
            profileUrl
        )}&text=${text}&source=${profileUrl}`,
        "linkedin-popup",
        windowFeatures
    )
}

const handleShareTelegram = (profileUrl, text) => {
    window.open(
        `https://telegram.me/share/url?url=${encodeURIComponent(profileUrl)}&text=${text}`,
        "telegram-popup",
        windowFeatures
    )
}

const cudyDef =
    "Hey, I've just created an account in Cudy Marketplace. Cudy Marketplace is a marketplace for you to find best tutors around Singapore, to teach you anything"

export default function ShareSection({ userData = {}, isNotLoggedin }) {
    const profileUrl = `${url}/${userData.userCode}-${userData.firstName}-${userData.lastName}`
    const theUrl = isNotLoggedin ? url + "/register" : profileUrl
    const theText = isNotLoggedin
        ? cudyDef
        : `Hey, check out ${userData.firstName} ${userData.lastName}'s profile on Cudy Marketplace. Cudy Marketplace is a marketplace for you to find best tutors around Singapore, to teach you anything`

    return (
        <Row gutter={16} type="flex" justify="center">
            <Col lg={4} style={{ textAlign: "center" }}>
                <Tooltip title="Facebook">
                    <StyledShareIcon bg="#3c589a" onClick={() => handleShareFacebook(theUrl, theText)}>
                        <DynamicIcon type="icon-facebook-fill" color="#fff" />
                    </StyledShareIcon>
                </Tooltip>
            </Col>
            <Col lg={4} style={{ textAlign: "center" }}>
                <Tooltip title="Twitter">
                    <StyledShareIcon bg="#5eaade" onClick={() => handleShareTwitter(theUrl, theText)}>
                        <DynamicIcon type="icon-twitter-fill" color="#fff" />
                    </StyledShareIcon>
                </Tooltip>
            </Col>
            {/* <Col lg={4} style={{ textAlign: "center" }}>
                    <Tooltip title="Messenger">
                        <StyledShareIcon
                            bg="#0985dd"
                            onClick={handleShareMessenger}
                        >
                            <DynamicIcon type="icon-messenger-fill" color="#fff" />
                        </StyledShareIcon>
                    </Tooltip>
                </Col> */}
            <Col lg={4} style={{ textAlign: "center" }}>
                <Tooltip title="LinkedIn">
                    <StyledShareIcon bg="#107ab6" onClick={() => handleShareLinkedin(theUrl, theText)}>
                        <DynamicIcon type="icon-linkedin-fill" color="#fff" />
                    </StyledShareIcon>
                </Tooltip>
            </Col>
            <Col lg={4} style={{ textAlign: "center" }}>
                <Tooltip title="Whatsapp">
                    <StyledShareIcon bg="#4dc247" onClick={() => handleShareWhatsapp(theUrl, theText)}>
                        <DynamicIcon type="icon-whatsapp-line" color="#fff" />
                    </StyledShareIcon>
                </Tooltip>
            </Col>
            <Col lg={4} style={{ textAlign: "center" }}>
                <Tooltip title="Telegram">
                    <StyledShareIcon bg="#00aef0" onClick={() => handleShareTelegram(theUrl, theText)}>
                        <DynamicIcon type="icon-telegram-fill" color="#fff" />
                    </StyledShareIcon>
                </Tooltip>
            </Col>
        </Row>
    )
}
