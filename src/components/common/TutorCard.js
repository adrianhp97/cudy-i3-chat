import React from "react"
import { Card, Heading, Button, Avatar } from "components"
import { Row, Col, Rate, Icon, Tooltip } from "antd"
import styled from "styled-components"
import { baseStyles } from "styles/base"
import { media, serverBaseUrl } from "helpers"

const StyledCard = styled(Card)`
    && {
        cursor: pointer;
        ${media.mobile`
            margin-bottom: 2em;
        `}
    }
`

const TitleHeading = styled(Heading).attrs(() => ({
    level: 4,
    marginBottom: "0.5em"
}))`
    line-height: 1;
    > .ant-typography:not(h4) {
        font-size: 0.85em;
        color: ${baseStyles.greyColor};
    }
    && {
        h4 {
            font-size: 1.1em;
            margin-bottom: 0.7em;
        }
    }
`

const DescriptionHeading = styled(Heading).attrs(() => ({
    level: 4
}))`
    > .ant-typography:not(h4) {
        font-size: 0.85em;
    }
    h4 {
        font-size: 0.9em;
    }
`

const BottomButton = styled(Button)`
    position: absolute;
    bottom: -15px;
    left: 50%;
    transform: translate(-50%);
    width: 80%;
`

export default function TutorCard({ pricePerHour = "", firstName = "", lastName = "", ...props }) {
    const { education = [], resource = {}, experienceYears = "", rating = 0 } = props

    const experience =
        experienceYears === "0" || experienceYears === null ? "No experience yet" : experienceYears + "y exp"

    const locexp = (
        <span style={{ lineHeight: 1.4 }}>
            {props.address || ""} &middot; {experience}
        </span>
    )

    const avatar = serverBaseUrl + resource.path
    // const mostExpensive = pricePerHour.reduce(
    //     (acc, curr) => (curr.price > (acc.price || 0) ? curr.price : acc.price),
    //     {}
    // )
    // const cheapest = pricePerHour.reduceRight(
    //     (acc, curr) => (curr.price > (acc.price || 0) ? curr.price : acc.price),
    //     {}
    // )
    // const pph =
    //     pricePerHour.length > 1
    //         ? `$${cheapest} - $${mostExpensive}`
    //         : pricePerHour.map(item => `$${item.price}`)

    return (
        <StyledCard {...props} description={null} height="100%">
            <Row type="flex" justify="center" style={{ textAlign: "center", marginBottom: "1em" }}>
                <Col lg={24}>
                    <Avatar src={avatar || ""} size={75} style={{ marginBottom: ".5em" }} />
                    <TitleHeading content={firstName + " " + lastName} subheader={locexp} />
                    <Rate value={rating || "no ratings received yet"} style={{ fontSize: ".9em" }} allowHalf disabled />
                </Col>
            </Row>
            <Row type="flex" justify="center" gutter={16}>
                <Col lg={24} style={{ textAlign: "center" }}>
                    <DescriptionHeading subheader={((education && education[0]) || {}).name || ""} />
                </Col>
                <Col lg={8} style={{ textAlign: "center" }}>
                    <Tooltip title="Amount of testimonials received">
                        <Icon type="crown" /> <span>{props.testimonial || 0}</span>
                    </Tooltip>
                </Col>
                <Col lg={8} style={{ textAlign: "center" }}>
                    <Tooltip title="Price for a lesson per hour">
                        <Icon type="dollar" /> <span>{pricePerHour || "0"}</span>
                    </Tooltip>
                </Col>
            </Row>
            <BottomButton block type="primary" size="medium">
                See profile
            </BottomButton>
        </StyledCard>
    )
}
