import React from "react"
import { List, Avatar, Typography, Rate, Input, Icon, Tooltip, Row, Col } from "antd"
import Button from "../Button"
import Heading from "../Heading"
import Section from "../Section"
import styled from "styled-components"
import { Formik, Field } from "formik"
import TextInput from "../forms/TextInput"

const Header = styled(Heading)`
    .date-time {
        font-size: 0.7em;
        color: #999;
        font-weight: normal;
    }
`

const Emot = styled.span`
    && {
        color: #ff9d00;
        font-size: 14px;
        cursor: pointer;
        margin-right: 1em;
        &:hover {
            font-weight: bold;
        }
    }
`

const ListItem = styled(List.Item)`
    width: 75%;
    .ant-list-item-meta {
        &.more-margin {
            margin-bottom: 14px;
        }
    }
    && {
        .ant-list-item-meta-description {
            div > {
                .ant-typography {
                    margin-bottom: 2em;
                }
            }
            input.ant-input {
                margin-bottom: 1.5em;
            }
            textarea {
                margin-bottom: 1.5em;
                &.no-margin {
                    margin-bottom: 0;
                }
            }
            .cancel {
                opacity: 0.8;
                cursor: pointer;
                &:hover {
                    opacity: initial;
                }
            }
        }
    }
`

// interface listItem = {
// ==== src
// ==== student
// ==== title
// ==== level
// ==== description
// ==== rating
// ==== date
// }

function ReviewList(props) {
    const { data = [], itemId, userType, activeTab } = props

    return (
        <List
            {...props}
            dataSource={data}
            renderItem={item => {
                if (activeTab === "review-gained") {
                    return (
                        <ListItem key={item.id}>
                            <List.Item.Meta
                                avatar={<Avatar size="large" src={item.src} />}
                                title={
                                    <Header
                                        content={
                                            <span>
                                                {item.student} &middot;{" "}
                                                <span className="date-time">{item.date}</span>
                                            </span>
                                        }
                                        level={4}
                                        subheader={`${item.title} - ${item.level}`}
                                    />
                                }
                                description={
                                    <div>
                                        {itemId === item.id ? (
                                            <Input
                                                onPressEnter={props.onCloseEdit}
                                                defaultValue={item.description}
                                            />
                                        ) : (
                                            <Typography.Paragraph
                                                ellipsis={{ rows: 3, expandable: true }}
                                            >
                                                {item.description}
                                            </Typography.Paragraph>
                                        )}
                                        Your rating: &nbsp; <Rate defaultValue={item.rating} />
                                        <Section noPadding textAlign="right">
                                            {userType === "student" ? (
                                                <>
                                                    {item.id === itemId && (
                                                        <span
                                                            className="cancel"
                                                            onClick={props.onCloseEdit}
                                                        >
                                                            Cancel &nbsp;{" "}
                                                        </span>
                                                    )}
                                                    <Button
                                                        type={
                                                            item.id === itemId ? "primary" : "ghost"
                                                        }
                                                        size="default"
                                                        icon={
                                                            item.id === itemId ? undefined : "edit"
                                                        }
                                                        onClick={
                                                            item.id === itemId
                                                                ? props.onCloseEdit
                                                                : () =>
                                                                      props.onShowTextInput(item.id)
                                                        }
                                                    >
                                                        {item.id === itemId
                                                            ? "Save"
                                                            : "Edit review"}
                                                    </Button>
                                                </>
                                            ) : (
                                                <Row type="flex" align="middle" justify="end">
                                                    <Col span={4} style={{ textAlign: "left" }}>
                                                        <Tooltip title="Smiles given">
                                                            <Emot>
                                                                <Icon type="smile" /> 24
                                                            </Emot>
                                                        </Tooltip>
                                                        &nbsp; &nbsp;
                                                        <Tooltip title="Frowns given">
                                                            <Emot>
                                                                <Icon type="frown" /> 11
                                                            </Emot>
                                                        </Tooltip>
                                                    </Col>
                                                    <Col>
                                                        <span onClick={() => ({})}>
                                                            <Tooltip title="Report inappropriate">
                                                                <Button
                                                                    icon="flag"
                                                                    type="dashed"
                                                                    size="default"
                                                                    shape="circle"
                                                                />
                                                            </Tooltip>
                                                        </span>
                                                    </Col>
                                                </Row>
                                            )}
                                        </Section>
                                    </div>
                                }
                            />
                        </ListItem>
                    )
                } else {
                    return (
                        <ListItem key={item.id}>
                            <List.Item.Meta
                                className="more-margin"
                                avatar={<Avatar size="large" src={item.src} />}
                                title={
                                    <Header
                                        content={
                                            <span>
                                                {item.student} &middot;{" "}
                                                <span className="date-time">{item.date}</span>
                                            </span>
                                        }
                                        level={4}
                                        subheader={`${item.title} - ${item.level}`}
                                    />
                                }
                                description={
                                    <Formik
                                        initialValues={{ review: "" }}
                                        onSubmit={() => ({})}
                                        render={({ dirty }) => (
                                            <>
                                                <Field
                                                    textarea
                                                    name="review"
                                                    placeholder="Write your review for this class"
                                                    className="no-margin"
                                                    // marginBottom="20px"
                                                    component={TextInput}
                                                />
                                                <Button
                                                    type="primary"
                                                    htmlType="submit"
                                                    icon="check"
                                                    size="default"
                                                    disabled={!dirty}
                                                >
                                                    Submit review
                                                </Button>{" "}
                                                &nbsp;
                                                <span className="cancel" onClick={() => ({})}>
                                                    Cancel &nbsp;{" "}
                                                </span>
                                            </>
                                        )}
                                    />
                                }
                            />
                        </ListItem>
                    )
                }
            }}
        />
    )
}

export default ReviewList
