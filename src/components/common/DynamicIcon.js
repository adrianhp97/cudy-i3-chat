import React from "react"
import { Icon } from "antd"

export default function DynamicIcon({ size, color, ...props }) {
    const TheIcon = Icon.createFromIconfontCN({
        scriptUrl: "//at.alicdn.com/t/font_1290431_3eh3kl2na77.js",
        extraCommonProps: { style: { fontSize: size || 20, color } }
    })

    return <TheIcon {...props} size={size} />
}
