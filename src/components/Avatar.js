import React from "react"
import Ava from "antd/lib/avatar"

function Avatar(props) {
	return <Ava {...props} />
}

export default Avatar
