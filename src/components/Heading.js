import React from "react"
import styled from "styled-components"
import Typography from "antd/lib/typography"
import Icon from "antd/lib/icon"
import Row from "antd/lib/row"
import Col from "antd/lib/col"
import { baseStyles } from "styles/base"

const StyledHeading = styled(Typography.Title)`
	&& {
		margin-bottom: 0;
		font-size: ${({ reverse }) => reverse && "12px"};
		text-transform: ${({ reverse }) => (reverse ? "uppercase" : "initial")};
		color: ${({ reverse }) => reverse && "#ff9d00"};
		font-weight: ${({ bold }) => bold && "bold"};
	}
`

const Paragraph = styled(Typography.Paragraph)`
	&& {
		margin-bottom: 0;
		color: ${({ color }) => color || baseStyles.greyColor};
	}
`

const Typo = styled(Typography)`
	&& {
		margin-bottom: ${({ marginBottom }) => (marginBottom ? marginBottom : "1em")};
		ul {
			margin-bottom: 0;
		}
	}
`

function Heading({
	content = "",
	subheader = "",
	icon = "",
	iconSize = 20,
	iconTheme = "outlined",
	level = 2,
	bold,
	color = "",
	...props
}) {
	if (props.image) {
		return (
			<Row
				{...props}
				type="flex"
				gutter={32}
				style={{ marginBottom: props.marginBottom ? props.marginBottom : "1em" }}
			>
				<Col lg={5}>
					<img src={props.image} alt="Some image" width="100%" />
				</Col>
				<Col lg={19}>
					<StyledHeading level={level} bold={bold} reverse={props.reverse || false}>
						{content}
					</StyledHeading>
					{subheader && (
						<Paragraph type="warning" color={color}>
							{icon && (
								<Icon
									type={icon}
									theme={iconTheme}
									style={{ fontSize: iconSize, verticalAlign: "middle" }}
								/>
							)}{" "}
							{subheader}
						</Paragraph>
					)}
				</Col>
			</Row>
		)
	}

	return (
		<Typo {...props}>
			{icon && <Icon type={icon} theme={iconTheme} style={{ fontSize: iconSize, verticalAlign: "middle" }} />}
			<StyledHeading reverse={props.reverse || false} bold={bold} level={level}>
				{content}
			</StyledHeading>
			{subheader && <Paragraph color={color}>{subheader}</Paragraph>}
		</Typo>
	)
}

export default Heading
