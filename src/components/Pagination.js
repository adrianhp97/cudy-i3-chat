import React from "react"
import { Pagination as Pager } from "antd"

function Pagination(props) {
    return <Pager {...props} />
}

export default Pagination
