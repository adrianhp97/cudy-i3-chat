import React from "react"
import Pop from "antd/lib/modal"
import styled from "styled-components"

const StyledModal = styled(Pop)`
	border-radius: 10px;
	.ant-modal-content {
		border-radius: 10px;
		.ant-modal-header {
			border-radius: 10px 10px 0 0;
		}
	}
`

function Modal({ children, ...props }) {
	return <StyledModal {...props}>{children}</StyledModal>
}

export default Modal
