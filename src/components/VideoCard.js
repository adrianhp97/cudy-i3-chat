import React from "react"
import styled from "styled-components"
import { Icon, Tag, Typography, Popconfirm } from "antd"
import Heading from "./Heading"
import { boxShadow, baseStyles } from "styles/base"
import moment from "moment"

const { Paragraph } = Typography

const StyledCard = styled.div`
    position: relative;
    height: 200px;
    width: 100%;
    padding: 1.5em;
    border-radius: 8px;
    background: ${({ src }) => `url(${src}) center no-repeat`};
    background-size: cover;
    background-color: rgba(0, 0, 0, 0.4);
    background-blend-mode: multiply;
    box-shadow: ${boxShadow[1]};
    cursor: pointer;
    transition: all 0.15s ease;
    &:hover {
        /* transform: translateY(-3px); */
        background-color: rgba(0, 0, 0, 0.2);
        .delete {
            display: flex;
            visibility: visible;
        }
        .play {
            color: #ff9d00;
        }
    }
    .play {
        margin-bottom: 1em;
        color: #eee;
    }
    .ant-typography {
        color: #eee;
        h4 {
            line-height: 1.3;
            font-size: 1.2em;
            > .ant-typography {
                margin-bottom: 0.5em;
            }
        }
    }
`

const DeleteButton = styled.span`
    justify-content: center;
    align-items: center;
    position: absolute;
    top: -10px;
    right: -5px;
    width: 25px;
    height: 25px;
    background-color: ${baseStyles.primaryColor};
    border-radius: 50px;
    color: #fff;
    display: none;
    visibility: hidden;
    z-index: 10;
    cursor: pointer;
`

const TheSection = styled.div`
    position: relative;
    &:hover {
        ${DeleteButton} {
            display: flex;
            visibility: visible;
        }
    }
`

// prettier-ignore
export default function VideoCard({ fileName = "", description = "", durationSecs, ...props }) {
    const { setModal, onSelectItem, onDeleteVideo } = props
    fileName = fileName.includes(".") ? fileName.slice(0, -4) : fileName
    durationSecs = moment(durationSecs, "mm").format("HH:mm")
    return (
        <TheSection>
            <Popconfirm title="Delete this introduction video?" onConfirm={() => onDeleteVideo(props)}>
                <DeleteButton className="delete" onClick={() => setModal(false)}>
                    <Icon type="close" />
                </DeleteButton>
            </Popconfirm>
            <StyledCard {...props} onClick={() => onSelectItem(props)}>
                <div className="play">
                    <Icon type="play-circle" theme="filled" />
                </div>
                <Heading
                    level={4}
                    content={<Paragraph ellipsis={{ rows: 4 }}>{fileName}</Paragraph>}
                    subheader={description}
                />
                <Tag color="black">{durationSecs || ""}</Tag>
            </StyledCard>
        </TheSection>
    )
}
