import React from "react"
import Tip from "antd/lib/tooltip"

function Tooltip({ children, ...props }) {
	return <Tip {...props}>{children}</Tip>
}

export default Tooltip
