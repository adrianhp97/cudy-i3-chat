import React from "react"
import styled from "styled-components"

const StyledLogo = styled.div`
    margin-bottom: ${({ mb }) => mb || "1em"};
    cursor: pointer;
`

export default function Logo({ width, onClick, mb, ...props }) {
    return (
        <StyledLogo {...props} mb={mb}>
            <img
                src="https://app.cudy.co/img/logo_live.png"
                onClick={onClick}
                width={width || 100}
            />
        </StyledLogo>
    )
}
