import React, { useState } from "react"
import { Layout as PageLayout, Breadcrumb, Button, Affix, Icon } from "antd"
import { connect } from "react-redux"
import Navbar from "./Navbar"
import styled from "styled-components"
import Section from "./Section"
import { withRouter, Link } from "react-router-dom"
import SidebarMenu from "./common/SidebarMenu"
import { unauthUser } from "../store/actions/authActions"
import useMobile from "../helpers/hooks/useMobile"
import { media } from "helpers"

const Header = styled(PageLayout.Header)`
    && {
        background-color: #fff;
        height: 5em;
        ${media.mobile`
            height: auto;
            padding: 0;
        `}
    }
`
const Footer = styled(PageLayout.Footer)`
    text-align: center;
    padding: 3em;
`

const Bread = styled(Breadcrumb)`
    margin-bottom: 2em;
`

const Sidebar = styled(PageLayout.Sider)`
    && {
        min-height: 100vh;
        background: #fff;
        border-right: 1px solid #e8e8e8;
        .ant-layout-sider-trigger {
            background: #ff9d00;
        }
    }
`

const StickyButton = styled.div`
    position: sticky;
    bottom: 10px;
    right: 10px;
    text-align: right;
    margin-right: 20px;
    padding-bottom: 10px;
`

const pathMap = {
    "/browse/tutors": "Browse tutors",
    "/profile/me": "My profile",
    "/profile/viewed": "Who's viewed me",
    "/profile": "Profile",
    "/assignments": "Assignments",
    "/assignments/post_assignment": "Post an assignment",
    "/browse": "Tutors"
}

const unlinkedUrl = ["/profile", "/browse"]

function Layout({ breadcrumb = true, customBread = "", location = {}, history, ...props }) {
    const { basic = false, sidebar = false, children } = props
    const isMobile = useMobile("(max-width: 414px)")

    const [collapsed, setCollapsed] = useState(true)
    const pathSnippets = location.pathname.split("/").filter(item => item)
    const extraBreadcrumbItems = pathSnippets.map((_, index) => {
        const url = `/${pathSnippets.slice(0, index + 1).join("/")}`

        if (customBread) return <Breadcrumb.Item>{customBread}</Breadcrumb.Item>

        return (
            <Breadcrumb.Item key={url}>
                {unlinkedUrl.includes(url) ? pathMap[url] : <Link to={url}>{pathMap[url]}</Link>}
            </Breadcrumb.Item>
        )
    })

    const breadcrumbItems = [
        <Breadcrumb.Item key="home">
            <Link to="/">
                <Icon type="home" />
            </Link>
        </Breadcrumb.Item>,
        ...extraBreadcrumbItems
    ]

    return (
        <PageLayout>
            {!basic && (
                <Header>
                    <Navbar loading={props.loading} onUnauthUser={props.unauthUser} />
                </Header>
            )}

            <PageLayout.Content>
                <PageLayout>
                    {sidebar && (
                        <Sidebar
                            collapsible
                            collapsed={collapsed}
                            // onCollapse={() => setCollapsed(!collapsed)}
                            onMouseEnter={() => setCollapsed(false)}
                            onMouseLeave={() => setCollapsed(true)}
                            trigger={!isMobile ? <Button icon="menu" type="link" /> : ""}
                            breakpoint="lg"
                            collapsedWidth={isMobile ? 0 : 0}
                            onBreakpoint={broken => console.log(broken)}
                        >
                            <Affix offsetTop={20}>
                                <SidebarMenu onSetCollapsed={setCollapsed} history={history} />
                            </Affix>
                        </Sidebar>
                    )}

                    <PageLayout>
                        <PageLayout.Content>
                            {breadcrumb && (
                                <Section paddingHorizontal="padded">
                                    <Bread>{breadcrumbItems}</Bread>
                                </Section>
                            )}
                            {children}
                        </PageLayout.Content>
                        {sidebar && (
                            <Footer>
                                <div>
                                    <img src="https://app.cudy.co/img/logo_live.png" width="100" /> <br />
                                    <p>Cudy Pte Ltd &middot; all rights reserved 2019</p>
                                </div>
                            </Footer>
                        )}
                    </PageLayout>
                </PageLayout>
                <StickyButton>
                    <Button type="dashed" icon="coffee" id="show-updates">
                        Show updates
                    </Button>
                </StickyButton>
            </PageLayout.Content>

            {!basic && !sidebar && (
                <Footer>
                    <div>
                        <img src="https://app.cudy.co/img/logo_live.png" width="100" /> <br />
                        <p>Cudy Pte Ltd &middot; all rights reserved 2019</p>
                    </div>
                </Footer>
            )}
        </PageLayout>
    )
}

const mapState = ({ auth }) => ({
    user: auth.user,
    loading: auth.loading
})

export default connect(
    mapState,
    { unauthUser }
)(withRouter(Layout))
