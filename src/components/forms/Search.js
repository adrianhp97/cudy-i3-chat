import React from "react"
import { Input } from "antd"
import styled from "styled-components"

const StyledSearch = styled(Input.Search)`
    && {
        width: ${({ width }) => width || "200px"};
        height: 45px;
        border-radius: 6px;
        .ant-input {
            /* background: #f3f3f3; */
            border: none;
            &:focus {
                width: 100%;
                background: initial;
                box-shadow: 1px 3px 7px rgba(0, 0, 0, 0.2);
            }
        }
    }
`

function Search(props) {
    return <StyledSearch {...props} />
}

export default Search
