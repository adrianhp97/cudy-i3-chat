import React from "react"
import { Select, FormItem } from "formik-antd"
import styled from "styled-components"
import { Form, Select as SelectAntd, Typography } from "antd"

const Item = styled(FormItem)`
    && {
        .ant-form-item-label {
            label {
                font-size: ${({ reverse }) => reverse && "11px"};
                color: ${({ reverse }) => reverse && "#bbb"};
                text-transform: ${({ reverse }) => reverse && "uppercase"};
                line-height: ${({ reverse }) => reverse && "1.4"};
                font-weight: ${({ reverse }) => reverse && "600"};
            }
        }
        .help-text {
            font-size: 12px;
            color: #999;
        }
    }
`

function SelectInput({ reverse, options, helpText, name, label, independent = false, ...props }) {
    if (independent)
        return (
            <Form.Item
                name={name || ""}
                label={label || ""}
                style={{ marginBottom: props.marginBottom || "" }}
            >
                <SelectAntd
                    {...props}
                    name={name || ""}
                    onChange={props.onChange}
                    style={{ width: props.width || "100%" }}
                >
                    {(options || []).map(item => (
                        <SelectAntd.Option key={item.value} value={item.value}>
                            {item.label}
                        </SelectAntd.Option>
                    ))}
                </SelectAntd>
            </Form.Item>
        )

    return (
        <Item
            name={name}
            reverse={reverse}
            label={label}
            style={{ width: props.width || "100%", marginBottom: props.marginBottom || "" }}
        >
            <Select {...props} name={name}>
                {(options || []).map(item => (
                    <Select.Option key={item.value} value={item.value}>
                        <Typography.Text ellipsis>{item.label}</Typography.Text>
                    </Select.Option>
                ))}
            </Select>
            {helpText && <span className="help-text">{helpText}</span>}
        </Item>
    )
}

export default SelectInput
