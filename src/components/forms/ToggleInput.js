import React from "react"
import { Switch, FormItem } from "formik-antd"
import { Form, Switch as AntdSwitch } from "antd"

export default function ToggleInput({ name, children, label, independent, ...props }) {
    if (independent) {
        return (
            <Form.Item name={name} label={label}>
                <AntdSwitch {...props} name={name}>
                    {children}
                </AntdSwitch>
            </Form.Item>
        )
    }

    return (
        <FormItem name={name} label={label}>
            <Switch {...props} name={name}>
                {children}
            </Switch>
        </FormItem>
    )
}
