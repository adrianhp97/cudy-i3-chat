import React from "react"
import { FormItem, Radio } from "formik-antd"
import styled from "styled-components"
import { Form, Radio as AntdRadio } from "antd"

const Item = styled(FormItem)`
    && {
        .ant-radio-wrapper {
            margin-right: 15px;
            > .ant-radio {
                margin-right: 5px;
            }
        }
    }
`

function RadioInput({ options, name, label, independent, ...props }) {
    if (independent) {
        return (
            <Form.Item name={name} label={label}>
                <AntdRadio.Group {...props} name={name} buttonStyle="solid">
                    {options.map(item => (
                        <AntdRadio.Button value={item.value} key={item.value}>
                            {item.label}
                        </AntdRadio.Button>
                    ))}
                </AntdRadio.Group>
            </Form.Item>
        )
    }

    return (
        <FormItem name={name} label={label}>
            <Radio.Group {...props} name={name} buttonStyle="solid">
                {options.map(item => (
                    <Radio.Button value={item.value} key={item.value}>
                        {item.label}
                    </Radio.Button>
                ))}
            </Radio.Group>
        </FormItem>
    )
}

export default RadioInput
