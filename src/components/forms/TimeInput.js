import React from "react"
import { TimePicker, FormItem } from "formik-antd"

export default function TimeInput({ name, label, ...props }) {
    return (
        <FormItem name={name} label={label}>
            <TimePicker {...props} name={name} />
        </FormItem>
    )
}
