import React from "react"
import { Checkbox } from "formik-antd"

export default function CheckboxInput({ group, ...props }) {
    if (group) return <Checkbox.Group {...props} />

    return <Checkbox {...props} />
}
