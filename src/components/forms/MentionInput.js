import React from "react"
import { FormItem } from "formik-antd"
import { Mentions, Form } from "antd"

export default function MentionInput({ field, form, label, options, name, independent, ...props }) {
    // const touched = form.touched[field.name]
    // const hasError = form.errors[field.name]
    // const submittedError = hasError
    // const touchedError = hasError && touched
    // const onChange = value => form.setFieldValue(field.name, value)
    // const onBlur = () => form.setFieldTouched(field.name, true)

    if (independent) {
        return (
            <Form.Item name={name} label={label}>
                <Mentions {...props} name={name}>
                    {(options || []).map(item => (
                        <Mentions.Option key={item.value} value={item.value}>
                            {item.label}
                        </Mentions.Option>
                    ))}
                </Mentions>
            </Form.Item>
        )
    }

    // return (
    //     <FormItem
    //         name={field.name}
    //         label={label}
    //         help={submittedError || touchedError ? hasError : false}
    //         validateStatus={submittedError || touchedError ? "error" : "success"}
    //     >
    //         <Mentions {...field} {...props} onChange={onChange} onBlur={onBlur}>
    //             {(options || []).map(({ value, label }) => (
    //                 <Mentions.Option key={value}>{label}</Mentions.Option>
    //             ))}
    //         </Mentions>
    //     </FormItem>
    // )
}
