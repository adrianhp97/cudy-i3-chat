import React from "react"
import { FormItem as Item, Input, InputNumber } from "formik-antd"
import { Input as InputAntd, InputNumber as InputAntdNumber, Form } from "antd"
import styled from "styled-components"
import { Field } from "formik"

const FormItem = styled(Item)`
    && {
        margin-bottom: 1em;
        .ant-input-number,
        input {
            width: 100%;
        }
        .ant-input-prefix {
            color: rgba(0, 0, 0, 0.25);
        }
        .ant-form-item-label {
            label {
                font-size: ${({ reverse }) => reverse && "11px"};
                color: ${({ reverse }) => reverse && "#bbb"};
                text-transform: ${({ reverse }) => reverse && "uppercase"};
                line-height: ${({ reverse }) => reverse && "1.4"};
                font-weight: ${({ reverse }) => reverse && "600"};
            }
        }
        .help-text {
            font-size: 12px;
            color: #999;
        }
    }
`

export const SpecialComponent = ({ field, ...props }) => {
    field.value = props.value

    return (
        <Form.Item
            {...field}
            {...props}
            value={props.value}
            validateStatus={props.error && "error"}
            help={props.error && props.error}
            label={props.label}
        >
            <InputAntd {...field} {...props} value={props.value} onChange={props.onChange} id={props.id} />
        </Form.Item>
    )
}

function TextInput({ helpText, independent, special, ...props }) {
    let Inputee = independent ? InputAntd : Input
    let InputeeNumber = independent ? InputAntdNumber : InputNumber

    if (special) return <Field {...props} id={props.id} name={props.name} component={SpecialComponent} />

    if (independent) {
        if (props.number)
            return (
                <Form.Item name={props.name || ""} label={props.label || ""}>
                    <InputAntdNumber {...props} style={{ width: "100%" }} />
                </Form.Item>
            )

        if (props.textarea)
            return (
                <Form.Item name={props.name || ""} label={props.label || ""}>
                    <InputAntd.TextArea {...props} style={{ width: "100%" }} />
                </Form.Item>
            )

        return (
            <Form.Item
                help={props.error || ""}
                validateStatus={props.error && "error"}
                name={props.name || ""}
                label={props.label || ""}
                onPressEnter={props.onPressEnter}
            >
                <InputAntd {...props} style={{ width: "100%" }} />
            </Form.Item>
        )
    }

    return (
        <FormItem name={props.name} style={props.style} reverse={props.reverse} extra={props.extra} label={props.label}>
            {props.textarea ? (
                <>
                    <Inputee.TextArea {...props} />
                    {helpText && <span className="help-text">{helpText}</span>}
                </>
            ) : props.password ? (
                <>
                    <Inputee.Password {...props} />
                    {helpText && <span className="help-text">{helpText}</span>}
                </>
            ) : props.number ? (
                <>
                    <InputeeNumber {...props} style={{ width: "100%" }} />
                    {helpText && <span className="help-text">{helpText}</span>}
                </>
            ) : (
                <>
                    <Inputee {...props} />
                    {helpText && <span className="help-text">{helpText}</span>}
                </>
            )}
        </FormItem>
    )
}

export default TextInput
