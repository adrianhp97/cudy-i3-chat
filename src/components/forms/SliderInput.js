import React from "react"
import { Slider, FormItem } from "formik-antd"

export default function SliderInput({ name, label, extra, ...props }) {
	return (
		<FormItem name={name} label={label} extra={extra}>
			<Slider {...props} name={name} />
		</FormItem>
	)
}
