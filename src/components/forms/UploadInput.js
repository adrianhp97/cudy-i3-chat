import React from "react"
import { Upload, Icon, message } from "antd"
import Section from "../Section"
import Heading from "../Heading"
import styled from "styled-components"
import Button from "../Button"

const Dragger = styled(Upload.Dragger)`
    && {
        .ant-upload {
            margin-bottom: 1.5em;
        }
        .ant-upload-list {
            margin-bottom: 1.5em;
        }
    }
`

const Uploader = styled(Upload)`
    && {
        .ant-upload {
            margin-bottom: 1.5em;
        }
        .ant-upload-list {
            margin-bottom: 1.5em;
        }
    }
`

function UploadInput(props) {
    const { draggable = true } = props

    const baseProps = {
        name: props.name,
        multiple: props.multiple || true,
        action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
        onChange(info) {
            const status = info.file.status
            if (status !== "uploading") {
                console.log(info.file, info.fileList)
            }
            if (status === "done") {
                message.success(`${info.file.name} file uploaded successfully.`)
            } else if (status === "error") {
                message.error(`${info.file.name} file upload failed.`)
            }
        }
    }

    if (draggable)
        return (
            <Dragger {...props} {...baseProps}>
                <Section>
                    <p>
                        <Icon type="inbox" style={{ fontSize: 40 }} />
                    </p>
                    <Heading
                        level={4}
                        content="Click or drag file to this to upload"
                        subheader="Support for a single or bulk upload. Go ahead!"
                    />
                </Section>
            </Dragger>
        )

    return (
        <Upload {...props} {...baseProps}>
            <Button type={props.buttonType || "dashed"} size="default">
                <Icon type="upload" /> {props.buttonText || "Attach a file"}
            </Button>
        </Upload>
    )
}

export default UploadInput
