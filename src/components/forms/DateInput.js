import React from "react"
import { Form } from "antd"
import moment from "moment"
import { FormItem, DatePicker } from "formik-antd"
import styled from "styled-components"

const StyledItem = styled(Form.Item)`
    .ant-input {
        width: ${({ width }) => width || "100%"};
    }
`

function DateInput({ monthly, width, ...props }) {
    // const { field, form } = props
    // const name = field.name
    // const { touched, errors, setFieldValue } = form
    // const value = form.values && form.values[name]

    if (monthly)
        return (
            <FormItem name={props.name} label={props.label}>
                <DatePicker.MonthPicker name={props.name} {...props} style={{ width: "100%" }} />
            </FormItem>
        )

    return (
        <StyledItem
            name={props.name}
            label={props.label}
            // validateStatus={errors[name] && touched[name] ? "error" : ""}
            // help={errors[name] && touched[name] && errors[name]}
        >
            <DatePicker {...props} style={{ width: width || "100%" }} />
            {/* <DatePicker
                {...field}
                onChange={(date, dateString) => setFieldValue(field[name], dateString)}
                name={name}
                placeholder={props.placeholder}
                style={props.style || { display: "block" }}
                value={value !== "" ? moment(value) : ""}
            /> */}
        </StyledItem>
    )
}

export default DateInput
