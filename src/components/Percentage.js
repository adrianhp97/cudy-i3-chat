import React from "react"
import { Progress } from "antd"
import { baseStyles } from "styles/base"

export default function Percentage(props) {
    return (
        <Progress
            {...props}
            status="active"
            strokeColor={{ "0%": baseStyles.primaryColor, "100%": baseStyles.yellowColor }}
        />
    )
}
