import React, { useState, useEffect } from "react"
import styled from "styled-components"
import Headroom from "react-headroom"
import md5 from "md5";
import { Row, Col, Dropdown, Avatar, Icon, Menu, Badge, Drawer, Divider } from "antd"
import Button from "./Button"
import Search from "./forms/Search"
import Modal from "./Modal"
import Login from "../pages/Login"
import Heading from "./Heading"
import Register from "../pages/Register"
import { Link, withRouter } from "react-router-dom"
import Section from "./Section"
import NotificationPanel from "./common/NotificationPanel"
import { reviews } from "../dummy"
import { useLazyQuery, useMutation } from "@apollo/react-hooks"
import { queryLogout } from "queries/auth"
import { mutateUserProfileOnNavbar, mutateUserProfile } from "queries/profile"
import { unauthUser } from "store/actions/authActions"
import { fetchUserOptions } from "store/actions/userActions"
import { mobile, media, serverBaseUrl, baseUrl, renderAvatar } from "helpers"
import Percentage from "./Percentage"
import ProfileCompletion from "./common/ProfileCompletion"

const token = localStorage.getItem("token")

const Header = styled.header`
    background-color: transparent;
    #google_translate_element {
        margin-right: 2em;
        display: inline-block;
        + * {
            display: inline-block;
        }
    }
    .main-logo {
        height: auto;
        img {
            cursor: pointer;
        }
    }
`

const StyledHeadRoom = styled(Headroom)`
    .headroom {
        top: 0;
        left: 0;
        right: 0;
        z-index: 10;
        > header {
            padding: 0 2em;
        }
    }
    .headroom--unfixed {
        position: relative;
        transform: translateY(0);
    }
    .headroom--scrolled {
        transition: transform 0.3s ease;
    }
    .headroom--unpinned {
        position: fixed;
        transform: translateY(-100%);
    }
    .headroom--pinned {
        position: fixed;
        transform: translateY(0%);
        > header {
            background-color: #fff;
        }
    }
`

const ModalSection = styled(Section)`
    padding: 20px 20px 0;
    margin-bottom: 0;
`

const Logo = styled.img`
    margin-right: 5px;
    cursor: pointer;
`

const LoggedinItems = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: center;
    min-width: 120px;
    .ant-badge {
        margin-right: 2em;
        button {
            margin-top: 7px;
            border-color: #d9d9d9;
            + sup {
                top: 5px;
            }
        }
    }

    ${media.mobile`
        display: block;
        .ant-menu {
            border-right: none;
            margin-bottom: 2em;
        }
        .ant-badge {
            margin-right: 0;
            display: block;
        }
    `}
`

const UnloggedinItems = styled.div`
    text-align: right;

    ${media.mobile`
        > * {
            display: block;
        }
    `}
`

const ProfileHeading = styled(Heading).attrs({
    level: 4,
    marginBottom: "0"
})`
    h4.ant-typography {
        font-size: 1.2em;
    }
`

const ProfileComplete = styled(Row)`
    .ant-typography {
        h4 {
            font-size: 1em;
        }
    }
`

const detailsFromStorage = JSON.parse(localStorage.getItem("userDetails")) || {}

function Navbar({ history }) {
    const [modal, setModal] = useState(false)
    const [active, setActive] = useState("")
    const [showDrawer, setDrawer] = useState(false)
    const [menuDrawer, setMenuDrawer] = useState(false)
    const [profileCompletionDrawer, setProfileCompletionDrawer] = useState(false)

    const [setSignout] = useLazyQuery(queryLogout, unauthUser)
    const [fetchUser, { data }] = useMutation(mutateUserProfile, fetchUserOptions(history))

    const userData = (data || {}).getProfile || {}
    const avatar = (userData.avatar || [])[0] || {}

    const handleSendKeywords = value => {
        history.push(`/browse/tutors/?keywords=${value}`)
    }

    const { completionUser = {}, completionDetail = [] } = userData
    const percent = completionUser.completionPercent

    const menu = (
        <Menu style={{ padding: "1.4em 1.2em" }}>
            <Menu.Item key="hi" disabled>
                <ProfileHeading content={`Hi, ${userData.firstName}`} />
            </Menu.Item>
            {(detailsFromStorage.role || {}).code === "TTR0" && (
                <Menu.Item key="completion" onClick={() => setProfileCompletionDrawer(true)}>
                    <ProfileComplete gutter={32} type="flex" align="middle">
                        <Col lg={6}>
                            <Percentage type="circle" percent={percent} width={50} />
                        </Col>{" "}
                        <Col lg={18}>
                            <Heading
                                content="You're almost there"
                                subheader="Complete your profile"
                                level={4}
                                marginBottom="0"
                            />
                        </Col>
                    </ProfileComplete>
                </Menu.Item>
            )}
            {(detailsFromStorage.role || {}).code === "TTR0" && (
                <Menu.Item key="profile">
                    <Link to="/profile/me">
                        <Icon type="user" /> &nbsp; Your profile
                    </Link>
                </Menu.Item>
            )}
            {(detailsFromStorage.role || {}).code === "STDN" && (
                <Menu.Item key="assignment">
                    <Link to="/assignments">
                        <Icon type="trophy" /> &nbsp; Assignments
                    </Link>
                </Menu.Item>
            )}
            <Menu.Item key="settings">
                <Link to="/profile/settings">
                    <Icon type="setting" /> &nbsp; Settings
                </Link>
            </Menu.Item>
            <Menu.Item key="logout" onClick={() => setSignout()}>
                Logout
            </Menu.Item>
        </Menu>
    )

    useEffect(() => {
        if (token) fetchUser()
    }, [token])

    console.log({userData})

    return (
        <StyledHeadRoom disableInlineStyles>
            <ProfileCompletion
                data={{ completionUser, completionDetail }}
                placement="right"
                visible={profileCompletionDrawer}
                onClose={() => setProfileCompletionDrawer(false)}
            />

            <Drawer
                title={
                    token ? (
                        <span>
                            <a href="#">
                                <Avatar
                                    size="large"
                                    src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                                />
                            </a>
                            &nbsp; Hi, {userData.firstName}{" "}
                        </span>
                    ) : (
                        "Menu"
                    )
                }
                placement="right"
                onClose={() => setMenuDrawer(false)}
                visible={menuDrawer}
            >
                <div id="google_translate_element"></div>
                <NotificationPanel visible={showDrawer} onClose={() => setDrawer(false)} data={reviews} />
                {token ? (
                    <LoggedinItems>
                        <Menu>
                            <Menu.Item key="0">
                                <Link to="/profile/me">
                                    <Icon type="user" /> &nbsp; Your profile
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="1">
                                <Link to="/profile/settings">
                                    <Icon type="setting" /> &nbsp; Settings
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="3" onClick={() => setSignout()}>
                                Logout
                            </Menu.Item>
                        </Menu>
                        <Divider />
                    </LoggedinItems>
                ) : (
                    <UnloggedinItems>
                        <Link to="/login">
                            <Button type="ghost" size="default" block={mobile}>
                                Login
                            </Button>
                        </Link>
                        &nbsp; &nbsp;
                        <Link to="/register">
                            <Button type="primary" size="default" block={mobile}>
                                Sign up
                            </Button>
                        </Link>
                    </UnloggedinItems>
                )}
                <Row>
                    <Col lg={24} xs={24} style={{ marginBottom: "4em" }}>
                        <Search
                            size="large"
                            onSearch={handleSendKeywords}
                            placeholder="Search for tutors..."
                            style={{ marginBottom: "2em" }}
                        />{" "}
                        <Button
                            type="primary"
                            size="medium"
                            icon="team"
                            onClick={() => history.push("/browse/tutors")}
                            block
                        >
                            Browse tutors
                        </Button>
                    </Col>
                </Row>
                {token && (
                    <Row type="flex" justify="center">
                        <Col xs={24} style={{ textAlign: "center" }}>
                            <Badge count={5} onClick={() => setDrawer(!showDrawer)}>
                                <Icon type="bell" />
                                &nbsp; See notifications
                            </Badge>
                        </Col>
                    </Row>
                )}
            </Drawer>

            <Header>
                <Modal
                    visible={modal}
                    title={
                        <ModalSection>
                            <Heading
                                level={3}
                                content={active === "login" ? "Welcome back!" : "Let's get started"}
                                marginBottom={0}
                                subheader={
                                    active === "login"
                                        ? "Please login to continue"
                                        : "It takes 2.5 minutes only to register"
                                }
                            />
                        </ModalSection>
                    }
                    footer={null}
                    onCancel={() => setModal(false)}
                >
                    {active === "login" ? (
                        <Login onSetActive={setActive} onSetModal={setModal} />
                    ) : (
                        <Register active={active} onSetActive={setActive} />
                    )}
                </Modal>

                <Row type="flex" justify="space-between">
                    <Col lg={12}>
                        <Row>
                            <Col lg={6} xs={12}>
                                <div className="main-logo">
                                    <Logo
                                        src="https://app.cudy.co/img/logo_live.png"
                                        onClick={() => history.push("/")}
                                        width={mobile ? "60" : "100"}
                                    />
                                </div>
                            </Col>
                            {!mobile && (
                                <Col lg={18} xs={12}>
                                    <div className="search">
                                        <Search
                                            size="large"
                                            onSearch={handleSendKeywords}
                                            placeholder="Search for tutors..."
                                        />{" "}
                                        <Link to="/browse/tutors">
                                            <Button type="primary" size="medium" icon="team">
                                                Browse tutors
                                            </Button>
                                        </Link>
                                    </div>
                                </Col>
                            )}
                        </Row>
                    </Col>
                    <Col lg={12} style={{ textAlign: !mobile && "right" }}>
                        {!mobile && <div id="google_translate_element"></div>}

                        {token ? (
                            mobile ? (
                                <Button
                                    shape="circle"
                                    icon="more"
                                    size="medium"
                                    type="primary"
                                    onClick={() => setMenuDrawer(!menuDrawer)}
                                />
                            ) : (
                                <LoggedinItems>
                                    <Badge count={5}>
                                        <Button
                                            onClick={() => setDrawer(!showDrawer)}
                                            shape="circle"
                                            icon="bell"
                                            size="default"
                                            type="dashed"
                                        />
                                    </Badge>
                                    <NotificationPanel
                                        visible={showDrawer}
                                        onClose={() => setDrawer(false)}
                                        data={reviews}
                                    />
                                    <Dropdown overlay={menu} placement="bottomRight">
                                        <a href="#">
                                            <Avatar size="large" src={serverBaseUrl + avatar.path} />{" "}
                                            <Icon type="down" />
                                        </a>
                                    </Dropdown>
                                </LoggedinItems>
                            )
                        ) : mobile ? (
                            <Button
                                type="primary"
                                shape="circle"
                                size="medium"
                                icon="more"
                                onClick={() => setMenuDrawer(!menuDrawer)}
                            />
                        ) : (
                            <UnloggedinItems>
                                <Link to="/login">
                                    <Button type="ghost" size="default">
                                        Login
                                    </Button>
                                </Link>
                                &nbsp; &nbsp;
                                <Link to="/register">
                                    <Button type="primary" size="default">
                                        Sign up
                                    </Button>
                                </Link>
                            </UnloggedinItems>
                        )}
                    </Col>
                </Row>
            </Header>
        </StyledHeadRoom>
    )
}

export default withRouter(Navbar)
