import React from "react"
import { Card as Cardee } from "antd"
import styled from "styled-components"
import { boxShadow } from "../styles/base"

const StyledCard = styled(Cardee)`
    && {
        height: ${props => (props.autoHeight ? "auto" : props.height ? props.height : "300px")};
        margin-bottom: 1.5em;
        border-radius: 8px;
        border: none;
        box-shadow: ${boxShadow[0]};
        transition: all 0.2s ease;
        &:hover {
            box-shadow: ${({ noHover }) => (noHover ? undefined : boxShadow[1])};
            transform: ${({ noHover }) => (noHover ? "none" : "translateY(-1px)")};
        }
    }
    .ant-card-cover {
        max-height: 200px;
    }
    .ant-card-meta-detail {
        margin-bottom: 2em;
    }
`

function Card({ title = "", description = "", src = "", children, noHover = false, ...props }) {
    return (
        <StyledCard
            {...props}
            noHover={noHover}
            cover={src ? <img alt={title} src={src} width="100%" /> : null}
        >
            {(title || description) && <Cardee.Meta title={title} description={description} />}
            {children}
        </StyledCard>
    )
}

export default Card
