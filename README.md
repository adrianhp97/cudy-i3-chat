# Cudy Frontend - Iteration 3 ✨

A list of tech stacks used in this project can be found in `package.json`

## Preview

```bash
$ npm install
$ npm start
```

or:

```bash
$ yarn
$ yarn start
```
