const { override, fixBabelImports, addLessLoader } = require("customize-cra")

module.exports = override(
    fixBabelImports("import", {
        libraryName: "antd",
        libraryDirectory: "es",
        style: true
    }),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: {
            "@primary-color": "#FF9D00"
            // You can add another options/customization overridings here
            // See https://ant.design/docs/react/customize-theme for some major variables
            // and https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
            // for a whole variables
        }
    })
)
